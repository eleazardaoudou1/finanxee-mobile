import 'package:finanxee/models/depense.dart';
import 'package:finanxee/models/payloads/depense_by_day.dart';
import 'package:finanxee/utils/dbhelper.dart';
import "package:collection/collection.dart";

import '../models/categorie_depense.dart';
import '../models/payloads/stats_depenses_mois.dart';

class DepenseService {
  DbHelper helper = DbHelper();

  // Cette fonction renvoie le total des depenses complet
  Future<double> total_depenses() async {
    final List<Depense> comptes = await helper.getDepenses();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

// Cette fonction renvoie le total d'une liste de depenses qu'elle prend en parametre
  Future<double> totalDepensesFromList(List<Depense> depenses) async {
    double solde = 0.0;
    for (int i = 0; i < depenses.length; i++) {
      solde += depenses[i].montant;
    }
    return solde;
  }

  /// Cette fonction renvoie le total des depenses du mois en cours
  Future<double> total_depense_current_month() async {
    final List<Depense> comptes = await helper.getDepensesOfMonth();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  // Cette fonction renvoie le total des depenses de la semaine
  Future<double> total_depense_current_week() async {
    final List<Depense> comptes = await helper.getDepensesOfWeek();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  // Cette fonction renvoie le total des depenses aujourd'hui
  Future<double> total_depense_today() async {
    final List<Depense> comptes = await helper.getDepensesOfToday();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  // Cette fonction renvoie le total des depenses d'un jour particulier
  Future<double> totalDepenseSpecialDay(DateTime day) async {
    final List<Depense> comptes = await helper.getDepensesOfSpecialDay(day);
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  // Cette fonction renvoie le total des depenses aujourd'hui
  Future<double> total_depense_between(String t1, String t2) async {
    final List<Depense> comptes = await helper.getDepensesBetween(t1, t2);
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  Future<StatsDepensesMois> getStatsDepensesMois() async {
    List<Depense> depenses = await helper.getDepensesOfMonth();
    var newMap = groupBy(depenses, (Depense d) {
      return d.categorie;
    });
    List<CategorieDepense> categories_ = [];
    List<double> depensesSum = [];

    for (var element in newMap.keys) {
      var cat = await helper.getCatById(element);
      categories_.add(cat!);
    }
    //print(newMap.values.elementAt(0));
    for (int i = 0; i < newMap.length; i++) {
      double s = 0;
      var t = newMap.values.elementAt(i);
      for (int j = 0; j < t.length; j++) {
        s += t.elementAt(j).montant;
      }
      depensesSum.add(s);
    }
    StatsDepensesMois stats = StatsDepensesMois(categories_, depensesSum, '');
    return stats;
  }

  Future<StatsDepensesMois> getStatsDepenses(List<Depense> depenses) async {
    //List<Depense> depenses = await helper.getDepensesOfMonth();
    var newMap = groupBy(depenses, (Depense d) {
      return d.categorie;
    });
    List<CategorieDepense> categories_ = [];
    List<double> depensesSum = [];

    for (var element in newMap.keys) {
      var cat = await helper.getCatById(element);
      categories_.add(cat!);
    }
    //print(newMap.values.elementAt(0));
    for (int i = 0; i < newMap.length; i++) {
      double s = 0;
      var t = newMap.values.elementAt(i);
      for (int j = 0; j < t.length; j++) {
        s += t.elementAt(j).montant;
      }
      depensesSum.add(s);
    }
    StatsDepensesMois stats = StatsDepensesMois(categories_, depensesSum, '');
    return stats;
  }

  Future<DepenseByDay> getDepensesFilterByDay(
      DateTime debut, DateTime fin) async {
    DepenseByDay depenseByDay = DepenseByDay([], 0, [], []);

    int days = fin.difference(debut).inDays;

    for (int i = 0; i <= days; i++) {
      debut = DateTime(debut.year, debut.month, debut.day);
      fin = DateTime(fin.year, fin.month, fin.day);

      DateTime theDate = debut.add(Duration(days: i));
      double totalDepense = await totalDepenseSpecialDay(theDate);

      depenseByDay.days.add(theDate);
      depenseByDay.depensesSum.add(totalDepense);
    }

    return depenseByDay;
  }
}
