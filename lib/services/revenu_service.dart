import 'package:collection/collection.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/models/revenu.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

import '../models/payloads/stats_revenus_mois.dart';

class RevenuService {
  DbHelper helper = DbHelper();

  /// Cette fonction renvoie le total des comptes bancaires
  Future<double> total_revenus() async {
    final List<Revenu> comptes = await helper.getRevenus();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  /// Cette fonction renvoie le total des revenus du mois en cours
  Future<double> total_revenu_current_month() async {
    final List<Revenu> comptes = await helper.getRevenusOfMonth();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  /// Cette fonction renvoie le total des revenus de la semaine en cours
  Future<double> total_revenu_current_week() async {
    final List<Revenu> comptes = await helper.getRevenusOfWeek();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  /// Cette fonction renvoie le total des revenus de la semaine en cours
  Future<double> total_revenu_between(String t1, String t2) async {
    final List<Revenu> comptes = await helper.getRevenusBetween(t1, t2);
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  // Cette fonction renvoie le total d'une liste de depenses qu'elle prend en parametre
  Future<double> totalRevenuFromList(List<Revenu> revenus) async {
    double solde = 0.0;
    for (int i = 0; i < revenus.length; i++) {
      solde += revenus[i].montant;
    }
    return solde;
  }

  Future<StatsRevenusMois> getStatsRevenusMois() async {
    //recuperer les revenus du mois
    List<Revenu> revenus = await helper.getRevenusOfMonth();
    var newMap = groupBy(revenus, (Revenu r) {
      return r.type_revenu;
    });
    List<TypeRevenu> types_ = [];
    List<double> revenuSum = [];

    for (var element in newMap.keys) {
      var cat = await helper.getTypeRevenuById(element);
      types_.add(cat!);
    }

    for (int i = 0; i < newMap.length; i++) {
      double s = 0;
      var t = newMap.values.elementAt(i);
      for (int j = 0; j < t.length; j++) {
        s += t.elementAt(j).montant;
      }
      revenuSum.add(s);
    }
    StatsRevenusMois stats = StatsRevenusMois(types_, revenuSum);
    return stats;
  }
}
