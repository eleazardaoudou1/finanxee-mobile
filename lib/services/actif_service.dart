import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/services/compe_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';

class ActifService {
  //var f = StaticDate();
  DbHelper helper = DbHelper();
  //CompteService compteService = CompteService();
  int total_actif = 0;

  //function pour obtenir la valeur des autres actifs
  Future<double> get_actifs_valeur() async {
    final List<Actif> actifs = await helper.getActifs();
    double solde = 0.0;
    for (int i = 0; i < actifs.length; i++) {
      solde += actifs[i].valeur;
    }
    return solde;
  }

//function pour obtenir la valeur des autres actifs
  Future<double> getActifsValeurBefore(String t1) async {
    final List<Actif> actifs = await helper.getActifsBefore(t1);
    double solde = 0.0;
    for (int i = 0; i < actifs.length; i++) {
      solde += actifs[i].valeur;
    }
    return solde;
  }

  Future<double> get_actifs_type_valeur(int type) async {
    final List<Actif> actifs = await helper.getActifsType(type);
    double solde = 0.0;
    for (int i = 0; i < actifs.length; i++) {
      solde += actifs[i].valeur;
    }
    return solde;
  }

  /// Cette fonction renvoie le nombre des autres actifs
  Future<int> nbre_autre_actifs() async {
    final List<Actif> comptes = await helper.getActifs();
    return comptes.length;
  }
  //function to get the assets
  //actif = comptes + autres actifs
}
