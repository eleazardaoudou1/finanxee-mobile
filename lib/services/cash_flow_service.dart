import 'package:finanxee/services/compe_service.dart';
import 'package:finanxee/services/depense_service.dart';
import 'package:finanxee/services/revenu_service.dart';
import 'package:flutter/material.dart';

class CashFlowService {
  double totalDepenses = 10.0;
  double totalRevenus = 15.0;

  RevenuService revenuService = RevenuService();
  CompteService compteService = CompteService();
  DepenseService depenseService = DepenseService();

  Future<void> getData() async {
    totalRevenus = await revenuService.total_revenu_current_month();
    totalDepenses = await depenseService.total_depense_current_month();
  }
}
