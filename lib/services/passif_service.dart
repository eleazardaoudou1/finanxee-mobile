import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/passif.dart';
import 'package:finanxee/services/compe_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';

class PassifService {
  //var f = StaticDate();
  DbHelper helper = DbHelper();
  //CompteService compteService = CompteService();
  int total_passif = 0;

  //function pour obtenir la valeur des autres actifs
  Future<double> get_passifs_valeur() async {
    final List<Passif> actifs = await helper.getPassifs();
    double solde = 0.0;
    for (int i = 0; i < actifs.length; i++) {
      solde += actifs[i].valeur;
    }
    return solde;
  }

  Future<double> get_passif_type_valeur(int type) async {
    final List<Passif> actifs = await helper.getPassifsType(type);
    double solde = 0.0;
    for (int i = 0; i < actifs.length; i++) {
      solde += actifs[i].valeur;
    }
    return solde;
  }

  /// Cette fonction renvoie le nombre des autres actifs
  Future<int> nbre_autre_passifs() async {
    final List<Passif> comptes = await helper.getPassifs();
    return comptes.length;
  }
  //function to get the assets
  //actif = comptes + autres actifs

}
