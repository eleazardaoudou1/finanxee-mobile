import 'package:finanxee/models/depense.dart';
import 'package:finanxee/models/facture.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

class FactureService {
  DbHelper helper = DbHelper();

  // Cette fonction renvoie le montant total des factures
  Future<double> total_factures() async {
    final List<Facture> comptes = await helper.getFacturesLists();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  /// Cette fonction renvoie le total des factures payees
  Future<double> total_factures_payees() async {
    final List<Facture> comptes = await helper.getFacturesPayees();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  /// Cette fonction renvoie le total des factures payees
  Future<double> total_factures_impayees() async {
    final List<Facture> comptes = await helper.getFacturesNonPayees();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }

  /// Cette fonction renvoie le total des factures payees
  Future<double> total_factures_archivees() async {
    final List<Facture> comptes = await helper.getFacturesArchivees();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].montant;
    }
    return solde;
  }
}
