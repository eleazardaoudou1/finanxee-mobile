import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/models/passif.dart';
import 'package:finanxee/services/actif_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';

import '../models/depense.dart';

class CompteService {
  DbHelper helper = DbHelper();
  ActifService actifService = ActifService();

  //f.numF.format(this.depense.montant),

  // Cette fonction renvoie le total des comptes bancaires
  Future<double> total_compte_bancaire() async {
    final List<Compte> comptes = await helper.getCompteBancaires();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].solde;
    }
    return solde;
  }

  // Cette fonction renvoie le total des comptes bancaires
  Future<double> totalCompteBancaireBefore(String t1) async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month - 1, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month, 0).toString();

    final List<Compte> comptes =
        await helper.getCompteBancairesBefore(lastOfMonth);
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].solde;
    }
    return solde;
  }

  // Cette fonction renvoie le solde total des compte Momo
  Future<double> total_compte_momo() async {
    final List<Compte> comptes = await helper.getCompteMomo();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].solde;
    }
    return solde;
  }

// Cette fonction renvoie le solde total des compte Momo
  Future<double> totalCompteMomoBefore(String t1) async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month - 1, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month, 0).toString();

    final List<Compte> comptes = await helper.getCompteMomoBefore(lastOfMonth);
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].solde;
    }
    return solde;
  }

  // Cette fonction renvoie le solde total des autres comptes
  Future<double> total_autre_comptes() async {
    final List<Compte> comptes = await helper.getCompteAutres();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].solde;
    }
    return solde;
  }

// Cette fonction renvoie le solde total des autres comptes
  Future<double> totalAutreComptesBefore(String t1) async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month - 1, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month, 0).toString();

    final List<Compte> comptes =
        await helper.getCompteAutresBefore(lastOfMonth);
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].solde;
    }
    return solde;
  }

  // Cette fonction renvoie le solde total des autres comptes
  Future<double> total_comptes() async {
    final List<Compte> comptes = await helper.getComptes();
    double solde = 0.0;
    for (int i = 0; i < comptes.length; i++) {
      solde += comptes[i].solde;
    }
    return solde;
  }

  // Cette fonction renvoie le solde total des passifs
  Future<double> total_passifs() async {
    final List<Passif> passifs = await helper.getPassifs();
    double solde = 0.0;
    for (int i = 0; i < passifs.length; i++) {
      solde += passifs[i].valeur;
    }
    return solde;
  }

  // Cette fonction renvoie le solde total des passifs
  Future<double> totalPassifsBefore(String t1) async {
    final List<Passif> passifs = await helper.getPassifsBefore(t1);
    double solde = 0.0;
    for (int i = 0; i < passifs.length; i++) {
      solde += passifs[i].valeur;
    }
    return solde;
  }

  /// Cette fonction renvoie le solde total des actifs
  Future<double> total_actif_solde() async {
    return await actifService.get_actifs_valeur() + await total_comptes();
  }

  //cette fonction renvoie l'avoir net
  Future<double> avoir_net() async {
    double avoirNet = 0.0;
    avoirNet = await total_autre_comptes() +
        await total_compte_bancaire() +
        await total_compte_momo() +
        await actifService.get_actifs_valeur() -
        //on retranche les passifs
        await total_passifs();
    ;
    // + les totaux des autres actifs
    // -la somme de tous les passifs
    return avoirNet;
  }

//cette fonction renvoie l'avoir net
  Future<double> avoirNetBetween(String t1, String t2) async {
    double avoirNet = 0.0;
    avoirNet = await totalAutreComptesBefore(t2) +
        await totalCompteBancaireBefore(t2) +
        await totalCompteMomoBefore(t2) +
        await actifService.getActifsValeurBefore(t2) -
        //on retranche les passifs
        await totalPassifsBefore(t2) +
        await getCashFlowBetween(t1, t2);
    ;
    // + les totaux des autres actifs
    // -la somme de tous les passifs
    return avoirNet;
  }

  Future<double> getCashFlowBetween(String t1, String t2) async {
    List<Depense> depenses = await helper.getDepensesBetween(t1, t2);
    List<Revenu> revenus = await helper.getRevenusBetween(t1, t2);

    double soldeDep = 0.0;
    for (int i = 0; i < depenses.length; i++) {
      soldeDep += depenses[i].montant;
    }
    double soldeRev = 0.0;
    for (int i = 0; i < revenus.length; i++) {
      soldeRev += revenus[i].montant;
    }

    return soldeRev - soldeDep;
  }

  //cette fonction renvoie le nombre de compte bancaires
  Future<int> nbre_compte_bancaire() async {
    final List<Compte> comptes = await helper.getCompteBancaires();
    return comptes.length;
  }

  //cette fonction renvoie le nombre de compte momo
  Future<int> nbre_compte_momo() async {
    final List<Compte> comptes = await helper.getCompteMomo();
    return comptes.length;
  }

  //cette fonction renvoie le nombre des autres comptes
  Future<int> nbre_compte_autres() async {
    final List<Compte> comptes = await helper.getCompteAutres();
    return comptes.length;
  }

  //Cette fonction renvoie le nombre total de comptes
  Future<int> nbre_compte_total() async {
    final List<Compte> comptes = await helper.getComptes();
    return comptes.length;
  }

  int total_actif = 0;

  //function pour obtenir la valeur des autres actifs
  Future<double> get_actifs_valeur() async {
    final List<Actif> actifs = await helper.getActifs();
    double solde = 0.0;
    for (int i = 0; i < actifs.length; i++) {
      solde += actifs[i].valeur;
    }
    return solde;
  }

  /// Cette fonction renvoie le nombre des autres actifs
  Future<int> nbre_autre_actifs() async {
    final List<Actif> comptes = await helper.getActifs();
    return comptes.length;
  }
}
