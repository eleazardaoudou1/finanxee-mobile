import 'package:finanxee/screens/analyses_screen.dart';
import 'package:finanxee/screens/avoirs/avoir_screen.dart';
import 'package:finanxee/screens/dashboard.dart';
import 'package:finanxee/screens/transactions/transactions_screen.dart';
import 'package:flutter/material.dart';

class TabManager extends ChangeNotifier {
  static List<Widget> pages = [
    Dashboard(),
    AvoirScreen(),
    TransactionsScreen(),
    AnalysesScreen(),
    //NotificationsScreen()
  ];

  //funcction do display the Bottom nav Bar
  BottomNavigationBar manageBottomNavBar(context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      selectedItemColor: Theme.of(context).textSelectionTheme.selectionColor,
      currentIndex: selectedTab,
      onTap: (index) {
        goToTab(index);
      },
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home_outlined),
          label: 'Aperçu',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.work_rounded),
          label: 'Avoirs',
          //backgroundColor: Color(0xFFCCCCCC)
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.currency_exchange),
          label: 'Transactions',
          //backgroundColor: Color(0xFFCCCCCC)
        ),
        BottomNavigationBarItem(icon: Icon(Icons.explore), label: 'Analyses'),
      ],
    );
  }

  int selectedTab = 0;

  //
  void goToTab(index) {
    selectedTab = index;
    notifyListeners();
  }

  void gotToAccount() {
    selectedTab = 1;
    notifyListeners();
  }

  void gotToTransactions() {
    selectedTab = 2;
    notifyListeners();
  }
}
