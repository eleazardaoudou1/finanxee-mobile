import 'dart:async';

import 'package:finanxee/cache/cache_helper.dart';
import 'package:flutter/material.dart';

import '../utils/static_data.dart';

class FinanxeeTab {
  static const int apercu = 0;
  static const int avoirs = 1;
  static const int transactions = 2;
  static const int analyses = 3;
}

class AppStateManager extends ChangeNotifier {
  bool _initialized = false;
  bool _loggedIn = false;
  bool _onboardingComplete = false;
  bool _passcodeSet = false;
  bool _termsAndConditions = false;
  bool _questionsSecuriteComplete = false;
  int _selectedTab = FinanxeeTab.apercu;
  bool _redirectToHome = false;
  bool _countrySet = false;

  //getters
  bool get isInitialized => _initialized;
  bool get isLoggedIn => _loggedIn;
  bool get isOnboardingComplete => _onboardingComplete;
  bool get isPasscodeSet => _passcodeSet;
  bool get isTermsAndConditionsComplete => _termsAndConditions;
  bool get isQuestionsComplete => _questionsSecuriteComplete;
  int get getSelectedTab => _selectedTab;
  bool get isRedirectedToHome => _redirectToHome;
  bool get isCountrySet => _countrySet;

  //the initialize method
  void initializeApp() {
    Timer(const Duration(seconds: 5), () {
      _initialized = true;
      notifyListeners();
    });
  }

  //the login method
  //dans mon cas, ce sera d'abord qu'il faut choisir un mot de passe pour ouvrir l'application
  void loginWithPassCode() {
    _loggedIn = true;
    notifyListeners();
  }

  //complete OnBoarding
  void completeSettingPasscode() {
    _passcodeSet = true;
    notifyListeners();
  }

  //complete OnBoarding
  void completeOnboarding() {
    _onboardingComplete = true;
    notifyListeners();
  }

//complete termsAndCondition
  void completeTermsAndConditions() {
    _termsAndConditions = true;
    notifyListeners();
  }

  //complete termsAndCondition
  void completeQuestions() {
    _questionsSecuriteComplete = true;
    notifyListeners();
  }

  //set the country
  void setCountry() {
    _countrySet = true;
    notifyListeners();
  }

  bool _checkifCountrySet() {
    String selectCountryCode = StaticDate.selectedCountryCode;
    Future<String> countryCode = CacheHelper.getString(selectCountryCode);
    print("App State countryCode = ");
    print(countryCode.toString());
    if ((countryCode ?? '') == '') return false;
    return true;
  }

  void goToTab(index) {
    _selectedTab = index;
    notifyListeners();
  }

  void gotToDashboard() {
    _selectedTab = FinanxeeTab.apercu;
    notifyListeners();
  }

  void gotToAvoirs() {
    _selectedTab = FinanxeeTab.avoirs;
    notifyListeners();
  }

  void gotToTransactions() {
    _selectedTab = FinanxeeTab.transactions;
    notifyListeners();
  }

  void redirectToHome() {
    _redirectToHome = true;
    notifyListeners();
  }

  //the logout method
  void logout() {
    _initialized = false;
    _loggedIn = false;
    _onboardingComplete = false;
    _selectedTab = FinanxeeTab.apercu;

    //initializing the app
    initializeApp();
    notifyListeners();
  }
}
