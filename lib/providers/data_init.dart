import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/sous_categorie_depense.dart';
import 'package:finanxee/models/type_revenu.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

import '../utils/static_data.dart';

class DataInit extends ChangeNotifier {
  DbHelper helper = DbHelper();

  double _compte_bancaire_solde = 10000;
  double _compte_momo_solde = 10000;
  double _compte_autre = 10000;
  String _banque_name = StaticDate.intituleChoixBanque;
  String _gsm_name = StaticDate.intituleChoixGSM;

  double get compte_bancaire_solde => _compte_bancaire_solde;
  double get compte_momo_solde => _compte_momo_solde;
  double get compte_autre => _compte_autre;
  String get banque_name => _banque_name;
  String get gsm_name => _gsm_name;

  //
  String _question = StaticDate.intituleChoixQuestion;
  String get question => _question;
  void updateQuestion(String question) {
    _question = question;
    notifyListeners();
  }

  String _question2 = StaticDate.intituleChoixQuestion;
  String get question2 => _question2;
  void updateQuestion2(String question2) {
    _question2 = question2;
    notifyListeners();
  }

  void updateSoldeBanq(double soldeBanq) {
    _compte_bancaire_solde = soldeBanq;
    notifyListeners();
  }

  void updateSoldeMomo(double soldeMomo) {
    _compte_momo_solde = soldeMomo;
    notifyListeners();
  }

  void updateSoldeAutre(double soldeAutre) {
    _compte_autre = soldeAutre;
    notifyListeners();
  }

  void updateBanqueName(String name) {
    _banque_name = name;
    notifyListeners();
  }

  void updateGsmName(String name) {
    _gsm_name = name;
    notifyListeners();
  }

  final String _type_revenu = '';
  String get type_revenu => _type_revenu;
  TypeRevenu _type_revenu_selected = TypeRevenu(
      1451, StaticDate.intituleChoixTypeRevenu, '', '', '', '', '', '');
  TypeRevenu get type_revenu_selected => _type_revenu_selected;
  void updateTypeRevenuSelected(TypeRevenu type) {
    _type_revenu_selected = type;
    notifyListeners();
  }

  Compte _compte_selected =
      Compte(0, 0, StaticDate.intituleChoixCompte, 0.0, '', '', '', '', '');
  Compte _compte_selected_2 =
      Compte(0, 1, StaticDate.intituleChoixCompte, 0.0, '', '', '', '', '');
  Compte _compte_selected_3 = Compte(
      0,
      2,
      StaticDate.intituleChoixCompte,
      0.0,
      '',
      '',
      'assets/autre_compte.jpg',
      DateTime.now().toString(),
      DateTime.now().toString());

  Compte get compte_selected => _compte_selected;
  Compte get compte_selected_2 => _compte_selected_2;
  Compte get compte_selected_3 => _compte_selected_3;

  void updateCompteSelected(Compte c) {
    _compte_selected = c;
    notifyListeners();
  }

  void updateCompteSelected2(Compte c) {
    _compte_selected_2 = c;
    notifyListeners();
  }

  void updateCompteSelected3(Compte c) {
    _compte_selected_3 = c;
    notifyListeners();
  }

  void resetCompteSelected() {
    _compte_selected =
        Compte(0, 0, StaticDate.intituleChoixCompte, 0.0, '', '', '', '', '');
    notifyListeners();
  }

  final String _sous_categories_depense = '';
  String get sous_categories_depense => _sous_categories_depense;
  SousCategorieDepense _sous_categories_depense_selected = SousCategorieDepense(
      0, 0, StaticDate.intituleChoixCategorie, '', '', '', '', '');

  SousCategorieDepense get sous_categories_depense_selected =>
      _sous_categories_depense_selected;

  void updateSousCategoriesDepenseSelected(SousCategorieDepense type) {
    _sous_categories_depense_selected = type;
    notifyListeners();
  }
}
