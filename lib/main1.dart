import 'package:finanxee/FinanxeeTheme.dart';
import 'package:finanxee/providers/app_state_manager.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:finanxee/providers/tab_manager.dart';
import 'package:finanxee/screens/home_page.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'navigation/app_router.dart';

void main() {
  runApp(const FinanxeeApp());
}

class FinanxeeApp extends StatelessWidget {
  const FinanxeeApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    final lightTheme = FinanxeeTheme.lightTheme;
    DbHelper helper = DbHelper();
    helper.openDb(); //opening or creating the database
    AppRouter _appRouter;
    //creating the tables
    //helper.createTableFactures();

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => TabManager()),
        ChangeNotifierProvider(create: (context) => DataInit()),
        ChangeNotifierProvider(create: (context) => AppStateManager()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        theme: lightTheme,
        home: const LauchScreen(),
      ),
    );
  }
}
