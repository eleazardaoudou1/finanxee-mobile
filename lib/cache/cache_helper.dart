import 'package:shared_preferences/shared_preferences.dart';

class CacheHelper {
  //static late SharedPreferences prefs;

  static Future<void> init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
  }

  static Future<bool> containsKey(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.containsKey(key);
  }

  static Future<void> remove(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.remove(key);
  }

  static Future<String> getString(String key, [String? defValue]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(key) ?? defValue ?? "";
  }

  static Future<bool> setString(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(key, value); //?? Future.value(false);
  }

  static Future<bool> getBool(String key, [bool? defValue]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(key) ?? defValue ?? false;
  }

  static Future<bool> setBool(String key, bool value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool(key, value); //?? Future.value(false);
  }

  static Future<int> getInt(String key, [int? defValue]) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getInt(key) ?? defValue ?? 0;
  }

  static Future<bool> setInt(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setInt(key, value); // ?? Future.value(false);
  }
}
