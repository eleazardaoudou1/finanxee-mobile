import 'dart:async';
import 'package:finanxee/models/models.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../models/epargne_projet.dart';

class DbHelper {
  //use of factory constructor
  static final DbHelper _dbHelper = DbHelper._internal();
  DbHelper._internal();
  factory DbHelper() {
    return _dbHelper;
  }

  late int version = 1;
  Database? db;

  //function used to open or create the database
  Future<Database?> openDb() async {
    db ??= await openDatabase(join(await getDatabasesPath(), 'finanxee_v37.db'),
        onCreate: (database, version) {
      database.execute(
          'CREATE TABLE parametres(id INTEGER PRIMARY KEY, name TEXT, cle TEXT, valeur TEXT, created_at TEXT,updated_at TEXT, deleted_at TEXT)');
      //create the lists table
      database.execute(
          'CREATE TABLE expense_lists(id INTEGER PRIMARY KEY, name TEXT, archived INTEGER, created_at TEXT, updated_at TEXT, deleted_at TEXT)');
      //create the items table
      database.execute(
          'CREATE TABLE expense_tasks(id INTEGER PRIMARY KEY, idExpenseList INTEGER , label TEXT, price REAL, date TEXT, completed INTEGER,deleted_at TEXT, FOREIGN KEY(idExpenseList) REFERENCES expense_lists(id))');
      database.execute(
          'CREATE TABLE epargnes_projet (id INTEGER PRIMARY KEY, idProjet INTEGER , label TEXT, montant REAL, date TEXT, deleted_at TEXT, FOREIGN KEY(idProjet) REFERENCES projets(id))');
      //create the factures table
      database.execute(
          'CREATE TABLE factures(id INTEGER PRIMARY KEY, intitule TEXT, montant REAL, echeance TEXT, completed INTEGER, archived INTEGER, created_at TEXT ,deleted_at TEXT)');
      database.execute(
          'CREATE TABLE projets(id INTEGER PRIMARY KEY, intitule TEXT, montant REAL, montant_epargne_mensuel REAL, solde_actuel REAL, description TEXT, delai TEXT,  completed INTEGER,  archived INTEGER, created_at TEXT, updated_at TEXT ,deleted_at TEXT)');
      database.execute(
          'CREATE TABLE categories_depenses(id INTEGER PRIMARY KEY, intitule TEXT, author TEXT, icon TEXT , code TEXT, created_at TEXT, updated_at TEXT ,deleted_at TEXT)');
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (1,'Alimentation et Restaurant','APP','ri-cake-3-fill','ALM','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (2,'Auto et Transports','APP','ri-car-fill','AUTO-TRA','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (3,'Achats et Shopping','APP','ri-shopping-bag-2-line','ACH-SHO','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (4,'Abonnement et factures','APP','ri-cake-3-fill','ABN-FAC','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (5,'Divers','APP','ri-file-info-fill','DIV','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (6,'Soins et Esthétique','APP','ri-bear-smile-line','SOI-EST','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (7,'Logement','APP','ri-home-8-line','LOG','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (8,'Loisirs et sorties','APP','ri-motorbike-line','LOI-SOR','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (9,'Santé et Bien être','APP','ri-stethoscope-fill','SANT','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (10,'Dépenses professionnelles','APP','ri-stethoscope-fill','DEP-PRO','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (11,'Enfants','APP','pencil','ENF','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (12,'Education','APP','ri-book-fill','EDU','2022-06-16','2022-06-16',NULL)");
      database.execute(
          "INSERT INTO `categories_depenses` VALUES (13,'Impôts et taxes','APP','pencil','IMP-TAX','2022-06-16','2022-06-16',NULL)");

      /**************** Sous Categories**********/
      database.execute(
          'CREATE TABLE sous_categories_depenses(id INTEGER PRIMARY KEY, categorie_depense_id INTEGER, intitule TEXT, author TEXT, icon TEXT , code TEXT, created_at TEXT, updated_at TEXT,deleted_at TEXT, FOREIGN KEY(categorie_depense_id) REFERENCES categories_depenses(id))');
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (1,1,'Restaurant','APP','ri-book-fill','ALM-REST','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (2,1,'Supermarché/épicerie','APP','ri-book-fill','ALM-SUP-EPI','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (3,1,'Café','APP','ri-book-fill','ALM-CAF','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (4,1,'Fast-food','APP','ri-book-fill','ALM-FAS-FOO','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (5,1,'Livraison de nourriture','APP','ri-book-fill','ALM-LIV-NOU','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (6,1,'Autres','APP','ri-book-fill','ALM-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (7,2,'Carburant','APP','ri-book-fill','AUTO-TRA-CARB','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (8,2,'Péage','APP','ri-book-fill','AUTO-TRA-PEA','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (9,2,'Garde-vélo / stationnement','APP','ri-book-fill','AUTO-TRA-GAR-VEL','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (10,2,'Assurance véhicule','APP','ri-book-fill','AUTO-TRA-ASS-VEH','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (11,2,'Billets de voyage','APP','ri-book-fill','AUTO-TRA-BIL-VOY','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (12,2,'Entretien véhicule','APP','ri-book-fill','AUTO-TRA-ENT-VEH','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (13,2,'Taxi','APP','ri-book-fill','AUTO-TRA-TAXI','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (14,2,'Transport en commun','APP','ri-book-fill','AUTO-TRA-TRA-COM','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (15,2,'Location de véhicule','APP','ri-book-fill','AUTO-TRA-LOC-VEH','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (16,2,'Autres','APP','ri-book-fill','AUTO-TRA-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (17,3,'Electronique','APP','ri-book-fill','ACH-SHO-ELC','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (18,3,'Habillement et chaussures','APP','ri-book-fill','ACH-SHO-HAB-CHAU','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (19,3,'Accessoires de mode','APP','ri-book-fill','ACH-SHO-ACC-MOD','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (20,3,'Logiciels','APP','ri-book-fill','ACH-SHO-LOG','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (21,3,'Ebooks, dvd, audio','APP','ri-book-fill','ACH-SHO-EDA','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (22,3,'Fourniture de bureau','APP','ri-book-fill','ACH-SHO-FOUR-BUR','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (23,3,'Accessoires de cuisine','APP','ri-book-fill','ACH-SHO-ACC-CUI','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (24,3,'Autres','APP','ri-book-fill','ACH-SHO-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (25,4,'Internet','APP','ri-book-fill','ABN-FAC-INT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (26,4,'Téléphonie','APP','ri-book-fill','ABN-FAC-TEL','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (27,4,'Autres','APP','ri-book-fill','ABN-FAC-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (28,5,'Dons et charité','APP','ri-book-fill','DIV-DON','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (29,5,'Offrandes','APP','ri-book-fill','DIV-OFFR','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (30,5,'Dîme','APP','ri-book-fill','DIV-DIM','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (31,5,'Autres','APP','ri-book-fill','DIV-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (32,6,'Coiffure/Tresse','APP','ri-book-fill','SOI-EST-COIF','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (33,6,'Cosmétique','APP','ri-book-fill','SOI-EST-COSM','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (34,6,'Esthétique','APP','ri-book-fill','SOI-EST-ESTH','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (35,6,'Autres','APP','ri-book-fill','SOI-EST-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (36,7,'Meubles et décoration','APP','ri-book-fill','LOG-MEU-DECO','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (37,7,'Eau','APP','ri-book-fill','LOG-EAU','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (38,7,'Electricité','APP','ri-book-fill','LOG-ELEC','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (39,7,'Loyer','APP','ri-book-fill','LOG-LOY','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (40,7,'Gaz','APP','ri-book-fill','LOG-GAZ','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (41,7,'Autres','APP','ri-book-fill','LOG-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (42,8,'Divertissement','APP','ri-book-fill','LOI-SOR-DIV','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (43,8,'Hobbies','APP','ri-book-fill','LOI-SOR-HOB','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (44,8,'Journaux et magazines','APP','ri-book-fill','LOI-SOR-JOUR-MAG','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (45,8,'Cinéma','APP','ri-book-fill','LOI-SOR-CINE','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (46,8,'Hôtels','APP','ri-book-fill','LOI-SOR-HOT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (47,8,'Activités Culturelles','APP','ri-book-fill','LOI-SOR-CULT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (48,8,'Voyages/vacances','APP','ri-book-fill','LOI-SOR-VOY-VAC','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (49,8,'Autres','APP','ri-book-fill','LOI-SOR-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (50,9,'Hygiène corporelle','APP','ri-book-fill','SANT-DEN','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (51,9,'Frais Hôpital','APP','ri-book-fill','SANT-MED','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (52,9,'Assurance santé','APP','ri-book-fill','SANT-ASS-SAN','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (53,9,'Abonnement Salle de Gym','APP','ri-book-fill','SANT-GYM','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (54,9,'Sports','APP','ri-book-fill','SANT-SPORT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (55,9,'Pharmacie','APP','ri-book-fill','SANT-PHAR','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (56,9,'Autres','APP','ri-book-fill','SANT-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (57,10,'Marketing','APP','ri-book-fill','DEP-PRO-MARK','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (58,10,'Tirage de document','APP','ri-book-fill','DEP-PRO-IMP','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (59,10,'Frais de livraison','APP','ri-book-fill','DEP-PRO-FRA-LIVR','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (60,10,'Publicité','APP','ri-book-fill','DEP-PRO-PUB','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (61,10,'Souscriptions diverses','APP','ri-book-fill','DEP-PRO-SOUSC','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (62,10,'Autres','APP','ri-book-fill','DEP-PRO-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (63,11,'Baby-sitting','APP','ri-book-fill','ENF-BAB-SIT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (64,11,'Allocation / Argent de poche','APP','ri-book-fill','ENF-ALLOC','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (65,11,'Jouets','APP','ri-book-fill','ENF-JOUE','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (66,11,'Activités des enfants','APP','ri-book-fill','ENF-ACT-ENF','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (67,11,'Autres','APP','ri-book-fill','ENF-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (68,12,'Livres et fournitures scolaires','APP','ri-book-fill','EDU-LIVR','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (69,12,'Frais de scolarité','APP','ri-book-fill','EDU-SCO','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (70,12,'Prêts','APP','ri-book-fill','EDU-PRE','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (71,12,'Autres','APP','ri-book-fill','EDU-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (72,13,'Amendes','APP','ri-book-fill','IMP-TAX-AMD','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (73,13,'Taxes sur ventes/achats','APP','ri-book-fill','IMP-TAX-TAX-VEN','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (74,13,'Taxes sur propriétés','APP','ri-book-fill','IMP-TAX-TAX-PRO','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (75,13,'Autres taxes','APP','ri-book-fill','IMP-TAX-AUT','2022-12-22','2022-12-22',NULL)");
      database.execute(
          "INSERT INTO `sous_categories_depenses` VALUES (76,3,'Immobilier','APP','ri-book-fill','ACH-SHO-IMMO','2022-12-22','2022-12-22',NULL)");

      database.execute(
          'CREATE TABLE comptes(id INTEGER PRIMARY KEY, intitule TEXT, type INTEGER, solde REAL, code TEXT, author TEXT, imageProvider TEXT , created_at TEXT, updated_at TEXT, deleted_at TEXT)');
      database.execute(
          'CREATE TABLE type_revenus(id INTEGER PRIMARY KEY, intitule TEXT, code TEXT, author TEXT , icon TEXT, description TEXT, created_at TEXT, updated_at TEXT, deleted_at TEXT)');

      database.execute(
          "INSERT INTO `type_revenus` VALUES (1,'Salaire','SAL', 'APP','pencil','Salaire','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (2,'Honoraire','HON', 'APP','pencil','Honoraire','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (3,'Commission','COM', 'APP','pencil','Commission','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (4,'Intérêts réalisés','INT', 'APP','pencil','Intérêts réalisés','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (5,'Chiffre d''Affaire','CA', 'APP','pencil','Chiffre d''Affaire','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (6,'Investissements','INV', 'APP','pencil','Investissements','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (7,'Dons perçus','DON', 'APP','pencil','Dons perçus','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (8,'Pension','PENS', 'APP','pencil','Pensions','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (9,'Allocation','ALL', 'APP','pencil','Allocations(bourse, argent de poche…), ','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (10,'Paiements reçus du gouvernement','PAIE-GOUV', 'APP','pencil','Paiements reçus du gouvernement','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (11,'Redevance','RED', 'APP','pencil','Redevance','2022-12-19','2022-12-19',NULL)");
      database.execute(
          "INSERT INTO `type_revenus` VALUES (12,'Encaissement de loyer','LOY', 'APP','pencil','Encaissement de loyer','2022-12-19','2022-12-19',NULL)");

      database.execute(
          'CREATE TABLE revenus(id INTEGER PRIMARY KEY, intitule TEXT, compte_id INTEGER, type_revenu_id INTEGER, montant REAL, description TEXT, date TEXT, created_at TEXT, updated_at TEXT,deleted_at TEXT, FOREIGN KEY(compte_id) REFERENCES comptes(id) , FOREIGN KEY(type_revenu_id) REFERENCES type_revenus(id))');
      database.execute(
          'CREATE TABLE depenses(id INTEGER PRIMARY KEY, intitule TEXT, compte_id INTEGER, categorie_id INTEGER, sous_categorie_id INTEGER, montant REAL, description TEXT, date TEXT, created_at TEXT, updated_at TEXT, deleted_at TEXT, FOREIGN KEY(compte_id) REFERENCES comptes(id) , FOREIGN KEY(categorie_id) REFERENCES categorie_ids(id),  FOREIGN KEY(sous_categorie_id) REFERENCES sous_categories(id))');
      database.execute(
          'CREATE TABLE actifs (id INTEGER PRIMARY KEY, intitule TEXT, type_actif INTEGER, valeur REAL, description TEXT, created_at TEXT, updated_at TEXT,deleted_at TEXT )');
      database.execute(
          'CREATE TABLE passifs (id INTEGER PRIMARY KEY, intitule TEXT, type_passif INTEGER, valeur REAL,  description TEXT, created_at TEXT, updated_at TEXT ,deleted_at TEXT)');
    }, version: version);
    return db;
  }

  //function used to create the table factures
  Future<Database?> createTableFactures() async {
    version = 7;
    db = await openDatabase(join(await getDatabasesPath(), 'finanxee_v2.db'),
        version: version, onUpgrade: (database, oldVersion, newVersion) {
      //create the factures table
      database.execute(
          'CREATE TABLE factures(id INTEGER PRIMARY KEY, intitule TEXT, montant REAL, echeance TEXT, completed INTEGER, created_at TEXT)');
      print('Factures table created');
    });

    //create the factures table
    return db;
  } //function used to create the table categorie de depenses

  Future<Database?> createTableCategorieDepense() async {
    version = 7;
    db = await openDatabase(join(await getDatabasesPath(), 'finanxee_v2.db'),
        version: version, onUpgrade: (database, oldVersion, newVersion) {
      //create the factures table
      database.execute(
          'CREATE TABLE categories_depenses(id INTEGER PRIMARY KEY, intitule TEXT, description TEXT, author TEXT, icon TEXT , code TEXT, created_at TEXT)');
      print('Factures table created');
    });

    //create the factures table
    return db;
  }

  /// ****************End Table creation**********************/

  Future<int?> insertParametre(Parametre parametre) async {
    int? id = await db?.insert(
      'parametres',
      parametre.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //get one parametre by Cle
  Future<Parametre?> getParametreByCle(cle) async {
    final maps =
        await db!.query('parametres', where: 'cle = ?', whereArgs: [cle]);
    return Parametre.fromMap(maps[0]);
  }

//get one Parametre by name
  Future<Parametre?> getParametreByName(name) async {
    final maps =
        await db!.query('parametres', where: 'name = ?', whereArgs: [name]);
    return Parametre.fromMap(maps[0]);
  }

  //********************* Expense List ***************************//

  //function to insert the list
  Future<int?> insertExpenseList(ExpenseList list) async {
    int? id = await db?.insert(
      'expense_lists',
      list.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //function to get the list of expensesLists
  Future<List<ExpenseList>> getExpenseLists() async {
    final List<Map<String, dynamic>> maps = await db!.query('expense_lists',
        where: 'archived = ? AND deleted_at IS NULL',
        whereArgs: [0],
        orderBy: "created_at DESC");
    return List.generate(maps.length, (i) {
      return ExpenseList(
        maps[i]['id'],
        maps[i]['name'],
        maps[i]['archived'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

//function to get the list of expensesLists archivees
  Future<List<ExpenseList>> getExpenseListsArchivees() async {
    final List<Map<String, dynamic>> maps = await db!.query('expense_lists',
        where: 'archived = ? AND deleted_at IS NULL',
        whereArgs: [1],
        orderBy: "created_at DESC");
    return List.generate(maps.length, (i) {
      return ExpenseList(
        maps[i]['id'],
        maps[i]['name'],
        maps[i]['archived'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  // await db!.query('revenus', orderBy: "date DESC", limit: 5);
  //function to get the 5 last expensesLists
  Future<List<ExpenseList>> getLastExpenseLists() async {
    final List<Map<String, dynamic>> maps =
        await db!.query('expense_lists', orderBy: "created_at DESC", limit: 3);
    return List.generate(maps.length, (i) {
      return ExpenseList(
        maps[i]['id'],
        maps[i]['name'],
        maps[i]['archived'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //function to update Expense List
  Future<void> updateExpenseList(ExpenseList ep) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.update("expense_lists", ep.toMap(),
            where: "id = ?", whereArgs: [ep.id]);
    return;
  }

  //soft restore facture
  Future<void> softRestoreList(int idList) async {
    await db!.rawQuery(
        "UPDATE expense_lists SET deleted_at = NULL WHERE id = ?", [idList]);
    return;
  }

  //function to delete an expenseList
  Future<void> deleteExpenseList(int idList) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.delete("expense_lists", where: "id = ?", whereArgs: [idList]);
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return;
  }

  //function to get the list of expensesTasks
  Future<List<ExpenseTask>> getDepensesFromList(int idList) async {
    final List<Map<String, dynamic>> maps = await db!.query('expense_tasks',
        orderBy: "date ASC", where: 'idExpenseList = ?', whereArgs: [idList]);
    return List.generate(maps.length, (i) {
      return ExpenseTask(
        maps[i]['id'],
        maps[i]['idExpenseList'],
        maps[i]['label'],
        maps[i]['price'],
        maps[i]['date'],
        maps[i]['completed'],
        maps[i]['deleted_at'],
      );
    });
  }

  //function to get the list of expensesTasks
  Future<List<ExpenseTask>> getDepensesFromListUndeleted(int idList) async {
    final List<Map<String, dynamic>> maps = await db!.query('expense_tasks',
        orderBy: "date DESC",
        where: 'idExpenseList = ? AND deleted_at IS NULL',
        whereArgs: [idList]);
    return List.generate(maps.length, (i) {
      return ExpenseTask(
        maps[i]['id'],
        maps[i]['idExpenseList'],
        maps[i]['label'],
        maps[i]['price'],
        maps[i]['date'],
        maps[i]['completed'],
        maps[i]['deleted_at'],
      );
    });
  }

  //insert expenseTasks
//function to insert a task into the list
  Future<int?> insertExpenseTask(ExpenseTask expenseTask) async {
    int? id = await db?.insert(
      'expense_tasks',
      expenseTask.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  ExpenseTask expenseTask = ExpenseTask(1, 1, 'label', 25000,
      DateTime.now().toString(), 0, DateTime.now().toString());

  Future<int?> insertAnExpenseTask() async {
    insertExpenseTask(expenseTask);
    return null;
  }

  //update  undelete an ExpenseTask
  Future<void> softRestoreExpenseTask(ExpenseTask task) async {
    await db!.rawQuery(
        "UPDATE expense_tasks SET deleted_at = NULL WHERE id = ?", [task.id]);
    return;
  }

//update  delete an ExpenseTask
  Future<void> updateDeleteExpenseTask(ExpenseTask task) async {
    await db!.rawQuery(
        "UPDATE expense_tasks SET deleted_at = CURRENT_TIMESTAMP WHERE id = ?",
        [task.id]);
    return;
  }

  //function to update Expense Task
  Future<void> updateExpenseTask(ExpenseTask task) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.update("expense_tasks", task.toMap(),
            where: "id = ?", whereArgs: [task.id]);
    return;
  }

  //********************* Factures ***************************//
  //function to get the list of factures
  Future<List<Facture>> getFacturesLists() async {
    final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return List.generate(mapsF.length, (i) {
      return Facture(
        mapsF[i]['id'],
        mapsF[i]['intitule'],
        mapsF[i]['montant'],
        mapsF[i]['echeance'],
        mapsF[i]['completed'],
        mapsF[i]['archived'],
        mapsF[i]['created_at'],
        mapsF[i]['deleted_at'],
      );
    });
  }

//function to get factures non payees
  Future<List<Facture>> getFacturesNonPayees() async {
    final List<Map<String, dynamic>> maps = await db!.query('factures',
        where: 'completed = ? AND deleted_at IS NULL', whereArgs: [0]);
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return List.generate(maps.length, (i) {
      return Facture(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['echeance'],
        maps[i]['completed'],
        maps[i]['archived'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //function to get factures payees
  Future<List<Facture>> getFacturesPayees() async {
    final List<Map<String, dynamic>> maps =
        await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return List.generate(maps.length, (i) {
      return Facture(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['echeance'],
        maps[i]['completed'],
        maps[i]['archived'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  Future<List<Facture>> getFacturesPayeesNonArchivees() async {
    final List<Map<String, dynamic>> maps = await db!.query('factures',
        where: 'completed = ? AND archived = ?', whereArgs: [1, 0]);
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return List.generate(maps.length, (i) {
      return Facture(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['echeance'],
        maps[i]['completed'],
        maps[i]['archived'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //function to get factures archivees
  Future<List<Facture>> getFacturesArchivees() async {
    final List<Map<String, dynamic>> maps =
        await db!.query('factures', where: 'archived = ?', whereArgs: [1]);
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return List.generate(maps.length, (i) {
      return Facture(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['echeance'],
        maps[i]['completed'],
        maps[i]['archived'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //function to update factures
  Future<void> updateFacture(Facture facture) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.update("factures", facture.toMap(),
            where: "id = ?", whereArgs: [facture.id]);
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return;
  }

  //function to delete a facture
  Future<void> deleteFacture(int idFacture) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.delete("factures", where: "id = ?", whereArgs: [idFacture]);
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return;
  }

  //soft restore facture
  Future<void> softRestoreFacture(int idFacture) async {
    await db!.rawQuery(
        "UPDATE factures SET deleted_at = NULL WHERE id = ?", [idFacture]);
    return;
  }

  //function to insert a facture
  Future<int?> insertFacture(Facture facture) async {
    int? id = await db?.insert(
      'factures',
      facture.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //********************* categorie depense ***************************//
//function to get the list of CategorieDepense
  Future<List<CategorieDepense>> getCategoriesDepense() async {
    final List<Map<String, dynamic>> maps =
        await db!.query('categories_depenses');
    return List.generate(maps.length, (i) {
      return CategorieDepense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['author'],
        maps[i]['icon'],
        maps[i]['code'],
        maps[i]['created_at'],
        [],
      );
    });
  }

//function to get the list of sous categories
  Future<List<SousCategorieDepense>> getSousCategoriesDepense() async {
    final List<Map<String, dynamic>> maps =
        await db!.query('sous_categories_depenses');
    return List.generate(maps.length, (i) {
      return SousCategorieDepense(
        maps[i]['id'],
        maps[i]['categorie_depense_id'],
        maps[i]['intitule'],
        maps[i]['author'],
        maps[i]['icon'],
        maps[i]['code'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //get one categoriedepense
  Future<CategorieDepense?> getCategorieById(idCat) async {
    final CategorieDepense categorie = (await db!.query('categories_depenses',
        where: 'id = ?', whereArgs: [idCat])) as CategorieDepense;
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return categorie;
  }

  //get Cat By Id
  Future<CategorieDepense?> getCatById(idCpt) async {
    final maps = await db!
        .query('categories_depenses', where: 'id = ?', whereArgs: [idCpt]);
    return CategorieDepense.fromMap(maps[0]);
  }

  Future<List<CategorieDepense>> generateFullcategories() async {
    List<SousCategorieDepense> list1 = await getSousCategoriesDepense();
    List<CategorieDepense> list2 = await getCategoriesDepense();

    List<CategorieDepense> dataFinal = [];

    for (int i = 0; i < list2.length; i++) {
      dataFinal.add(list2[i]);
      print(dataFinal[0].intitule);

      for (int j = 0; j < list1.length; j++) {
        if (list1[j].categorie_depense_id == list2[i].id) {
          dataFinal[i].sous_categories.add(list1[j]);
        }
      }
    }
    return dataFinal;
  }

  /// *********************************  Depenses   ************************************************************************/
  //get all depenses
  Future<List<Depense>> getDepenses() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps =
        await db!.query('depenses', orderBy: "date DESC");
    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all revenus
  Future<List<Depense>> getLastDepense() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    //db.query(dbTable, orderBy: "userId DESC", limit: 1);
    final List<Map<String, dynamic>> maps =
        await db!.query('depenses', orderBy: "date DESC", limit: 5);

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //get all depenses of current month
  Future<List<Depense>> getDepensesOfMonth() async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month + 1, 0).toString();

    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: 'date >= ? AND date<= ?',
        whereArgs: [firstOfMonth, lastOfMonth],
        orderBy: "date DESC");

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all depenses of current month - 1
  Future<List<Depense>> getDepensesOfMonth_1() async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month - 1, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month, 0).toString();

    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: 'date >= ? AND date<= ?',
        whereArgs: [firstOfMonth, lastOfMonth],
        orderBy: "date DESC");
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all depenses of current month - 2
  Future<List<Depense>> getDepensesOfMonth_2() async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month - 2, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month - 1, 0).toString();

    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: 'date >= ? AND date<= ?',
        whereArgs: [firstOfMonth, lastOfMonth],
        orderBy: "date DESC");
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all depenses of current week
  Future<List<Depense>> getDepensesOfWeek() async {
    DateTime date = DateTime.now();
    DateTime today_0 = DateTime(date.year, date.month, date.day);
    DateTime start_of_week =
        today_0.subtract(Duration(days: today_0.weekday - 1));
    DateTime end_of_week =
        today_0.add(Duration(days: DateTime.daysPerWeek - today_0.weekday));

    var start = start_of_week.toString();
    var end = end_of_week.toString();

    final List<Map<String, dynamic>> maps = await db!.query(
      'depenses',
      where: 'date >= ? AND date<= ?',
      whereArgs: [start, end],
      orderBy: "date DESC",
    );
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all depenses of today
  Future<List<Depense>> getDepensesOfToday() async {
    DateTime date = DateTime.now();
    DateTime today_0 = DateTime(date.year, date.month, date.day);
    DateTime tomorrow_ = DateTime(date.year, date.month, date.day + 1);
    var tod = today_0.toString();
    var tom = tomorrow_.toString();
    //print(tod);

    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: 'date >= ? AND date < ?',
        whereArgs: [tod, tom],
        orderBy: "date DESC");
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all depenses of a particular day
  Future<List<Depense>> getDepensesOfSpecialDay(DateTime date) async {
    DateTime dayAfter = date.add(const Duration(days: 1));
    var tod = date.toString();
    var tom = dayAfter.toString();
    //print(tod);

    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: 'date >= ? AND date < ?',
        whereArgs: [tod, tom],
        orderBy: "date DESC");
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all depenses between t1 and t2
  Future<List<Depense>> getDepensesBetween(String t1, String t2) async {
    /* DateTime date = DateTime.now();
    DateTime today_0 = DateTime(date.year, date.month, date.day);
    DateTime tomorrow_ = DateTime(date.year, date.month, date.day + 1);
    var tod = today_0.toString();
    var tom = tomorrow_.toString();*/
    /*  DateTime t2_add = t2.add(Duration(days: 1));
    String t1_s = t1.toString();
    String t2_s = t2.toString();
    String t2_add_s = t2_add.toString();*/
    //print(tod);

    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: 'date >= ? AND date < ?',
        whereArgs: [t1, t2],
        orderBy: "date DESC");
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all depenses between t1 and t2 . more efficient
  Future<List<Depense>> getDepensesBetween_v2(DateTime t1, DateTime t2) async {
    DateTime date = DateTime.now();
    DateTime t_1 = DateTime(t1.year, t1.month, t1.day);
    DateTime t_2 = DateTime(t2.year, t2.month, t2.day);
    //t_1 = t_1.subtract(Duration(days: 1));
    t_2 = t_2.add(const Duration(days: 1));

    String t1_ = t_1.toString();
    String t2_ = t_2.toString();

    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: 'date >= ? AND date < ?',
        whereArgs: [t1_, t2_],
        orderBy: "date DESC");
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all depenses between t1 and t2 . more efficient
  Future<List<Depense>> searchStringInDepenses(String search) async {
    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: 'intitule LIKE %?%', whereArgs: [search], orderBy: "date DESC");
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//function to insert une depense
  Future<int?> insertDepense(Depense depense) async {
    int? id = await db?.insert(
      'depenses',
      depense.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //function to update Depense
  Future<void> updateDepense(Depense dep) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.update("depenses", dep.toMap(),
            where: "id = ?", whereArgs: [dep.id]);
    return;
  }

  //function to delete depense
  Future<void> deleteDepense(int idDep) async {
    final int ro =
        await db!.delete("depenses", where: "id = ?", whereArgs: [idDep]);
    return;
  }

  //searxh string in revenu
  Future<List<Depense>> searchStringInDepense(String search) async {
    final List<Map<String, dynamic>> maps = await db!.query('depenses',
        where: "intitule LIKE ? ",
        whereArgs: ['%$search%'],
        orderBy: "date DESC");
    return List.generate(maps.length, (i) {
      return Depense(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['description'],
        maps[i]['compte_id'],
        maps[i]['categorie_id'],
        maps[i]['sous_categorie_id'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  /// **************Compte************************************************************************/
  //function to insert UN COMPTE
  Future<int?> insertCompte(Compte compte) async {
    int? id = await db?.insert(
      'comptes',
      compte.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  Future<Compte?> getCompteById(idCpt) async {
    final maps =
        await db!.query('comptes', where: 'id = ?', whereArgs: [idCpt]);
    return Compte.fromMap(maps[0]);
  }

  //get all comptes
  Future<List<Compte>> getComptes() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps =
        await db!.query('comptes', where: 'deleted_at IS NULL');
    return List.generate(maps.length, (i) {
      return Compte(
        maps[i]['id'],
        maps[i]['type'],
        maps[i]['intitule'],
        maps[i]['solde'],
        maps[i]['code'],
        maps[i]['author'],
        maps[i]['imageProvider'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //get comptes bancaires
  Future<List<Compte>> getCompteBancaires() async {
    final List<Map<String, dynamic>> maps = await db!.query('comptes',
        where: 'type = ? AND deleted_at IS NULL', whereArgs: [0]);

    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    return List.generate(maps.length, (i) {
      return Compte(
        maps[i]['id'],
        maps[i]['type'],
        maps[i]['intitule'],
        maps[i]['solde'],
        maps[i]['code'],
        maps[i]['author'],
        maps[i]['imageProvider'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //get comptes bancaires crees avant une date
  Future<List<Compte>> getCompteBancairesBefore(String t1) async {
    final List<Map<String, dynamic>> maps = await db!.query('comptes',
        where: 'type = ? AND created_at <= ? AND deleted_at IS NULL',
        whereArgs: [0, t1]);

    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    return List.generate(maps.length, (i) {
      return Compte(
        maps[i]['id'],
        maps[i]['type'],
        maps[i]['intitule'],
        maps[i]['solde'],
        maps[i]['code'],
        maps[i]['author'],
        maps[i]['imageProvider'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //Get the list of comptes MOmo************************/
  Future<List<Compte>> getCompteMomo() async {
    final List<Map<String, dynamic>> maps = await db!.query('comptes',
        where: 'type = ? AND deleted_at IS NULL', whereArgs: [1]);
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    return List.generate(maps.length, (i) {
      return Compte(
        maps[i]['id'],
        maps[i]['type'],
        maps[i]['intitule'],
        maps[i]['solde'],
        maps[i]['code'],
        maps[i]['author'],
        maps[i]['imageProvider'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //Get the list of comptes MOmo************************/
//Get the list of comptes MOmo************************/
  Future<List<Compte>> getCompteMomoBefore(String t1) async {
    final List<Map<String, dynamic>> maps = await db!.query('comptes',
        where: 'type = ? AND created_at <= ? AND deleted_at IS NULL',
        whereArgs: [1, t1]);
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    return List.generate(maps.length, (i) {
      return Compte(
        maps[i]['id'],
        maps[i]['type'],
        maps[i]['intitule'],
        maps[i]['solde'],
        maps[i]['code'],
        maps[i]['author'],
        maps[i]['imageProvider'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }
  //Get the list of comptes MOmo************************/

  Future<List<Compte>> getCompteAutres() async {
    final List<Map<String, dynamic>> maps = await db!.query('comptes',
        where: 'type = ? AND deleted_at IS NULL', whereArgs: [2]);
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    return List.generate(maps.length, (i) {
      return Compte(
        maps[i]['id'],
        maps[i]['type'],
        maps[i]['intitule'],
        maps[i]['solde'],
        maps[i]['code'],
        maps[i]['author'],
        maps[i]['imageProvider'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  Future<List<Compte>> getCompteAutresBefore(String t1) async {
    final List<Map<String, dynamic>> maps = await db!.query('comptes',
        where: 'type = ? AND created_at <= ? AND deleted_at IS NULL',
        whereArgs: [2, t1]);
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    return List.generate(maps.length, (i) {
      return Compte(
        maps[i]['id'],
        maps[i]['type'],
        maps[i]['intitule'],
        maps[i]['solde'],
        maps[i]['code'],
        maps[i]['author'],
        maps[i]['imageProvider'],
        maps[i]['created_at'],
        maps[i]['deleted_at'],
      );
    });
  }

  //function to delete compte
  Future<void> deleteCompte(int idCompte) async {
    final int ro =
        await db!.delete("comptes", where: "id = ?", whereArgs: [idCompte]);
    return;
  }

  //********************* Revenus ***************************//
//function to get the list of expensesLists
  Future<List<TypeRevenu>> getTypeRevenus() async {
    final List<Map<String, dynamic>> maps = await db!.query('type_revenus');
    return List.generate(maps.length, (i) {
      return TypeRevenu(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['code'],
        maps[i]['author'],
        maps[i]['icon'],
        maps[i]['description'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  Future<TypeRevenu?> getTypeRevenuById(idType) async {
    final maps =
        await db!.query('type_revenus', where: 'id = ?', whereArgs: [idType]);
    return TypeRevenu.fromMap(maps[0]);
  }

  //revenus of month
  Future<List<Revenu>> getRevenusOfMonth() async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month + 1, 0).toString();

    final List<Map<String, dynamic>> maps = await db!.query('revenus',
        orderBy: "date DESC",
        where: 'date >= ? AND date<= ?',
        whereArgs: [firstOfMonth, lastOfMonth]);

    return List.generate(maps.length, (i) {
      return Revenu(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['compte_id'],
        maps[i]['type_revenu_id'],
        maps[i]['montant'],
        maps[i]['description'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //function to get revenus of month
  Future<List<Revenu>> getRevenusOfWeek() async {
    DateTime date = DateTime.now();
    DateTime today_0 = DateTime(date.year, date.month, date.day);
    DateTime start_of_week =
        today_0.subtract(Duration(days: today_0.weekday - 1));
    DateTime end_of_week =
        today_0.add(Duration(days: DateTime.daysPerWeek - today_0.weekday));

    var start = start_of_week.toString();
    var end = end_of_week.toString();

    final List<Map<String, dynamic>> maps = await db!.query('revenus',
        where: 'date >= ? AND date<= ?',
        whereArgs: [start, end],
        orderBy: "date DESC");

    return List.generate(maps.length, (i) {
      return Revenu(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['compte_id'],
        maps[i]['type_revenu_id'],
        maps[i]['montant'],
        maps[i]['description'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//function to get revenus of month
  Future<List<Revenu>> getRevenusBetween(String t1, String t2) async {
    final List<Map<String, dynamic>> maps = await db!.query('revenus',
        where: 'date >= ? AND date<= ?',
        whereArgs: [t1, t2],
        orderBy: "date DESC");

    return List.generate(maps.length, (i) {
      return Revenu(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['compte_id'],
        maps[i]['type_revenu_id'],
        maps[i]['montant'],
        maps[i]['description'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //get all revenus between t1 and t2 . more efficient
  Future<List<Revenu>> getRevenusBetween_v2(DateTime t1, DateTime t2) async {
    DateTime date = DateTime.now();
    DateTime t_1 = DateTime(t1.year, t1.month, t1.day);
    DateTime t_2 = DateTime(t2.year, t2.month, t2.day);
    //t_1 = t_1.subtract(Duration(days: 1));
    t_2 = t_2.add(const Duration(days: 1));

    String t1_ = t_1.toString();
    String t2_ = t_2.toString();

    final List<Map<String, dynamic>> maps = await db!.query('revenus',
        where: 'date >= ? AND date < ?',
        whereArgs: [t1_, t2_],
        orderBy: "date DESC");
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    return List.generate(maps.length, (i) {
      return Revenu(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['compte_id'],
        maps[i]['type_revenu_id'],
        maps[i]['montant'],
        maps[i]['description'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //function to insert revenu
  Future<int?> insertRevenu(Revenu revenu) async {
    int? id = await db?.insert(
      'revenus',
      revenu.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //function to update Revenu
  Future<void> updateRevenu(Revenu project) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.update("revenus", project.toMap(),
            where: "id = ?", whereArgs: [project.id]);
    return;
  }

  //function to delete revenu
  Future<void> deleteRevenu(int idRevenu) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.delete("revenus", where: "id = ?", whereArgs: [idRevenu]);
    return;
  }

  //get all revenus
  Future<List<Revenu>> getRevenus() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps =
        await db!.query('revenus', orderBy: "date DESC");
    return List.generate(maps.length, (i) {
      return Revenu(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['compte_id'],
        maps[i]['type_revenu_id'],
        maps[i]['montant'],
        maps[i]['description'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//searxh string in revenu
  Future<List<Revenu>> searchStringInRevenus(String search) async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');

    final List<Map<String, dynamic>> maps = await db!.query('revenus',
        where: "intitule LIKE ? ",
        whereArgs: ['%$search%'],
        orderBy: "date DESC");
    return List.generate(maps.length, (i) {
      return Revenu(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['compte_id'],
        maps[i]['type_revenu_id'],
        maps[i]['montant'],
        maps[i]['description'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //get 5 last revenus
  Future<List<Revenu>> getLastRevenus() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    //db.query(dbTable, orderBy: "userId DESC", limit: 1);
    final List<Map<String, dynamic>> maps =
        await db!.query('revenus', orderBy: "date DESC", limit: 5);
    return List.generate(maps.length, (i) {
      return Revenu(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['compte_id'],
        maps[i]['type_revenu_id'],
        maps[i]['montant'],
        maps[i]['description'],
        maps[i]['date'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  /// **************Actif************************************************************************/
  //function to insert un actif
  Future<int?> insertActif(Actif actif) async {
    int? id = await db?.insert(
      'actifs',
      actif.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //get all actifs
  Future<List<Actif>> getActifs() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps = await db!.query('actifs');
    return List.generate(maps.length, (i) {
      return Actif(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['type_actif'],
        maps[i]['valeur'],
        maps[i]['description'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

//get all actifs before
  Future<List<Actif>> getActifsBefore(String t1) async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps =
        await db!.query('actifs', where: 'created_at <= ? ', whereArgs: [t1]);
    return List.generate(maps.length, (i) {
      return Actif(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['type_actif'],
        maps[i]['valeur'],
        maps[i]['description'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //get all actifs immobilier
  Future<List<Actif>> getActifsType(int type) async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps =
        await db!.query('actifs', where: 'type_actif = ?', whereArgs: [type]);
    return List.generate(maps.length, (i) {
      return Actif(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['type_actif'],
        maps[i]['valeur'],
        maps[i]['description'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //function to delete compte
  Future<void> deleteActif(int idActif) async {
    final int ro =
        await db!.delete("actifs", where: "id = ?", whereArgs: [idActif]);
    return;
  }

  /// **************Passif************************************************************************/
  //function to insert un actif
  Future<int?> insertPassif(Passif passif) async {
    int? id = await db?.insert(
      'passifs',
      passif.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //get all actifs
  Future<List<Passif>> getPassifs() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps = await db!.query('passifs');
    return List.generate(maps.length, (i) {
      return Passif(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['type_passif'],
        maps[i]['valeur'],
        maps[i]['description'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //get all actifs
  Future<List<Passif>> getPassifsBefore(String t1) async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps =
        //await db!.query('actifs', where: 'created_at <= ? ', whereArgs: [t1]);
        await db!.query('passifs', where: 'created_at <= ? ', whereArgs: [t1]);
    return List.generate(maps.length, (i) {
      return Passif(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['type_passif'],
        maps[i]['valeur'],
        maps[i]['description'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //get all actifs immobilier
  Future<List<Passif>> getPassifsType(int type) async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps =
        await db!.query('passifs', where: 'type_passif = ?', whereArgs: [type]);
    return List.generate(maps.length, (i) {
      return Passif(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['type_passif'],
        maps[i]['valeur'],
        maps[i]['description'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
      );
    });
  }

  //function to delete compte
  Future<void> deletePassif(int idActif) async {
    final int ro =
        await db!.delete("passifs", where: "id = ?", whereArgs: [idActif]);
    return;
  }

  //**************************Fin Passif********************************

//****************************Projet*****************************
  //function to insert a project
  Future<int?> insertProjet(Project projet) async {
    int? id = await db?.insert(
      'projets',
      projet.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //get all projets
  Future<List<Project>> getProjetsAll() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps =
        await db!.query('projets', orderBy: "created_at DESC");
    return List.generate(maps.length, (i) {
      return Project(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['montant_epargne_mensuel'],
        maps[i]['solde_actuel'],
        maps[i]['description'],
        maps[i]['delai'],
        maps[i]['completed'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
        maps[i]['archived'],
      );
    });
  }

  //get all projets
  Future<List<Project>> getProjets() async {
    //final List<Map<String, dynamic>> maps = await db!.query('comptes');
    final List<Map<String, dynamic>> maps = await db!.query('projets',
        where: 'archived = ? AND deleted_at IS NULL',
        whereArgs: [0],
        orderBy: "created_at DESC");
    return List.generate(maps.length, (i) {
      return Project(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['montant_epargne_mensuel'],
        maps[i]['solde_actuel'],
        maps[i]['description'],
        maps[i]['delai'],
        maps[i]['completed'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
        maps[i]['archived'],
      );
    });
  }

  //get 5 last revenus
  Future<List<Project>> getLastProjets() async {
    final List<Map<String, dynamic>> maps =
        await db!.query('projets', orderBy: "created_at DESC", limit: 5);
    return List.generate(maps.length, (i) {
      return Project(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['montant_epargne_mensuel'],
        maps[i]['solde_actuel'],
        maps[i]['description'],
        maps[i]['delai'],
        maps[i]['completed'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
        maps[i]['archived'],
      );
    });
  }

  //function to get factures archivees
  Future<List<Project>> getProjetsArchivees() async {
    final List<Map<String, dynamic>> maps = await db!.query('projets',
        where: 'archived = ? AND deleted_at IS NULL ', whereArgs: [1]);
    //final List<Map<String, dynamic>> mapsF = await db!.query('factures');
    return List.generate(maps.length, (i) {
      return Project(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['montant_epargne_mensuel'],
        maps[i]['solde_actuel'],
        maps[i]['description'],
        maps[i]['delai'],
        maps[i]['completed'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
        maps[i]['archived'],
      );
    });
  }

  //function to update Project
  Future<void> updateProjet(Project project) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.update("projets", project.toMap(),
            where: "id = ?", whereArgs: [project.id]);
    return;
  }

  //function to delete a project
  Future<void> deleteProjet(int idProjet) async {
    final int ro =
        await db!.delete("projets", where: "id = ?", whereArgs: [idProjet]);
    return;
  }

  //function to insert a projet epargne into the projet
  Future<int?> insertEpargneProjet(EpargneProjet epProjet) async {
    int? id = await db?.insert(
      //'expense_tasks',
      'epargnes_projet',
      epProjet.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  //get all depenses of current month
  Future<List<Project>> getProjetsOfMonth() async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month + 1, 0).toString();

    final List<Map<String, dynamic>> maps = await db!.query('projets',
        where: 'created_at >= ? AND created_at<= ?',
        whereArgs: [firstOfMonth, lastOfMonth],
        orderBy: "created_at DESC");

    return List.generate(maps.length, (i) {
      return Project(
        maps[i]['id'],
        maps[i]['intitule'],
        maps[i]['montant'],
        maps[i]['montant_epargne_mensuel'],
        maps[i]['solde_actuel'],
        maps[i]['description'],
        maps[i]['delai'],
        maps[i]['completed'],
        maps[i]['created_at'],
        maps[i]['updated_at'],
        maps[i]['archived'],
      );
    });
  }

  //function to get the list of expensesTasks
  Future<List<EpargneProjet>> getEpargneProjetFromProjet(int idProjet) async {
    final List<Map<String, dynamic>> maps = await db!.query('epargnes_projet',
        orderBy: "date DESC",
        where: 'idProjet = ? AND deleted_at IS NULL',
        whereArgs: [idProjet]);
    return List.generate(maps.length, (i) {
      return EpargneProjet(
        maps[i]['id'],
        maps[i]['idProjet'],
        maps[i]['label'],
        maps[i]['montant'],
        maps[i]['date'],
        maps[i]['deleted_at'],
      );
    });
  }

  //function to update Expense Task
  Future<void> updateEpargneProjet(EpargneProjet task) async {
    final int ro =
        //await db!.query('factures', where: 'completed = ?', whereArgs: [1]);
        await db!.update("epargnes_projet", task.toMap(),
            where: "id = ?", whereArgs: [task.id]);
    return;
  }

  Future<void> softRestoreEpargneProjet(EpargneProjet task) async {
    await db!.rawQuery(
        "UPDATE epargnes_projet SET deleted_at = NULL WHERE id = ?", [task.id]);
    return;
  }
//****************************Fin Projet*************************
}
