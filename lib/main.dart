import 'package:finanxee/FinanxeeTheme.dart';
import 'package:finanxee/navigation/app_router.dart';
import 'package:finanxee/providers/profile_state_manager.dart';
import 'package:finanxee/services/local_notification_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:finanxee/providers/providers.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'cache/cache_helper.dart';
import 'navigation/app_route_parser.dart';

void main() {
  runApp(
      const RootRestorationScope(restorationId: 'root', child: FinanxeeApp()));
}

class FinanxeeApp extends StatefulWidget {
  const FinanxeeApp({Key? key}) : super(key: key);

  @override
  _FinanxeeAppState createState() => _FinanxeeAppState();
}

class _FinanxeeAppState extends State<FinanxeeApp> with RestorationMixin {
  final lightTheme = FinanxeeTheme.lightTheme;
  DbHelper helper = DbHelper();
  late AppRouter _appRouter; // = AppRouter(appStateManager: _appStateManager);
  late final AppStateManager _appStateManager = AppStateManager();
  late final ProfileStateManager _profileStateManager = ProfileStateManager();
  late bool _isCountrySet = false;
  late AppRouteParser routeParser = AppRouteParser();

  @override
  void initState() {
    helper.openDb(); //opening or creating the database
    _appRouter = AppRouter(
      appStateManager: _appStateManager,
      profileStateManager: _profileStateManager,
      //isCountrySet: _isCountrySet,
    );
    //_checkifCountrySet();
    setupNotification();
    super.initState();
  }

  Future<void> setupNotification() async {
    await LocalNotificationService().setup();
  }

  Future<void> _checkifCountrySet() async {
    final prefs = await SharedPreferences.getInstance();
    String selectCountryCode = StaticDate.selectedCountryCode;
    if (prefs.containsKey(selectCountryCode)) {
      Provider.of<AppStateManager>(context, listen: false).setCountry();
      setState(() {
        _isCountrySet = true;
      });
    }
    //Future<String> countryCode = CacheHelper.getString(selectCountryCode);
    print("main countryCode = ");
    print(prefs.getString(selectCountryCode));
  }

  @override
  Widget build(BuildContext context) {
    //creating the tables
    //helper.createTableFactures();

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => TabManager()),
        ChangeNotifierProvider(create: (context) => DataInit()),
        ChangeNotifierProvider(create: (context) => _appStateManager),
        ChangeNotifierProvider(create: (context) => _profileStateManager),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        restorationScopeId: "finanxee",
        debugShowCheckedModeBanner: false,
        theme: lightTheme,
        home: Router(
          routeInformationParser: routeParser,
          restorationScopeId: 'the_router',
          routerDelegate: _appRouter,
          backButtonDispatcher: RootBackButtonDispatcher(),
          //TODO: add backButtonDispatcher
        ), //const LauchScreen(),
      ),
    );
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'root_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    //registerForRestoration(property, restorationId)
    // TODO: implement restoreState
  }

  /* @override
  // TODO: implement restorationId
  String? get restorationId => throw UnimplementedError();

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
  }*/
}
