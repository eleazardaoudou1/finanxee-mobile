import 'package:finanxee/components/components.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'dialogs/expense_task_dialog.dart';

class ExpenseTaskCard extends StatefulWidget {
  final ExpenseTask eptask;
  ExpenseTaskCard(this.eptask);

  @override
  State<ExpenseTaskCard> createState() => _ExpenseTaskCardState(this.eptask);
}

class _ExpenseTaskCardState extends State<ExpenseTaskCard> {
  var f = StaticDate();
  late final ExpenseTask eptask1 =
      ExpenseTask(0, 1, "label", 0, DateTime.now().toString(), 0, "deleted_at");
  late ExpenseTaskDialog dialog;

  _ExpenseTaskCardState(ExpenseTask eptask);

  @override
  void initState() {
    dialog =
        ExpenseTaskDialog(widget.eptask, false, ExpenseList(0, '', 0, '', ''));
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        //color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        /* boxShadow: [
            BoxShadow(
                color: Color(0xFFD0CECE),
                offset: Offset(1, 1),
                blurRadius: 1,
                spreadRadius: 1)
          ]*/
      ),
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: Row(
        children: [
          const CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: Icon(
              Icons.add_business_sharp,
              color: Colors.green,
              size: 20.0,
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        widget.eptask.label,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
                Row(
                  children: [
                    Expanded(
                      child:
                          /*Text(
                        eptask1.date,
                        style: const TextStyle(color: Colors.black),
                      ),*/
                          Text(
                        '${DateTime.parse(widget.eptask.date).day}  '
                        '${fromNumToMonth(DateTime.parse(widget.eptask.date).month)} '
                        '${(DateTime.parse(widget.eptask.date).year)} ',
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      f.numF.format(widget.eptask.price).toString(),
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                    widget.eptask.completed != 0
                        ? const Icon(Icons.check_circle, color: Colors.green)
                        : const Icon(Icons.pending, color: Colors.red),
                  ],
                ),
                /* Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(
                      onTap: () async {
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => dialog);
                      },
                      child: const Icon(
                        Icons.edit,
                        color: Colors.orange,
                        size: 15,
                      ),
                    ),
                    const SizedBox(
                      width: 15,
                    ),
                    const Icon(
                      Icons.delete_forever,
                      color: Colors.red,
                      size: 15,
                    )
                  ],
                )*/
              ],
            ),
          ),
        ],
      ),
    );
  }
}
