import 'package:finanxee/models/categorie_depense.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/models/stats_compte.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class StatsCompteCard extends StatelessWidget {
  StatsCompte stat;
  StatsCompteCard(this.stat);

  var f = StaticDate();
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
      child: Row(
        children: [
          const CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: Icon(
              Icons.monetization_on_outlined,
              color: Colors.green,
              size: 20.0,
            ),
          ),
          //Spacer(),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  stat.intitule,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                Text(f.numF.format(stat.solde_total)),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                stat.nbre_total.toString(),
                style: Theme.of(context).textTheme.labelLarge,
              ),
            ],
          )
        ],
      ),
    );
  }
}

class StatsCompteCad1 extends StatefulWidget {
  Depense depense;
  StatsCompteCad1(this.depense);

  @override
  State<StatsCompteCad1> createState() => _StatsCompteCad1State(depense);
}

class _StatsCompteCad1State extends State<StatsCompteCad1> {
  Compte compte1 = Compte(0, TypeCompte.BANK.index, '', 0.0, '', 'APP',
      'assets/nsia_logo.png', '', '');
  CategorieDepense cat = CategorieDepense(0, '', '', '', '', '', []);

  DbHelper helper = DbHelper();

  @override
  void initState() {
    // TODO: implement initState
    //getDepensesSubObject();
    super.initState();
  }

  Future<Compte> getDepensesSubObject() async {
    final compte2 = await helper.getCompteById(widget.depense.id);
    print(compte2?.intitule);

    final cat2 = await helper
        .getCatById(widget.depense.categorie); //get the list of factures
    //final comptesMomo = await helper.getCompteMomo();
    setState(() {
      //facturesList = factures;
      compte1 = compte2!;
      cat = cat2!;
    });
    return compte1;
  }

  Depense depense =
      Depense(0, '', '', 0, 0, 0, 0.0, DateTime.now().toString(), '', '');
  _StatsCompteCad1State(this.depense);
  var f = StaticDate();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: helper.getSousCategoriesDepense(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );

              // if we got our data
            } else {
              return Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  /*boxShadow: [
            BoxShadow(
                color: Color(0xFFD8F1E7),
                spreadRadius: 2,
                blurRadius: 1,
                offset: Offset(0, 1))
          ]*/
                ),
                padding:
                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
                child: Row(
                  children: [
                    const CircleAvatar(
                      backgroundColor: Color(0xFFF1F1F1),
                      child: Icon(
                        Icons.fastfood,
                        color: Colors.green,
                        size: 20.0,
                      ),
                    ),
                    //Spacer(),
                    const SizedBox(
                      width: 20,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          depense.intitule,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        Text(
                          'cateogire',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    ),
                    const Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          f.numF.format(depense.montant),
                          //this.depense.montant.toString(),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        Text(
                          DateFormat('dd-MM-yyyy')
                              .format(DateTime.parse(depense.date)),
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    )
                  ],
                ),
              );
            }
          }

          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        });
  }
}
