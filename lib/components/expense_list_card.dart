import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/screens/one_expense_list.dart';
import 'package:flutter/material.dart';

class ExpenseListCard extends StatefulWidget {
  final ExpenseList expenseList;
  const ExpenseListCard(this.expenseList);

  @override
  State<ExpenseListCard> createState() => _ExpenseListCardState(expenseList);
}

class _ExpenseListCardState extends State<ExpenseListCard> {
  late final ExpenseList expenseList;
  _ExpenseListCardState(this.expenseList);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        //height: 150.0,
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          /*boxShadow: [
              BoxShadow(
                  color: Color(0xFFD8F1E7),
                  spreadRadius: 2,
                  blurRadius: 1,
                  offset: Offset(0, 1))
            ]*/
        ),
        padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
        child: Row(
          children: [
            const CircleAvatar(
              backgroundColor: Color(0xffd7fff0),
              child: Icon(
                Icons.checklist_sharp,
                color: Colors.green,
                size: 20.0,
              ),
            ),
            const SizedBox(
              width: 15.0,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.expenseList.name,
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    widget.expenseList.name,
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  /* Text(
                    widget.expenseList.created_at,
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),*/
                  Text(
                    '${DateTime.parse(widget.expenseList.created_at).day}  '
                    '${fromNumToMonth(DateTime.parse(widget.expenseList.created_at).month)} '
                    '${(DateTime.parse(widget.expenseList.created_at).year)} ',
                    style: Theme.of(context).textTheme.bodyMedium,
                  )
                ],
              ),
            ),
            /* Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '5 dépenses',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  '${DateTime.parse(widget.expenseList.created_at).day}  '
                  '${fromNumToMonth(DateTime.parse(widget.expenseList.created_at).month)}',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
              ],
            )*/
          ],
        ),
      ),
    );
  }
}

String fromNumToMonth(int month) {
  if (month == 1) {
    return 'Jan';
  } else if (month == 2) {
    return 'Février';
  } else if (month == 3) {
    return 'Mars';
  } else if (month == 4) {
    return 'Avril';
  } else if (month == 5) {
    return 'Mai';
  } else if (month == 6) {
    return 'Juin';
  } else if (month == 7) {
    return 'Juillet';
  } else if (month == 8) {
    return 'Août';
  } else if (month == 9) {
    return 'Septembre';
  } else if (month == 10) {
    return 'Octobre';
  } else if (month == 11) {
    return 'Novembre';
  } else {
    return 'Décembre';
  }
}
