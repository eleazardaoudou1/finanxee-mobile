import 'package:finanxee/components/depense_card.dart';
import 'package:finanxee/components/stats_compte_card.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/models/stats_compte.dart';
import 'package:finanxee/services/compe_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BilanComptes extends StatefulWidget {
  const BilanComptes({Key? key}) : super(key: key);

  @override
  State<BilanComptes> createState() => _BilanComptesState();
}

class _BilanComptesState extends State<BilanComptes> {
  CompteService depenseService = CompteService();
  List<StatsCompte> statsList = [];
  StatsCompte statsCompte = StatsCompte("Comptes bancaires", 07, 1200);
  double cash = 0.0;
  var f = StaticDate();
  DbHelper helper = DbHelper();

  @override
  void initState() {
    // TODO: implement initState
    getComptes();
    super.initState();
  }

  Future<List<StatsCompte>> getComptes() async {
    var solde_banq = await CompteService().total_compte_bancaire();
    var solde_momo = await CompteService().total_compte_momo();
    var solde_autre = await CompteService().total_autre_comptes();

    var nbre_cpte_banq = await CompteService().nbre_compte_bancaire();
    var nbre_cpte_momo = await CompteService().nbre_compte_momo();
    var nbre_cpte_autre = await CompteService().nbre_compte_autres();

    StatsCompte statsCompteBancaires =
        new StatsCompte("Comptes bancaires", nbre_cpte_banq, solde_banq);
    StatsCompte statsCompteMomo =
        new StatsCompte("Comptes Momo", nbre_cpte_momo, solde_momo);
    StatsCompte statsAutresComptes =
        new StatsCompte("Autres comptes", nbre_cpte_autre, solde_autre);

    setState(() {
      statsList.add(statsCompteBancaires);
      statsList.add(statsCompteMomo);
      statsList.add(statsAutresComptes);
      cash = solde_banq + solde_momo + solde_autre;
    });
    return statsList;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return FutureBuilder(
      future: helper.getComptes(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                '${snapshot.error} occurred',
                style: const TextStyle(fontSize: 18),
              ),
            );
          } else if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xFFD8F1E7),
                          offset: Offset(1, 1),
                          blurRadius: 1,
                          spreadRadius: 1)
                    ]),
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Cash",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                        ),
                        /* Text(
                          "Aujourd'hui",
                          style: Theme.of(context).textTheme.labelLarge,
                        )*/
                      ],
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          f.numF.format(cash),
                          style: GoogleFonts.lato(fontSize: 25.0),
                        ),
                        /* Container(
                          child: GestureDetector(
                            onTap: () {
                              SnackBar snackBar = const SnackBar(
                                  content: Text("Voir le bilan des comptes"));
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 8.0,
                              ),
                              decoration: BoxDecoration(
                                color: const Color(0xFF21ca79),
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: const Center(
                                child: Text(
                                  "Détails",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        )*/
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, index) {
                          //Account account = StaticDate.userAccounts[index];
                          return StatsCompteCard(statsList[index]);
                        },
                        separatorBuilder: (BuildContext context, index) {
                          return const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            child: Divider(
                              color: Color(0xFFD4D4D4),
                            ),
                          );
                        },
                        itemCount: statsList.length),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            );
          }
        }
        return const Center(
          child: CircularProgressIndicator(
            color: Colors.green,
          ),
        );
      },
    );
  }
}
