import 'package:finanxee/services/compe_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

import '../screens/dashboard/courbe_avoir_net.dart';
import '../screens/dashboard/details_avoir_net.dart';

class NetWorth extends StatefulWidget {
  const NetWorth({Key? key}) : super(key: key);

  @override
  State<NetWorth> createState() => _NetWorthState();
}

class _NetWorthState extends State<NetWorth> {
  CompteService depenseService = CompteService();
  DbHelper helper = DbHelper();
  var f = StaticDate();
  double data = 0;

  Future<double> getData() async {
    var total_dep_today = await depenseService.avoir_net();
    setState(() {
      data = total_dep_today;
    });
    return total_dep_today;
  }

  @override
  void initState() {
    // TODO: implement initState
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return FutureBuilder(
        future: helper.getDepensesOfToday(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );
            } else if (snapshot.hasData) {
              return Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white, //Color(0xFF00BB),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xFFD8F1E7),
                            offset: Offset(1, 1),
                            blurRadius: 5,
                            spreadRadius: 5)
                      ]),
                  padding: const EdgeInsets.symmetric(
                      vertical: 20.0, horizontal: 16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Avoir Net',
                        style: Theme.of(context).textTheme.bodyLarge,
                      ),
                      const SizedBox(
                        height: 18.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Text(
                              f.numF.format(data),
                              //style: Theme.of(context).textTheme.headline2,
                              style: GoogleFonts.lato(fontSize: 30.0),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 7.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const DetailsAvoirNet()));
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 8.0,
                              ),
                              decoration: BoxDecoration(
                                color: const Color(0xFF21ca79),
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                              child: const Center(
                                child: Text(
                                  "Détails",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      const CourbeAvoirNet()));
                        },
                        child: Row(
                          children: const [
                            Text(
                              'Evolution',
                              style: TextStyle(
                                  color: Color(0xFF21ca79),
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold),
                            ),
                            Icon(
                              Icons.arrow_upward,
                              color: Color(0xFF21ca79),
                              size: 15,
                            ),
                            Icon(
                              Icons.arrow_downward,
                              color: Color(0xffca2135),
                              size: 15,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              );
            }
          }
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        });
  }
}
