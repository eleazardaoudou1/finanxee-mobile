import 'package:finanxee/models/expense_list.dart';
import 'package:flutter/material.dart';

class ListDepenseCard extends StatelessWidget {
  const ListDepenseCard(this.epList);
  final ExpenseList epList;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        /*boxShadow: [
            BoxShadow(
                color: Color(0xFFD8F1E7),
                spreadRadius: 2,
                blurRadius: 1,
                offset: Offset(0, 1))
          ]*/
      ),
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
      child: Row(
        children: [
          const CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: Icon(
              Icons.menu_book_rounded,
              color: Colors.green,
              size: 20.0,
            ),
          ),
          SizedBox(width: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                epList.name,
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Text(
                'Salaire',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
