import 'package:finanxee/components/dialogs/comptes_dialog.dart';
import 'package:finanxee/components/dialogs/sous_categories_dialog.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/providers/providers.dart';
import 'package:finanxee/screens/screens.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../models/payloads/depense_form_payload.dart';

class DepenseForm extends StatefulWidget {
  Depense depense;
  bool isNew;
  DepenseForm(this.depense, this.isNew);

  @override
  State<DepenseForm> createState() => _DepenseFormState(depense, isNew);
}

class _DepenseFormState extends State<DepenseForm> {
  final intituleController = TextEditingController();
  final montantController = TextEditingController();
  DateTime echeance = DateTime.now();
  DbHelper helper = DbHelper();

  //late List<Compte> comptes = [];
  late List<SousCategorieDepense> categories = [];
  late List<CategorieDepense> fullCategories = [];
  List<CategorieDepense> catDep = [];
  bool _isLoading = false;
  @override
  void initState() {
    // TODO: implement initState
    depense =
        Depense(0, '', '', 0, 0, 0, 0.0, DateTime.now().toString(), '', '');

    super.initState();
  }

  Depense depense =
      Depense(0, '', '', 0, 0, 0, 0.0, DateTime.now().toString(), '', '');
  bool isNew = true;
  bool updateCompte = true;

  _DepenseFormState(this.depense, this.isNew);

  //function to get the types of revenues
  Future getCategories() async {
    //open the database
    await helper.openDb();
    categories = await helper.getSousCategoriesDepense();
    catDep = await helper.getCategoriesDepense();

    fullCategories = await helper.generateFullcategories();
    //print(this.categories);
  }

  /* //function to get comptes
  Future getComptes() async {
    //open the database
    await helper.openDb();
    comptes = await helper.getComptes();
  }*/

  Future<DepenseFormPayload> getData() async {
    //recuperer les categories de depenses
    getCategories();
    //recuperer les comptes de l'utilisateur
    List<Compte> comptes = await helper.getComptes();
    print('Comptes from helper: ');
    print(comptes.length);
    DepenseFormPayload depenseFormPayload = DepenseFormPayload([], []);
    depenseFormPayload.comptes = comptes;
    return depenseFormPayload;
  }

  @override
  Widget build(BuildContext context) {
    final dataToUse = Provider.of<DataInit>(context);

    return Scaffold(
      appBar: AppBar(
        //leading: Icon(FlutterIcons.menu_fea),
        //centerTitle: true,
        title: const Text('Ajouter une dépense '),
        backgroundColor: const Color(0xfff3f3f3),
        foregroundColor: Colors.black,
      ),
      body: FutureBuilder(
        future: getData(),
        builder:
            (BuildContext context, AsyncSnapshot<DepenseFormPayload> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            DepenseFormPayload depFP = snapshot.data!;
            print('The Data comptes');
            print(depFP.comptes.length);
            return Container(
              color: const Color(0xFFECECEC), // Colors.green[100],
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 5.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFD8F1E7),
                                  offset: Offset(1, 1),
                                  blurRadius: 1,
                                  spreadRadius: 1)
                            ]),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Remplissez le formulaire",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 40.0,
                            ),
                            buildLabelField(),
                            const SizedBox(
                              height: 40,
                            ),
                            buildCategoriesField(context),
                            const SizedBox(
                              height: 40,
                            ),
                            buildCompteField(context, depFP.comptes),
                            const SizedBox(
                              height: 40,
                            ),
                            buildMontantField(),
                            const SizedBox(
                              height: 40,
                            ),
                            buildRadioField(),
                            const SizedBox(
                              height: 40,
                            ),
                            buildDateField(context),
                            const SizedBox(
                              height: 40,
                            ),
                            Center(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color(0xFF21cA79)),
                                child: const Text('Ajouter'),
                                onPressed: () async {
                                  //faire les controles sur les eventuelles champs vides
                                  if (intituleController.text.isEmpty) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content:
                                          Text("L'intitulé doit être défini."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  } else if (montantController.text.isEmpty) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content:
                                          Text("Le montant doit être défini."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  } else if (dataToUse.compte_selected.id ==
                                      0) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content:
                                          Text("Vous devez choisir un compte."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  } else if (dataToUse
                                          .sous_categories_depense_selected
                                          .id ==
                                      0) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content: Text(
                                          "Vous devez choisir une catégorie."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  }

                                  depense.intitule = intituleController.text;

                                  depense.date = echeance.toString();
                                  depense.compte =
                                      dataToUse.compte_selected.id; //
                                  depense.sous_categorie = dataToUse
                                      .sous_categories_depense_selected.id;
                                  depense.categorie = dataToUse
                                      .sous_categories_depense_selected
                                      .categorie_depense_id;
                                  depense.montant =
                                      double.parse(montantController.text);
                                  if (updateCompte) {
                                    if (depense.montant <=
                                        dataToUse.compte_selected.solde) {
                                      dataToUse.compte_selected.solde -=
                                          depense.montant;

                                      await helper.insertCompte(
                                          dataToUse.compte_selected);

                                      SnackBar snackBar1 = SnackBar(
                                        content: Text(
                                            "Nouveau solde du compte ${dataToUse.compte_selected.intitule} : ${dataToUse.compte_selected.solde}"),
                                        backgroundColor: Colors.green,
                                      );
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar1);
                                    } else {
                                      SnackBar snackBar = SnackBar(
                                        content: Text(
                                            'Attention! Ce montant est supérieur au solde du compte ${dataToUse.compte_selected.intitule}.'),
                                        backgroundColor: Colors.red,
                                      );
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);

                                      return;
                                    }
                                  }

                                  Future<int?> future =
                                      helper.insertDepense(depense);

                                  future.then((news) {
                                    //Navigator.pop(context);
                                    /*Provider.of<AppStateManager>(context,
                                            listen: false)
                                        .gotToTransactions();*/
                                    Navigator.pop(
                                        context, 'Data from DepenseForm');

                                    print(news);
                                  });
                                  /* Navigator.pop(context);
                                  Navigator.maybePop(context);
                                  Provider.of<AppStateManager>(context,
                                          listen: false)
                                      .gotToDashboard();*/

                                  SnackBar snackBar = const SnackBar(
                                    content:
                                        Text('Dépense ajoutée avec succès!'),
                                    backgroundColor: Colors.green,
                                  );
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);

                                  /* Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DepenseScreen()));*/
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Color(0xFF21ca79),
              ),
            );
          }
        },
      ),
    );
  }

  Widget buildProgressIndicator(BuildContext context, depense) {
    return Center(
      child: FutureBuilder(
          future: helper.insertDepense(depense),
          builder: (BuildContext context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              /* Navigator.pop(context);
              Navigator.maybePop(context);
              Provider.of<AppStateManager>(context, listen: false)
                  .gotToDashboard();
              return Center();*/
              return const ActionsUser();
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Color(0xFF21ca79),
                ),
              );
            }
          }),
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Intitulé de la dépense',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: intituleController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Achat au supermarché ",
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildMontantField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Montant',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: montantController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: 'Ex: 1500000 ',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        ),
      ],
    );
  }

  Widget buildRadioField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Voulez-vous soustraire ce montant au solde du compte ?',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        RadioListTile(
          title: Text("Oui"),
          value: true,
          //selected: true,
          groupValue: updateCompte,
          onChanged: (value) {
            setState(() {
              updateCompte = value as bool;
            });
          },
        ),

        RadioListTile(
          title: const Text("Non"),
          value: false,
          groupValue: updateCompte,
          onChanged: (value) {
            setState(() {
              updateCompte = value as bool;
            });
          },
        ),
      ],
    );
  }

  Widget buildDateField(BuildContext context) {
// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Row(
// 3
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
// 4
            Text(
              'Date',
              style: GoogleFonts.lato(fontSize: 16.0),
            ),
// 5
            TextButton(
              child: const Text('Choisir'),
// 6
              onPressed: () async {
                final currentDate = DateTime.now();
// 7
                final selectedDate = await showDatePicker(
                  context: context,
                  initialDate: currentDate,
                  firstDate: DateTime(currentDate.year - 10),
                  lastDate: DateTime(currentDate.year + 5),
                );
                setState(() {
                  if (selectedDate != null) {
                    echeance = selectedDate;
                  }
                });
              },
            ),
          ],
        ),
// 9
        if (echeance != null) Text(DateFormat('dd-MM-yyyy').format(echeance)),
      ],
    );
  }

  Widget buildCategoriesField(BuildContext context) {
    final dataToUse = Provider.of<DataInit>(context);

// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Text(
          'Catégorie',
          style: GoogleFonts.lato(fontSize: 16.0),
        ),
        const SizedBox(
          height: 15,
        ),
// 9

        GestureDetector(
            onTap: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      SousCategoriesDialog(fullCategories));
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFFD0CECE),
                        offset: Offset(1, 1),
                        //blurRadius: 1,
                        spreadRadius: 1)
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(dataToUse.sous_categories_depense_selected.intitule),
                  const Icon(
                    Icons.arrow_drop_down,
                    size: 30,
                  )
                ],
              ),
            )),
      ],
    );
  }

  Widget buildCompteField(BuildContext context, List<Compte> _comptes) {
    final dataCompte = Provider.of<DataInit>(context);

// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Text(
          'Compte',
          style: GoogleFonts.lato(fontSize: 16.0),
        ),
        const SizedBox(
          height: 15,
        ),
// 9 Text(
//           'Compte',
//           style: GoogleFonts.lato(fontSize: 16.0),
//         ),

        GestureDetector(
            onTap: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => CompteDialog(_comptes));
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFFD0CECE),
                        offset: Offset(1, 1),
                        //blurRadius: 1,
                        spreadRadius: 1)
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(dataCompte.compte_selected.intitule),
                  const Icon(
                    Icons.arrow_drop_down,
                    size: 30,
                  )
                ],
              ),
            )),
      ],
    );
  }
}
