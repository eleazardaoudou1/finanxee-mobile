import 'package:finanxee/models/facture.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class FactureForm extends StatefulWidget {
  Facture facture;
  bool isNew;
  //ExpenseList expenseList;
  //const FactureForm({Key? key}) : super(key: key);
  FactureForm(this.facture, this.isNew);

  @override
  State<FactureForm> createState() => _FactureFormState(facture, isNew);
}

class _FactureFormState extends State<FactureForm> {
  final intituleController = TextEditingController();
  final montantController = TextEditingController();
  DateTime echeance = DateTime.now();
  DbHelper helper = DbHelper();
  Facture facture =
      Facture(0, '', 0.0, DateTime.now().toString(), 0, 0, '', '');
  bool isNew = true;

  _FactureFormState(this.facture, this.isNew);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //leading: Icon(FlutterIcons.menu_fea),
        //centerTitle: true,
        title: const Text('Ajouter une facture'),
        backgroundColor: const Color(0xfff3f3f3),
        foregroundColor: Colors.black,
      ),
      body: Container(
        color: const Color(0xFFECECEC), // Colors.green[100],
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 5.0,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xFFD8F1E7),
                            offset: Offset(1, 1),
                            blurRadius: 1,
                            spreadRadius: 1)
                      ]),
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Les détails de la facture",
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 40.0,
                      ),
                      buildLabelField(),
                      const SizedBox(
                        height: 40,
                      ),
                      buildPriceField(),
                      const SizedBox(
                        height: 40,
                      ),
                      buildDateField(context),
                      const SizedBox(
                        height: 40,
                      ),
                      Center(
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xFF21cA79)),
                          child: const Text('Ajouter'),
                          onPressed: () async {
                            if (intituleController.text.isEmpty) {
                              SnackBar snackBar1 = const SnackBar(
                                content: Text("L'intitulé doit être défini"),
                                backgroundColor: Colors.red,
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar1);
                              return;
                            } else if (montantController.text.isEmpty) {
                              SnackBar snackBar2 = const SnackBar(
                                content: Text("Le montant doit être défini"),
                                backgroundColor: Colors.red,
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar2);
                              return;
                            }
                            facture.intitule = intituleController.text;
                            facture.montant =
                                double.parse(montantController.text);
                            facture.echeance = echeance.toString();
                            //helper.createTableFactures();
                            helper.insertFacture(facture);
                            SnackBar snackBar = const SnackBar(
                              content: Text('Facture ajoutée avec succès!'),
                              backgroundColor: Colors.green,
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar);
                            Navigator.pop(context);
/*
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FacturesListUI()));
*/
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Intitulé de la facture',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: intituleController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Ex: Facture d'électricité ",
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildPriceField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Montant',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: montantController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: 'Ex: 1800 ',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        ),
      ],
    );
  }

  Widget buildDateField(BuildContext context) {
// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Row(
// 3
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
// 4
            Text(
              'Echéance',
              style: GoogleFonts.lato(fontSize: 16.0),
            ),
// 5
            TextButton(
              child: const Text('Choisir'),
// 6
              onPressed: () async {
                final currentDate = DateTime.now();
// 7
                final selectedDate = await showDatePicker(
                  context: context,
                  initialDate: currentDate,
                  firstDate: DateTime(currentDate.year - 5),
                  lastDate: DateTime(currentDate.year + 5),
                );
                setState(() {
                  if (selectedDate != null) {
                    echeance = selectedDate;
                  }
                });
              },
            ),
          ],
        ),
// 9
        if (echeance != null) Text(DateFormat('dd-MM-yyyy').format(echeance)),
      ],
    );
  }
}
