import 'dart:ffi';

import 'package:finanxee/components/dialogs/comptes_dialog.dart';
import 'package:finanxee/components/dialogs/type_revenu_dialog.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/revenu.dart';
import 'package:finanxee/models/type_revenu.dart';
import 'package:finanxee/providers/app_state_manager.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:finanxee/providers/tab_manager.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class RevenuForm extends StatefulWidget {
  Revenu revenu;
  bool isNew;
  RevenuForm(this.revenu, this.isNew);

  @override
  State<RevenuForm> createState() => _RevenuFormState(revenu, isNew);
}

class _RevenuFormState extends State<RevenuForm> {
  Revenu revenu = Revenu(0, '', 0, 0, 0.0, '', '', DateTime.now().toString(),
      DateTime.now().toString());
  bool isNew;
  late List<TypeRevenu> typeRevenus = [];
  late List<Compte> comptes = [];
  _RevenuFormState(this.revenu, this.isNew);

  final intituleController = TextEditingController();
  final montantController = TextEditingController();
  DateTime echeance = DateTime.now();
  DbHelper helper = DbHelper();
  bool updateCompte = true;
  @override
  void initState() {
    // TODO: implement initState
    revenu = Revenu(0, '', 0, 0, 0.0, '', '', DateTime.now().toString(),
        DateTime.now().toString());
    intituleController.text = widget.revenu.intitule;
    montantController.text = widget.revenu.montant.toString();
    super.initState();
  }

  //function to get the types of revenues
  Future getTypeRevenus() async {
    //open the database
    await helper.openDb();
    typeRevenus = await helper.getTypeRevenus();
  }

  //function to get comptes
  Future getComptes() async {
    //open the database
    await helper.openDb();
    comptes = await helper.getComptes();
  }

  Future getData() async {
    getTypeRevenus();
    return getComptes();
  }

  @override
  Widget build(BuildContext context) {
    final dataRevenu = Provider.of<DataInit>(context);

    return Scaffold(
      appBar: AppBar(
        //leading: Icon(FlutterIcons.menu_fea),
        //centerTitle: true,
        title: const Text('Ajouter un revenu'),
        backgroundColor: const Color(0xfff3f3f3),
        foregroundColor: Colors.black,
      ),
      body: FutureBuilder(
        future: getData(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Container(
              color: const Color(0xFFECECEC), // Colors.green[100],
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 5.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFD8F1E7),
                                  offset: Offset(1, 1),
                                  blurRadius: 1,
                                  spreadRadius: 1)
                            ]),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Remplissez le formulaire",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 40.0,
                            ),
                            buildLabelField(),
                            const SizedBox(
                              height: 40,
                            ),
                            buildTypeRevenuField(context, typeRevenus[0]),
                            const SizedBox(
                              height: 40,
                            ),
                            buildCompteField(context),
                            const SizedBox(
                              height: 40,
                            ),
                            buildPriceField(),
                            const SizedBox(
                              height: 40,
                            ),
                            buildRadioField(),
                            const SizedBox(
                              height: 40,
                            ),
                            buildDateField(context),
                            const SizedBox(
                              height: 40,
                            ),
                            Center(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color(0xFF21cA79)),
                                child: const Text('Ajouter'),
                                onPressed: () {
                                  //faire les controles
                                  if (intituleController.text.isEmpty) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content:
                                          Text("L'intitulé doit être défini."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  } else if (montantController.text.isEmpty) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content:
                                          Text("Le montant doit être défini."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  } else if (dataRevenu.compte_selected.id ==
                                      0) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content:
                                          Text("Vous devez choisir un compte."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  } else if (dataRevenu
                                          .type_revenu_selected.id ==
                                      0) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content: Text(
                                          "Vous devez choisir un type de revenu."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  }

                                  widget.revenu.intitule =
                                      intituleController.text;
                                  widget.revenu.montant =
                                      double.parse(montantController.text);
                                  widget.revenu.date = echeance.toString();
                                  widget.revenu.compte =
                                      dataRevenu.compte_selected.id; //
                                  widget.revenu.type_revenu =
                                      dataRevenu.type_revenu_selected.id;
                                  helper.insertRevenu(widget.revenu);

                                  Navigator.pop(context);
                                  Provider.of<AppStateManager>(context,
                                      listen: false);

                                  if (isNew) {
                                    dataRevenu.compte_selected.solde +=
                                        widget.revenu.montant;
                                    helper.insertCompte(
                                        dataRevenu.compte_selected);
                                    SnackBar snackBar = const SnackBar(
                                      content:
                                          Text('Revenu ajouté avec succès!'),
                                      backgroundColor: Colors.green,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                    SnackBar snackBar1 = SnackBar(
                                      content: Text(
                                          "Nouveau solde du compte ${dataRevenu.compte_selected.intitule} : ${dataRevenu.compte_selected.solde}"),
                                      backgroundColor: Colors.green,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                  } else {
                                    helper.insertCompte(
                                        dataRevenu.compte_selected);
                                    SnackBar snackBar = const SnackBar(
                                      content: Text(
                                          "Revenu mis à jour avec succès !!"),
                                      backgroundColor: Colors.green,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Color(0xFF21ca79),
              ),
            );
          }
        },
      ),
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Intitulé du revenu',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: intituleController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Salaire du mois ",
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildPriceField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Montant',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: montantController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: 'Ex: 1500000 ',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        ),
      ],
    );
  }

  Widget buildRadioField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Voulez-vous ajouter ce montant au solde du compte ?',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        RadioListTile(
          title: Text("Oui"),
          value: true,
          //selected: true,
          groupValue: updateCompte,
          onChanged: (value) {
            setState(() {
              updateCompte = value as bool;
            });
          },
        ),

        RadioListTile(
          title: Text("Non"),
          value: false,
          groupValue: updateCompte,
          onChanged: (value) {
            setState(() {
              updateCompte = value as bool;
            });
          },
        ),
      ],
    );
  }

  Widget buildDateField(BuildContext context) {
// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Row(
// 3
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
// 4
            Text(
              'Date',
              style: GoogleFonts.lato(fontSize: 16.0),
            ),
// 5
            TextButton(
              child: const Text('Choisir'),
// 6
              onPressed: () async {
                final currentDate = DateTime.now();
// 7
                final selectedDate = await showDatePicker(
                  context: context,
                  initialDate: currentDate,
                  firstDate: DateTime(currentDate.year - 10),
                  lastDate: DateTime(currentDate.year + 5),
                );
                setState(() {
                  if (selectedDate != null) {
                    echeance = selectedDate;
                  }
                });
              },
            ),
          ],
        ),
// 9
        if (echeance != null) Text(DateFormat('dd-MM-yyyy').format(echeance)),
      ],
    );
  }

  Widget buildTypeRevenuField(BuildContext context, typeRevenuSelected) {
    final dataRevenu = Provider.of<DataInit>(context);

// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Text(
          'Type de revenu',
          style: GoogleFonts.lato(fontSize: 16.0),
        ),
        const SizedBox(
          height: 15,
        ),
// 9

        GestureDetector(
            onTap: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) =>
                      TypeRevenuDialog(typeRevenus));
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFFD0CECE),
                        offset: Offset(1, 1),
                        //blurRadius: 1,
                        spreadRadius: 1)
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(dataRevenu.type_revenu_selected.intitule),
                  const Icon(
                    Icons.arrow_drop_down,
                    size: 30,
                  )
                ],
              ),
            )),
      ],
    );
  }

  Widget buildCompteField(BuildContext context) {
    final dataRevenu = Provider.of<DataInit>(context);

// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Text(
          'Compte',
          style: GoogleFonts.lato(fontSize: 16.0),
        ),
        const SizedBox(
          height: 15,
        ),
// 9 Text(
//           'Compte',
//           style: GoogleFonts.lato(fontSize: 16.0),
//         ),

        GestureDetector(
            onTap: () async {
              showDialog(
                  context: context,
                  builder: (BuildContext context) => CompteDialog(comptes));
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFFD0CECE),
                        offset: Offset(1, 1),
                        //blurRadius: 1,
                        spreadRadius: 1)
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(dataRevenu.compte_selected.intitule),
                  const Icon(
                    Icons.arrow_drop_down,
                    size: 30,
                  )
                ],
              ),
            )),
      ],
    );
  }
}
