import 'package:finanxee/models/compte.dart';
import 'package:flutter/material.dart';

class CompteForm extends StatefulWidget {
  Compte compte;
  bool isNew;
  CompteForm(this.compte, this.isNew);

  @override
  State<CompteForm> createState() => _CompteFormState(compte, isNew);
}

class _CompteFormState extends State<CompteForm> {
  Compte compte;
  bool isNew;
  _CompteFormState(this.compte, this.isNew);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
