import 'package:finanxee/models/models.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ProjectForm extends StatefulWidget {
  Project project;
  bool isNew;
  ProjectForm(this.project, this.isNew);

  @override
  State<ProjectForm> createState() =>
      _ProjectFormState(this.project, this.isNew);
}

class _ProjectFormState extends State<ProjectForm> {
  Project project = Project(
      0,
      "intitule",
      0,
      0,
      0,
      "description",
      DateTime.now().toString(),
      0,
      DateTime.now().toString(),
      DateTime.now().toString(),
      0);

  final intituleController = TextEditingController();
  final montantController = TextEditingController();
  final montantEpargneController = TextEditingController();
  DateTime delai = DateTime.now(); // project.delai!="" ?DateTime.now():;
  DbHelper helper = DbHelper();
  bool isNew = true;

  _ProjectFormState(Project project, bool isNew);

  @override
  void initState() {
    // TODO: implement initState
    intituleController.text = widget.project.intitule;
    montantController.text = widget.project.montant.toString();
    montantEpargneController.text =
        widget.project.montant_epargne_mensuel.toString();
    delai = DateTime.parse(widget.project.delai);
    super.initState();
  }

  Future getData() async {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //leading: Icon(FlutterIcons.menu_fea),
        //centerTitle: true,
        title: const Text('Ajouter un projet '),
        backgroundColor: const Color(0xfff3f3f3),
        foregroundColor: Colors.black,
      ),
      body: FutureBuilder(
        future: getData(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Container(
              color: const Color(0xFFECECEC), // Colors.green[100],
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 5.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFD8F1E7),
                                  offset: Offset(1, 1),
                                  blurRadius: 1,
                                  spreadRadius: 1)
                            ]),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Remplissez le formulaire",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 40.0,
                            ),
                            buildLabelField(),
                            const SizedBox(
                              height: 40,
                            ),
                            buildMontantField(),
                            const SizedBox(
                              height: 40,
                            ),
                            buildDateField(context),
                            const SizedBox(
                              height: 40,
                            ),
                            buildMontantProjeteField(),
                            const SizedBox(
                              height: 40,
                            ),
                            Center(
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color(0xFF21cA79)),
                                child: const Text('Ajouter'),
                                onPressed: () async {
                                  //faire les controles sur les eventuelles champs vides
                                  if (intituleController.text.isEmpty) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content:
                                          Text("L'intitulé doit être défini."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  } else if (montantController.text.isEmpty) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content:
                                          Text("Le montant doit être défini."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  } else if (montantEpargneController
                                      .text.isEmpty) {
                                    SnackBar snackBar1 = const SnackBar(
                                      content: Text(
                                          "Le montant mensuel doit être défini."),
                                      backgroundColor: Colors.red,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar1);
                                    return;
                                  }
                                  project.id = widget.project.id;
                                  project.intitule = intituleController.text;

                                  project.delai = delai.toString();

                                  project.montant =
                                      double.parse(montantController.text);
                                  project.montant_epargne_mensuel =
                                      double.parse(
                                          montantEpargneController.text);

                                  Future<int?> future =
                                      helper.insertProjet(project);

                                  SnackBar snackBar = const SnackBar(
                                    content: Text('Projet ajouté avec succès!'),
                                    backgroundColor: Colors.green,
                                  );
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);

                                  Navigator.pop(context);
                                  Navigator.pop(context);
                                  /* Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DepenseScreen()));*/
                                },
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Color(0xFF21ca79),
              ),
            );
          }
        },
      ),
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Donnez un nom au projet',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: intituleController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Acheter une voiture, Epargner pour investir ",
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildMontantField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Combien peut coûter le projet ?',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: montantController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: 'Ex: 3000000 ',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        ),
      ],
    );
  }

  Widget buildMontantProjeteField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Combien seriez-vous prêt à épargner par mois pour ce projet ?',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: montantEpargneController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: 'Ex: 100 0000 ',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        ),
      ],
    );
  }

  Widget buildDateField(BuildContext context) {
// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Row(
// 3
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
// 4
            Expanded(
              child: Text(
                'Super! Quel délai prévoyez-vous pour le projet ?',
                style: GoogleFonts.lato(fontSize: 16.0),
              ),
            ),
// 5
            TextButton(
              child: const Text('Choisir une date'),
// 6
              onPressed: () async {
                final currentDate =
                    DateTime.parse(widget.project.delai); //DateTime.now();
// 7
                final selectedDate = await showDatePicker(
                  context: context,
                  initialDate: currentDate,
                  firstDate: DateTime(currentDate.year - 10),
                  lastDate: DateTime(currentDate.year + 5),
                );
                setState(() {
                  if (selectedDate != null) {
                    delai = selectedDate;
                  }
                });
              },
            ),
          ],
        ),
// 9
        const SizedBox(
          height: 15,
        ),
        if (delai != null) Text(DateFormat('dd-MM-yyyy').format(delai)),
      ],
    );
  }
}
