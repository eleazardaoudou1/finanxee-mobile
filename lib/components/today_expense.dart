import 'package:finanxee/components/depense_card.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/services/depense_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../screens/dashboard/details_depenses_today.dart';

class TodayExpense extends StatefulWidget {
  const TodayExpense({Key? key}) : super(key: key);

  @override
  State<TodayExpense> createState() => _TodayExpenseState();
}

class _TodayExpenseState extends State<TodayExpense> {
  DepenseService depenseService = DepenseService();
  DbHelper helper = DbHelper();
  var f = StaticDate();
  double data = 0;
  Depense depense =
      Depense(0, '', '', 0, 0, 0, 0.0, DateTime.now().toString(), '', '');

  Future<double> getData() async {
    var total_dep_today = await depenseService.total_depense_today();
    setState(() {
      data = total_dep_today;
    });
    return total_dep_today;
  }

  @override
  void initState() {
    // TODO: implement initState
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return FutureBuilder(
        future: helper.getDepensesOfToday(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );
            } else if (snapshot.hasData) {
              return Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                      boxShadow: [
                        BoxShadow(
                            color: Color(0xFFD8F1E7),
                            offset: Offset(1, 1),
                            blurRadius: 1,
                            spreadRadius: 1)
                      ]),
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Total Dépenses",
                            style: Theme.of(context)
                                .textTheme
                                .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                          ),
                          Text(
                            "Aujourd'hui",
                            style: Theme.of(context).textTheme.labelLarge,
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            f.numF.format(data),
                            style: GoogleFonts.lato(fontSize: 25.0),
                          ),
                          Container(
                            child: GestureDetector(
                              onTap: () {
                                //
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const DetailsDepensesToday()));
                              },
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0,
                                  vertical: 8.0,
                                ),
                                decoration: BoxDecoration(
                                  color: const Color(0xFF21ca79),
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                                child: const Center(
                                  child: Text(
                                    "Détails",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              );
            }
          }
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        });
  }
}
