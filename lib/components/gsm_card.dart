import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/pays_gsm.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'circle_image.dart';

class GSMCard extends StatelessWidget {
  final PaysGSM paysGSM;

  const GSMCard(this.paysGSM);

  @override
  Widget build(BuildContext context) {
    final namesData = Provider.of<DataInit>(context);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 50,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Colors.grey[200]),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
            child: Text(
              paysGSM.intitule,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ),
        const SizedBox(
          height: 5.0,
        ),
        ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, index) {
              Compte compte = paysGSM.comptes[index];
              return GestureDetector(
                onTap: () {
                  namesData.updateCompteSelected2(compte);
                  Navigator.pop(context);
                },
                child: ListTile(
                  title: Text(compte.intitule),
                  trailing: CircleImage(
                    imageProvider: AssetImage(compte.imageProvider),
                    imageRadius: 20,
                  ),
                ),
              );
            },
            separatorBuilder: (BuildContext context, index) {
              return const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Divider(
                  color: Color(0xFFD4D4D4),
                ),
              );
            },
            itemCount: paysGSM.comptes.length),
      ],
    );
  }
}
