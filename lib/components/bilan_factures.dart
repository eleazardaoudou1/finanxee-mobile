import 'package:finanxee/components/depense_card.dart';
import 'package:finanxee/components/stats_compte_card.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/models/stats_compte.dart';
import 'package:finanxee/screens/factures/factures_list_ui.dart';
import 'package:finanxee/services/compe_service.dart';
import 'package:finanxee/services/facture_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BilanFactures extends StatefulWidget {
  const BilanFactures({Key? key}) : super(key: key);

  @override
  State<BilanFactures> createState() => _BilanFacturesState();
}

class _BilanFacturesState extends State<BilanFactures> {
  FactureService factureService = FactureService();
  int bilanFactPay = 0;
  int bilanFactNonPay = 0;
  var f = StaticDate();
  DbHelper helper = DbHelper();

  @override
  void initState() {
    // TODO: implement initState
    getFactures();
    super.initState();
  }

  Future<int> getFactures() async {
    var facP = await helper.getFacturesPayees();
    var facNonP = await helper.getFacturesNonPayees();
    setState(() {
      bilanFactPay = facP.length;
      bilanFactNonPay = facNonP.length;
    });
    return bilanFactPay;
  }

  @override
  Widget build(BuildContext context) {
    ThemeData themeData = Theme.of(context);
    return FutureBuilder(
      future: helper.getComptes(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                '${snapshot.error} occurred',
                style: const TextStyle(fontSize: 18),
              ),
            );
          } else if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xFFD8F1E7),
                          offset: Offset(1, 1),
                          blurRadius: 1,
                          spreadRadius: 1)
                    ]),
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Factures",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                        ),
                        /* Text(
                          "Aujourd'hui",
                          style: Theme.of(context).textTheme.labelLarge,
                        )*/
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        /*  Text(
                          f.numF.format(0.0),
                          style: GoogleFonts.lato(fontSize: 25.0),
                        ),*/
                        Expanded(
                          child: Text(
                            "Bilan des factures payées et impayées.",
                            style: Theme.of(context).textTheme.labelLarge,
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const FacturesListUI()))
                                .then((value) => getFactures());
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 16.0,
                              vertical: 8.0,
                            ),
                            decoration: BoxDecoration(
                              color: const Color(0xFF21ca79),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: const Center(
                              child: Text(
                                "Détails",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    Column(
                      children: [
                        Container(
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 0.0),
                          child: Row(
                            children: [
                              const CircleAvatar(
                                backgroundColor: Color(0xFFF1F1F1),
                                child: Icon(
                                  Icons.monetization_on_outlined,
                                  color: Colors.green,
                                  size: 20.0,
                                ),
                              ),
                              //Spacer(),
                              const SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Factures payées',
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                    Text(bilanFactPay.toString()),
                                  ],
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    bilanFactPay.toString(),
                                    style:
                                        Theme.of(context).textTheme.labelLarge,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.0),
                          child: Divider(
                            color: Color(0xFFD4D4D4),
                          ),
                        ),
                        Container(
                          decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          padding: const EdgeInsets.symmetric(
                              vertical: 5.0, horizontal: 0.0),
                          child: Row(
                            children: [
                              const CircleAvatar(
                                backgroundColor: Color(0xFFF1F1F1),
                                child: Icon(
                                  Icons.monetization_on_outlined,
                                  color: Colors.red,
                                  size: 20.0,
                                ),
                              ),
                              //Spacer(),
                              const SizedBox(
                                width: 20,
                              ),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Factures non payées',
                                      style:
                                          Theme.of(context).textTheme.bodyText1,
                                    ),
                                    Text(bilanFactNonPay.toString()),
                                  ],
                                ),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    bilanFactNonPay.toString(),
                                    style:
                                        Theme.of(context).textTheme.labelLarge,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            );
          }
        }
        return const Center(
          child: CircularProgressIndicator(
            color: Colors.green,
          ),
        );
      },
    );
  }
}
