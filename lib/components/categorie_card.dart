import 'package:finanxee/models/categorie_depense.dart';
import 'package:finanxee/models/sous_categorie_depense.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CategorieCard extends StatelessWidget {
  CategorieDepense categorieDepense;
  CategorieCard(this.categorieDepense);

  @override
  Widget build(BuildContext context) {
    final catData = Provider.of<DataInit>(context);

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            height: 25,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(color: Colors.grey[200]),
            child: Text(
              categorieDepense.intitule,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          const SizedBox(
            height: 5.0,
          ),
          ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, index) {
                SousCategorieDepense sCat =
                    categorieDepense.sous_categories[index];
                return GestureDetector(
                  onTap: () {
                    catData.updateSousCategoriesDepenseSelected(sCat);
                    Navigator.pop(context);
                  },
                  child: Container(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: ListTile(
                      title: Text(sCat.intitule),
                    ),
                  ),
                );
              },
              separatorBuilder: (BuildContext context, index) {
                return const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.0),
                  child: Divider(
                    color: Color(0xFFD4D4D4),
                  ),
                );
              },
              itemCount: categorieDepense.sous_categories.length),
        ],
      ),
    );
  }
}
