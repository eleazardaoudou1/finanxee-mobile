import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../components/square_image.dart';
import '../../utils/static_data.dart';

class AvoirNetMonthCard extends StatefulWidget {
  const AvoirNetMonthCard(
      this.value, this.moisAnnee /*, this.actifSolde, this.passifSolde*/);
  final double value;
  final DateTime moisAnnee;
/*  final double actifSolde;
  final double passifSolde;*/
  @override
  State<AvoirNetMonthCard> createState() => _AvoirNetMonthCardState();
}

class _AvoirNetMonthCardState extends State<AvoirNetMonthCard> {
  var f = StaticDate();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFD0CECE),
                offset: Offset(1, 1),
                blurRadius: 1,
                spreadRadius: 1)
          ]),
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Container(
            width: 70,
            height: 70,
            decoration: const BoxDecoration(
              color: Colors.lightBlue,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            child: const Icon(
              Icons.assessment,
              color: Colors.white,
              size: 30.0,
            ),
          ),
          const SizedBox(
            width: 30,
          ),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    f.monthYear(widget.moisAnnee),
                    //"${widget.moisAnnee.month}. .${widget.moisAnnee.year} ",
                    style: GoogleFonts.lato(
                        fontSize: 15.0, fontWeight: FontWeight.bold),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8.0,
                      vertical: 8.0,
                    ),
                    decoration: BoxDecoration(
                        color: widget.value >= 0
                            ? const Color(0xff92fac4)
                            : const Color(0xfffa9292),
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          color: widget.value >= 0
                              ? const Color(0xFF21ca79)
                              : const Color(0xffca2121),
                          width: 1,
                        )),
                    child: Text(
                      widget.value >= 0
                          ? "+${f.numF.format(widget.value).toString()}"
                          : f.numF.format(widget.value).toString(),
                      style: GoogleFonts.lato(
                          color: widget.value >= 0
                              ? const Color(0xFF21ca79)
                              : const Color(0xffca2121),
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ],
          )),
        ],
      ),
    );
  }
}
