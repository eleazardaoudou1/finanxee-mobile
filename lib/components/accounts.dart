import 'package:finanxee/components/account_card.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';


class Accounts extends StatelessWidget {
  const Accounts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Tous les comptes',
                style: Theme.of(context).textTheme.bodyLarge,
              ),
              Text(
                'Solde total',
                style: Theme.of(context).textTheme.labelLarge,
              ),
            ],
          ),
          const SizedBox(
            height: 10.0,
          ),
          ListView.separated(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, index) {
                Compte account = StaticDate.paysBanques[0].comptes[index];
                return AccountCard(
                  account,
                );
              },
              separatorBuilder: (BuildContext context, index) {
                return const SizedBox(
                  height: 20.0,
                );
              },
              itemCount: StaticDate.userAccounts.length)
        ],
      ),
    );
  }
}
