import 'package:finanxee/components/components.dart';
import 'package:finanxee/models/revenu.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class RevenueCard extends StatelessWidget {
  final Revenu revenu;
  RevenueCard(this.revenu);
  var f = StaticDate();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        /*boxShadow: [
            BoxShadow(
                color: Color(0xFFD8F1E7),
                spreadRadius: 2,
                blurRadius: 1,
                offset: Offset(0, 1))
          ]*/
      ),
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Row(
              children: [
                const CircleAvatar(
                  backgroundColor: Color(0xFFF1F1F1),
                  child: Icon(
                    Icons.account_balance_wallet_outlined,
                    color: Colors.green,
                    size: 15.0,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              revenu.intitule,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        '${DateTime.parse(revenu.date).day}  '
                        '${fromNumToMonth(DateTime.parse(revenu.date).month)} '
                        '${(DateTime.parse(revenu.date).year)} ',
                        style: Theme.of(context).textTheme.bodyMedium,
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Text(
            f.numF.format(revenu.montant),
            //this.depense.montant.toString(),
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ],
      ),
    );
  }
}
