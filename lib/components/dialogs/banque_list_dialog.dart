import 'package:finanxee/components/banque_card.dart';
import 'package:finanxee/models/pays_banque.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BanqueListDialog extends StatefulWidget {
  //final double solde1;
  const BanqueListDialog();

  @override
  State<BanqueListDialog> createState() => _BanqueListDialogState();
}

class _BanqueListDialogState extends State<BanqueListDialog> {
  List<PaysBanque> paysBanquesList = StaticDate.paysBanques;

  _BanqueListDialogState();

  @override
  void initState() {
    // TODO: implement initState
    //this.solde = widget.solde1;
    super.initState();
  }

  Future<List<PaysBanque>> setPaysBanque() async {
    final prefs = await SharedPreferences.getInstance();
    String selectCountryCode = StaticDate.selectedCountryCode;
    String? countryCode = prefs.getString(selectCountryCode);
    print("GSM CountryCode = ");
    print(countryCode);
    PaysBanque pays = getByCode(countryCode!);
    List<PaysBanque> paysList = [];
    paysList.add(pays);
    print("GSM Pays = ");
    print(pays.intitule);
    return paysList;
  }

  PaysBanque getByCode(String countryCode) {
    for (int i = 0; i < paysBanquesList.length; i++) {
      if (paysBanquesList[i].id == countryCode) {
        return paysBanquesList[i];
      }
    }
    return paysBanquesList[0];
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: setPaysBanque(),
        builder:
            (BuildContext context, AsyncSnapshot<List<PaysBanque>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );

              // if we got our data
            } else {
              var paysList1 = snapshot.data!;
              return Center(
                child: AlertDialog(
                  title: Center(
                    child: Text(
                      'Choisir votre banque',
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                  content: Container(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                    height: MediaQuery.of(context).size.height * 0.8,
                    decoration: const BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Color(0xffffffff),
                      border: Border(
                          top: BorderSide(
                        color: Colors.black,
                        width: 1.0,
                      )),
                      //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Expanded(
                          child: SingleChildScrollView(
                            child: ListView.separated(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (BuildContext context, index) {
                                  return BanqueCard(paysList1[index]);
                                  //BanqueCard est un composant qui affiche toutes les banques
                                  // d'un pays. Il prend en paramètre le pays. On fait des itérations
                                  // dans ce card.
                                },
                                separatorBuilder:
                                    (BuildContext context, index) {
                                  return const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  );
                                },
                                itemCount: paysList1.length),
                          ),
                        ),
                      ],
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                ),
              );
            }
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.green,
              ),
            );
          }
        });
  }
}
