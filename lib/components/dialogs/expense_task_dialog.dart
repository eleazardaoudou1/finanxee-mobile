import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/models/expense_task.dart';
import 'package:finanxee/screens/expense_list_ui.dart';
import 'package:finanxee/screens/one_expense_list.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ExpenseTaskDialog extends StatefulWidget {
  ExpenseTask expenseTask;
  bool isNew;
  ExpenseList expenseList;
  ExpenseTaskDialog(this.expenseTask, this.isNew, this.expenseList);

  //const ExpenseTaskDialog({Key? key}) : super(key: key);

  @override
  State<ExpenseTaskDialog> createState() =>
      _ExpenseTaskDialogState(expenseTask, isNew, expenseList);
}

class _ExpenseTaskDialogState extends State<ExpenseTaskDialog> {
  ExpenseTask expenseTask = ExpenseTask(0, 1, '', 0.0, '', 0, '');
  bool isNew = false;
  ExpenseList expenseList;

  _ExpenseTaskDialogState(this.expenseTask, this.isNew, this.expenseList);

  final labelController = TextEditingController();
  final priceController = TextEditingController();
  DateTime _dueDate = DateTime.now();
  DbHelper helper = DbHelper();

  @override
  void initState() {
    // TODO: implement initState
    if (!isNew) {
      // we are doing an update, since the list exists.
      labelController.text = widget.expenseTask.label;
      priceController.text = widget.expenseTask.price.toString();
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            (isNew) ? 'Ajouter dépense à effectuer' : 'Editer',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 300.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildLabelField(),
                    const SizedBox(
                      height: 10,
                    ),
                    buildPriceField(),
                    const SizedBox(
                      height: 10,
                    ),
                    buildDateField(context),
                  ],
                ),
              ),
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(primary: const Color(0xFF21cA79)),
                child: widget.isNew
                    ? const Text('Ajouter')
                    : const Text('Modifier'),
                onPressed: () {
                  if (labelController.text.isEmpty) {
                    SnackBar snackBar1 = const SnackBar(
                      content: Text("L'intitulé doit être défini."),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                    return;
                  }
                  if (priceController.text.isEmpty) {
                    SnackBar snackBar1 = const SnackBar(
                      content: Text("Le prix doit être défini."),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                    return;
                  }

                  expenseTask.label = labelController.text;
                  expenseTask.idExpenseList = expenseList.id;
                  expenseTask.price = double.parse(priceController.text);
                  expenseTask.date = _dueDate.toString();
                  helper.insertExpenseTask(expenseTask);
                  Navigator.pop(context);
                },
              )
            ],
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Intitulé de la depense',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: labelController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: 'E.g. Acheter les fournitures des enfants',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildPriceField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Prix',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: priceController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: '1800',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        ),
      ],
    );
  }

  Widget buildDateField(BuildContext context) {
// 1
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 2
        Row(
// 3
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
// 4
            Text(
              'Date',
              style: GoogleFonts.lato(fontSize: 16.0),
            ),
// 5
            TextButton(
              child: const Text('Choisir'),
// 6
              onPressed: () async {
                final currentDate = DateTime.now();
// 7
                final selectedDate = await showDatePicker(
                  context: context,
                  initialDate: currentDate,
                  firstDate: DateTime(currentDate.year - 5),
                  lastDate: DateTime(currentDate.year + 5),
                );
                setState(() {
                  if (selectedDate != null) {
                    _dueDate = selectedDate;
                  }
                });
              },
            ),
          ],
        ),
// 9
        if (_dueDate != null) Text(DateFormat('dd-MM-yyyy').format(_dueDate)),
      ],
    );
  }
}

class ExpenseTask1Dialog {
  Widget buildDialog(
      BuildContext context, ExpenseTask expenseTask, bool isNew) {
    DbHelper helper = DbHelper();
    final labelController = TextEditingController();
    final priceController = TextEditingController();
    DateTime _dueDate = DateTime.now();
    //CHECKING  if the list parameter is null or exists
    if (!isNew) {
      // we are doing an update, since the list exists.
      labelController.text = expenseTask.label;
    }
    //showing the alert
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            (isNew) ? 'Créer une liste de dépense' : 'Editer la liste',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 300.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    //buildLabelField(),
                    SizedBox(
                      height: 10,
                    ),
                    //buildPriceField(),
                    SizedBox(
                      height: 10,
                    ),
                    //buildDateField(context),
                  ],
                ),
              ),
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(primary: const Color(0xFF21cA79)),
                child: const Text('Ajouter'),
                onPressed: () {
                  //expenseTask.label = txtName.text;
                  helper.insertExpenseTask(expenseTask);

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ExpenseListUI()));
                  //Navigator.pop(context);
                },
              )
            ],
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
