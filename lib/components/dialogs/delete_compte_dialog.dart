import 'package:finanxee/models/compte.dart';
import 'package:finanxee/providers/providers.dart';
import 'package:finanxee/screens/avoirs/avoir_screen.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DeleteCompteDialog extends StatefulWidget {
  Compte compte;

  DeleteCompteDialog(this.compte);

  @override
  State<DeleteCompteDialog> createState() => _DeleteCompteDialogState();
}

class _DeleteCompteDialogState extends State<DeleteCompteDialog> {
  late DbHelper helper = DbHelper();
  late Compte compte =
      Compte(0, TypeCompte.BANK.index, "", 0.0, "", "", "", "", "");

  @override
  void initState() {
    // TODO: implement initState
    compte = widget.compte;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Supprimer le compte',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 200.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                          "Confirmer la suppression du compte. Toutes les transactions associées au comptes seront également"
                          "supprimées. Voulez-vous vraiment supprimer le compte ?"),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color(0xFF21cA79)),
                      child: const Text('Annuler'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color(0xFFFF1111)),
                      child: const Text('Supprimer'),
                      onPressed: () {
                        helper.deleteCompte(compte.id);
                        SnackBar snackBar =
                            const SnackBar(content: Text('Compte supprimé'));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        Provider.of<AppStateManager>(context, listen: false)
                            .gotToAvoirs();
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
