import 'package:finanxee/components/categorie_card.dart';
import 'package:finanxee/models/categorie_depense.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SousCategoriesDialog extends StatefulWidget {
  final List<CategorieDepense> categories;

  const SousCategoriesDialog(this.categories);

  @override
  State<SousCategoriesDialog> createState() =>
      _SousCategoriesDialogState(categories);
}

class _SousCategoriesDialogState extends State<SousCategoriesDialog> {
  List<CategorieDepense> categories;

  _SousCategoriesDialogState(this.categories);

  @override
  Widget build(BuildContext context) {
    final dataRevenu = Provider.of<DataInit>(context);

    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Choisir la catégorie',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          padding: const EdgeInsets.symmetric(vertical: 15.0),
          height: MediaQuery.of(context).size.height * 0.8,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Center(
                child: Text(
                  "Défilez vers le haut pour voir toutes les catégories.",
                  style: Theme.of(context).textTheme.caption,
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, index) {
                        return CategorieCard(categories[index]);
                      },
                      separatorBuilder: (BuildContext context, index) {
                        return const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.0),
                          child: Divider(
                            color: Color(0xFFD4D4D4),
                          ),
                        );
                      },
                      itemCount: categories.length),
                ),
              ),
            ],
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
