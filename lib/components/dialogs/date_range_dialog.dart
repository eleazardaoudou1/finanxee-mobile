import 'package:finanxee/models/depense.dart';
import 'package:finanxee/models/models.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:finanxee/screens/screens.dart';

import '../../utils/dbhelper.dart';

class DateRangeDialog extends StatefulWidget {
  final String type;
  const DateRangeDialog(this.type);

  @override
  State<DateRangeDialog> createState() => _DateRangeDialogState();
}

class _DateRangeDialogState extends State<DateRangeDialog> {
  DateTime dateD = DateTime.now();
  DateTime dateF = DateTime.now();
  DbHelper helper = DbHelper();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Choisir une période',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          height: 215,
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
// 3
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
// 4
                      Column(
                        children: [
                          Text(
                            'Début',
                            style: GoogleFonts.lato(fontSize: 16.0),
                          ),
// 5
                          TextButton(
                            child: const Text('Choisir'),
// 6
                            onPressed: () async {
                              final currentDate = DateTime.now();
// 7
                              final selectedDate = await showDatePicker(
                                context: context,
                                initialDate: currentDate,
                                firstDate: DateTime(currentDate.year - 10),
                                lastDate: DateTime(currentDate.year + 5),
                              );
                              setState(() {
                                if (selectedDate != null) {
                                  dateD = selectedDate;
                                }
                              });
                            },
                          ),
                        ],
                      )
                    ],
                  ),
// 9
                  if (dateD != null)
                    Text(DateFormat('dd-MM-yyyy').format(dateD)),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
// 3
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
// 4
                      Column(
                        children: [
                          Text(
                            'Fin',
                            style: GoogleFonts.lato(fontSize: 16.0),
                          ),
// 5
                          TextButton(
                            child: const Text('Choisir'),
// 6
                            onPressed: () async {
                              final currentDate = DateTime.now();
// 7
                              final selectedDate = await showDatePicker(
                                context: context,
                                initialDate: dateD,
                                firstDate: dateD,
                                lastDate: DateTime(currentDate.year + 5),
                              );
                              setState(() {
                                if (selectedDate != null) {
                                  dateF = selectedDate;
                                }
                              });
                            },
                          ),
                        ],
                      )
                    ],
                  ),
// 9
                  if (dateF != null)
                    Text(DateFormat('dd-MM-yyyy').format(dateF)),
                ],
              ),
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(backgroundColor: const Color(0xFF21ca79)),
                child: const Text('valider'),
                onPressed: () async {
                  if (dateD.isAfter(dateF)) {
                    SnackBar snackBar1 = const SnackBar(
                      content: Text(
                          "La date de début doit être plus ancienne que la date de fin."),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                    return;
                  }
                  /* helper.deleteCompte(compte.id);
                  SnackBar snackBar =
                  const SnackBar(content: Text('Compte supprimé'));
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  Provider.of<AppStateManager>(context, listen: false)
                      .gotToAvoirs();*/

                  /* print("Dialog Depenses dateD");
                  print(dateD);
                  print("Dialog Depenses dateF");
                  print(dateF);*/
                  //recuperer les dépenses dans l'intervalle
                  List<Depense> depenses =
                      await helper.getDepensesBetween_v2(dateD, dateF);

                  List<Revenu> revenus =
                      await helper.getRevenusBetween_v2(dateD, dateF);

                  /*  print("Dialog Depenses");
                  print(depenses.length);*/
                  if (widget.type == 'D') {
                    Navigator.pop(context, [
                      depenses,
                      dateD,
                      dateF,
                      dateF.add(const Duration(days: 1))
                    ]); //
                  } else {
                    Navigator.pop(context, [
                      revenus,
                      dateD,
                      dateF,
                      dateF.add(const Duration(days: 1))
                    ]); //
                  }

                  // depenses;
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
