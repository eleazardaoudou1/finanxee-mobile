import 'package:finanxee/models/facture.dart';
import 'package:finanxee/screens/factures/factures_list_ui.dart';
import 'package:finanxee/screens/factures/one_facture.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

class DeleteFactureDialog extends StatefulWidget {
  Facture facture;
  DeleteFactureDialog(this.facture);

  @override
  State<DeleteFactureDialog> createState() =>
      _DeleteFactureDialogState(facture);
}

class _DeleteFactureDialogState extends State<DeleteFactureDialog> {
  final Facture facture;
  late DbHelper helper = DbHelper();

  _DeleteFactureDialogState(this.facture);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Supprimer la facture',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 120.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(
                        height: 10,
                      ),
                      Text("Confirmer la suppression de la facture."),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color(0xFF21cA79)),
                      child: const Text('Annuler'),
                      onPressed: () {
                        //expenseTask.date = _dueDate.toString();
                        Navigator.pop(context);
                      },
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color(0xFFFF1111)),
                      child: const Text('Supprimer'),
                      onPressed: () {
                        helper.deleteFacture(facture.id);
                        SnackBar snackBar =
                            const SnackBar(content: Text('supprimée !'));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
