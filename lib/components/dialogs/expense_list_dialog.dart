import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/screens/expense_list_ui.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

class ExpenseListDialog {
  final txtName = TextEditingController();
  final txtPriority = TextEditingController();

  Widget buildDialog(BuildContext context, ExpenseList list, bool isNew) {
    DbHelper helper = DbHelper();

    //CHECKING  if the list parameter is null or exists
    if (!isNew) {
      // we are doing an update, since the list exists.
      txtName.text = list.name;
    }
    //showing the alert
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            (isNew) ? 'Créer une liste de dépense' : 'Editer la liste',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 230.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    TextField(
                      controller: txtName,
                      decoration: const InputDecoration(
                          hintText: 'Entrez le nom de la liste'),
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(primary: const Color(0xFF21cA79)),
                child: (isNew) ? const Text('Ajouter') : const Text('Editer'),
                onPressed: () {
                  if (txtName.text.isEmpty) {
                    SnackBar snackBar1 = const SnackBar(
                      content: Text("Vous devez donner un nom à la liste."),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                    return;
                  }

                  list.name = txtName.text;
                  helper.insertExpenseList(list);
                  Navigator.pop(context);

                  /*  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ExpenseListUI()));*/
                  //Navigator.pop(context);
                },
              )
            ],
          ),
        ),
        // SingleChildScrollView(
        //   child: Column(children: <Widget>[
        //     TextField(
        //       controller: txtName,
        //       decoration: InputDecoration(hintText: 'Entrez le nom de la liste'),
        //     ),
        //     SizedBox(
        //       height: 10,
        //     ),
        //     ElevatedButton(
        //       style: ElevatedButton.styleFrom(primary: Color(0xFF21cA79)),
        //       child: Text('Ajouter'),
        //       onPressed: () {
        //         list.name = txtName.text;
        //         helper.insertExpenseList(list);
        //         Navigator.pop(context);
        //       },
        //     )
        //   ]),
        // ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
