import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/screens/avoirs/avoir_screen.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

class LoadingDialog extends StatefulWidget {
  LoadingDialog();

  @override
  State<LoadingDialog> createState() => _LoadingDialogState();
}

class _LoadingDialogState extends State<LoadingDialog> {
  late DbHelper helper = DbHelper();
  late Actif compte =
      new Actif(0, "intitule", 0, 0, "description", "created_at", "updated_at");

  @override
  void initState() {
    // TODO: implement initState
    //compte = widget.compte;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Patientez !',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 200.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(
                        height: 10,
                      ),
                      Center(
                        child: CircularProgressIndicator(
                          color: Color(0xFF21ca79),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
