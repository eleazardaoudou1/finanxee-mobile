import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/passif.dart';
import 'package:finanxee/screens/avoirs/avoir_screen.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

class DeletePassifDialog extends StatefulWidget {
  Passif compte;

  DeletePassifDialog(this.compte);

  @override
  State<DeletePassifDialog> createState() => _DeletePassifDialog();
}

class _DeletePassifDialog extends State<DeletePassifDialog> {
  late DbHelper helper = DbHelper();
  late Passif compte = new Passif(
      0, "intitule", 1, 0, "description", "created_at", "updated_at");

  @override
  void initState() {
    // TODO: implement initState
    compte = widget.compte;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Supprimer',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 200.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(
                        height: 10,
                      ),
                      Text("Confirmer la suppression ?"),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color(0xFF21cA79)),
                      child: const Text('Annuler'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color(0xFFFF1111)),
                      child: const Text('Supprimer'),
                      onPressed: () {
                        helper.deletePassif(compte.id);
                        SnackBar snackBar =
                            const SnackBar(content: Text('supprimé !'));
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
