import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/models/expense_task.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/screens/expense_list_ui.dart';
import 'package:finanxee/screens/one_expense_list.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../models/epargne_projet.dart';

class ProjetEpargneDialog extends StatefulWidget {
  EpargneProjet epargneProjet;
  bool isNew;
  Project projet;
  ProjetEpargneDialog(this.epargneProjet, this.isNew, this.projet);

  //const ProjetEpargneDialog({Key? key}) : super(key: key);

  @override
  State<ProjetEpargneDialog> createState() =>
      _ProjetEpargneDialogState(epargneProjet, isNew, projet);
}

class _ProjetEpargneDialogState extends State<ProjetEpargneDialog> {
  //EpargneProjet expenseTask = ExpenseTask(0, 1, '', 0.0, '', 0, '');
  EpargneProjet epargneProjet = EpargneProjet(0, 0, "", 0, "", "");

  bool isNew = false;
  Project project;

  _ProjetEpargneDialogState(this.epargneProjet, this.isNew, this.project);

  final labelController = TextEditingController();
  final priceController = TextEditingController();
  DbHelper helper = DbHelper();

  @override
  Widget build(BuildContext context) {
    if (!isNew) {
      // we are doing an update, since the list exists.
      labelController.text = epargneProjet.label;
    }
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            (isNew) ? 'Ajouter une épargne' : 'Editer',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 300.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    buildLabelField(),
                    const SizedBox(
                      height: 10,
                    ),
                    buildPriceField(),
                    const SizedBox(
                      height: 10,
                    ),
                    /* buildDateField(context),*/
                  ],
                ),
              ),
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(backgroundColor: const Color(0xFF21cA79)),
                child: const Text('Ajouter'),
                onPressed: () {
                  if (labelController.text.isEmpty) {
                    SnackBar snackBar1 = const SnackBar(
                      content: Text("La description doit être définie."),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                    return;
                  }
                  if (priceController.text.isEmpty) {
                    SnackBar snackBar1 = const SnackBar(
                      content: Text("Le montant doit être défini."),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                    return;
                  }

                  epargneProjet.label = labelController.text;
                  epargneProjet.date = DateTime.now().toString();
                  epargneProjet.idProjet = widget.projet.id;
                  epargneProjet.montant = double.parse(priceController.text);
                  helper.insertEpargneProjet(epargneProjet);
                  Navigator.pop(context);
                },
              )
            ],
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Description',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: labelController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: 'Epargne du mois de Mars...',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildPriceField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          'Montant à ajouter',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: priceController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: '50 000',
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        ),
      ],
    );
  }
}

class ExpenseTask1Dialog {
  Widget buildDialog(
      BuildContext context, ExpenseTask expenseTask, bool isNew) {
    DbHelper helper = DbHelper();
    final labelController = TextEditingController();
    final priceController = TextEditingController();
    DateTime _dueDate = DateTime.now();
    //CHECKING  if the list parameter is null or exists
    if (!isNew) {
      // we are doing an update, since the list exists.
      labelController.text = expenseTask.label;
    }
    //showing the alert
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            (isNew) ? 'Créer une liste de dépense' : 'Editer la liste',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          width: 260.0,
          height: 300.0,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    //buildLabelField(),
                    SizedBox(
                      height: 10,
                    ),
                    //buildPriceField(),
                    SizedBox(
                      height: 10,
                    ),
                    //buildDateField(context),
                  ],
                ),
              ),
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(primary: const Color(0xFF21cA79)),
                child: const Text('Ajouter'),
                onPressed: () {
                  //expenseTask.label = txtName.text;
                  helper.insertExpenseTask(expenseTask);

                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ExpenseListUI()));
                  //Navigator.pop(context);
                },
              )
            ],
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
