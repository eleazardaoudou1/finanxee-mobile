import 'package:finanxee/components/gsm_card.dart';
import 'package:finanxee/models/pays_gsm.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GsmListDialog extends StatefulWidget {
  const GsmListDialog({Key? key}) : super(key: key);

  @override
  State<GsmListDialog> createState() => _GsmListDialogState();
}

class _GsmListDialogState extends State<GsmListDialog> {
  List<PaysGSM> paysGSMList = StaticDate.paysGSM;

  /* @override
  void initState() {
    // TODO: implement initState
    setPaysGsm();
    super.initState();
  }*/

  Future<List<PaysGSM>> setPaysGsm() async {
    final prefs = await SharedPreferences.getInstance();
    String selectCountryCode = StaticDate.selectedCountryCode;
    String? countryCode = prefs.getString(selectCountryCode);
    print("GSM CountryCode = ");
    print(countryCode);
    PaysGSM pays = getByCode(countryCode!);
    List<PaysGSM> paysList = [];
    paysList.add(pays);
    print("GSM Pays = ");
    print(pays.intitule);
    return paysList;
  }

  PaysGSM getByCode(String countryCode) {
    for (int i = 0; i < paysGSMList.length; i++) {
      if (paysGSMList[i].id == countryCode) {
        return paysGSMList[i];
      }
    }
    return paysGSMList[0];
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: setPaysGsm(),
        builder: (BuildContext context, AsyncSnapshot<List<PaysGSM>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );

              // if we got our data
            } else {
              var paysList1 = snapshot.data!;
              return Center(
                child: AlertDialog(
                  title: Center(
                    child: Text(
                      'Choisir votre opérateur',
                      style: Theme.of(context).textTheme.bodyLarge,
                    ),
                  ),
                  content: Container(
                    padding: const EdgeInsets.symmetric(vertical: 15.0),
                    height: MediaQuery.of(context).size.height * 0.8,
                    decoration: const BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Color(0xffffffff),
                      border: Border(
                          top: BorderSide(
                        color: Colors.black,
                        width: 1.0,
                      )),
                      //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Expanded(
                          child: SingleChildScrollView(
                            child: ListView.separated(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (BuildContext context, index) {
                                  return GSMCard(paysList1[index]);
                                },
                                separatorBuilder:
                                    (BuildContext context, index) {
                                  return const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  );
                                },
                                itemCount: paysList1.length),
                          ),
                        ),
                      ],
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                ),
              );
            }
          } else {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.green,
              ),
            );
          }
        });
  }
}
