import 'package:finanxee/models/compte.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../screens/type_compte_screen.dart';

class CompteDialog extends StatefulWidget {
  final List<Compte> comptes;
  const CompteDialog(this.comptes);

  @override
  State<CompteDialog> createState() => _CompteDialogState(this.comptes);
}

class _CompteDialogState extends State<CompteDialog> {
  final List<Compte> comptes1;
  _CompteDialogState(this.comptes1);
  @override
  Widget build(BuildContext context) {
    print(widget.comptes.length);
    final dataRevenu = Provider.of<DataInit>(context);

    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Choisir le compte',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          padding: const EdgeInsets.symmetric(vertical: 15.0),
          height: MediaQuery.of(context).size.height * 0.5,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //if (widget.comptes.isNotEmpty)
              Center(
                child: Text(
                  "Défilez vers le haut pour voir tous les comptes.",
                  style: Theme.of(context).textTheme.caption,
                ),
              ),

              ///if (widget.comptes.isNotEmpty)
              const SizedBox(
                height: 25,
              ),
              //if (widget.comptes.isNotEmpty)
              Expanded(
                child: SingleChildScrollView(
                  child: ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, index) {
                        return GestureDetector(
                          onTap: () {
                            dataRevenu
                                .updateCompteSelected(widget.comptes[index]);
                            Navigator.pop(context);
                          },
                          child: ListTile(
                            title: Text(widget.comptes[index].intitule),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, index) {
                        return const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.0),
                          child: Divider(
                            color: Color(0xFFD4D4D4),
                          ),
                        );
                      },
                      itemCount: widget.comptes.length),
                ),
              ),
              if (widget.comptes.isEmpty)
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "Vous n'avez aucun compte pour l'instant ",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 21.0),
                      ),
                      const Text(
                        'Cliquez sur le bouton \n + pour ajouter',
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(
                          height: MediaQuery.of(context).size.height * 0.1),
                      const SizedBox(height: 16.0),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const TypeCompteScreen(true)))
                              .then((value) => null);
                        },
                        child: const Icon(
                          Icons.add,
                          size: 50,
                        ),
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
