import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/data_init.dart';
import '../../utils/static_data.dart';

class QuestionSecuriteDialog extends StatefulWidget {
  final int q;
  const QuestionSecuriteDialog(this.q);

  @override
  State<QuestionSecuriteDialog> createState() => _QuestionSecuriteDialogState();
}

class _QuestionSecuriteDialogState extends State<QuestionSecuriteDialog> {
  List<String> questionsList = StaticDate.questionsSecurite;

  /* Future<List<String>> getQuestions() async {

  }*/
  @override
  Widget build(BuildContext context) {
    final providerData = Provider.of<DataInit>(context);

    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Questions',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: Container(
          padding: const EdgeInsets.symmetric(vertical: 15.0),
          height: MediaQuery.of(context).size.height * 0.8,
          decoration: const BoxDecoration(
            shape: BoxShape.rectangle,
            color: Color(0xffffffff),
            border: Border(
                top: BorderSide(
              color: Colors.black,
              width: 1.0,
            )),
            //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  child: ListView.separated(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, index) {
                        return GestureDetector(
                          onTap: () {
                            widget.q == 1
                                ? providerData
                                    .updateQuestion(questionsList[index])
                                : providerData
                                    .updateQuestion2(questionsList[index]);
                            Navigator.pop(context);
                          },
                          child: ListTile(
                            title: Text(questionsList[index]),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, index) {
                        return const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.0),
                          child: Divider(
                            color: Color(0xFFD4D4D4),
                          ),
                        );
                      },
                      itemCount: questionsList.length),
                ),
              ),
            ],
          ),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
