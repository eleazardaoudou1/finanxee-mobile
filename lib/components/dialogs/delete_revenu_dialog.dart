import 'package:finanxee/models/revenu.dart';
import 'package:finanxee/screens/transactions/transactions_screen.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

class DeleteRevenuDialog extends StatefulWidget {
  Revenu revenu;
  DeleteRevenuDialog(this.revenu);

  @override
  State<DeleteRevenuDialog> createState() => _DeleteRevenuDialogState();
}

class _DeleteRevenuDialogState extends State<DeleteRevenuDialog> {
  late DbHelper helper = DbHelper();
  Revenu revenu = Revenu(0, '', 0, 0, 0.0, '', '', DateTime.now().toString(),
      DateTime.now().toString());
  @override
  void initState() {
    // TODO: implement initState
    revenu = widget.revenu;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        title: Center(
          child: Text(
            'Supprimer le revenu',
            style: Theme.of(context).textTheme.bodyLarge,
          ),
        ),
        content: const Text("Voulez-vous vraiment supprimer ce revenu ?"),
        actions: [
          TextButton(
            child: const Text("Annuler"),
            onPressed: () {
              Navigator.pop(context, 'n');
            },
          ),
          TextButton(
              onPressed: () {
                helper.deleteRevenu(revenu.id);
                SnackBar snackBar = const SnackBar(
                  content: Text('Revenu supprimée avec succès !'),
                  backgroundColor: Colors.green,
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.pop(context, 'x');
              },
              child: const Text('Supprimer'))
        ],
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      ),
    );
  }
}
