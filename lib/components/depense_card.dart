import 'package:finanxee/components/components.dart';
import 'package:finanxee/models/categorie_depense.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DepenseCard extends StatelessWidget {
  Depense depense;
  DepenseCard(this.depense);

  var f = StaticDate();
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        /*boxShadow: [
            BoxShadow(
                color: Color(0xFFD8F1E7),
                spreadRadius: 2,
                blurRadius: 1,
                offset: Offset(0, 1))
          ]*/
      ),
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
      child: Row(
        children: [
          if (depense.categorie == 1)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.fastfood,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 2)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.emoji_transportation,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 3)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.shopping_basket,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 4)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.feed,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 5)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.wallet,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 6)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.sanitizer,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 7)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.home_work,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 8)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.airplane_ticket_outlined,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 9)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.health_and_safety_outlined,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 10)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.work_rounded,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 11)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.child_care_sharp,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 12)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.school,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          if (depense.categorie == 13)
            const CircleAvatar(
              backgroundColor: Color(0xFFF1F1F1),
              child: Icon(
                Icons.account_balance,
                color: Colors.grey,
                size: 25.0,
              ),
            ),
          //Spacer(),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  depense.intitule,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  f.numF.format(depense.montant),
                  //this.depense.montant.toString(),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  '${DateTime.parse(depense.date).day}  '
                  '${fromNumToMonth(DateTime.parse(depense.date).month)} '
                  '${(DateTime.parse(depense.date).year)} ',
                  style: Theme.of(context).textTheme.bodyMedium,
                )
                /*Text(
                  this.depense.categorie.toString(),
                  style: Theme.of(context).textTheme.bodyMedium,
                )*/
              ],
            ),
          ),
          /* Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                f.numF.format(depense.montant),
                //this.depense.montant.toString(),
                style: Theme.of(context).textTheme.bodyText1,
              ),
              Text(
                DateFormat('dd-MM-yyyy').format(DateTime.parse(depense.date)),
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            ],
          )*/
        ],
      ),
    );
  }
}

class DepenseCard1 extends StatefulWidget {
  Depense depense;
  DepenseCard1(this.depense);

  @override
  State<DepenseCard1> createState() => _DepenseCard1State(depense);
}

class _DepenseCard1State extends State<DepenseCard1> {
  Compte compte1 = Compte(0, TypeCompte.BANK.index, '', 0.0, '', 'APP',
      'assets/nsia_logo.png', '', '');
  CategorieDepense cat = CategorieDepense(0, '', '', '', '', '', []);

  DbHelper helper = DbHelper();

  @override
  void initState() {
    // TODO: implement initState
    //getDepensesSubObject();
    super.initState();
  }

  Future<Compte> getDepensesSubObject() async {
    final compte2 = await helper.getCompteById(widget.depense.id);
    print(compte2?.intitule);

    final cat2 = await helper
        .getCatById(widget.depense.categorie); //get the list of factures
    //final comptesMomo = await helper.getCompteMomo();
    setState(() {
      //facturesList = factures;
      compte1 = compte2!;
      cat = cat2!;
    });
    return compte1;
  }

  Depense depense =
      Depense(0, '', '', 0, 0, 0, 0.0, DateTime.now().toString(), '', '');
  _DepenseCard1State(this.depense);
  var f = StaticDate();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: helper.getSousCategoriesDepense(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );

              // if we got our data
            } else {
              return Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  /*boxShadow: [
            BoxShadow(
                color: Color(0xFFD8F1E7),
                spreadRadius: 2,
                blurRadius: 1,
                offset: Offset(0, 1))
          ]*/
                ),
                padding:
                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
                child: Row(
                  children: [
                    const CircleAvatar(
                      backgroundColor: Color(0xFFF1F1F1),
                      child: Icon(
                        Icons.fastfood,
                        color: Colors.green,
                        size: 20.0,
                      ),
                    ),
                    //Spacer(),
                    const SizedBox(
                      width: 20,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          depense.intitule,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        Text(
                          'cateogire',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    ),
                    const Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          f.numF.format(depense.montant),
                          //this.depense.montant.toString(),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        Text(
                          DateFormat('dd-MM-yyyy')
                              .format(DateTime.parse(depense.date)),
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    )
                  ],
                ),
              );
            }
          }

          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        });
  }
}
