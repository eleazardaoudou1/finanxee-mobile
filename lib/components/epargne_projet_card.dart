import 'package:finanxee/components/components.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../models/epargne_projet.dart';

class EpargneProjetCard extends StatefulWidget {
  final EpargneProjet epargneProjet;
  EpargneProjetCard(this.epargneProjet);

  @override
  State<EpargneProjetCard> createState() =>
      _EpargneProjetCardState(this.epargneProjet);
}

class _EpargneProjetCardState extends State<EpargneProjetCard> {
  var f = StaticDate();
  final EpargneProjet _epPro;

  _EpargneProjetCardState(this._epPro);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        //color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        /* boxShadow: [
            BoxShadow(
                color: Color(0xFFD0CECE),
                offset: Offset(1, 1),
                blurRadius: 1,
                spreadRadius: 1)
          ]*/
      ),
      padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
      child: Row(
        children: [
          const CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: Icon(
              Icons.monetization_on,
              color: Colors.green,
              size: 20.0,
            ),
          ),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        widget.epargneProjet.label,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        DateFormat('dd-MM-yyyy').format(DateTime.parse(
                          widget.epargneProjet.date,
                        )),
                        style: const TextStyle(color: Colors.black),
                      ),
                      /* Text(
                        '${DateTime.parse(widget.epargneProjet.date).day}  '
                        '${fromNumToMonth(DateTime.parse(widget.epargneProjet.date).month)} '
                        '${(DateTime.parse(widget.epargneProjet.date).year)} ',
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),*/
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      f.numF.format(widget.epargneProjet.montant).toString(),
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
              ],
            ),
          ),
          GestureDetector(
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return DeleteEpargneProjet(
                      depense: widget.epargneProjet,
                    );
                  },
                ).then((value) => {if (value == 'x') Navigator.pop(context)});
              },
              child: const Icon(Icons.delete_forever, color: Colors.red)),
        ],
      ),
    );
  }
}

class DeleteEpargneProjet extends StatelessWidget {
  const DeleteEpargneProjet({Key? key, required this.depense})
      : super(key: key);
  final EpargneProjet depense;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer cette épargne?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            depense.deleted_at = DateTime.now().toString();
            await helper.updateEpargneProjet(depense);
            Navigator.pop(context, 'x');
            //Navigator.pop(context, 'x');
          },
        ),
      ],
    );
  }
}
