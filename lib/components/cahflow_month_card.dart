import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../components/square_image.dart';
import '../../utils/static_data.dart';

class CashFlowMonthCard extends StatefulWidget {
  const CashFlowMonthCard(
      this.value, this.moisAnnee, this.depenseSolde, this.revenuSolde);
  final double value;
  final DateTime moisAnnee;
  final double depenseSolde;
  final double revenuSolde;
  @override
  State<CashFlowMonthCard> createState() => _CashFlowMonthCardState();
}

class _CashFlowMonthCardState extends State<CashFlowMonthCard> {
  var f = StaticDate();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFD0CECE),
                offset: Offset(1, 1),
                blurRadius: 1,
                spreadRadius: 1)
          ]),
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Container(
            width: 70,
            height: 70,
            decoration: const BoxDecoration(
              color: Colors.lightGreen,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            child: const Icon(
              Icons.area_chart,
              color: Colors.white,
              size: 30.0,
            ),
          ),
          const SizedBox(
            width: 30,
          ),
          Expanded(
              child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          f.monthYear(widget.moisAnnee),
                          //"${widget.moisAnnee.month}. .${widget.moisAnnee.year} ",
                          style: GoogleFonts.lato(
                              fontSize: 12.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Text(
                              "+",
                              style: GoogleFonts.lato(
                                  color: Colors.green,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              f.numF.format(widget.revenuSolde).toString(),
                              style: GoogleFonts.lato(
                                  color: Colors.green,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: [
                            Text(
                              "-",
                              style: GoogleFonts.lato(
                                  color: Colors.red,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              f.numF.format(widget.depenseSolde).toString(),
                              style: GoogleFonts.lato(
                                  color: Colors.red,
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8.0,
                      vertical: 8.0,
                    ),
                    decoration: BoxDecoration(
                        color: widget.value >= 0
                            ? const Color(0xff92fac4)
                            : const Color(0xfffa9292),
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          color: widget.value >= 0
                              ? const Color(0xFF21ca79)
                              : const Color(0xffca2121),
                          width: 1,
                        )),
                    child: Text(
                      widget.value >= 0
                          ? "+${f.numF.format(widget.value).toString()}"
                          : f.numF.format(widget.value).toString(),
                      style: GoogleFonts.lato(
                          color: widget.value >= 0
                              ? const Color(0xFF21ca79)
                              : const Color(0xffca2121),
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ],
          )),
        ],
      ),
    );
  }
}
