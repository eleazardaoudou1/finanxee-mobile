import 'package:finanxee/components/budget_card.dart';
import 'package:finanxee/components/expense_list_card.dart';
import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/screens/expense_list_ui.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';

import '../screens/one_expense_list.dart';
import 'list_depense_card.dart';

class LastExpenseList extends StatefulWidget {
  const LastExpenseList({Key? key}) : super(key: key);

  @override
  State<LastExpenseList> createState() => _LastExpenseListState();
}

class _LastExpenseListState extends State<LastExpenseList> {
  DbHelper helper = DbHelper();
  List<ExpenseList> lastList = [];

  Future<int> getList() async {
    var list = await helper.getLastExpenseLists();
    setState(() {
      lastList = list;
    });
    return 1;
  }

  @override
  void initState() {
    // TODO: implement initState
    getList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: helper.getLastExpenseLists(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                '${snapshot.error} occurred',
                style: const TextStyle(fontSize: 18),
              ),
            );
          } else if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xFFD8F1E7),
                          offset: Offset(1, 1),
                          blurRadius: 1,
                          spreadRadius: 1)
                    ]),
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Créez des listes pour noter vos dépenses à l'avance et ne les oubliez plus.",
                      style: Theme.of(context).textTheme.bodyLarge,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        /* Text(
                          "Listes de dépenses",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                        ),*/
                        Text(
                          "Récemment ajoutées",
                          style: Theme.of(context).textTheme.labelLarge,
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, index) {
                          //Account account = StaticDate.userAccounts[index];
                          return ExpenseListCard(lastList[index]);
                        },
                        separatorBuilder: (BuildContext context, index) {
                          return const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            child: Divider(
                              color: Color(0xFFD4D4D4),
                            ),
                          );
                        },
                        itemCount: lastList.length),
                    const SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const ExpenseListUI()))
                            .then((value) => getList());
                      },
                      child: Wrap(children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                            //horizontal: 16.0,
                            vertical: 8.0,
                          ),
                          decoration: BoxDecoration(
                              //color: Color(0xFF21ca79),
                              borderRadius: BorderRadius.circular(20.0),
                              border: Border.all(
                                color: const Color(0xFF21ca79),
                                width: 1,
                              )),
                          child: Center(
                            child: Text(
                              "Tout voir",
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        ),
                      ]),
                    )
                  ],
                ),
              ),
            );
          }
        }
        return const Center(
          child: CircularProgressIndicator(
            color: Colors.green,
          ),
        );
      },
    );
  }
}

class LastExpenseList1 extends StatelessWidget {
  const LastExpenseList1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFD8F1E7),
                  offset: Offset(1, 1),
                  blurRadius: 1,
                  spreadRadius: 1)
            ]),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Listes de dépenses",
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                ),
                Text(
                  "Récemment ajoutées",
                  style: Theme.of(context).textTheme.labelLarge,
                )
              ],
            ),
            const SizedBox(
              height: 10.0,
            ),
            ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, index) {
                  //Account account = StaticDate.userAccounts[index];
                  return const BudgetCard();
                },
                separatorBuilder: (BuildContext context, index) {
                  return const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5.0),
                    child: Divider(
                      color: Color(0xFFD4D4D4),
                    ),
                  );
                },
                itemCount: 5),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ExpenseListUI()));
              },
              child: Wrap(children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                    //horizontal: 16.0,
                    vertical: 8.0,
                  ),
                  decoration: BoxDecoration(
                      //color: Color(0xFF21ca79),
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                        color: const Color(0xFF21ca79),
                        width: 1,
                      )),
                  child: Center(
                    child: Text(
                      "Tout voir",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ),
              ]),
            )
          ],
        ),
      ),
    );
  }
}
