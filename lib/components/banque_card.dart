import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/pays_banque.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BanqueCard extends StatelessWidget {
  final PaysBanque paysBanque;
  const BanqueCard(this.paysBanque);

  @override
  Widget build(BuildContext context) {
    final namesData = Provider.of<DataInit>(context);

    ThemeData themeData = Theme.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          height: 50,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Colors.grey[200]),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
            child: Text(
              paysBanque.intitule,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
        ),
        const SizedBox(
          height: 5.0,
        ),
        ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, index) {
              Compte compte = paysBanque.comptes[index];
              return GestureDetector(
                onTap: () {
                  namesData.updateCompteSelected(compte);
                  Navigator.pop(context);
                },
                child: ListTile(
                  title: Text(compte.intitule),
                ),
              );
            },
            separatorBuilder: (BuildContext context, index) {
              return const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Divider(
                  color: Color(0xFFD4D4D4),
                ),
              );
            },
            itemCount: paysBanque.comptes.length),
      ],
    );
  }
}
