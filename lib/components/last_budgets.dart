import 'package:finanxee/components/budget_card.dart';
import 'package:flutter/material.dart';

class LastBudgets extends StatelessWidget {
  const LastBudgets({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFD8F1E7),
                  offset: Offset(1, 1),
                  blurRadius: 1,
                  spreadRadius: 1)
            ]),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Budgets",
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                ),
                Text(
                  "En cours",
                  style: Theme.of(context).textTheme.labelLarge,
                )
              ],
            ),
            const SizedBox(
              height: 10.0,
            ),
            ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, index) {
                  //Account account = StaticDate.userAccounts[index];
                  return const BudgetCard();
                },
                separatorBuilder: (BuildContext context, index) {
                  return const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 5.0),
                    child: Divider(
                      color: Color(0xFFD4D4D4),
                    ),
                  );
                },
                itemCount: 5),
            const SizedBox(
              height: 10,
            ),
            GestureDetector(
              onTap: () {
                SnackBar snackBar = const SnackBar(
                    content: Text('You want to see all your ongoing budgets '));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
              child: Wrap(children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                    //horizontal: 16.0,
                    vertical: 8.0,
                  ),
                  decoration: BoxDecoration(
                      //color: Color(0xFF21ca79),
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                        color: const Color(0xFF21ca79),
                        width: 1,
                      )),
                  child: Center(
                    child: Text(
                      "Tout voir",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ),
              ]),
            )
          ],
        ),
      ),
    );
  }
}
