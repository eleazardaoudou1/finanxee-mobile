import 'package:finanxee/components/circle_image.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/screens/avoirs/details_compte_bancaire_screen.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_icons/flutter_icons.dart';

class AccountCard extends StatelessWidget {
  final Compte account;

  const AccountCard(this.account);

  @override
  Widget build(BuildContext context) {
    String toDisplay = '';
    if (account.type == TypeCompte.BANK.index) {
      toDisplay = 'Compte Bancaire';
    } else if (account.type == TypeCompte.MOMO.index) {
      toDisplay = 'Compte Mobile Money';
    } else {
      toDisplay = 'Autre compte';
    }

    ThemeData themeData = Theme.of(context);
    var f = StaticDate();

    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFD0CECE),
                offset: Offset(1, 1),
                blurRadius: 1,
                spreadRadius: 1)
          ]),
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          CircleImage(
            imageProvider: AssetImage(account.imageProvider),
            imageRadius: 28,
          ),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        account.intitule,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        toDisplay,
                        style: const TextStyle(color: Colors.black),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      f.numF.format(account.solde).toString(),
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ],
                ),
              ],
            ),
          ),
          /*  Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                f.numF.format(account.solde).toString(),
                style: Theme.of(context).textTheme.bodyText2,
              ),
              const SizedBox(
                height: 5.0,
              ),
            ],
          )*/
        ],
      ),
    );
  }
}
