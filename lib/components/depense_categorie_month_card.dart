import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../components/square_image.dart';
import '../../utils/static_data.dart';

class DepenseCategorieMonthCard extends StatefulWidget {
  const DepenseCategorieMonthCard(
      this.value, this.categorie /*, this.actifSolde, this.passifSolde*/);
  final double value;
  final String categorie;
/*  final double actifSolde;
  final double passifSolde;*/
  @override
  State<DepenseCategorieMonthCard> createState() =>
      _DepenseCategorieMonthCardState();
}

class _DepenseCategorieMonthCardState extends State<DepenseCategorieMonthCard> {
  var f = StaticDate();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFD0CECE),
                offset: Offset(1, 1),
                blurRadius: 1,
                spreadRadius: 1)
          ]),
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Container(
            width: 70,
            height: 70,
            decoration: const BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            child: const Icon(
              Icons.pie_chart,
              color: Colors.white,
              size: 30.0,
            ),
          ),
          const SizedBox(
            width: 30,
          ),
          Expanded(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      widget.categorie,
                      //"${widget.moisAnnee.month}. .${widget.moisAnnee.year} ",
                      style: GoogleFonts.lato(
                          fontSize: 15.0, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 8.0,
                      vertical: 8.0,
                    ),
                    decoration: BoxDecoration(
                        color: widget.value >= 0
                            ? const Color(0xff92fac4)
                            : const Color(0xfffa9292),
                        borderRadius: BorderRadius.circular(20.0),
                        border: Border.all(
                          color: widget.value >= 0
                              ? const Color(0xFF21ca79)
                              : const Color(0xffca2121),
                          width: 1,
                        )),
                    child: Text(
                      f.numF.format(widget.value).toString(),
                      style: GoogleFonts.lato(
                          //color: const Color(0xFF21ca79),
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ],
          )),
        ],
      ),
    );
  }
}
