import 'package:flutter/material.dart';

class SquareImage extends StatelessWidget {
  const SquareImage({
    Key? key,
    required this.icon,
    this.imageRadius = 5,
    this.backgroundColor,
  }) : super(key: key);

  final double imageRadius;
  final Icon icon;
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: backgroundColor,
      radius: imageRadius,
      child: CircleAvatar(
        radius: imageRadius - 2,
        child: icon,
      ),
    );
  }
}
