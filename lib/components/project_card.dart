import 'package:finanxee/components/expense_list_card.dart';
import 'package:finanxee/models/models.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import '../utils/utils.dart';

class ProjectCard extends StatefulWidget {
  final Project project;
  ProjectCard(this.project);

  @override
  State<ProjectCard> createState() => _ProjectCardState(this.project);
}

class _ProjectCardState extends State<ProjectCard> {
  late Project _project = Project(0, "intitule", 0.0, 0, 0.0, "description",
      "delai", 0, DateTime.now().toString(), DateTime.now().toString(), 0);
  _ProjectCardState(this._project);
  var f = StaticDate();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        /*boxShadow: [
            BoxShadow(
                color: Color(0xFFD8F1E7),
                spreadRadius: 2,
                blurRadius: 1,
                offset: Offset(0, 1))
          ]*/
      ),
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
      child: Row(
        children: [
          const CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: Icon(
              Icons.business_sharp,
              color: Colors.green,
              size: 15.0,
            ),
          ),
          //Spacer(),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  _project.intitule,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  '${f.numF.format(_project.montant_epargne_mensuel)} / mois',
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  //'${DateTime.parse(_project.delai).day}  '
                  '${fromNumToMonth(DateTime.parse(_project.delai).month)} '
                  '${(DateTime.parse(_project.delai).year)} ',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                const SizedBox(
                  height: 10.0,
                ),

                /*Text(
                  this.depense.categorie.toString(),
                  style: Theme.of(context).textTheme.bodyMedium,
                )*/
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                f.numF.format(_project.montant),
                //this.depense.montant.toString(),
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ],
          )
        ],
      ),
    );
  }
}
