import 'package:finanxee/models/compte.dart';
import 'package:finanxee/screens/add_compte_bancaire.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:provider/provider.dart';

import '../providers/data_init.dart';

class TypeCompteBancaire extends StatefulWidget {
  final bool fromForm;
  const TypeCompteBancaire(this.fromForm);

  @override
  State<TypeCompteBancaire> createState() => _TypeCompteBancaireState();
}

class _TypeCompteBancaireState extends State<TypeCompteBancaire> {
  _TypeCompteBancaireState();

  Compte compte = Compte(0, TypeCompte.BANK.index, 'Choisir', 0.0, '', 'APP',
      'assets/nsia_logo.png', '', '');

  @override
  Widget build(BuildContext context) {
    final dataProvider = Provider.of<DataInit>(context);

    ThemeData themeData = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
      child: Container(
        height: 0.3 * MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
            color: Colors.white, //Color(0xFF00BB),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFF3F3F3),
                  offset: Offset(1, 1),
                  blurRadius: 5,
                  spreadRadius: 5)
            ]),
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              FlutterIcons.piggy_bank_faw5s,
              color: Color(0xFF21ca79),
            ),
            const SizedBox(
              height: 18.0,
            ),
            Text('Compte Bancaire',
                style: Theme.of(context).textTheme.headline2),
            const SizedBox(
              height: 5.0,
            ),
            const Text(
              'Ajoutez un compte bancaire en choisissant dans la liste des banques disponibles.',
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 7.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddCompteBancaire(
                                compte, true, widget.fromForm)))
                    .then((value) => dataProvider.resetCompteSelected());
                //resetCompteSelected
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  //horizontal: 16.0,
                  vertical: 8.0,
                ),
                decoration: BoxDecoration(
                    //color: Color(0xFF21ca79),
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(
                      color: const Color(0xFF21ca79),
                      width: 1,
                    )),
                child: Center(
                  child: Text(
                    "Ajouter",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
