import 'package:finanxee/models/facture.dart';
import 'package:finanxee/screens/factures/one_facture.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FactureCard extends StatelessWidget {
  const FactureCard(this.facture);
  final Facture facture;

  @override
  Widget build(BuildContext context) {
    var f = StaticDate();
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        /*boxShadow: [
            BoxShadow(
                color: Color(0xFFD8F1E7),
                spreadRadius: 2,
                blurRadius: 1,
                offset: Offset(0, 1))
          ]*/
      ),
      padding: const EdgeInsets.symmetric(vertical: 5.0, horizontal: 0.0),
      child: Row(
        children: [
          CircleAvatar(
            backgroundColor: Color(0xFFF1F1F1),
            child: Icon(
              Icons.pending_actions,
              color: facture.completed == 0 ? Colors.red : Colors.green,
              size: 15.0,
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  facture.intitule,
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  getDelaiFacture(facture),
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  f.numF.format(facture.montant),
                  //facture.montant.toString(),
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (facture.archived != 0)
                const Icon(
                  Icons.archive_rounded,
                  color: Color.fromRGBO(43, 80, 108, 1),
                  size: 20.0,
                )
              else if (facture.completed != 0)
                Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 20.0,
                )
              else //(facture.completed == 0)
                Icon(
                  Icons.pending,
                  color: Colors.red,
                  size: 20.0,
                ),
            ],
          )
        ],
      ),
    );
  }
}

String getDelaiFacture(Facture fac) {
  var jours = DateTime.parse(fac.echeance)
      .difference(DateTime(
          DateTime.now().year, DateTime.now().month, DateTime.now().day))
      .inDays;
  if (jours == 0)
    return "A payer Aujourd'hui";
  else if (jours < 0) {
    jours = -1 * jours;
    return "Echéance passée il y a ${jours.toString()} jour(s)";
  }
  return "Echéance dans ${jours.toString()} jour(s)";
}
