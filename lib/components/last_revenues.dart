import 'package:finanxee/components/revenue_card.dart';
import 'package:finanxee/models/revenu.dart';
import 'package:finanxee/services/revenu_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../screens/dashboard/details_revenus_mois.dart';

class LastRevenues extends StatefulWidget {
  const LastRevenues();

  @override
  State<LastRevenues> createState() => _LastRevenuesState();
}

class _LastRevenuesState extends State<LastRevenues> {
  RevenuService revenuService = RevenuService();
  DbHelper helper = DbHelper();
  var f = StaticDate();
  double data = 0;

  List<Revenu> revenusList = [];
  _LastRevenuesState();

  Future<double> getLastRevenus() async {
    var total_rev_mnth = await revenuService.total_revenu_current_month();
    setState(() {
      data = total_rev_mnth;
    });
    return total_rev_mnth;
  }

  @override
  void initState() {
    // TODO: implement initState
    getLastRevenus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: helper.getLastRevenus(),
      builder: (BuildContext context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                        color: Color(0xFFD8F1E7),
                        offset: Offset(1, 1),
                        blurRadius: 1,
                        spreadRadius: 1)
                  ]),
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Revenus",
                        style: Theme.of(context)
                            .textTheme
                            .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                      ),
                      Text(
                        "Ce mois",
                        style: Theme.of(context).textTheme.labelLarge,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        f.numF.format(data),
                        style: GoogleFonts.lato(fontSize: 25.0),
                      ),
                      Container(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const DetailsRevenusMois()));
                          },
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 16.0,
                              vertical: 8.0,
                            ),
                            decoration: BoxDecoration(
                              color: const Color(0xFF21ca79),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            child: const Center(
                              child: Text(
                                "Détails",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
