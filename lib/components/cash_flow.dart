import 'package:finanxee/services/cash_flow_service.dart';
import 'package:finanxee/services/compe_service.dart';
import 'package:finanxee/services/depense_service.dart';
import 'package:finanxee/services/revenu_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
//'package:syncfusion_flutter_charts/charts.dart';

import '../screens/transactions/evolution_cash_flow.dart';
import 'chart_cash_flow.dart';

class CashFlow extends StatefulWidget {
  const CashFlow({Key? key}) : super(key: key);

  @override
  State<CashFlow> createState() => _CashFlowState();
}

class _CashFlowState extends State<CashFlow> {
  late List<_ChartData> data = [];
  late TooltipBehavior _tooltip;
  DbHelper helper = DbHelper();
  RevenuService revenuService = RevenuService();
  CompteService compteService = CompteService();
  DepenseService depenseService = DepenseService();
  CashFlowService cashFlowService = CashFlowService();

  double totalDepenses = 10.0;
  double totalRevenus = 15.0;
  double max = 0.0;

  Future<double> setData() async {
    totalRevenus = await revenuService.total_revenu_current_month();
    totalDepenses = await depenseService.total_depense_current_month();
    max = maxi(totalDepenses, totalRevenus);

    data = [
      _ChartData('CashFlow', totalDepenses, totalRevenus),
      //_ChartData('Revenus', 15, 19),
    ];
    _tooltip = TooltipBehavior(enable: true);

    if (max == 0) {
      max = 1.0;
    }
    return max;
  }

  @override
  void initState() {
    _tooltip = TooltipBehavior(enable: true);
    setData();
    super.initState();
  }

  double maxi(double a1, double a2) {
    if (a1 > a2) return a1;
    return a2;
  }

  String currentMonthYear() {
    DateTime today = DateTime.now();
    var month = '';
    switch (today.month) {
      case 1:
        month = 'Janvier';
        break;
      case 2:
        month = 'Fevrier';
        break;
      case 3:
        month = 'Mars';
        break;
      case 4:
        month = 'Avril';
        break;
      case 5:
        month = 'Mai';
        break;
      case 6:
        month = 'Juin';
        break;
      case 7:
        month = 'Juillet';
        break;
      case 8:
        month = 'Août';
        break;
      case 9:
        month = 'Septembre';
        break;
      case 10:
        month = 'Octobre';
        break;
      case 11:
        month = 'Novembre';
        break;
      case 12:
        month = 'Décembre';
        break;
    }

    return month + ' ' + today.year.toString();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: setData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );

              // if we got our data
            } else if (snapshot.hasData) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 25.0),
                child: Column(
                  children: [
                    Text(currentMonthYear()),
                    const SizedBox(
                      height: 10,
                    ),
                    SfCartesianChart(
                        primaryXAxis: CategoryAxis(),
                        primaryYAxis: NumericAxis(
                            minimum: 0, maximum: max, interval: max / 10),
                        tooltipBehavior: _tooltip,
                        series: <ChartSeries<_ChartData, String>>[
                          ColumnSeries<_ChartData, String>(
                              dataSource: data,
                              xValueMapper: (_ChartData data, _) => data.x,
                              yValueMapper: (_ChartData data, _) => data.y,
                              name: 'Dépenses',
                              color: const Color.fromRGBO(43, 80, 108, 1.0)),
                          ColumnSeries<_ChartData, String>(
                              dataSource: data,
                              xValueMapper: (_ChartData data, _) => data.x,
                              yValueMapper: (_ChartData data, _) => data.z,
                              name: 'Revenus',
                              color: const Color.fromRGBO(0, 151, 111, 1.0))
                        ]),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const EvolutionCashFlow()));
                            },
                            child: const Text("+ Voir l'évolution")),
                      ],
                    ),
                  ],
                ),
              );
            }
          }

          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        });

    /*
        child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25.0),
        child: Container(
          child: Column(
            children: [
              Text('Jan 2022'),
              SizedBox(
                height: 10,
              ),
              SfCartesianChart(
                  primaryXAxis: CategoryAxis(),
                  primaryYAxis:
                      NumericAxis(minimum: 0, maximum: 40, interval: 10),
                  tooltipBehavior: _tooltip,
                  series: <ChartSeries<_ChartData, String>>[
                    ColumnSeries<_ChartData, String>(
                        dataSource: data,
                        xValueMapper: (_ChartData data, _) => data.x,
                        yValueMapper: (_ChartData data, _) => data.y,
                        name: 'Dépenses',
                        color: const Color.fromRGBO(255, 8, 115, 1.0)),
                    ColumnSeries<_ChartData, String>(
                        dataSource: data,
                        xValueMapper: (_ChartData data, _) => data.x,
                        yValueMapper: (_ChartData data, _) => data.z,
                        name: 'Revenus',
                        color: const Color.fromRGBO(0, 151, 111, 1.0))
                  ]),
            ],
          ),
        ),
    ),
      );*/
  }
}

class _ChartData {
  _ChartData(this.x, this.y, this.z);

  final String x;
  final double y;
  final double z;
}

class CashFlow0 extends StatelessWidget {
  const CashFlow0({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFD8F1E7),
                  offset: Offset(1, 1),
                  blurRadius: 1,
                  spreadRadius: 1)
            ]),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  "Cash Flow",
                  style: Theme.of(context)
                      .textTheme
                      .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                ),
              ],
            ),
            const ChartCashFlow(),
          ],
        ),
      ),
    );
  }
}
