import 'package:finanxee/components/circle_image.dart';
import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/passif.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';

class PassifCard extends StatelessWidget {
  final Passif actif;

  const PassifCard(this.actif);

  @override
  Widget build(BuildContext context) {
    String toDisplay = '';
    if (actif.typePassif == 1) {
      toDisplay = 'Hypothèques';
    } else if (actif.typePassif == 2) {
      toDisplay = "Emprunts et dettes";
    } else {
      toDisplay = actif.typePassif.toString(); //'Autre passif';
    }

    ThemeData themeData = Theme.of(context);
    var f = StaticDate();

    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFD0CECE),
                offset: Offset(1, 1),
                blurRadius: 1,
                spreadRadius: 1)
          ]),
      //color: Color.fromRGBO(250, 250, 250, 1),
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          const CircleImage(
            imageProvider: AssetImage('assets/passif.jpg'),
            imageRadius: 20,
          ),
          const SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: Text(
                        actif.intitule,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      f.numF.format(actif.valeur).toString(),
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
                  ],
                ),
                /* Text(
                  actif.typePassif.toString(),
                  style: const TextStyle(color: Colors.black),
                )*/
              ],
            ),
          ),
          /* Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                f.numF.format(actif.valeur).toString(),
                style: Theme.of(context).textTheme.bodyText2,
              ),
              const SizedBox(
                height: 5.0,
              ),
            ],
          )*/
        ],
      ),
    );
  }
}
