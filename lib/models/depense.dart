import 'package:finanxee/models/compte.dart';
import 'package:finanxee/utils/dbhelper.dart';

enum TabDay { today, month }

class Depense {
  DbHelper helper = DbHelper();

  int id;
  String intitule;
  int compte;
  int categorie;
  int sous_categorie;
  double montant;
  String description;
  String date;
  String created_at;
  String updated_at;

  Depense(
      this.id,
      this.intitule,
      this.description,
      this.compte,
      this.categorie,
      this.sous_categorie,
      this.montant,
      this.date,
      this.created_at,
      this.updated_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'description': description,
      'compte_id': compte,
      'categorie_id': categorie,
      'sous_categorie_id': sous_categorie,
      'montant': montant,
      'date': date,
      'created_at': created_at,
      'updated_at': updated_at,
    };
  }

  //get the compte appropriate
  Future<Compte?> compte_(int compteId) async {
    await helper.openDb();
    return await helper.getCompteById(compteId);
  }

  //get the compte appropriate
  Future<Compte?> categorie_(int cId) async {
    await helper.openDb();
    return await helper.getCompteById(cId);
  }
}
