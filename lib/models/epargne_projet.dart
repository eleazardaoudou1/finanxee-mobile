class EpargneProjet {
  int id;
  int idProjet;
  String label;
  double montant;
  String date;
  String deleted_at;

  EpargneProjet(this.id, this.idProjet, this.label, this.montant, this.date,
      this.deleted_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'idProjet': idProjet,
      'label': label,
      'montant': montant,
      'date': date,
      'deleted_at': (deleted_at == '') ? null : deleted_at,
    };
  }
}
