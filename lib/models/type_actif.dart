class TypeActif {
  int id;
  String intitule;
  String code;
  String description;
  List<String> actifs_elements;

  TypeActif(this.id, this.intitule, this.code, this.description,
      this.actifs_elements);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'code': code,
      'description': description,
    };
  }
}
