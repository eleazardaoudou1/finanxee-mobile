//enum TypeActif { IMMOBILIER, BIENS_PERSONNELS, PLACEMENTS, LIQUIDITES, AUTRE }

import 'package:finanxee/models/type_actif.dart';

class Actif {
  int id;
  String intitule;
  int typeActif;
  double valeur;
  String description;
  String created_at;
  String updated_at;

  Actif(this.id, this.intitule, this.typeActif, this.valeur, this.description,
      this.created_at, this.updated_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'type_actif': typeActif,
      'valeur': valeur,
      'description': description,
      'created_at': created_at,
      'updated_at': updated_at,
    };
  }
}
