class Facture {
  int id;
  String intitule;
  double montant;
  String echeance;
  int completed;
  int archived;
  String created_at;
  String deleted_at;

  Facture(this.id, this.intitule, this.montant, this.echeance, this.completed,
      this.archived, this.created_at, this.deleted_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'montant': montant,
      'echeance': echeance,
      'completed': completed,
      'archived': archived,
      'created_at': created_at,
      'deleted_at': (deleted_at == '') ? null : deleted_at,
    };
  }
}
