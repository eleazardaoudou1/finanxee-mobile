class Project {
  int id;
  String intitule;
  double montant;
  double montant_epargne_mensuel;
  double solde_actuel;
  String description;
  String delai;
  int completed;
  String created_at;
  String updated_at;
  int archived;

  Project(
    this.id,
    this.intitule,
    this.montant,
    this.montant_epargne_mensuel,
    this.solde_actuel,
    this.description,
    this.delai,
    this.completed,
    this.created_at,
    this.updated_at,
    this.archived,
  );

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'montant': montant,
      'montant_epargne_mensuel': montant_epargne_mensuel,
      'solde_actuel': solde_actuel,
      'description': description,
      'delai': delai,
      'completed': completed,
      'created_at': created_at,
      'updated_at': updated_at,
      'archived': archived,
    };
  }
}
