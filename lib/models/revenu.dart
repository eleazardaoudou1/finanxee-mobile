
class Revenu {
  int id;
  String intitule;
  int compte;
  int type_revenu;
  double montant;
  String description;
  String date;
  String created_at;
  String updated_at;

  Revenu(this.id, this.intitule, this.compte, this.type_revenu, this.montant,
      this.description, this.date, this.created_at, this.updated_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'compte_id': compte,
      'type_revenu_id': type_revenu,
      'montant': montant,
      'description': description,
      'date': date,
      'created_at': created_at,
      'updated_at': updated_at,
    };
  }
}
