
import 'package:finanxee/models/sous_categorie_depense.dart';

class CategorieDepense {
  int id;
  String intitule;
  String author;
  String icon;
  String code;
  String created_at;
  //String updated_at;
  List<SousCategorieDepense> sous_categories = [];

  CategorieDepense(this.id, this.intitule, this.author, this.icon, this.code,
      this.created_at, this.sous_categories);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'author': author,
      'icon': icon,
      'code': code,
      'created_at': created_at,
      'sous_categories': sous_categories
    };
  }

  static CategorieDepense fromMap(Map map) {
    CategorieDepense blog = CategorieDepense(0, '', '', '', '', '', []);
    blog.id = map['id'];
    blog.intitule = map['intitule'];
    blog.author = map['author'];
    blog.icon = map['icon'];
    blog.code = map['code'];
    blog.created_at = map['created_at'];
    return blog;
  }
}
