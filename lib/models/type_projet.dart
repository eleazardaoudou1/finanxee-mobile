class TypeProjet {
  int id;
  String intitule;
  String code;
  String author;
  String icon;
  String description;
  String created_at;
  String updated_at;

  TypeProjet(this.id, this.intitule, this.code, this.author, this.icon,
      this.description, this.created_at, this.updated_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'code': code,
      'author': author,
      'icon': icon,
      'description': description,
      'created_at': created_at,
      'updated_at': updated_at,
    };
  }
}
