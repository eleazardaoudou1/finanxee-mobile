class Pays {
  int id;
  String intitule;
  String code;
  String image;

  Pays(this.id, this.intitule, this.code, this.image);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'code': code,
      'image': image,
    };
  }
}
