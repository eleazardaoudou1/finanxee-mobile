enum TypeCompte { BANK, MOMO, WALLET, USER_CUSTOM }

class Compte {
  int id;
  int type;
  String intitule;
  double solde;
  String code;
  String author;
  String imageProvider;
  String created_at;
  //String updated_at;
  String deleted_at;

  Compte(this.id, this.type, this.intitule, this.solde, this.code, this.author,
      this.imageProvider, this.created_at, this.deleted_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'type': type,
      'intitule': intitule,
      'solde': solde,
      'code': code,
      'author': author,
      'imageProvider': imageProvider,
      'created_at': created_at,
      //'updated_at': updated_at,
      'deleted_at': (deleted_at == '') ? null : deleted_at,
    };
  }

  static Compte fromMap(Map map) {
    Compte blog = Compte(0, TypeCompte.BANK.index, '', 0.0, '', 'APP',
        'assets/nsia_logo.png', '', '');

    blog.id = map['id'];
    blog.type = map['type'];
    blog.intitule = map['intitule'];
    blog.solde = map['solde'];
    blog.code = map['code'];
    blog.author = map['author'];
    blog.imageProvider = map['imageProvider'];
    blog.created_at = map['created_at'];
    blog.deleted_at = map['deleted_at'];
    return blog;
  }
}
