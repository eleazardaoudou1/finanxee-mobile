class ExpenseList {
  int id;
  String name;
  int archived;
  String created_at;
  String deleted_at;

  ExpenseList(
      this.id, this.name, this.archived, this.created_at, this.deleted_at);

  //creating a map method : this allows us to get the object as a mapping; we can use it to insert
  //data in the table
  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'name': name,
      'archived': archived,
      'created_at': created_at,
      'deleted_at': (deleted_at == '') ? null : deleted_at,
    };
  }
}
