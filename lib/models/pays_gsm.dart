
import 'package:finanxee/models/compte.dart';

//enum TypeCompte { BANK, MOMO, WALLET, USER_CUSTOM }

class PaysGSM {
  String id;
  String intitule;
  List<Compte> comptes;

  PaysGSM(this.id, this.intitule, this.comptes);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'solde': comptes,
    };
  }
}
