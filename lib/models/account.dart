
import 'package:flutter/cupertino.dart';

enum AccountType { BANK, MOMO, WALLET, USER_CUSTOM }

class Account {
  final String name;
  final AccountType type;
  final double balance;
  final ImageProvider imageProvider;

  Account({
    required this.name,
    required this.type,
    required this.balance,
    required this.imageProvider,
  });
}
