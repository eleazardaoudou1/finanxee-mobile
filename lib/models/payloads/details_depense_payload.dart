import '../depense.dart';
import '../models.dart';

class DetailsDepensePayload {
  Compte compte;
  CategorieDepense categorie;
  String description;
  Depense depense;
  DetailsDepensePayload(
      this.compte, this.categorie, this.depense, this.description);
}

class DetailsRevenuPayload {
  Compte compte;
  TypeRevenu typeRevenu;
  Revenu revenu;
  DetailsRevenuPayload(this.compte, this.typeRevenu, this.revenu);
}
