class StatsAvoirNet {
  double total_actif = 0;
  double cash_comptes_bancaires = 0;
  double cash_comptes_momo = 0;
  double cash_comptes_autres = 0;
  double cash_immo = 0;
  double cash_biens = 0;
  double cash_placements = 0;
  double cash_autres_liquid = 0;
  double cash_autres_actifs = 0;

  //passif
  double total_passif = 0;
  double cash_hypotheque = 0;
  double cash_dettes = 0;

  double avoir_net = 0;

  StatsAvoirNet(
    this.total_actif,
    this.cash_comptes_bancaires,
    this.cash_comptes_momo,
    this.cash_comptes_autres,
    this.cash_immo,
    this.cash_biens,
    this.cash_placements,
    this.cash_autres_liquid,
    this.cash_autres_actifs,
    this.total_passif,
    this.cash_hypotheque,
    this.cash_dettes,
    this.avoir_net,
  );
}
