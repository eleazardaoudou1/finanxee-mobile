import '../depense.dart';
import '../models.dart';

class DepenseFormPayload {
  List<Compte> comptes = [];
  List<CategorieDepense> categories;
  DepenseFormPayload(this.comptes, this.categories);
}
