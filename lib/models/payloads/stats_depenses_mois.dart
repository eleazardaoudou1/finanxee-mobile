import '../categorie_depense.dart';

class StatsDepensesMois {
  List<CategorieDepense> categories = [];
  List<double> sumDepenses = [];
  String intituleMois = '';

  StatsDepensesMois(this.categories, this.sumDepenses, this.intituleMois);
}
