import 'package:finanxee/models/models.dart';

class StatsRevenusMois {
  List<TypeRevenu> types = [];
  List<double> sumRevenu = [];

  StatsRevenusMois(this.types, this.sumRevenu);
}
