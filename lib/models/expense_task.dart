class ExpenseTask {
  int id;
  int idExpenseList;
  String label;
  double price;
  String date;
  int completed;
  String deleted_at;

  ExpenseTask(this.id, this.idExpenseList, this.label, this.price, this.date,
      this.completed, this.deleted_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'idExpenseList': idExpenseList,
      'label': label,
      'price': price,
      'date': date,
      'completed': completed,
      'deleted_at': (deleted_at == '') ? null : deleted_at,
    };
  }
}
