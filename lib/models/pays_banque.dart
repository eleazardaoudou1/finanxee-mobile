
import 'package:finanxee/models/compte.dart';

//enum TypeCompte { BANK, MOMO, WALLET, USER_CUSTOM }

class PaysBanque {
  String id;
  String intitule;
  List<Compte> comptes;

  PaysBanque(this.id, this.intitule, this.comptes);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'solde': comptes,
    };
  }
}
