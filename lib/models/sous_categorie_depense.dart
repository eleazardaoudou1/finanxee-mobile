import 'package:finanxee/models/categorie_depense.dart';
import 'package:finanxee/utils/dbhelper.dart';

class SousCategorieDepense {
  DbHelper helper = DbHelper();

  int id;
  int categorie_depense_id;
  String intitule;
  String author;
  String icon;
  String code;
  String created_at;
  String updated_at;

  Future<CategorieDepense?> categorieDepense(int categorieDepenseId) async {
    await helper.openDb();
    return await helper.getCategorieById(categorieDepenseId);
  }

  SousCategorieDepense(this.id, this.categorie_depense_id, this.intitule,
      this.author, this.icon, this.code, this.created_at, this.updated_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'categorie_depense_id': categorie_depense_id,
      'intitule': intitule,
      'author': author,
      'icon': icon,
      'code': code,
      'created_at': created_at,
      'updated_at': updated_at
    };
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['categorie_depense'] = categorie_depense_id;
    _data['intitule'] = intitule;
    _data['author'] = author;
    _data['icon'] = icon; //user.toJson();
    _data['code'] = code;
    _data['created_at'] = created_at; //category.toJson();
    _data['updated_at'] = updated_at; //category.toJson();

    return _data;
  }
}
