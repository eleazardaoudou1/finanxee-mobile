//enum TypePassif { DETTES, CARTES_DE_CREDIT }

import 'package:finanxee/models/type_passif.dart';

class Passif {
  int id;
  String intitule;
  int typePassif;
  double valeur;
  String description;
  String created_at;
  String updated_at;

  Passif(this.id, this.intitule, this.typePassif, this.valeur, this.description,
      this.created_at, this.updated_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'intitule': intitule,
      'type_passif': typePassif,
      'valeur': valeur,
      'description': description,
      'created_at': created_at,
      'updated_at': updated_at,
    };
  }
}
