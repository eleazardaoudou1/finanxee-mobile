class Parametre {
  int id;
  String cle;
  String name;
  String valeur;
  String created_at;
  String updated_at;
  String deleted_at;

  Parametre(this.id, this.name, this.cle, this.valeur, this.created_at,
      this.updated_at, this.deleted_at);

  Map<String, dynamic> toMap() {
    return {
      'id': (id == 0) ? null : id,
      'name': name,
      'cle': cle,
      'valeur': valeur,
      'created_at': created_at,
      'updated_at': updated_at,
      'deleted_at': deleted_at,
    };
  }

  static Parametre fromMap(Map map) {
    Parametre blog = Parametre(0, "", "", "", DateTime.now().toString(),
        DateTime.now().toString(), DateTime.now().toString());

    blog.id = map['id'];
    blog.name = map['name'];
    blog.cle = map['cle'];
    blog.valeur = map['valeur'];
    blog.created_at = map['created_at'];
    blog.updated_at = map['updated_at'];
    blog.deleted_at = map['deleted_at'];
    return blog;
  }
}
