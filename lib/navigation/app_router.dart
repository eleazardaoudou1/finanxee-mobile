import 'package:finanxee/navigation/finanxee_pages.dart';
import 'package:finanxee/screens/screens.dart';
import 'package:finanxee/providers/app_state_manager.dart';
import 'package:finanxee/screens/screens.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import '../providers/profile_state_manager.dart';
import '../screens/auth/passcode_check_screen.dart';
import '../screens/conditions_generales.dart';
import '../screens/security_question_screen.dart';
import 'app_link.dart';

class AppRouter extends RouterDelegate<AppLink>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin {
  @override
  final GlobalKey<NavigatorState> navigatorKey;
  final AppStateManager appStateManager;
  final ProfileStateManager profileStateManager;
  //final bool isCountrySet;

  AppRouter({
    required this.appStateManager,
    required this.profileStateManager,
  }) : navigatorKey = GlobalKey<NavigatorState>() {
    //Adding the listeners
    appStateManager.addListener(notifyListeners);
    profileStateManager.addListener(notifyListeners);
  }

  //Dispose listeners

  @override
  void dispose() {
    appStateManager.removeListener(notifyListeners);
    profileStateManager.removeListener(notifyListeners);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      onPopPage: _handlePopPage,
      pages: [
        //launching
        //onboarding
        //set country
        //set passcode
        //set questions
        //accept conditions

        // TOD: Add SplashScreen
        if (!appStateManager.isInitialized) LauchScreen.page(),

        //onboarding
        if (appStateManager.isInitialized &&
            !appStateManager.isOnboardingComplete)
          OnboardingScreen.page(),

        //then choose the country
        if (appStateManager.isInitialized &&
            appStateManager.isOnboardingComplete &&
            !appStateManager.isCountrySet)
          PaysScreen.page(),

        //then choose the passcode
        if (appStateManager.isInitialized &&
            appStateManager.isOnboardingComplete &&
            appStateManager.isCountrySet &&
            !appStateManager.isPasscodeSet)
          LoginScreen.page(),

        // then choose and answer the security questions
        if (appStateManager.isInitialized &&
            appStateManager.isOnboardingComplete &&
            appStateManager.isCountrySet &&
            appStateManager.isPasscodeSet &&
            !appStateManager.isQuestionsComplete)
          SecurityQuestionScreen.page(),

        if (appStateManager.isInitialized &&
            appStateManager.isOnboardingComplete &&
            appStateManager.isCountrySet &&
            appStateManager.isPasscodeSet &&
            appStateManager.isQuestionsComplete &&
            !appStateManager.isTermsAndConditionsComplete)
          ConditionsGenerales.page(),
        if (appStateManager.isInitialized &&
            appStateManager.isOnboardingComplete &&
            appStateManager.isCountrySet &&
            appStateManager.isPasscodeSet &&
            appStateManager.isQuestionsComplete &&
            appStateManager.isTermsAndConditionsComplete &&
            !appStateManager.isLoggedIn)
          PasscodeCheckScreen.page()
        else if (appStateManager.isOnboardingComplete &&
            appStateManager.isCountrySet &&
            appStateManager.isPasscodeSet &&
            appStateManager.isQuestionsComplete &&
            appStateManager.isTermsAndConditionsComplete &&
            appStateManager.isLoggedIn)
          //Go to Home
          FinanxeeHomePage.page(appStateManager.getSelectedTab),

        /* if (appStateManager.getSelectedTab > 0)
          FinanxeeHomePage.page(appStateManager.getSelectedTab),*/

// TODO: Create new item
// TODO: Select GroceryItemScreen
// TOD: Add Profile Screen
        if (profileStateManager.didSelectUser)
          ProfileScreen.page(/* profileStateManager.getUser*/),
// TODO: Add WebView Screen
      ],
    );
  }

  bool _handlePopPage(Route<dynamic> route, result) {
    if (!route.didPop(result)) {
      return false;
    }
// 5
// TOD: Handle Onboarding and splash
    if (route.settings.name == FinanxeePages.onboardingPath) {
      appStateManager.logout();
    }
// TODO: Handle state when user closes grocery item screen
// TODO: Handle state when user closes profile screen
// TODO: Handle state when user closes WebView screen
// 6
    return true;
  }

  AppLink getCurrentPath() {
// 1
    if (!appStateManager.isLoggedIn) {
      return AppLink(location: AppLink.kLoginPath, currentTab: 0, itemId: '');
// 2
    } else if (!appStateManager.isOnboardingComplete) {
      return AppLink(
          location: AppLink.kOnboardingPath, currentTab: 0, itemId: '');
// 3
    }
    /*else if (profileManager.didSelectUser) {
      return AppLink(location: AppLink.kProfilePath, currentTab: 0, itemId: '');
// 4
    } else if (groceryManager.isCreatingNewItem) {
      return AppLink(location: AppLink.kItemPath, currentTab: 0, itemId: '');
// 5
    } else if (groceryManager.selectedGroceryItem != null) {
      final id = groceryManager.selectedGroceryItem.id;
      return AppLink(location: AppLink.kItemPath, itemId: id, currentTab: 0);
      // 6
    }*/
    else {
      return AppLink(
          location: AppLink.kHomePath,
          currentTab: appStateManager.getSelectedTab,
          itemId: '');
    }
  }

  @override
  AppLink get currentConfiguration => getCurrentPath();
/*
  @override
  Future<void> setNewRoutePath(configuration) async => null;*/
  @override
  Future<void> setNewRoutePath(AppLink newLink) async {
// 2
    switch (newLink.location) {
// 3
      /* case AppLink.kProfilePath:
        profileManager.tapOnProfile(true);
        break;
// 4
      case AppLink.kItemPath:
// 5
        if (newLink.itemId != null) {
          groceryManager.setSelectedGroceryItem(newLink.itemId);
        } else {
// 6
          groceryManager.createNewItem();
        }
// 7
        profileManager.tapOnProfile(false);
        break;
// 8
      case AppLink.kHomePath:
// 9
        appStateManager.goToTab(newLink.currentTab ?? 0);
// 10
        profileManager.tapOnProfile(false);
        groceryManager.groceryItemTapped(null);
        break;*/
// 11
      default:
        break;
    }
  }
}
