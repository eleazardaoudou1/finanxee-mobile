class FinanxeePages {
  static String launchPath = '/launch';
  static String loginPath = '/login';
  static String passcodePath = '/passcode';
  static String onboardingPath = '/onboarding';
  static String termsAndConditionsPath = '/terms';
  static String questionsSecurite = '/questions';
  static String home = '/';
  static String groceryItemDetails = '/item';
  static String profilePath = '/profile';
  static String raywenderlich = '/raywenderlich';
  static String accountPath = '/account';
  static String countryListPath = '/countries';
}
