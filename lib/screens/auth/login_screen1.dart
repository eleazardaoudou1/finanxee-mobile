import 'dart:async';

import 'package:finanxee/navigation/finanxee_pages.dart';
import 'package:finanxee/providers/app_state_manager.dart';
import 'package:flutter/material.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/models.dart';
import '../../utils/utils.dart';

class PasscodeScreen2 extends StatefulWidget {
  /*static MaterialPage page() {
    return MaterialPage(
        name: FinanxeePages.loginPath,
        key: ValueKey(FinanxeePages.loginPath),
        child: const PasscodeScreen2());
  }*/

  final String passCode;
  const PasscodeScreen2(this.passCode);

  @override
  State<PasscodeScreen2> createState() => _PasscodeScreen2State();
}

class _PasscodeScreen2State extends State<PasscodeScreen2>
    with RestorationMixin {
  DbHelper helper = DbHelper();

  RestorableBool myVar = RestorableBool(true);
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PasscodeScreen(
      backgroundColor: Colors.white,
      circleUIConfig: const CircleUIConfig(
        borderColor: Colors.black,
        fillColor: Colors.black,
        //borderWidth;
        //circleSize:
      ),
      keyboardUIConfig: const KeyboardUIConfig(
        digitTextStyle: TextStyle(fontSize: 30, color: Colors.black),
        deleteButtonTextStyle: TextStyle(fontSize: 16, color: Colors.white),
        primaryColor: Colors.black,
        digitFillColor: Colors.transparent,
        //keyboardRowMargin:
      ),
      title: const Text("Retapez le code secret"),
      passwordEnteredCallback: _onPasscodeEntered,
      cancelButton: const Text('Cancel'),
      deleteButton: const Text('Annuler'),
      shouldTriggerVerification: _verificationNotifier.stream,
      isValidCallback: () {
        Provider.of<AppStateManager>(context, listen: false)
            .completeSettingPasscode();
      },
    );
  }

  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();

  _onPasscodeEntered(String enteredPasscode) async {
    bool isValid = false;
    if (enteredPasscode == widget.passCode) {
      Parametre parametre1 = Parametre(0, "", "", "", DateTime.now().toString(),
          DateTime.now().toString(), DateTime.now().toString());
      parametre1.name = StaticDate.passCodeKey;
      parametre1.cle = StaticDate.passCodeKey;
      parametre1.valeur = enteredPasscode;
      helper.insertParametre(parametre1);

      final prefs = await SharedPreferences.getInstance();
      String passcodeKey = StaticDate.passCodeKey;
      prefs.setBool(passcodeKey, true);
      //Provider.of<AppStateManager>(context, listen: false).completeSettingPasscode();
      isValid = true;
    }
    _verificationNotifier.add(isValid);
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'login_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(myVar, 'theValue');
  }
}
