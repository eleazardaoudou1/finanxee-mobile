import 'dart:async';

import 'package:finanxee/navigation/finanxee_pages.dart';
import 'package:finanxee/providers/app_state_manager.dart';
import 'package:finanxee/screens/auth/login_screen1.dart';
import 'package:flutter/material.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';
import 'package:provider/provider.dart';

import '../../models/models.dart';
import '../../utils/utils.dart';

class LoginScreen extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
        name: FinanxeePages.loginPath,
        key: ValueKey(FinanxeePages.loginPath),
        child: const LoginScreen());
  }

  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with RestorationMixin {
  DbHelper helper = DbHelper();

  RestorableBool myVar = RestorableBool(true);

  String enteredCode = "";
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PasscodeScreen(
        backgroundColor: Colors.white,
        circleUIConfig: const CircleUIConfig(
          borderColor: Colors.black,
          fillColor: Colors.black,
          //borderWidth;
          //circleSize:
        ),
        keyboardUIConfig: const KeyboardUIConfig(
          digitTextStyle: TextStyle(fontSize: 30, color: Colors.black),
          deleteButtonTextStyle: TextStyle(fontSize: 16, color: Colors.white),
          primaryColor: Colors.black,
          digitFillColor: Colors.transparent,
          //keyboardRowMargin:
        ),
        title: const Text("Choisissez un code secret"),
        passwordEnteredCallback: _onPasscodeEntered,
        cancelButton: const Text('Annuler'),
        deleteButton: const Text('Delete'),
        shouldTriggerVerification: _verificationNotifier.stream,
        isValidCallback: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PasscodeScreen2(enteredCode)));
        });
  }

  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();

  _onPasscodeEntered(String enteredPasscode) {
    bool isValid = true; // "123456" == enteredPassword;
    //bool isValid = false; // "123456" == enteredPassword;

    setState(() {
      enteredCode = enteredPasscode;
    });
    _verificationNotifier.add(isValid);

    //Provider.of<AppStateManager>(context, listen: false).login('mockUsername');
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'login_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(myVar, 'theValue');
  }
}
