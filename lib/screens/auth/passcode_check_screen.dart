import 'dart:async';

import 'package:finanxee/navigation/finanxee_pages.dart';
import 'package:finanxee/providers/app_state_manager.dart';
import 'package:finanxee/screens/auth/login_screen1.dart';
import 'package:flutter/material.dart';
import 'package:passcode_screen/circle.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';
import 'package:provider/provider.dart';
import '../screens.dart';

import '../../models/models.dart';
import '../../utils/utils.dart';

class PasscodeCheckScreen extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
        name: FinanxeePages.passcodePath,
        key: ValueKey(FinanxeePages.passcodePath),
        child: const PasscodeCheckScreen());
  }

  const PasscodeCheckScreen({Key? key}) : super(key: key);

  @override
  State<PasscodeCheckScreen> createState() => _PasscodeCheckScreenState();
}

class _PasscodeCheckScreenState extends State<PasscodeCheckScreen>
    with RestorationMixin {
  DbHelper helper = DbHelper();

  RestorableBool myVar = RestorableBool(true);

  String passCode = "";
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }

  Future<void> getData() async {
    String passcodeKey = StaticDate.passCodeKey;
    final codeData = await helper.getParametreByCle(passcodeKey);
    setState(() {
      passCode = codeData!.valeur;
    });

    print("passCode = ");
    print(codeData!.valeur);
  }

  @override
  void initState() {
    // TODO: implement initState
    getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PasscodeScreen(
        backgroundColor: Colors.white,
        circleUIConfig: const CircleUIConfig(
          borderColor: Colors.black,
          fillColor: Colors.black,
          //borderWidth;
          //circleSize:
        ),
        keyboardUIConfig: const KeyboardUIConfig(
          digitTextStyle: TextStyle(fontSize: 30, color: Colors.black),
          deleteButtonTextStyle: TextStyle(fontSize: 16, color: Colors.white),
          primaryColor: Colors.black,
          digitFillColor: Colors.transparent,
          //keyboardRowMargin:
        ),
        title: Column(
          children: [
            const Text(
              "Saisissez votre code secret",
              style: TextStyle(fontSize: 21.0),
            ),
            TextButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SecurityQuestionLoginScreen()));
                },
                child: const Text(
                  "J'ai oublié mon code secret",
                  style: TextStyle(fontSize: 15.0),
                ))
          ],
        ),
        passwordEnteredCallback: _onPasscodeEntered,
        cancelButton: const Text('Annuler'),
        deleteButton: const Text('Annuler'),
        shouldTriggerVerification: _verificationNotifier.stream,
        isValidCallback: () {
          Provider.of<AppStateManager>(context, listen: false)
              .loginWithPassCode();
        });
  }

  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();

  _onPasscodeEntered(String enteredPasscode) {
    print('Code saisi : ');
    print(enteredPasscode);
    bool isValid = false;

    if (enteredPasscode == passCode) {
      isValid = true;
    }
    _verificationNotifier.add(isValid);
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'login_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    registerForRestoration(myVar, 'theValue');
  }
}
