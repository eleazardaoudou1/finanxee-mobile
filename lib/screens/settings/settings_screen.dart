import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  State<SettingsScreen> createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Paramètres"),
        backgroundColor: const Color(0xfff3f3f3),
        foregroundColor: Colors.black,
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              child: ListTile(
                title: Text(
                  'Compte',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                onTap: () {},
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Divider(
                color: Color(0xFF6A6A6A),
              ),
            ),
            ListTile(
              title: Text(
                'Options',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              onTap: () {},
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Divider(
                color: Color(0xFF6A6A6A),
              ),
            ),
            ListTile(
              title: Text(
                'Notifications',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              onTap: () {},
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Divider(
                color: Color(0xFF6A6A6A),
              ),
            ),
            ListTile(
              title: Text(
                'Sécurité',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              onTap: () {},
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Divider(
                color: Color(0xFF6A6A6A),
              ),
            ),
            ListTile(
              title: Text(
                'Aides et mentions légales',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              onTap: () {},
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 5.0),
              child: Divider(
                color: Color(0xFF6A6A6A),
              ),
            ),
          ],
        ),
      ),
      //bottomNavigationBar: tabManager.manageBottomNavBar(context),
    );
  }
}
