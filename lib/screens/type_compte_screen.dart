import 'package:finanxee/components/type_compte_autre.dart';
import 'package:finanxee/components/type_compte_bancaire.dart';
import 'package:finanxee/components/type_compte_momo.dart';
import 'package:finanxee/screens/screens.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_icons/flutter_icons.dart';

class TypeCompteScreen extends StatefulWidget {
  final bool fromForm;
  const TypeCompteScreen(this.fromForm);

  @override
  State<TypeCompteScreen> createState() => _TypeCompteScreenState();
}

class _TypeCompteScreenState extends State<TypeCompteScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Ajouter un compte"),
          backgroundColor: const Color(0xFF21ca79),
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Center(child: TypeCompteBancaire(widget.fromForm)),
                const SizedBox(
                  height: 2.0,
                ),
                const Center(
                  child: TypeCompteMomo(),
                ),
                const SizedBox(
                  height: 2.0,
                ),
                const Center(
                  child: TypeCompteAutre(),
                ),
                const SizedBox(
                  height: 2.0,
                ),
                Center(
                  child: TypeActifAdd(context),
                ),
                const SizedBox(
                  height: 2.0,
                ),
                Center(
                  child: TypePassifAdd(context),
                ),
              ],
            ),
          ),
        ));
  }

  Widget TypeActifAdd(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
      child: Container(
        height: 0.3 * MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
            color: Colors.white, //Color(0xFF00BB),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFF3F3F3),
                  offset: Offset(1, 1),
                  blurRadius: 5,
                  spreadRadius: 5)
            ]),
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.shopping_cart_sharp,
              //FlutterIcons.piggy_bank_faw5s,
              color: Color(0xFF21ca79),
            ),
            const SizedBox(
              height: 18.0,
            ),
            Text('Actif', style: Theme.of(context).textTheme.headline2),
            const SizedBox(
              height: 5.0,
            ),
            const Text(
              'Ajoutez un actif.',
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 7.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ActifsActionsScreen()));
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  //horizontal: 16.0,
                  vertical: 8.0,
                ),
                decoration: BoxDecoration(
                    //color: Color(0xFF21ca79),
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(
                      color: const Color(0xFF21ca79),
                      width: 1,
                    )),
                child: Center(
                  child: Text(
                    "Ajouter",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget TypePassifAdd(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10),
      child: Container(
        height: 0.3 * MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
            color: Colors.white, //Color(0xFF00BB),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFF3F3F3),
                  offset: Offset(1, 1),
                  blurRadius: 5,
                  spreadRadius: 5)
            ]),
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.shopping_cart_rounded,
              //FlutterIcons.piggy_bank_faw5s,
              color: Color(0xFF21ca79),
            ),
            const SizedBox(
              height: 18.0,
            ),
            Text('Passif', style: Theme.of(context).textTheme.headline2),
            const SizedBox(
              height: 5.0,
            ),
            const Text(
              'Ajoutez un passif.',
              textAlign: TextAlign.center,
            ),
            const SizedBox(
              height: 7.0,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PassifActionsScreen()));
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  //horizontal: 16.0,
                  vertical: 8.0,
                ),
                decoration: BoxDecoration(
                    //color: Color(0xFF21ca79),
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(
                      color: const Color(0xFF21ca79),
                      width: 1,
                    )),
                child: Center(
                  child: Text(
                    "Ajouter",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
