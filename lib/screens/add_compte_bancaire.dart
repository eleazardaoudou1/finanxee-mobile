import 'package:finanxee/components/dialogs/banque_list_dialog.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/providers/app_state_manager.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:finanxee/providers/tab_manager.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import '../utils/static_data.dart';

class AddCompteBancaire extends StatefulWidget {
  final Compte compte;
  final bool isNew;
  final bool
      fromForm; // verifie si l'on veut creer le compte a partir du dialog de creation des depenses
  const AddCompteBancaire(this.compte, this.isNew, this.fromForm);

  @override
  State<AddCompteBancaire> createState() =>
      _AddCompteBancaireState(compte, isNew);
}

class _AddCompteBancaireState extends State<AddCompteBancaire>
    with RestorationMixin {
  Compte compte1 = Compte(0, TypeCompte.BANK.index, '', 0.0, '', 'APP',
      'assets/nsia_logo.png', '', '');

  final Compte _compte_selected =
      Compte(0, 0, StaticDate.intituleChoixCompte, 0.0, '', '', '', '', '');
  double solde2 = 0.0;
  bool isNew = false;
  RestorableBool isNewR = RestorableBool(false);
  _AddCompteBancaireState(this.compte1, this.isNew);

  final soldeController = TextEditingController();

  DbHelper helper = DbHelper();
  int chosen_bank_id = 0;

  @override
  void initState() {
    // TODO: implement initState
    //intituleController.text = widget.intitule;
    if (!widget.isNew) {
      soldeController.text = widget.compte.solde.toString();
    }
    compte1 = widget.compte;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final solde = Provider.of<DataInit>(context);

    //if (soldeController.text == null) soldeController.text = '0';
    //soldeController.text = solde.compte_bancaire_solde.toString();
    return Scaffold(
        appBar: AppBar(
          title: const Text("Ajouter un compte bancaire"),
          backgroundColor: const Color(0xFF21ca79),
        ),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              Expanded(
                child: Center(
                  child: SingleChildScrollView(
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height / 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          buildSoldeField(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              ListTile(
                title: Text(
                  'Choisir la banque',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                subtitle: Text(
                  //compte1.intitule,
                  solde.compte_selected.intitule,
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                onTap: () {
                  if (soldeController.text.isEmpty) {
                    solde.updateSoldeBanq(1000);
                  } else {
                    solde.updateSoldeBanq(double.parse(soldeController.text));
                  }
                  /* soldeController.text =
                      solde.compte_bancaire_solde.toString();*/

                  showDialog(
                      context: context,
                      builder: (BuildContext context) =>
                          const BanqueListDialog());
                },
              ),
              const SizedBox(
                height: 15.0,
              ),
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(backgroundColor: const Color(0xFF21cA79)),
                child: const Text('Ajouter'),
                onPressed: () {
                  if (soldeController.text.isEmpty) {
                    SnackBar snackBar1 = const SnackBar(
                      content: Text("Le solde doit être défini"),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                    return;
                  } else if (solde.compte_selected.intitule == 'Choisir') {
                    SnackBar snackBar2 = const SnackBar(
                      content: Text("Choisissez une banque"),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar2);
                    return;
                  }
                  compte1 = solde.compte_selected;
                  compte1.solde = double.parse(soldeController.text);
                  compte1.deleted_at = '';
                  compte1.created_at = DateTime.now().toString();
                  //list.name = txtName.text;
                  helper.insertCompte(compte1);

                  if (widget.isNew) {
                    SnackBar snackBar = const SnackBar(
                      content: Text('Compte ajouté avec succès !'),
                      backgroundColor: Colors.green,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);

                    solde.updateCompteSelected(_compte_selected);
                    if (widget.fromForm) {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      Navigator.pop(context);
                    } else {
                      Navigator.pop(context);
                      Navigator.pop(context);
                      Provider.of<AppStateManager>(context, listen: true)
                          .gotToDashboard();
                    }
                  } else {
                    SnackBar snackBar = const SnackBar(
                      content: Text('Compte édité avec succès !'),
                      backgroundColor: Colors.green,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);

                    Navigator.pop(context);
                    Navigator.pop(context);

                    Provider.of<AppStateManager>(context, listen: true)
                        .gotToDashboard();
                  }
                },
              )
            ],
          ),
        ));
  }

  Widget buildSoldeField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
// 3
        Text(
          'Solde',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: soldeController,
          textAlign: TextAlign.center,
          style: GoogleFonts.lato(fontSize: 60.0),
// 6
          //cursorColor: _currentColor,
// 7
          decoration: InputDecoration(
// 8
            hintText: '25 000 FCFA ',
            hintStyle: GoogleFonts.lato(fontSize: 60.0),
// 9
            enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFBBBBBB)),
            ),
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFBBBBBB)),
            ),
            border: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFBBBBBB)),
            ),
          ),
          keyboardType: const TextInputType.numberWithOptions(
              decimal: true, signed: false)
          /*    .number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ]*/
          , // Only numbers can be entered
        ),
      ],
    );
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'add_compte_bancaire_screen';
  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
    registerForRestoration(isNewR, 'isNewR');
  }
}
