import 'package:finanxee/services/compe_service.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../components/avoir_net_month_card.dart';
import '../../models/payloads/evolution_avoir_net.dart';
import '../../models/payloads/stats_avoir_net.dart';
import '../../utils/dbhelper.dart';
import '../../utils/static_data.dart';

class CourbeAvoirNet extends StatefulWidget {
  const CourbeAvoirNet({Key? key}) : super(key: key);

  @override
  State<CourbeAvoirNet> createState() => _CourbeAvoirNetState();
}

class _CourbeAvoirNetState extends State<CourbeAvoirNet> {
  //var firstOfMonth = DateTime(today.year, today.month - 1, 1).toString();
  //var lastOfMonth = DateTime(today.year, today.month, 0).toString();

  //passif
  double cash_hypotheque = 0;
  double cash_dettes = 0;

  DbHelper helper = DbHelper();
  var f = StaticDate();

  Future<EvolutionAvoirNet> getEvolution() async {
    DateTime today = DateTime.now();
    var firstOfCurrentMonth = DateTime(today.year, today.month, 1).toString();
    var lastOfMonth_1 = DateTime(today.year, today.month, 0).toString();
    var firstOfMonth_1 = DateTime(today.year, today.month - 1, 1).toString();

    var lastOfMonth_2 = DateTime(today.year, today.month - 1, 0).toString();
    var firstOfMonth_2 = DateTime(today.year, today.month - 2, 1).toString();

    var lastOfMonth_3 = DateTime(today.year, today.month - 2, 0).toString();
    var firstOfMonth_3 = DateTime(today.year, today.month - 3, 1).toString();

    var lastOfMonth_4 = DateTime(today.year, today.month - 3, 0).toString();
    var firstOfMonth_4 = DateTime(today.year, today.month - 4, 1).toString();

    var lastOfMonth_5 = DateTime(today.year, today.month - 4, 0).toString();
    var firstOfMonth_5 = DateTime(today.year, today.month - 5, 1).toString();

    double avoirNetCurrentMonth = await CompteService().avoir_net();

    double avoirNetCurrentMonth_1 =
        await CompteService().avoirNetBetween(firstOfMonth_1, lastOfMonth_1);
    double avoirNetCurrentMonth_2 =
        await CompteService().avoirNetBetween(firstOfMonth_2, lastOfMonth_2);
    double avoirNetCurrentMonth_3 =
        await CompteService().avoirNetBetween(firstOfMonth_3, lastOfMonth_3);
    double avoirNetCurrentMonth_4 =
        await CompteService().avoirNetBetween(firstOfMonth_4, lastOfMonth_4);
    double avoirNetCurrentMonth_5 =
        await CompteService().avoirNetBetween(firstOfMonth_5, lastOfMonth_5);

    EvolutionAvoirNet ev = EvolutionAvoirNet(
        avoirNetCurrentMonth,
        avoirNetCurrentMonth_1,
        avoirNetCurrentMonth_2,
        avoirNetCurrentMonth_3,
        avoirNetCurrentMonth_4,
        avoirNetCurrentMonth_5);

    return ev;
  }

  final List<SalesData> chartData = [
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 5, 1), 33),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 4, 1), 35),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 3, 1), 35),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 2, 1), 35),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 1, 1), 35),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month, 1), 35),
  ];
  late TooltipBehavior _tooltipBehavior;

  @override
  void initState() {
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  void tool(TooltipArgs args) {
    args.locationX = 30;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
        future: getEvolution(),
        builder:
            (BuildContext context, AsyncSnapshot<EvolutionAvoirNet> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 14),
                ),
              );

              // if we got our data
            } else if (snapshot.hasData) {
              EvolutionAvoirNet? data = snapshot.data;

              List<SalesData> chartData = [
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 5, 1),
                    data!.current_month_5),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 4, 1),
                    data!.current_month_4),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 3, 1),
                    data!.current_month_3),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 2, 1),
                    data!.current_month_2),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 1, 1),
                    data!.current_month_1),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month, 1),
                    data!.current_month),
              ];

              return SingleChildScrollView(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(
                          "Evolution de l'avoir net",
                          style: GoogleFonts.lato(
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        SfCartesianChart(
                            primaryXAxis: DateTimeAxis(),
                            tooltipBehavior: _tooltipBehavior,
                            onTooltipRender: (TooltipArgs args) => tool(args),
                            onMarkerRender: (MarkerRenderArgs markerargs) {
                              if (markerargs.pointIndex == 2) {
                                markerargs.markerHeight = 20.0;
                                markerargs.markerWidth = 20.0;
                                markerargs.shape = DataMarkerType.triangle;
                              }
                            },
                            annotations: [
                              CartesianChartAnnotation(
                                  widget: Container(
                                      child: const Text('Annotation')),
                                  coordinateUnit: CoordinateUnit.point,
                                  region: AnnotationRegion.chart,
                                  x: 3,
                                  y: 60),
                            ],
                            series: <ChartSeries>[
                              // Renders line chart
                              LineSeries<SalesData, DateTime>(
                                  name: "Evolution de l'avoir net",
                                  dataSource: chartData,
                                  xValueMapper: (SalesData sales, _) =>
                                      sales.date,
                                  yValueMapper: (SalesData sales, _) =>
                                      sales.sales)
                            ]),
                        AvoirNetMonthCard(
                          data!.current_month,
                          DateTime(
                              DateTime.now().year, DateTime.now().month, 1),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AvoirNetMonthCard(
                          data!.current_month_1,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 1, 1),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AvoirNetMonthCard(
                          data!.current_month_2,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 2, 1),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AvoirNetMonthCard(
                          data!.current_month_3,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 3, 1),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AvoirNetMonthCard(
                          data!.current_month_4,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 4, 1),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        AvoirNetMonthCard(
                          data!.current_month_5,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 5, 1),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    )),
              );
            }
          }
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        },
      ),
    );
  }
}

class SalesData {
  SalesData(this.date, this.sales);
  final DateTime date;
  final double sales;
}
