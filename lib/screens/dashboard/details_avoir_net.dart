import 'package:finanxee/screens/dashboard/placement_card.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../models/payloads/stats_avoir_net.dart';
import '../../services/actif_service.dart';
import '../../services/compe_service.dart';
import '../../services/passif_service.dart';
import '../../utils/dbhelper.dart';
import '../../utils/static_data.dart';
import 'autre_actif_card.dart';
import 'autre_liquid_card.dart';
import 'bien_card.dart';
import 'compte_autre_card.dart';
import 'compte_bancaire_card.dart';
import 'compte_momo_card.dart';
import 'dette_card.dart';
import 'hypotheque_card.dart';
import 'immo_card.dart';

class DetailsAvoirNet extends StatefulWidget {
  const DetailsAvoirNet({Key? key}) : super(key: key);

  @override
  State<DetailsAvoirNet> createState() => _DetailsAvoirNetState();
}

class _DetailsAvoirNetState extends State<DetailsAvoirNet> {
  //actif
  double cash_comptes_bancaires = 0;
  double cash_comptes_momo = 0;
  double cash_comptes_autres = 0;
  double cash_immo = 0;
  double cash_biens = 0;
  double cash_placements = 0;
  double cash_autres_liquid = 0;
  double cash_autres_actifs = 0;

  //passif
  double cash_hypotheque = 0;
  double cash_dettes = 0;

  DbHelper helper = DbHelper();
  var f = StaticDate();

  Future<StatsAvoirNet> getData() async {
    var cash_comptes_bancaires = await CompteService().total_compte_bancaire();
    var cash_comptes_momo = await CompteService().total_compte_momo();
    var cash_comptes_autres = await CompteService().total_autre_comptes();
    var _avoir_net = await CompteService().avoir_net();
    var nbr_actif = await ActifService().nbre_autre_actifs();
    var cash_immo = await ActifService().get_actifs_type_valeur(1);
    var cash_biens = await ActifService().get_actifs_type_valeur(2);
    var cash_placements = await ActifService().get_actifs_type_valeur(3);
    var cash_autres_liquid = await ActifService().get_actifs_type_valeur(4);
    var cash_autres_actifs = await ActifService().get_actifs_type_valeur(5);

    var cash_hypotheque = await PassifService().get_passif_type_valeur(1);
    var cash_dettes = await PassifService().get_passif_type_valeur(2);

    var total_actif = await CompteService().total_actif_solde();
    var total_passif = await CompteService().total_passifs();

    StatsAvoirNet statsAvoirNet = StatsAvoirNet(
      total_actif,
      cash_comptes_bancaires,
      cash_comptes_momo,
      cash_comptes_autres,
      cash_immo,
      cash_biens,
      cash_placements,
      cash_autres_liquid,
      cash_autres_actifs,
      total_passif,
      cash_hypotheque,
      cash_dettes,
      total_actif - total_passif,
    );

    return statsAvoirNet;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
        future: getData(),
        builder: (BuildContext context, AsyncSnapshot<StatsAvoirNet> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 14),
                ),
              );

              // if we got our data
            } else if (snapshot.hasData) {
              StatsAvoirNet? data = snapshot.data;

              return SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                            color: Color.fromRGBO(0, 170, 129, 1.0),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFD8F1E7),
                                  offset: Offset(1, 1),
                                  blurRadius: 1,
                                  spreadRadius: 1)
                            ]),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  f.numF.format(data?.avoir_net),
                                  style: GoogleFonts.lato(
                                      fontSize: 22.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 20.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: const [
                                Text(
                                  "Avoir Net",
                                  style: /*Theme.of(context)
                                                      .textTheme
                                                      .bodyLarge,*/
                                      TextStyle(
                                          color: Colors.white,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 20.0,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 35,
                      ),
                      CompteBancaireCard(data!.cash_comptes_bancaires),
                      const SizedBox(
                        height: 25,
                      ),
                      CompteMomoCard(data!.cash_comptes_momo),
                      const SizedBox(
                        height: 25,
                      ),
                      CompteAutreCard(data!.cash_comptes_autres),
                      //
                      const SizedBox(
                        height: 25,
                      ),
                      ImmoCard(data!.cash_immo),
                      const SizedBox(
                        height: 25,
                      ),
                      BienCard(data!.cash_biens),
                      const SizedBox(
                        height: 25,
                      ),
                      PlacementCard(data!.cash_placements),
                      const SizedBox(
                        height: 25,
                      ),
                      AutreLiquiditeCard(data!.cash_autres_liquid),
                      const SizedBox(
                        height: 25,
                      ),
                      AutreActifCard(data!.cash_autres_actifs),
                      const SizedBox(
                        height: 25,
                      ),
                      HypothequeCard(data!.cash_hypotheque),
                      const SizedBox(
                        height: 25,
                      ),
                      DetteCard(data!.cash_dettes),
                      //
                    ],
                  ),
                ),
              );
            }
          }
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        },
      ),
    );
  }
}
