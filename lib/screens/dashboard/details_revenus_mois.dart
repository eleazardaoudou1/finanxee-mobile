import 'package:finanxee/models/payloads/stats_depenses_mois.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../components/depense_categorie_month_card.dart';
import '../../models/payloads/stats_revenus_mois.dart';
import '../../services/depense_service.dart';
import '../../services/revenu_service.dart';

class DetailsRevenusMois extends StatefulWidget {
  const DetailsRevenusMois({Key? key}) : super(key: key);

  @override
  State<DetailsRevenusMois> createState() => _DetailsRevenusMoisState();
}

class _DetailsRevenusMoisState extends State<DetailsRevenusMois> {
  Future<StatsRevenusMois> getData() async {
    return RevenuService().getStatsRevenusMois();
  }

  late TooltipBehavior _tooltipBehavior;

  @override
  void initState() {
    // TODO: implement initState
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
        future: getData(),
        builder:
            (BuildContext context, AsyncSnapshot<StatsRevenusMois> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 14),
                ),
              );

              // if we got our data
            } else if (snapshot.hasData) {
              StatsRevenusMois? data = snapshot.data;
              List<_ChartData> depenseChartData = [];
              for (int i = 0; i < data!.types.length; i++) {
                depenseChartData.add(
                    _ChartData(data!.types[i].intitule, data!.sumRevenu[i]));
              }
              if (data!.types.isNotEmpty) {
                return SingleChildScrollView(
                  child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          Text(
                            "Repartition des revenus du mois",
                            style: GoogleFonts.lato(
                                fontSize: 20.0, fontWeight: FontWeight.bold),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          SfCircularChart(
                              enableMultiSelection: true,
                              tooltipBehavior: _tooltipBehavior,
                              series: <CircularSeries<dynamic, dynamic>>[
                                DoughnutSeries<_ChartData, String>(
                                    legendIconType: LegendIconType.image,
                                    explodeAll: true,
                                    dataSource: depenseChartData,
                                    xValueMapper: (_ChartData data, _) =>
                                        data.x,
                                    yValueMapper: (_ChartData data, _) =>
                                        data.y,
                                    name: 'Revenus')
                              ]),
                          const SizedBox(
                            height: 10,
                          ),
                          ListView.separated(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemBuilder: (BuildContext context, index) {
                                //Account account = StaticDate.userAccounts[index];
                                return GestureDetector(
                                  onTap: () {
                                    /* Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailsCompteBancaireScreen(
                                              comptesMomoList[index])))
                                      .then((value) => getComptes());*/
                                  },
                                  child: DepenseCategorieMonthCard(
                                      data!.sumRevenu[index],
                                      data!.types[index].intitule),
                                );
                              },
                              separatorBuilder: (BuildContext context, index) {
                                return const Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 5.0),
                                  child: Divider(
                                    color: Color(0xFFD4D4D4),
                                  ),
                                );
                              },
                              itemCount: data!.sumRevenu.length),
                          //const DepenseCategorieMonthCard();
                        ],
                      )),
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.all(20),
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height * 0.8,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AspectRatio(
                            aspectRatio: 2 / 1,
                            child: Image.asset('assets/img-icons/income.png')),
                        const SizedBox(height: 8.0),
                        const Text(
                          "Aucun revenu enregistré ce mois  ",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 21.0),
                        ),
                        const SizedBox(height: 16.0),
                        const Text(
                          'Vous n\'avez pas encore ajouté des revenus pour ce mois',
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                );
              }
            }
          }
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        },
      ),
    );
  }
}

class _ChartData {
  _ChartData(this.x, this.y);

  final String x;
  final double y;
}
