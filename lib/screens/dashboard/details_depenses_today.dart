import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../components/depense_card.dart';
import '../../models/depense.dart';
import '../../services/depense_service.dart';
import '../../utils/dbhelper.dart';
import '../../utils/static_data.dart';
import '../transactions/details_depense_screen.dart';

class DetailsDepensesToday extends StatefulWidget {
  const DetailsDepensesToday({Key? key}) : super(key: key);

  @override
  State<DetailsDepensesToday> createState() => _DetailsDepensesTodayState();
}

class _DetailsDepensesTodayState extends State<DetailsDepensesToday> {
  DbHelper helper = DbHelper();
  List<Depense> depenseList = [];
  DepenseService depenseService = DepenseService();
  double total = 0.0;
  var f = StaticDate();

  Future<List> getDepensesToday() async {
    final depenses =
        await helper.getDepensesOfToday(); //get the list of factures
    var total1 = await depenseService.total_depense_today();

    var data = [];
    data.add(depenses);
    data.add(total1);

    return data;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
          future: getDepensesToday(),
          builder: (context, AsyncSnapshot<List> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(
                    '${snapshot.error} occurred',
                    style: const TextStyle(fontSize: 18),
                  ),
                );

                // if we got our data
              } else if (snapshot.hasData) {
                var depenseData = snapshot!.data;

                return SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      if (depenseData![0].isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                f.numF.format(depenseData![1]),
                                style: GoogleFonts.lato(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),

                      if (depenseData![0].isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFD8F1E7),
                                      offset: Offset(1, 1),
                                      blurRadius: 1,
                                      spreadRadius: 1)
                                ]),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 1.0,
                                ),
                                ListView.separated(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemBuilder: (BuildContext context, index) {
                                      //Account account = StaticDate.userAccounts[index];
                                      return GestureDetector(
                                        onTap: () async {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailsDepenseScreen(
                                                          depenseData![0]
                                                              [index])));
                                        },
                                        child:
                                            DepenseCard(depenseData![0][index]),
                                      );
                                    },
                                    separatorBuilder:
                                        (BuildContext context, index) {
                                      return const Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 5.0),
                                        child: Divider(
                                          color: Color(0xFFD4D4D4),
                                        ),
                                      );
                                    },
                                    itemCount: depenseData![0].length),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                          ),
                        ),
                      if (depenseData![0].isEmpty)
                        Padding(
                          padding: const EdgeInsets.all(20),
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height * 0.8,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                AspectRatio(
                                    aspectRatio: 2 / 1,
                                    child: Image.asset(
                                        'assets/img-icons/depense-2-img.png')),
                                const SizedBox(height: 8.0),
                                const Text(
                                  "Aucune dépense enregistrée aujourd'hui  ",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(fontSize: 21.0),
                                ),
                                const SizedBox(height: 16.0),
                                const Text(
                                  'Vous n\'avez pas encore ajouté des dépenses aujourd\'hui',
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                        ),
                      //Expanded(child: Accounts())
                    ],
                  ),
                );
              }
            }

            return const Center(
              child: CircularProgressIndicator(
                color: Colors.green,
              ),
            );
          }),
    );
  }
}
