import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../components/square_image.dart';
import '../../utils/static_data.dart';

class CompteMomoCard extends StatefulWidget {
  const CompteMomoCard(this.value);
  final double value;
  @override
  State<CompteMomoCard> createState() => _CompteMomoCardState();
}

class _CompteMomoCardState extends State<CompteMomoCard> {
  var f = StaticDate();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFD0CECE),
                offset: Offset(1, 1),
                blurRadius: 1,
                spreadRadius: 1)
          ]),
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          Container(
            width: 70,
            height: 70,
            decoration: const BoxDecoration(
              color: Colors.blueAccent,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            child: const Icon(
              Icons.monetization_on_outlined,
              color: Colors.white,
              size: 30.0,
            ),
          ),
          const SizedBox(
            width: 30,
          ),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Solde",
                      style: GoogleFonts.lato(fontSize: 15.0),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Comptes Momo",
                      style: GoogleFonts.lato(
                          fontSize: 15.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                width: 30,
              ),
              Text(
                f.numF.format(widget.value).toString(),
                style: GoogleFonts.lato(
                    fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
            ],
          )),
        ],
      ),
    );
  }
}
