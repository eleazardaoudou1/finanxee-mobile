import 'package:finanxee/components/dialogs/expense_list_dialog.dart';
import 'package:finanxee/components/forms/depense_form.dart';
import 'package:finanxee/components/forms/facture_form.dart';
import 'package:finanxee/components/forms/revenu_form.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/models/facture.dart';
import 'package:finanxee/models/revenu.dart';
import 'package:finanxee/providers/tab_manager.dart';
import 'package:finanxee/screens/analyses_screen.dart';
import 'package:finanxee/screens/avoirs/avoir_screen.dart';
import 'package:finanxee/screens/dashboard.dart';
import 'package:finanxee/screens/transactions/transactions_screen.dart';
import 'package:finanxee/screens/type_compte_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ActionsUser extends StatefulWidget {
  const ActionsUser({Key? key}) : super(key: key);

  @override
  State<ActionsUser> createState() => _ActionsUserState();
}

class _ActionsUserState extends State<ActionsUser> {
  late var tabManager; // = Provider.of<TabManager>(context);

  ExpenseListDialog dialog = ExpenseListDialog();
  @override
  void initState() {
    dialog = ExpenseListDialog();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> pages = [
      Dashboard(),
      AvoirScreen(),
      TransactionsScreen(),
      AnalysesScreen(),
    ];

    tabManager = Provider.of<TabManager>(context);
    final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey =
        GlobalKey<ScaffoldMessengerState>();

    return Scaffold(
      key: scaffoldMessengerKey,
      appBar: AppBar(
        title: const Text("Actions"),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: SizedBox(
                height: 150,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Que voulez-vous faire ?',
                      style: Theme.of(context).textTheme.headline2,
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              title: Text(
                'Ajouter un compte',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              subtitle: Text(
                'Ajoutez un compte bancaire ou momo',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const TypeCompteScreen(false)));
                /*SnackBar snackBar =
                    SnackBar(content: Text('You want to add an accont'));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);*/
              },
              trailing: IconButton(
                icon: const Icon(Icons.account_balance_wallet_outlined),
                onPressed: () {},
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            ListTile(
              title: Text(
                'Ajouter une dépense',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              subtitle: Text(
                'Ajoutez une dépense dans la liste des dépenses',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DepenseForm(
                            Depense(0, '', '', 0, 0, 0, 0.0,
                                DateTime.now().toString(), '', ''),
                            true)));
              },
              trailing: IconButton(
                icon: const Icon(Icons.all_inbox),
                onPressed: () {},
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            ListTile(
              title: Text(
                'Ajouter un revenu',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              subtitle: Text(
                'Ajoutez un revenu dans la liste des revenus',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              onTap: () {
                Revenu revenu = Revenu(1, '', 0, 0, 0.0, '', '', '', '');
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => RevenuForm(revenu, true)));
              },
              trailing: IconButton(
                icon: const Icon(Icons.edit),
                onPressed: () {},
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            ListTile(
              title: Text(
                'Créer une liste de dépenses',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              subtitle: Text(
                "Planifiez à l'avance des dépenses",
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              onTap: () {
                SnackBar snackBar = const SnackBar(
                    content: Text('You want to a list of expenses'));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);

                showDialog(
                    context: context,
                    builder: (BuildContext context) => dialog.buildDialog(
                        context,
                        ExpenseList(0, '', 0, DateTime.now().toString(), ""),
                        true));
              },
              trailing: IconButton(
                icon: const Icon(Icons.content_paste),
                onPressed: () {},
              ),
            ),
            const SizedBox(
              height: 15.0,
            ),
            ListTile(
              title: Text(
                'Ajouter une facture',
                style: Theme.of(context).textTheme.bodyText1,
              ),
              subtitle: Text(
                "Recensez vos fatures à l'avance",
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => FactureForm(
                            Facture(0, '', 0.0, DateTime.now().toString(), 0, 0,
                                '', ''),
                            true)));
              },
              trailing: IconButton(
                icon: const Icon(Icons.content_paste),
                onPressed: () {},
              ),
            ),
            const SizedBox(
              height: 20.0,
            )
          ],
        ),
      ),
      //bottomNavigationBar: tabManager.manageBottomNavBar(context),
    );
  }

  Widget mainScreen() {
    return Container(
      child: Column(
        children: [
          Expanded(
            child: SizedBox(
              height: 150,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Que voulez-vous faire ?',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ],
              ),
            ),
          ),
          ListTile(
            title: Text(
              'Ajouter un compte',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            subtitle: Text(
              'Ajoutez un compte bancaire ou momo',
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const TypeCompteScreen(true)));
              SnackBar snackBar =
                  const SnackBar(content: Text('You want to add an accont'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
            trailing: IconButton(
              icon: const Icon(Icons.account_balance_wallet_outlined),
              onPressed: () {},
            ),
          ),
          const SizedBox(
            height: 15.0,
          ),
          ListTile(
            title: Text(
              'Ajouter une dépense',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            subtitle: Text(
              'Ajoutez une dépense dans la liste des dépenses',
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DepenseForm(
                          Depense(0, '', '', 0, 0, 0, 0.0,
                              DateTime.now().toString(), '', ''),
                          true)));
            },
            trailing: IconButton(
              icon: const Icon(Icons.all_inbox),
              onPressed: () {},
            ),
          ),
          const SizedBox(
            height: 15.0,
          ),
          ListTile(
            title: Text(
              'Ajouter un revenu',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            subtitle: Text(
              'Ajoutez un revenu dans la liste des revenus',
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            onTap: () {
              Revenu revenu = Revenu(1, '', 0, 0, 0.0, '', '', '', '');
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => RevenuForm(revenu, true)));
            },
            trailing: IconButton(
              icon: const Icon(Icons.edit),
              onPressed: () {},
            ),
          ),
          const SizedBox(
            height: 15.0,
          ),
          ListTile(
            title: Text(
              'Ajouter une liste de dépenses',
              style: Theme.of(context).textTheme.bodyText1,
            ),
            subtitle: Text(
              "Planifiez à l'avance des dépenses",
              style: Theme.of(context).textTheme.bodyMedium,
            ),
            onTap: () {
              SnackBar snackBar = const SnackBar(
                  content: Text('You want to a list of expenses'));
              ScaffoldMessenger.of(context).showSnackBar(snackBar);

              showDialog(
                  context: context,
                  builder: (BuildContext context) => dialog.buildDialog(
                      context,
                      ExpenseList(0, '', 0, DateTime.now().toString(), ''),
                      true));
            },
            trailing: IconButton(
              icon: const Icon(Icons.content_paste),
              onPressed: () {},
            ),
          ),
          const SizedBox(
            height: 20.0,
          )
        ],
      ),
    );
  }
}

/*
class ActionsUser extends StatelessWidget {
  const ActionsUser({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Actions"),
          backgroundColor: Color(0xFF21ca79),
        ),
        body: Container(
          child: Column(
            children: [
              Expanded(
                child: Container(
                  height: 150,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Que voulez-vous faire ?',
                        style: Theme.of(context).textTheme.headline2,
                      ),
                    ],
                  ),
                ),
              ),
              ListTile(
                title: Text(
                  'Ajouter un compte',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                subtitle: Text(
                  'Ajoutez un compte bancaire ou momo',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                onTap: () {},
                trailing: IconButton(
                  icon: Icon(Icons.account_balance_wallet_outlined),
                  onPressed: () {},
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              ListTile(
                title: Text(
                  'Ajouter une dépense',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                subtitle: Text(
                  'Ajoutez un dépense dans la liste des dépenses',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                onTap: () {},
                trailing: IconButton(
                  icon: Icon(Icons.all_inbox),
                  onPressed: () {},
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              ListTile(
                title: Text(
                  'Ajouter un revenu',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                subtitle: Text(
                  'Ajoutez un revenu dans la liste des revenus',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                onTap: () {},
                trailing: IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {},
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              GestureDetector(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => dialog.buildDialog(
                          context, ShoppingList(0, '', 0), true));
                },
                child: ListTile(
                  title: Text(
                    'Ajouter une liste de dépenses',
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  subtitle: Text(
                    "Planifiez à l'avance des dépenses",
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                  onTap: () {},
                  trailing: IconButton(
                    icon: Icon(Icons.content_paste),
                    onPressed: () {},
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              )
            ],
          ),
        ));
  }
}*/
