import 'package:finanxee/models/models.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../components/dialogs/question_securite_dialog.dart';
import '../navigation/finanxee_pages.dart';
import '../providers/app_state_manager.dart';
import '../providers/data_init.dart';
import '../utils/static_data.dart';
import '../utils/utils.dart';
import 'package:finanxee/screens/screens.dart';

class SecurityQuestionLoginScreen extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: FinanxeePages.questionsSecurite,
      key: ValueKey(FinanxeePages.termsAndConditionsPath),
      child: const SecurityQuestionLoginScreen(),
    );
  }

  const SecurityQuestionLoginScreen({Key? key}) : super(key: key);

  @override
  State<SecurityQuestionLoginScreen> createState() =>
      _SecurityQuestionLoginScreenState();
}

class _SecurityQuestionLoginScreenState
    extends State<SecurityQuestionLoginScreen> {
  final reponse1Controller = TextEditingController();
  final reponse2Controller = TextEditingController();
  late FocusNode myFocusNode = FocusNode();
  late FocusNode myFocusNode2 = FocusNode();
  DbHelper helper = DbHelper();
  Parametre parametre1 = Parametre(0, "", "", "", DateTime.now().toString(),
      DateTime.now().toString(), DateTime.now().toString());
  Parametre parametre2 = Parametre(0, "", "", "", DateTime.now().toString(),
      DateTime.now().toString(), DateTime.now().toString());
  String question_1 = "";
  String question_2 = "";

  @override
  void initState() {
    // TODO: implement initState
    getData();
    super.initState();
  }

  Future<void> getData() async {
    String question1 = StaticDate.questionSecurityKey1;
    String question2 = StaticDate.questionSecurityKey2;
    final codeData = await helper.getParametreByName(question1);
    final codeData2 = await helper.getParametreByName(question2);
    setState(() {
      parametre1 = codeData!;
      parametre2 = codeData2!;
    });

    print("Question 1 = ");
    print(codeData!.valeur);
  }

  @override
  Widget build(BuildContext context) {
    final questionProvider = Provider.of<DataInit>(context);

    return Scaffold(
      //backgroundColor: const Color(0xfff3f3f3),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 25),
        child: ListView(
          children: [
            Column(
              children: [
                Text(
                  "Répondez aux questions de sécurité que vous avez définies",
                  style: Theme.of(context).textTheme.headline2,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Répondez correctement aux questions pour avoir accès à votre compte.",
                      style: Theme.of(context).textTheme.bodySmall,
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Row(
                      children: [
                        Text(
                          "Question 1",
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xFFD0CECE),
                                offset: Offset(1, 1),
                                blurRadius: 1,
                                spreadRadius: 1)
                          ]),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 20.0, horizontal: 10),
                              child: Text(
                                parametre1.cle,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                          ),
                          Container()
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    buildLabelField(),
                    const SizedBox(
                      height: 50,
                    ),
                    Row(
                      children: [
                        Text(
                          "Question 2",
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xFFD0CECE),
                                offset: Offset(1, 1),
                                blurRadius: 1,
                                spreadRadius: 1)
                          ]),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 20.0, horizontal: 10),
                              child: Text(
                                parametre2.cle,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                          ),
                          Container()
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    buildLabelField_2(),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ElevatedButton(
                          //recruitmenthr@nsiabanque.com
                          onPressed: () async {
                            print(reponse1Controller.text.toLowerCase());
                            print(reponse2Controller.text.toLowerCase());
                            if (reponse1Controller.text.isEmpty ||
                                reponse2Controller.text.isEmpty) {
                              if (reponse1Controller.text.isEmpty) {
                                myFocusNode.requestFocus();
                              }
                              if (reponse2Controller.text.isEmpty) {
                                myFocusNode2.requestFocus();
                              }

                              SnackBar snackBar = const SnackBar(
                                content: Text(
                                  "Vous devez fournir ces réponses avant de continuer",
                                ),
                                backgroundColor: Colors.red,
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                              return;
                            }
                            //comparer la reponse de l'utilisateur aux valeurs de la base

                            if (reponse1Controller.text.toLowerCase() !=
                                    parametre1.valeur.toLowerCase() ||
                                reponse2Controller.text.toLowerCase() !=
                                    parametre2.valeur.toLowerCase()) {
                              SnackBar snackBar = const SnackBar(
                                content: Text(
                                  "Les réponses fournies ne sont pas correctes!",
                                ),
                                backgroundColor: Colors.red,
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                              return;
                            }
                            //lorsque l'utilisateur a renseigne les deux champs correctement
                            Provider.of<AppStateManager>(context, listen: false)
                                .loginWithPassCode();
                          },
                          child: const Text('Continuer'),
                        )
                      ],
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3

// 4
        TextField(
          focusNode: myFocusNode,
// 5
          controller: reponse1Controller,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Votre réponse ",
// 9
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD0CECE)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD0CECE)),
            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFFD0CECE)),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4.0),
                    topRight: Radius.circular(4.0))),
          ),
        ),
      ],
    );
  }

  Widget buildLabelField_2() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3

// 4
        TextField(
// 5
          focusNode: myFocusNode2,
          controller: reponse2Controller,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Votre réponse ",
// 9
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD0CECE)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD0CECE)),
            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFFD0CECE)),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4.0),
                    topRight: Radius.circular(4.0))),
          ),
        ),
      ],
    );
  }
}
