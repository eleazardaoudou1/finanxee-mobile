import 'package:flutter/material.dart';

import '../components/expense_list_card.dart';
import '../models/expense_list.dart';
import '../utils/dbhelper.dart';
import 'one_expense_list.dart';
import 'package:google_fonts/google_fonts.dart';

class ArchivedExpenseListScreen extends StatefulWidget {
  const ArchivedExpenseListScreen({Key? key}) : super(key: key);

  @override
  State<ArchivedExpenseListScreen> createState() =>
      _ArchivedExpenseListScreenState();
}

class _ArchivedExpenseListScreenState extends State<ArchivedExpenseListScreen> {
  List<ExpenseList> expenseList = [];
  List<ExpenseList> expenseList_1 = [];
  List<ExpenseList> listD = [];
  DbHelper helper = DbHelper();
  final intituleController = TextEditingController();

  // c'est ici que nous faisons l'injection
  Future<List<ExpenseList>> getExpenseListArchivees(
      List<ExpenseList> listD) async {
    List<ExpenseList> expenseList1 = await helper.getExpenseListsArchivees();
    /* setState(() {
      expenseList = expenseList1;
    }); */ //get the shopping list from the helper
    if (intituleController.text.isNotEmpty) return listD;
    return expenseList1;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Listes Archivées'),
          backgroundColor: const Color(0xFF21ca79),
          actions: [
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  // Icon(Icons.add),
                  //Icon(Icons.account_circle_outlined),
                ],
              ),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(10.0),
          child: FutureBuilder(
            future: getExpenseListArchivees(listD),
            builder: (BuildContext context,
                AsyncSnapshot<List<ExpenseList>> snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      '${snapshot.error} occurred',
                      style: const TextStyle(fontSize: 18),
                    ),
                  );
                  // if we got our data
                } else {
                  expenseList = snapshot.data!;
                  if (intituleController.text.isEmpty) {
                    expenseList_1 = snapshot.data!;
                  }

                  return Column(
                    children: [
                      SizedBox(
                        height: 200,
                        width: MediaQuery.of(context).size.width,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            decoration: const BoxDecoration(
                                color: Color.fromRGBO(43, 80, 108, 1.0),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFD8F1E7),
                                      offset: Offset(1, 1),
                                      blurRadius: 1,
                                      spreadRadius: 1)
                                ]),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 20),
                            child: Center(
                              child: Text(
                                'Archivées',
                                //style: TextStyle(fontSize: 18),
                                style: GoogleFonts.lato(
                                    fontSize: 40.0,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        decoration: const BoxDecoration(
                            color: Color(0xFFEFEFEF),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFD0CECE),
                                  offset: Offset(1, 1),
                                  blurRadius: 1,
                                  spreadRadius: 1)
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: TextField(
                                    controller: intituleController,
                                    //cursorColor: _currentColor,
                                    decoration: const InputDecoration(
                                      hintText: "Ex: Achats à faire ",
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () async {
                                    expenseList = expenseList_1
                                        .where((x) => x.name
                                            .toLowerCase()
                                            .contains(intituleController.text
                                                .toLowerCase()))
                                        .toList();
                                    setState(() {
                                      listD = expenseList;
                                    });
                                    print("ExpenseList length : ");
                                    print(expenseList.length);
                                  },
                                  child: const Icon(
                                    Icons.search,
                                    color: Colors.grey,
                                    size: 25.0,
                                  ),
                                ),
                              ]),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Expanded(
                        child: ListView.separated(
                          shrinkWrap: true,
                          //physics: NeverScrollableScrollPhysics(),
                          itemCount: expenseList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  OneExpenseList(
                                                      expenseList![index])))
                                      .then((value) => setState(() {}));
                                },
                                child: ExpenseListCard(expenseList![index]));
                          },
                          separatorBuilder: (BuildContext context, index) {
                            return const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 2.0),
                              child: Divider(
                                color: Color(0xFFFFFFFF),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  );
                }
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ));
  }
}
