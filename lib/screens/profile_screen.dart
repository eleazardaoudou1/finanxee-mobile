import 'package:finanxee/navigation/finanxee_pages.dart';
import 'package:finanxee/providers/profile_state_manager.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../components/circle_image.dart';
import '../models/models.dart';
import '../providers/app_state_manager.dart';
import '../utils/static_data.dart';

class ProfileScreen extends StatefulWidget {
  // TOD: ProfileScreen MaterialPage Helper
  static MaterialPage page() {
    return MaterialPage(
      name: FinanxeePages.profilePath,
      key: ValueKey(FinanxeePages.profilePath),
      child: ProfileScreen(),
    );
  }

  //final User user;
  const ProfileScreen();

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  List<Pays> pays = StaticDate.pays;

  Future<Pays> getData() async {
    final prefs = await SharedPreferences.getInstance();
    String selectCountryCode = StaticDate.selectedCountryCode;
    if (prefs.containsKey(selectCountryCode)) {
      for (int i = 0; i < pays.length; i++) {
        if (pays[i].code == prefs.getString(selectCountryCode)) return pays[i]!;
      }
    }
    return pays[0];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profil'),
        backgroundColor: const Color(0xFF21ca79),
        /*leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            // TOO: Close Profile Screen
            Provider.of<ProfileStateManager>(context, listen: false)
                .tapOnProfile(false);
          },
        ),*/
      ),
      body: FutureBuilder(
          future: getData(),
          builder: (context, AsyncSnapshot<Pays> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(
                    '${snapshot.error} occurred',
                    style: const TextStyle(fontSize: 18),
                  ),
                );

                // if we got our data
              } else {
                Pays pays_ = snapshot.data!;
                return Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(height: 16.0),
                      buildProfile(),
                      Expanded(
                        child: buildMenu(pays_),
                      )
                    ],
                  ),
                );
              }
            }
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.green,
              ),
            );
          }),
    );
  }

  Widget buildMenu(Pays pays) {
    return ListView(
      children: [
        buildDarkModeRow(pays),
        ListTile(
          title: const Text('Visiter https://linktr.ee/finanxee'),
          onTap: () {
            // TODO: Open raywenderlich.com webview
          },
        ),
        ListTile(
          title: const Text('Mentions légales'),
          onTap: () {
            // TODO: Logout user
          },
        )
      ],
    );
  }

  Widget buildDarkModeRow(Pays pays) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text('Votre Pays'),
          /* Switch(
            value: widget.user.darkMode,
            onChanged: (value) {
              Provider.of<ProfileStateManager>(context, listen: false)
                  .darkMode = value;
            },
          )*/
          CircleImage(
            imageProvider: AssetImage(pays.image),
            imageRadius: 20,
          ),
        ],
      ),
    );
  }

  Widget buildProfile() {
    return Column(
      children: const [
        /* CircleImage(
          imageProvider: AssetImage(widget.user.profileImageUrl),
          imageRadius: 60.0,
        ),*/
        SizedBox(height: 16.0),
        Text(
          'Version 0.0.1',
          style: TextStyle(
            fontSize: 21,
          ),
        ),
        Text("Finanxee"),
        /* Text(
          '${widget.user.role} points',
          style: const TextStyle(
            fontSize: 30,
            color: Colors.green,
          ),
        ),*/
      ],
    );
  }
}
