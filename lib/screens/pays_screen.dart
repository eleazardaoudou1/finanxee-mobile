import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../cache/cache_helper.dart';
import '../components/circle_image.dart';
import '../models/pays.dart';
import '../navigation/finanxee_pages.dart';
import '../providers/app_state_manager.dart';

class PaysScreen extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: FinanxeePages.countryListPath,
      key: ValueKey(FinanxeePages.countryListPath),
      child: const PaysScreen(),
    );
  }

  const PaysScreen({Key? key}) : super(key: key);

  @override
  State<PaysScreen> createState() => _PaysScreenState();
}

class _PaysScreenState extends State<PaysScreen> with RestorationMixin {
  List<Pays> pays = StaticDate.pays;
  bool toShow = false;
  int selectedIndex = 0;
  String selectCountryCode = StaticDate.selectedCountryCode;

  @override
  void initState() {
    // TODO: implement initState
    CacheHelper.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff3f3f3),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
        child: Column(
          children: [
            SizedBox(
              height: 100,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Choisissez votre pays',
                    style: Theme.of(context).textTheme.headline2,
                  ),
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, index) {
                      Pays pay = pays[index];
                      return GestureDetector(
                        onTap: () {
                          setState(() {
                            toShow = true;
                            selectedIndex = index;
                          });
                        },
                        child: Container(
                          padding: const EdgeInsets.all(15),
                          foregroundDecoration:
                              _foreG(toShow, index, selectedIndex),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              CircleImage(
                                imageProvider: AssetImage(pay.image),
                                imageRadius: 20,
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                child: Text(pay.intitule,
                                    style: GoogleFonts.lato(fontSize: 20.0)),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, index) {
                      return const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      );
                    },
                    itemCount: pays.length),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [_showNextButton(context)],
            )
          ],
        ),
      ),
    );
  }

  Widget _showNextButton(BuildContext context) {
    if (toShow) {
      return ElevatedButton(
        onPressed: () async {
          final prefs = await SharedPreferences.getInstance();
          String selectCountryCode = StaticDate.selectedCountryCode;

          prefs.setString(selectCountryCode, pays[selectedIndex].code);
          print("Pays countryCode = ");
          print(prefs.getString(selectCountryCode));
          //CacheHelper.setString(selectCountryCode, pays[selectedIndex].code);
          Provider.of<AppStateManager>(context, listen: false).setCountry();
        },
        child: const Text('Continuer'),
      );
    } else {
      return Container();
    }
  }

  _foreG(bool toShow, int index, int selectedIndex) {
    if (toShow && selectedIndex == index) {
      return const BoxDecoration(
        color: Color.fromRGBO(0, 0, 0, 0.1),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      );
    }
    return const BoxDecoration(
      //color: Color.fromRGBO(0, 0, 0, 0.1),
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    );
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'pays_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
  }
}
