import 'package:finanxee/models/models.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../components/dialogs/question_securite_dialog.dart';
import '../navigation/finanxee_pages.dart';
import '../providers/app_state_manager.dart';
import '../providers/data_init.dart';
import '../utils/static_data.dart';
import '../utils/utils.dart';

class SecurityQuestionScreen extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: FinanxeePages.questionsSecurite,
      key: ValueKey(FinanxeePages.termsAndConditionsPath),
      child: const SecurityQuestionScreen(),
    );
  }

  const SecurityQuestionScreen({Key? key}) : super(key: key);

  @override
  State<SecurityQuestionScreen> createState() => _SecurityQuestionScreenState();
}

class _SecurityQuestionScreenState extends State<SecurityQuestionScreen> {
  final reponse1Controller = TextEditingController();
  final reponse2Controller = TextEditingController();
  late FocusNode myFocusNode = FocusNode();
  late FocusNode myFocusNode2 = FocusNode();
  DbHelper helper = DbHelper();

  @override
  Widget build(BuildContext context) {
    final questionProvider = Provider.of<DataInit>(context);

    return Scaffold(
      //backgroundColor: const Color(0xfff3f3f3),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 25),
        child: ListView(
          children: [
            Column(
              children: [
                Text(
                  "Définissez vos questions de sécurité",
                  style: Theme.of(context).textTheme.headline2,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Vous aurez besoin de ces questions pour opérer des actions sensibles"
                      " sur votre compte. Assurez-vous d'avoir ces réponses à portée de main.",
                      style: Theme.of(context).textTheme.bodySmall,
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Row(
                      children: [
                        Text(
                          "Question 1",
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xFFD0CECE),
                                offset: Offset(1, 1),
                                blurRadius: 1,
                                spreadRadius: 1)
                          ]),
                      child: GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const QuestionSecuriteDialog(1));
                        },
                        child: Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 20.0, horizontal: 10),
                                child: Text(
                                  questionProvider.question,
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                              ),
                            ),
                            const Icon(
                              Icons.arrow_drop_down,
                              size: 30,
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    buildLabelField(),
                    const SizedBox(
                      height: 50,
                    ),
                    Row(
                      children: [
                        Text(
                          "Question 2",
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                          boxShadow: [
                            BoxShadow(
                                color: Color(0xFFD0CECE),
                                offset: Offset(1, 1),
                                blurRadius: 1,
                                spreadRadius: 1)
                          ]),
                      child: GestureDetector(
                        onTap: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) =>
                                  const QuestionSecuriteDialog(2));
                        },
                        child: Row(
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 20.0, horizontal: 10),
                                child: Text(
                                  questionProvider.question2,
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                              ),
                            ),
                            const Icon(
                              Icons.arrow_drop_down,
                              size: 30,
                            )
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    buildLabelField_2(),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        ElevatedButton(
                          onPressed: () async {
                            if (questionProvider.question ==
                                    StaticDate.intituleChoixQuestion ||
                                questionProvider.question2 ==
                                    StaticDate.intituleChoixQuestion) {
                              SnackBar snackBar = const SnackBar(
                                content: Text(
                                  "Vous devez choisir une question",
                                ),
                                backgroundColor: Colors.red,
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                              return;
                            }

                            if (reponse1Controller.text.isEmpty ||
                                reponse2Controller.text.isEmpty) {
                              if (reponse1Controller.text.isEmpty) {
                                myFocusNode.requestFocus();
                              }
                              if (reponse2Controller.text.isEmpty) {
                                myFocusNode2.requestFocus();
                              }

                              SnackBar snackBar = const SnackBar(
                                content: Text(
                                  "Vous devez fournir ces réponses avant de continuer",
                                ),
                                backgroundColor: Colors.red,
                              );
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(snackBar);
                              return;
                            }

                            //lorsque l'utilisateur a renseigne les deux champs
                            Parametre parametre1 = Parametre(
                                0,
                                "",
                                "",
                                "",
                                DateTime.now().toString(),
                                DateTime.now().toString(),
                                DateTime.now().toString());
                            Parametre parametre2 = Parametre(
                                0,
                                "",
                                "",
                                "",
                                DateTime.now().toString(),
                                DateTime.now().toString(),
                                DateTime.now().toString());
                            parametre1.name = StaticDate.questionSecurityKey1;
                            parametre1.cle = questionProvider.question;
                            parametre1.valeur = reponse1Controller.text;
                            helper.insertParametre(parametre1);

                            //sauvegarder le parametre 2
                            parametre2.name = StaticDate.questionSecurityKey2;
                            parametre2.cle = questionProvider.question2;
                            parametre2.valeur = reponse2Controller.text;
                            helper.insertParametre(parametre2);

                            //definir dans l'application qu'on a repondu aux questions
                            final prefs = await SharedPreferences.getInstance();
                            String questionsCompletedStatus =
                                StaticDate.questionsCompletedStatus;
                            prefs.setBool(questionsCompletedStatus, true);
                            Provider.of<AppStateManager>(context, listen: false)
                                .completeQuestions();
                          },
                          child: const Text('Continuer'),
                        )
                      ],
                    )
                  ],
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3

// 4
        TextField(
          focusNode: myFocusNode,
// 5
          controller: reponse1Controller,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Votre réponse ",
// 9
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD0CECE)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD0CECE)),
            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFFD0CECE)),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4.0),
                    topRight: Radius.circular(4.0))),
          ),
        ),
      ],
    );
  }

  Widget buildLabelField_2() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3

// 4
        TextField(
// 5
          focusNode: myFocusNode2,
          controller: reponse2Controller,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Votre réponse ",
// 9
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD0CECE)),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD0CECE)),
            ),
            border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFFD0CECE)),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(4.0),
                    topRight: Radius.circular(4.0))),
          ),
        ),
      ],
    );
  }
}
