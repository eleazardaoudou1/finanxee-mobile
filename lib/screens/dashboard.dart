import 'package:finanxee/components/components.dart';
import 'package:finanxee/providers/profile_state_manager.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:finanxee/screens/screens.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../cache/cache_helper.dart';
import '../utils/static_data.dart';

class Dashboard extends StatefulWidget {
  //Dashboard();
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with RestorationMixin {
  DbHelper helper = DbHelper();
  String of = "1";
  @override
  void initState() {
    someTask();
    // TODO: implement initState
    super.initState();
  }

  someTask() async {
    final prefs = await SharedPreferences.getInstance();
    print("HomePage countryCode = ");
    String selectCountryCode = StaticDate.selectedCountryCode;
    if (prefs.containsKey(selectCountryCode)) {
      print("Dash countryCode = ");
      print(prefs.getString(selectCountryCode));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //leading: Icon(FlutterIcons.menu_fea),
        centerTitle: true,
        title: Text("Finanxee"),
        backgroundColor: const Color(0xFF21ca79),
        elevation: 0,
        actions: [
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                    //90 74 53 43
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const SettingsScreen()));
                    },
                    child: const Icon(Icons.settings)),
                const SizedBox(
                  width: 20,
                ),
                GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const ProfileScreen()));

                      /* Provider.of<ProfileStateManager>(context, listen: false)
                          .tapOnProfile(true);*/
                    },
                    child: const Icon(Icons.notifications)),
              ],
            ),
          ),
        ],
      ),
      body: Container(
        color: const Color(0xFFE3FAF0), // Colors.green[100],
        child: SingleChildScrollView(
          child: Column(
            children: const [
              SizedBox(
                height: 5.0,
              ),
              NetWorth(),
              TodayExpense(),
              SizedBox(
                height: 16.0,
              ),
              CashFlow(),
              SizedBox(
                height: 16.0,
              ),
              MonthExpense(),
              SizedBox(
                height: 15,
              ),
              LastRevenues(),
              SizedBox(
                height: 15,
              ),
              BilanComptes(),
              SizedBox(
                height: 16.0,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Divider(
                  color: Color(0xFFECECEC),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              /* Accounts(),
            SizedBox(
              height: 15,
            ),*/

              BilanFactures(),
              SizedBox(
                height: 15,
              ),
              /*LastBudgets(),
              SizedBox(
                height: 15,
              ),*/
              LastExpenseList(),
              SizedBox(
                height: 15,
              ),
              //Expanded(child: Accounts())
            ],
          ),
        ),
      ),
    );
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'dashboard_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
  }
}
