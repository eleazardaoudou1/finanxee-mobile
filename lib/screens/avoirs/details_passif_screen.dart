import 'package:finanxee/components/circle_image.dart';
import 'package:finanxee/components/dialogs/delete_passif_dialog.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/screens/avoirs/add_passif_form.dart';
import 'package:finanxee/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class DetailsPassifScreen extends StatefulWidget {
  final Passif actif;
  const DetailsPassifScreen(this.actif);

  @override
  State<DetailsPassifScreen> createState() => _DetailsPassifScreen(actif);
}

class _DetailsPassifScreen extends State<DetailsPassifScreen> {
  late final Passif compte;
  late List<String> transactions = [];

  //function to show the list of expense tasks
  Future getTransactions(compteId) async {
    //open the database
    await helper.openDb();
    transactions = []; //await helper.getDepensesFromList(compteId);
  }

  Future getData() async {
    return getTransactions(compte.id);
  }

  var f = StaticDate();
  List<TypePassif> typesActifs = StaticDate.typesPassifs;

  late DbHelper helper;

  _DetailsPassifScreen(this.compte);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(compte.intitule),
          backgroundColor: const Color(0xfff3f3f3),
          foregroundColor: Colors.black,
          actions: [
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    child: const Icon(Icons.delete_outline),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              DeletePassif(passif: compte)).then((value) => {
                            if (value == 'x')
                              {
                                Navigator.pop(context),
                                //print('Value is : ' + value)
                              }
                          });

                      SnackBar snackBar =
                          const SnackBar(content: Text('Supprimer le bien'));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    },
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                    child: const Icon(Icons.edit),
                    onTap: () {
                      SnackBar snackBar =
                          const SnackBar(content: Text("Editer"));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      //lancer le dialog
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddPassifForm(
                                  compte,
                                  compte.intitule,
                                  typesActifs[compte.typePassif - 1],
                                  false)));
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
        body: FutureBuilder(
            future: getData(), //helper.insertAnExpenseTask(), /,
            builder: (BuildContext context, snapshot) {
              // TODO: Add nested ListViews
              if (snapshot.connectionState == ConnectionState.done) {
                return Container(
                  child: Column(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 150,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                f.numF.format(compte.valeur),
                                style: GoogleFonts.lato(fontSize: 40.0),
                                //style: Theme.of(context).textTheme.headline2,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const CircleImage(
                                imageProvider: AssetImage('assets/passif.jpg'),
                                imageRadius: 38,
                              )
                            ],
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.sticky_note_2_outlined),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                compte.intitule,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Intitulé',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.calendar_month),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                DateFormat('dd-MM-yyyy')
                                    .format(DateTime.parse(compte.created_at)),
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Date de création',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );

                /*Padding(
                    padding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            height: 150,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  this.compte.solde.toString(),
                                  style: Theme.of(context).textTheme.headline2,
                                ),
                                Text(
                                  this.compte.created_at,
                                  style: Theme.of(context).textTheme.caption,
                                ),
                                Text(
                                  this.compte.id.toString(),
                                  style: Theme.of(context).textTheme.caption,
                                ),
                              ],
                            ),
                          ),
                          Container(
                            color: Colors.white,
                            child: Text(transactions.length.toString()),
                          ),
                          ListView.separated(
                            shrinkWrap: true,
                            //physics: NeverScrollableScrollPhysics(),
                            itemCount: (transactions != null)
                                ? transactions.length
                                : 0,
                            itemBuilder: (BuildContext context, int index) {
                              return ListTile(
                                title: Text(
                                  transactions[index],
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                subtitle: Text(
                                  transactions[index].toString(),
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                                onTap: () {
                                  SnackBar snackBar = SnackBar(
                                      content: Text(
                                          'Glissez vers la gauche ou vers la droite pour supprimer'));
                                  ScaffoldMessenger.of(context)
                                      .showSnackBar(snackBar);
                                },
                                trailing: IconButton(
                                  icon: Icon(
                                      Icons.account_balance_wallet_outlined),
                                  onPressed: () {},
                                ),
                              );
                            },
                            separatorBuilder: (BuildContext context, index) {
                              return Padding(
                                */ /* SizedBox(
                                  height: 15.0,
                                ),*/ /*
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 2.0),
                                child: Divider(
                                  color: Color(0xFFFFFFFF),
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ));*/
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Color(0xFF21ca79),
                  ),
                );
              }
            }));
  }
}

class DeletePassif extends StatelessWidget {
  const DeletePassif({Key? key, required this.passif}) : super(key: key);
  final Passif passif;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer ce passif ?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            //Navigator.pop(context);
            await //helper.deleteDepense(depense.id);
                helper.deletePassif(passif.id);
            SnackBar snackBar =
                const SnackBar(content: Text('Passif supprimé !'));
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ],
    );
  }
}
