import 'package:finanxee/screens/screens.dart';
import 'package:finanxee/components/components.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/services/services.dart';
import 'package:finanxee/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AvoirScreen extends StatefulWidget {
  const AvoirScreen({Key? key}) : super(key: key);

  @override
  State<AvoirScreen> createState() => _AvoirScreenState();
}

class _AvoirScreenState extends State<AvoirScreen> with RestorationMixin {
  var compte_service;

  DbHelper helper = DbHelper();
  List<Compte> comptesBanqList = [];
  List<Compte> comptesMomoList = [];
  List<Compte> comptesAutreList = [];
  List<Actif> actifsList = [];
  List<Actif> actifsImmoList = [];
  List<Actif> actifsBiensList = [];
  List<Actif> actifsPlacementsList = [];
  List<Actif> actifsLiquidList = [];
  List<Actif> actifsAutreList = [];
  List<Passif> passifHypList = [];
  List<Passif> passifEmprList = [];

  CompteService depenseService = CompteService();
  double total_compte_banc = 0.0;
  double total_compte_momo = 0.0;
  double total_autres_compte = 0.0;
  double total_autres_actifs = 0.0;
  double total_immo = 0.0;
  double total_biens = 0.0;
  double total_pl = 0.0;
  double total_liquid = 0.0;
  double total_autres = 0.0;
  double total_hypotheques = 0.0;
  double total_emprunts = 0.0;

  //
  double actifs_solde = 0.0;
  double passif_solde = 0.0;

  double avoir_net = 0.0;
  int nbreComptes = 0;
  int nbreActifsTotal = 0;
  int nbrePassifsTotal = 0;
  var f = StaticDate();

  @override
  void initState() {
    // TODO: implement initState
    getComptes();
    super.initState();
  }

  Future<List<Compte>> getComptes() async {
    final comptesBancaires =
        await helper.getCompteBancaires(); //get the list of factures
    final comptesMomo = await helper.getCompteMomo();
    final comptesAutres = await helper.getCompteAutres();
    final actifs = await helper.getActifs();
    final actifsImmo = await helper.getActifsType(1);
    final actifsBiens = await helper.getActifsType(2);
    final actifsPl = await helper.getActifsType(3);
    final actifsLiq = await helper.getActifsType(4);
    final actifsAutre = await helper.getActifsType(5);

    //gestion des passifs
    final passifHyp = await helper.getPassifsType(1);
    final passifDettes = await helper.getPassifsType(2);

    var _banc = await CompteService().total_compte_bancaire();
    var _momo = await CompteService().total_compte_momo();
    var _autre = await CompteService().total_autre_comptes();
    var _avoir_net = await CompteService().avoir_net();
    var nbr_actif = await ActifService().nbre_autre_actifs();
    var _actifs = await ActifService().get_actifs_valeur();
    var _immo = await ActifService().get_actifs_type_valeur(1);
    var _biens = await ActifService().get_actifs_type_valeur(2);
    var _pl = await ActifService().get_actifs_type_valeur(3);
    var _liq = await ActifService().get_actifs_type_valeur(4);
    var _aut = await ActifService().get_actifs_type_valeur(5);

    actifs_solde = await CompteService().total_actif_solde();
    passif_solde = await CompteService().total_passifs();

    //gestion des passifs
    var _hyp = await PassifService().get_passif_type_valeur(1);
    var _empru = await PassifService().get_passif_type_valeur(2);

    var result = [];
    result.add(comptesBancaires); //0
    result.add(comptesMomo); //1
    result.add(comptesAutres); //2
    result.add(actifs); //3
    result.add(actifsImmo); //4
    result.add(actifsBiens); //5
    result.add(actifsPl); //6
    result.add(actifsLiq); //7
    result.add(actifsAutre); //8
    result.add(passifHyp); //9
    result.add(passifDettes); //10
    result.add(result[0].length + result[1].length + result[2].length); //11
    result.add(result[11] + nbr_actif); //12
    result.add(_banc); //13
    result.add(_momo); //14
    result.add(_autre); //15
    result.add(_actifs); //16
    result.add(_immo); //17
    result.add(_biens); //18
    result.add(_pl); //19
    result.add(_liq); //20
    result.add(_aut); //21
    result.add(_hyp); //22
    result.add(_empru); //23
    result.add(result[9].length + result[10].length); //24
    result.add(actifs_solde); //25
    result.add(passif_solde); //26
    result.add(_avoir_net); //27

    setState(() {
      //facturesList = factures;
      comptesBanqList = comptesBancaires;
      comptesMomoList = comptesMomo;
      comptesAutreList = comptesAutres;
      actifsList = actifs;
      actifsImmoList = actifsImmo;
      actifsBiensList = actifsBiens;
      actifsPlacementsList = actifsPl;
      actifsLiquidList = actifsLiq;
      actifsAutreList = actifsAutre;

      //passifs
      passifHypList = passifHyp;
      passifEmprList = passifDettes;

      nbreComptes = comptesBanqList.length +
          comptesMomoList.length +
          comptesAutreList.length;
      nbreActifsTotal = nbreComptes + nbr_actif;
      total_compte_banc = _banc;
      total_compte_momo = _momo;
      total_autres_compte = _autre;
      total_autres_actifs = _actifs;
      total_immo = _immo;
      total_biens = _biens;
      total_pl = _pl;
      total_liquid = _liq;
      total_autres = _aut;

      //passifs
      total_hypotheques = _hyp;
      total_emprunts = _empru;
      nbrePassifsTotal = passifHypList.length + passifEmprList.length;

      actifs_solde = actifs_solde;
      passif_solde = passif_solde;
      avoir_net = _avoir_net;
    });
    return comptesBancaires;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          //leading: Icon(FlutterIcons.menu_fea),
          //centerTitle: true,
          title: const Text('Avoirs'),
          backgroundColor: const Color(0xFF21ca79),
          actions: [
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: const [
                  /*GestureDetector(
                    child:
                    Icon(Icons.add),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const TypeCompteScreen()));
                    },
                  )*/
                ],
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 5.0, horizontal: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  /* Text(
                    dropdownValue,
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                  ),*/
                ],
              ),
            ),
            /* PopupMenuButton<String>(
              icon:
              Icon(
                Icons.devices_other_sharp,
                color: Colors.white,
              ),
// 7
              onSelected: (String value) {},
              itemBuilder: (BuildContext context) {
                return [];
// 8
              },
            ),*/
          ],
        ),
        body: FutureBuilder(
            future: helper.getProjets(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      '${snapshot.error} occurred',
                      style: const TextStyle(fontSize: 18),
                    ),
                  );

                  // if we got our data
                } else if (snapshot.hasData &&
                    nbreActifsTotal == 0 &&
                    nbrePassifsTotal == 0) {
                  return const EmptyAvoirScreen();
                } else {
                  return Container(
                    color: const Color(0xFFECECEC), // Colors.green[100],
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 5.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Container(
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xFFD8F1E7),
                                        offset: Offset(1, 1),
                                        blurRadius: 1,
                                        spreadRadius: 1)
                                  ]),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15.0, vertical: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 40.0,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Text(
                                              f.numF
                                                  .format(avoir_net)
                                                  .toString(),
                                              //style: Theme.of(context).textTheme.headline2,
                                              style: GoogleFonts.lato(
                                                  fontSize: 30.0,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                            Text(
                                              'Avoir net',
                                              //style: Theme.of(context).textTheme.headline2,
                                              style: GoogleFonts.lato(
                                                  fontSize: 15.0,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  const Padding(
                                    padding: EdgeInsets.only(
                                        right: 5.0, left: 5.0, top: 30.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 20.0,
                                  ),
                                  actifCard(context),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  passifCard(context),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                    height: 25,
                                    width: MediaQuery.of(context).size.width,
                                    decoration:
                                        BoxDecoration(color: Colors.grey[200]),
                                    child: Center(
                                      child: Text(
                                        "ACTIF",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  /*************Comptes Bancaires*************/
                                  comptesBancaieres(context),
                                  /*******************Finc compte bancaires**********************/

                                  const SizedBox(
                                    height: 40,
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  ),

                                  /****************Comptes Momo****************************/
                                  comptesMomo(context),
                                  /*********************Fin compte Momo**********/
                                  const SizedBox(
                                    height: 40,
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  ),

                                  /*********************Autres comptes***************/
                                  autresComptes(context),
                                  /******************Fin autres comptes**************************/
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  ),
                                  /********************Immobilier*******************/
                                  immobiler(context),
                                  /****************Fin Immobilier************************/

                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Biens Personnels",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                      ),
                                      Text(
                                        f.numF.format(total_biens).toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  ListView.separated(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        //Account account = StaticDate.userAccounts[index];
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            DetailsActifScreen(
                                                                actifsBiensList[
                                                                    index])))
                                                .then((value) => getComptes());
                                          },
                                          child:
                                              ActifCard(actifsBiensList[index]),
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, index) {
                                        return const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 5.0),
                                          child: Divider(
                                            color: Color(0xFFD4D4D4),
                                          ),
                                        );
                                      },
                                      itemCount: actifsBiensList.length),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Placements",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                      ),
                                      Text(
                                        f.numF.format(total_pl).toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  ListView.separated(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        //Account account = StaticDate.userAccounts[index];
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailsActifScreen(
                                                            actifsPlacementsList[
                                                                index]))).then(
                                                (value) => getComptes());
                                          },
                                          child: ActifCard(
                                              actifsPlacementsList[index]),
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, index) {
                                        return const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 5.0),
                                          child: Divider(
                                            color: Color(0xFFD4D4D4),
                                          ),
                                        );
                                      },
                                      itemCount: actifsPlacementsList.length),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Liquidités",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                      ),
                                      Text(
                                        f.numF.format(total_liquid).toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  ListView.separated(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        //Account account = StaticDate.userAccounts[index];
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailsActifScreen(
                                                            actifsLiquidList[
                                                                index]))).then(
                                                (value) => getComptes());
                                          },
                                          child: ActifCard(
                                              actifsLiquidList[index]),
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, index) {
                                        return const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 5.0),
                                          child: Divider(
                                            color: Color(0xFFD4D4D4),
                                          ),
                                        );
                                      },
                                      itemCount: actifsLiquidList.length),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Autres actifs",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                      ),
                                      Text(
                                        f.numF.format(total_autres).toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  ListView.separated(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        //Account account = StaticDate.userAccounts[index];
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            DetailsActifScreen(
                                                                actifsAutreList[
                                                                    index])))
                                                .then((value) => getComptes());
                                          },
                                          child:
                                              ActifCard(actifsAutreList[index]),
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, index) {
                                        return const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 5.0),
                                          child: Divider(
                                            color: Color(0xFFD4D4D4),
                                          ),
                                        );
                                      },
                                      itemCount: actifsAutreList.length),
                                  const SizedBox(
                                    height: 50,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                    height: 25,
                                    width: MediaQuery.of(context).size.width,
                                    decoration:
                                        BoxDecoration(color: Colors.grey[200]),
                                    child: Center(
                                      child: Text(
                                        "PASSIF",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyText1,
                                      ),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Hypothèques",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                      ),
                                      Text(
                                        f.numF
                                            .format(total_hypotheques)
                                            .toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge,
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  ListView.separated(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        //Account account = StaticDate.userAccounts[index];
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            DetailsPassifScreen(
                                                                passifHypList[
                                                                    index])))
                                                .then((value) => getComptes());
                                          },
                                          child:
                                              PassifCard(passifHypList[index]),
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, index) {
                                        return const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 5.0),
                                          child: Divider(
                                            color: Color(0xFFD4D4D4),
                                          ),
                                        );
                                      },
                                      itemCount: passifHypList.length),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Emprunts et dettes",
                                        style: Theme.of(context)
                                            .textTheme
                                            .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                      ),
                                      Text(
                                        f.numF
                                            .format(total_emprunts)
                                            .toString(),
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge, //.labelLarge,
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  ListView.separated(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        //Account account = StaticDate.userAccounts[index];
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            DetailsPassifScreen(
                                                                passifEmprList[
                                                                    index])))
                                                .then((value) => getComptes());
                                          },
                                          child:
                                              PassifCard(passifEmprList[index]),
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, index) {
                                        return const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 5.0),
                                          child: Divider(
                                            color: Color(0xFFD4D4D4),
                                          ),
                                        );
                                      },
                                      itemCount: passifEmprList.length),
                                  const SizedBox(
                                    height: 10,
                                  )
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),

                          //Expanded(child: Accounts())
                        ],
                      ),
                    ),
                  );
                }
              }

              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              );
            }),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const TypeCompteScreen(false)))
                  .then((value) => getComptes());
            },
            tooltip: 'Ajouter un compte',
            child: const Icon(Icons.add),
            backgroundColor: const Color(0xFF21ca79)));
  }

  Widget actifCard(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(
        decoration: const BoxDecoration(
            color: Color.fromRGBO(0, 151, 111, 1.0),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFD8F1E7),
                  offset: Offset(1, 1),
                  blurRadius: 1,
                  spreadRadius: 1)
            ]),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
        child: FutureBuilder(
            future: helper.getDepensesOfMonth(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      '${snapshot.error} occurred',
                      style: const TextStyle(fontSize: 14),
                    ),
                  );

                  // if we got our data
                } else if (snapshot.hasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Text(
                            "Actif",
                            style: /*Theme.of(context)
                                            .textTheme
                                            .bodyLarge,*/
                                TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w300),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            f.numF.format(actifs_solde),
                            style: GoogleFonts.lato(
                                fontSize: 22.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                    ],
                  );
                }
              }
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              );
            }),
      ),
    );
  }

  Widget passifCard(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Container(
        decoration: const BoxDecoration(
            color: Color.fromRGBO(43, 80, 108, 1.0),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFD8F1E7),
                  offset: Offset(1, 1),
                  blurRadius: 1,
                  spreadRadius: 1)
            ]),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
        child: FutureBuilder(
            future: helper.getDepensesOfMonth(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      '${snapshot.error} occurred',
                      style: const TextStyle(fontSize: 14),
                    ),
                  );

                  // if we got our data
                } else if (snapshot.hasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Text(
                            "Passif",
                            style: /*Theme.of(context)
                                            .textTheme
                                            .bodyLarge,*/
                                TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w300),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            f.numF.format(passif_solde * -1),
                            style: GoogleFonts.lato(
                                fontSize: 22.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 10.0,
                      ),
                    ],
                  );
                }
              }
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              );
            }),
      ),
    );
  }

  Widget comptesBancaieres(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Comptes Bancaires",
              style: Theme.of(context)
                  .textTheme
                  .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
            ),
            Text(
              f.numF.format(total_compte_banc).toString(),
              style: Theme.of(context).textTheme.titleLarge,
            )
          ],
        ),
        const SizedBox(
          height: 10.0,
        ),
        ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, index) {
              //Account account = StaticDate.userAccounts[index];
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailsCompteBancaireScreen(
                                  comptesBanqList[index])))
                      .then((value) => getComptes());
                },
                child: AccountCard(comptesBanqList[index]),
              );
            },
            separatorBuilder: (BuildContext context, index) {
              return const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Divider(
                  color: Color(0xFFD4D4D4),
                ),
              );
            },
            itemCount: comptesBanqList.length),
      ],
    );
  }

  Widget comptesMomo(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Comptes Momo",
              style: Theme.of(context)
                  .textTheme
                  .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
            ),
            Text(
              f.numF.format(total_compte_momo).toString(),
              style: Theme.of(context).textTheme.titleLarge,
            )
          ],
        ),
        const SizedBox(
          height: 10.0,
        ),
        ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, index) {
              //Account account = StaticDate.userAccounts[index];
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailsCompteBancaireScreen(
                                  comptesMomoList[index])))
                      .then((value) => getComptes());
                },
                child: AccountCard(comptesMomoList[index]),
              );
            },
            separatorBuilder: (BuildContext context, index) {
              return const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Divider(
                  color: Color(0xFFD4D4D4),
                ),
              );
            },
            itemCount: comptesMomoList.length),
      ],
    );
  }

  Widget autresComptes(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Autres Comptes",
              style: Theme.of(context)
                  .textTheme
                  .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
            ),
            Text(
              f.numF.format(total_autres_compte).toString(),
              style: Theme.of(context).textTheme.titleLarge,
            )
          ],
        ),
        const SizedBox(
          height: 10.0,
        ),
        ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, index) {
              //Account account = StaticDate.userAccounts[index];
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DetailsCompteBancaireScreen(
                                  comptesAutreList[index])))
                      .then((value) => getComptes());
                },
                child: AccountCard(comptesAutreList[index]),
              );
            },
            separatorBuilder: (BuildContext context, index) {
              return const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Divider(
                  color: Color(0xFFD4D4D4),
                ),
              );
            },
            itemCount: comptesAutreList.length),
      ],
    );
  }

  Widget immobiler(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Immobilier",
              style: Theme.of(context)
                  .textTheme
                  .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
            ),
            Text(
              f.numF.format(total_immo).toString(),
              style: Theme.of(context).textTheme.titleLarge,
            )
          ],
        ),
        const SizedBox(
          height: 10.0,
        ),
        ListView.separated(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (BuildContext context, index) {
              //Account account = StaticDate.userAccounts[index];
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  DetailsActifScreen(actifsImmoList[index])))
                      .then((value) => getComptes());
                },
                child: ActifCard(actifsImmoList[index]),
              );
            },
            separatorBuilder: (BuildContext context, index) {
              return const Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.0),
                child: Divider(
                  color: Color(0xFFD4D4D4),
                ),
              );
            },
            itemCount: actifsImmoList.length),
      ],
    );
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'avoir_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
  }
}
