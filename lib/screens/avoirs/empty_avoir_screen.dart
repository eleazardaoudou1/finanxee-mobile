import 'package:finanxee/screens/avoirs/actifs_actions_screen.dart';
import 'package:flutter/material.dart';

class EmptyAvoirScreen extends StatefulWidget {
  const EmptyAvoirScreen({Key? key}) : super(key: key);

  @override
  State<EmptyAvoirScreen> createState() => _EmptyAvoirScreenState();
}

class _EmptyAvoirScreenState extends State<EmptyAvoirScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AspectRatio(
                aspectRatio: 1 / 1,
                child: Image.asset('assets/core/avoir-1.png')),
            const SizedBox(height: 8.0),
            const Text(
              'Aucun avoir ',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 21.0),
            ),
            const SizedBox(height: 16.0),
            const Text(
              'Vous n\'avez pas encore ajouté des avoirs pour l\'instant.\n Cliquez sur le bouton \n + pour ajouter',
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 16.0),
            /* GestureDetector(
              onTap: () {
                SnackBar snackBar = const SnackBar(
                    content: Text('You want to see all your ongoing budgets '));
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
              child: Wrap(children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                    //horizontal: 16.0,
                    vertical: 8.0,
                  ),
                  decoration: BoxDecoration(
                      //color: Color(0xFF21ca79),
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                        color: const Color(0xFF21ca79),
                        width: 1,
                      )),
                  child: Center(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ActifsActionsScreen()));
                      },
                      child: Text(
                        "Gérer mes avoirs",
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ),
                ),
              ]),
            )*/
            /*MaterialButton(
                  textColor: Colors.white,
                  child: Text('Ajouter'),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  color: Colors.green,
                  onPressed: () {})*/
          ],
        ),
      ),
    );
  }
}
