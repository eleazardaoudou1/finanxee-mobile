import 'package:finanxee/components/circle_image.dart';
import 'package:finanxee/components/dialogs/delete_actif_dialog.dart';
import 'package:finanxee/components/dialogs/delete_compte_dialog.dart';
import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/type_actif.dart';
import 'package:finanxee/screens/avoirs/add_actif_form.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class DetailsActifScreen extends StatefulWidget {
  final Actif actif;
  const DetailsActifScreen(this.actif);

  @override
  State<DetailsActifScreen> createState() => _DetailsActifScreenState(actif);
}

class _DetailsActifScreenState extends State<DetailsActifScreen> {
  late final Actif compte;
  late List<String> transactions = [];

  //function to show the list of expense tasks
  Future getTransactions(compteId) async {
    //open the database
    await helper.openDb();
    transactions = []; //await helper.getDepensesFromList(compteId);
  }

  Future getData() async {
    return getTransactions(compte.id);
  }

  var f = StaticDate();
  List<TypeActif> typesActifs = StaticDate.typesActifs;

  late DbHelper helper;

  _DetailsActifScreenState(this.compte);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(compte.intitule),
          backgroundColor: const Color(0xfff3f3f3),
          foregroundColor: Colors.black,
          actions: [
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    child: const Icon(Icons.delete_outline),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => DeleteActif(
                                actif: compte,
                              )).then((value) => {
                            if (value == 'x')
                              {
                                Navigator.pop(context),
                                //print('Value is : ' + value)
                              }
                          });
                    },
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                    child: const Icon(Icons.edit),
                    onTap: () {
                      SnackBar snackBar =
                          const SnackBar(content: Text("Editer le bien"));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      //lancer le dialog
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AddActifForm(
                                  compte,
                                  compte.intitule,
                                  typesActifs[compte.typeActif - 1],
                                  false))).then((value) => getData());
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
        body: FutureBuilder(
            future: getData(), //helper.insertAnExpenseTask(), /,
            builder: (BuildContext context, snapshot) {
              // TODO: Add nested ListViews
              if (snapshot.connectionState == ConnectionState.done) {
                return Container(
                  child: Column(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 150,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                f.numF.format(compte.valeur),
                                style: GoogleFonts.lato(fontSize: 40.0),
                                //style: Theme.of(context).textTheme.headline2,
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                              const CircleImage(
                                imageProvider: AssetImage('assets/actif.jpg'),
                                imageRadius: 38,
                              )
                            ],
                          ),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.sticky_note_2_outlined),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                compte.intitule,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Intitulé',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.calendar_month),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                DateFormat('dd-MM-yyyy')
                                    .format(DateTime.parse(compte.created_at)),
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Date de création',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                );
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Color(0xFF21ca79),
                  ),
                );
              }
            }));
  }
}

class DeleteActif extends StatelessWidget {
  const DeleteActif({Key? key, required this.actif}) : super(key: key);
  final Actif actif;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer cet actif ?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            //Navigator.pop(context);
            await //helper.deleteDepense(depense.id);
                helper.deleteActif(actif.id);
            SnackBar snackBar =
                const SnackBar(content: Text('Actif supprimé !'));
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ],
    );
  }
}
