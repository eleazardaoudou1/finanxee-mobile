import 'package:finanxee/components/circle_image.dart';
import 'package:finanxee/components/dialogs/delete_compte_dialog.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:finanxee/screens/add_compte_autre.dart';
import 'package:finanxee/screens/add_compte_momo.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../add_compte_bancaire.dart';

class DetailsCompteBancaireScreen extends StatefulWidget {
  final Compte compteBancaire;
  const DetailsCompteBancaireScreen(this.compteBancaire);

  @override
  State<DetailsCompteBancaireScreen> createState() =>
      _DetailsCompteBancaireScreenState(compteBancaire);
}

class _DetailsCompteBancaireScreenState
    extends State<DetailsCompteBancaireScreen> {
  late final Compte compte;
  late List<String> transactions = [];

  //function to show the list of expense tasks
  Future getTransactions(compteId) async {
    //open the database
    await helper.openDb();
    transactions = []; //await helper.getDepensesFromList(compteId);
  }

  Future getData() async {
    return getTransactions(compte.id);
  }

  var f = StaticDate();

  late DbHelper helper;

  _DetailsCompteBancaireScreenState(this.compte);
  @override
  Widget build(BuildContext context) {
    final namesData = Provider.of<DataInit>(context);

    return Scaffold(
        appBar: AppBar(
          title: Text(compte.intitule),
          backgroundColor: const Color(0xfff3f3f3),
          foregroundColor: Colors.black,
          actions: [
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    child: const Icon(Icons.delete_outline),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              DeleteCompte(depense: compte)).then((value) => {
                            if (value == 'x')
                              {
                                Navigator.pop(context),
                                //print('Value is : ' + value)
                              }
                          });
                    },
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                    child: const Icon(Icons.edit),
                    onTap: () {
                      SnackBar snackBar =
                          const SnackBar(content: Text('Editer le compte'));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);

                      //mettre a jour le provider pour que le compte à afficher dans l'édition soit le compte
                      //sélectionné
                      namesData.updateCompteSelected(compte);
                      namesData.updateCompteSelected2(compte);
                      namesData.updateCompteSelected3(compte);

                      //lancer le dialog
                      if (compte.type == TypeCompte.BANK.index) {
                        //lancer le dialog
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    AddCompteBancaire(compte, false, false)));
                      } else if (compte.type == TypeCompte.MOMO.index) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    AddCompteMomo(compte, false)));
                      } else {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    AddCompteAutre(compte, false)));
                      }
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
        body: FutureBuilder(
            future: getData(), //helper.insertAnExpenseTask(), /,
            builder: (BuildContext context, snapshot) {
              // TODO: Add nested ListViews
              if (snapshot.connectionState == ConnectionState.done) {
                return Column(
                  children: [
                    Expanded(
                      child: SizedBox(
                        height: 150,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              f.numF.format(compte.solde),
                              style: GoogleFonts.lato(fontSize: 40.0),
                              //style: Theme.of(context).textTheme.headline2,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            CircleImage(
                              imageProvider: AssetImage(compte.imageProvider),
                              imageRadius: 38,
                            )
                          ],
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        IconButton(
                          icon: const Icon(Icons.sticky_note_2_outlined),
                          onPressed: () {},
                        ),
                        Expanded(
                          child: ListTile(
                            title: Text(
                              compte.intitule,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            subtitle: Text(
                              'Intitulé',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      child: Divider(
                        color: Color(0xFFD4D4D4),
                      ),
                    ),
                    Row(
                      children: [
                        IconButton(
                          icon: const Icon(Icons.calendar_month),
                          onPressed: () {},
                        ),
                        Expanded(
                          child: ListTile(
                            title: Text(
                              DateFormat('dd-MM-yyyy')
                                  .format(DateTime.parse(compte.created_at)),
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            subtitle: Text(
                              'Date de création',
                              style: Theme.of(context).textTheme.bodyMedium,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                );
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Color(0xFF21ca79),
                  ),
                );
              }
            }));
  }
}

class DeleteCompte extends StatefulWidget {
  const DeleteCompte({Key? key, required this.depense}) : super(key: key);
  final Compte depense;
  @override
  State<DeleteCompte> createState() => _DeleteCompteState();
}

class _DeleteCompteState extends State<DeleteCompte> {
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();
    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer ce compte ?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            //Navigator.pop(context);
            widget.depense.deleted_at = DateTime.now().toString();
            //await helper.deleteCompte(depense.id);
            await helper.insertCompte(widget.depense);
            SnackBar snackBar =
                const SnackBar(content: Text('Compte supprimé !'));
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ],
    );
  }
}

/*
class DeleteCompte extends StatelessWidget {
  const DeleteCompte({Key? key, required this.depense}) : super(key: key);
  final Compte depense;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return
      AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer ce compte ?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            //Navigator.pop(context);
            widget.depense.deleted_at = DateTime.now().toString();
            //await helper.deleteCompte(depense.id);
            await helper.insertCompte(depense);
            SnackBar snackBar =
                const SnackBar(content: Text('Compte supprimé !'));
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ],
    );
  }
}
*/
