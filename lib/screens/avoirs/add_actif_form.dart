import 'package:finanxee/components/dialogs/banque_list_dialog.dart';
import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/type_actif.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:finanxee/providers/tab_manager.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class AddActifForm extends StatefulWidget {
  final Actif actif;
  final String intitule;
  final TypeActif typeActif;
  final bool isNew;
  const AddActifForm(this.actif, this.intitule, this.typeActif, this.isNew);

  @override
  State<AddActifForm> createState() => _AddActifFormState(actif);
}

class _AddActifFormState extends State<AddActifForm> {
  Actif actif = Actif(0, 'intitule', 1, 120000, "description",
      DateTime.now().toString(), DateTime.now().toString());

  double solde2 = 0.0;
  _AddActifFormState(this.actif);

  final soldeController = TextEditingController();
  final intituleController = TextEditingController();

  DbHelper helper = DbHelper();
  int chosen_bank_id = 0;

  @override
  void initState() {
    // TODO: implement initState
    //compte1 = widget.actif;
    intituleController.text = widget.intitule;
    soldeController.text = widget.actif.valeur.toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final solde = Provider.of<DataInit>(context);

    //if (soldeController.text == null) soldeController.text = '0';
    //soldeController.text = solde.compte_bancaire_solde.toString();
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.intitule),
          backgroundColor: const Color(0xFF21ca79),
        ),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Container(
            child: Column(
              children: [
                Expanded(
                  child: Center(
                    child: SingleChildScrollView(
                      child: SizedBox(
                        height: MediaQuery.of(context).size.height / 2,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            buildSoldeField(),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                buildLabelField(),
                const SizedBox(
                  height: 15.0,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: const Color(0xFF21cA79)),
                  child: const Text('Ajouter'),
                  onPressed: () {
                    if (soldeController.text.isEmpty) {
                      SnackBar snackBar1 = const SnackBar(
                        content: Text("La valeur doit être définie"),
                        backgroundColor: Colors.red,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                      return;
                    } else if (intituleController.text.isEmpty) {
                      SnackBar snackBar2 = const SnackBar(
                        content: Text("Donnez un intitulé à cet actif."),
                        backgroundColor: Colors.red,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar2);
                      return;
                    }

                    // on ajoute un nouvel actif
                    actif.intitule = intituleController.text;
                    actif.valeur = double.parse(soldeController.text);
                    actif.typeActif = widget.typeActif.id;
                    actif.created_at = DateTime.now().toString();
                    actif.updated_at = DateTime.now().toString();

                    //compte1 = solde.compte_selected;
                    //compte1.solde = double.parse(soldeController.text);
                    //list.name = txtName.text;
                    helper.insertActif(actif);
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Navigator.pop(context);

                    Provider.of<TabManager>(context, listen: false)
                        .gotToAccount();

                    if (widget.isNew) {
                      SnackBar snackBar = const SnackBar(
                        content: Text('Actif ajouté avec succès !'),
                        backgroundColor: Colors.green,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    } else {
                      SnackBar snackBar = const SnackBar(
                        content: Text('Actif édité avec succès !'),
                        backgroundColor: Colors.green,
                      );
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  },
                )
              ],
            ),
          ),
        ));
  }

  Widget buildSoldeField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
// 3
        Text(
          'Valeur',
          style: GoogleFonts.lato(fontSize: 25.0),
        ),
// 4
        TextField(
// 5
          controller: soldeController,
          textAlign: TextAlign.center,
          style: GoogleFonts.lato(fontSize: 60.0),
// 6
          //cursorColor: _currentColor,
// 7
          decoration: InputDecoration(
// 8
            hintText: '1 000 000 ',
            hintStyle:
                GoogleFonts.lato(fontSize: 60.0, color: Color(0xFFC2C2C2)),
// 9
            enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD2D2D2)),
            ),
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD2D2D2)),
            ),
            border: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFD2D2D2)),
            ),
          ),
          keyboardType: const TextInputType.numberWithOptions(
              decimal: true, signed: false)
          /*    .number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ]*/
          , // Only numbers can be entered
        ),
      ],
    );
  }

  Widget buildLabelField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
// 3
        Text(
          "Intitulé de l'actif",
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: intituleController,
// 6
          //cursorColor: _currentColor,
// 7
          decoration: const InputDecoration(
// 8
            hintText: "Intitulé de l'actif",
// 9
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.black),
            ),
          ),
        ),
      ],
    );
  }
}
