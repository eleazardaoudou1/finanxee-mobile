import 'package:finanxee/models/actif.dart';
import 'package:finanxee/models/type_actif.dart';
import 'package:finanxee/screens/avoirs/add_actif_form.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';

class ActifsActionsScreen extends StatefulWidget {
  const ActifsActionsScreen({Key? key}) : super(key: key);

  @override
  State<ActifsActionsScreen> createState() => _ActifsActionsScreenState();
}

class _ActifsActionsScreenState extends State<ActifsActionsScreen> {
  TypeActif _typeActif = StaticDate.typesActifs[0];
  Actif actif = Actif(0, 'intitule', 1, 120000, "description",
      DateTime.now().toString(), DateTime.now().toString());

  List<TypeActif> typesActifs = StaticDate.typesActifs;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Gestion des actifs"),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 15.0),
        decoration: const BoxDecoration(
          shape: BoxShape.rectangle,
          color: Color(0xffffffff),
          border: Border(
              top: BorderSide(
            color: Colors.black,
            width: 1.0,
          )),
          //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, index) {
                      return Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 10.0, vertical: 10),
                              height: 40,
                              width: MediaQuery.of(context).size.width,
                              decoration:
                                  BoxDecoration(color: Colors.grey[200]),
                              child: Text(
                                typesActifs[index].intitule,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                            const SizedBox(
                              height: 5.0,
                            ),
                            ListView.separated(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (BuildContext context, index1) {
                                  String act = typesActifs[index]
                                      .actifs_elements[index1];
                                  return GestureDetector(
                                    onTap: () {
                                      //catData.updateSousCategoriesDepenseSelected(sCat);
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AddActifForm(
                                                      actif,
                                                      act,
                                                      typesActifs[index],
                                                      true)));
                                    },
                                    child: Container(
                                      padding:
                                          const EdgeInsets.only(left: 20.0),
                                      child: ListTile(
                                        title: Text(act),
                                      ),
                                    ),
                                  );
                                },
                                separatorBuilder:
                                    (BuildContext context, index) {
                                  return const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  );
                                },
                                itemCount:
                                    typesActifs[index].actifs_elements.length),
                            /* Container(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: ListTile(
                                title: Text("Maison"),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 5.0),
                              child: Divider(
                                color: Color(0xFFD4D4D4),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(left: 20.0),
                              child: ListTile(
                                title: Text("Maison"),
                              ),
                            ),*/
                          ],
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, index) {
                      return const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      );
                    },
                    itemCount: typesActifs.length),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
