import 'package:finanxee/components/facture_card.dart';
import 'package:finanxee/components/forms/facture_form.dart';
import 'package:finanxee/models/facture.dart';
import 'package:finanxee/screens/factures/empty_factures_list_ui.dart';
import 'package:finanxee/screens/factures/one_facture.dart';
import 'package:finanxee/services/facture_service.dart';
import 'package:finanxee/services/local_notification_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FacturesListUI extends StatefulWidget {
  const FacturesListUI({Key? key}) : super(key: key);

  @override
  State<FacturesListUI> createState() => _FacturesListUIState();
}

class _FacturesListUIState extends State<FacturesListUI> {
  List<Facture> facturesNonPayees = [];
  List<Facture> facturesPayees = [];
  List<Facture> facturesList = [];
  FactureService factureService = FactureService();
  double total = 0.0;
  var f = StaticDate();
  Facture facture = Facture(0, "intitule", 1000, "", 0, 0, "", '');

  DbHelper helper = DbHelper();
  late final LocalNotificationService _notificationService =
      LocalNotificationService();

  @override
  void initState() {
    //notification init
    _notificationService.setup();
    // TODO: implement initState
    getFacturesImpay();
    btn1Pressed();
    super.initState();
  }

  Future<List<Facture>> getFacturesImpay() async {
    final nonPayees =
        await helper.getFacturesNonPayees(); //get the list of factures
    var imp = await factureService.total_factures_impayees();
    final payees = await helper.getFacturesPayees(); //get the list of factures
    var pay = await factureService.total_factures_payees();
    //final factures = await helper.getFacturesLists(); //get the list of factures

    setState(() {
      //facturesList = factures;
      facturesList = nonPayees;
      facturesPayees = payees;
      total = imp;
    });

    return facturesList;
  }

  Future<List<Facture>> getFacturesPay() async {
    final payees =
        await helper.getFacturesPayeesNonArchivees(); //get the list of factures
    var pay = await factureService.total_factures_payees();
    //final factures = await helper.getFacturesLists(); //get the list of factures

    setState(() {
      facturesList = payees;
      total = pay;
    });
    return facturesList;
  }

  Future<List<Facture>> getFactureArchivees() async {
    final archivees =
        await helper.getFacturesArchivees(); //get the list of factures
    var pay = await factureService.total_factures_archivees();
    //final factures = await helper.getFacturesLists(); //get the list of factures

    setState(() {
      facturesList = archivees;
      total = pay;
    });
    return facturesList;
  }

  var btn1 = true;
  var btn2 = false;
  var btn3 = false;
  var btn4 = false;
  var title = "Impayées";

  void btn1Pressed() {
    setState(() {
      btn1 = true;
      btn2 = false;
      btn3 = false;
      btn4 = false;
      title = "Impayées";
    });
  }

  void btn2Pressed() {
    setState(() {
      btn1 = false;
      btn2 = true;
      btn3 = false;
      btn4 = false;
      title = "Payées";
    });
  }

  void btn3Pressed() {
    setState(() {
      btn1 = false;
      btn2 = false;
      btn3 = true;
      btn4 = false;
      title = "Payées";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          //leading: Icon(FlutterIcons.menu_fea),
          //centerTitle: true,
          title: const Text('Factures'),
          backgroundColor: const Color(0xFF21ca79),
          actions: [
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  /* GestureDetector(
                  child: const Icon(Icons.add),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FactureForm(
                                Facture(0, '', 0.0, DateTime.now().toString(),
                                    0, ''),
                                true)));
                  },
                )*/
                ],
              ),
            ),
          ],
        ),
        body: FutureBuilder(
            future: helper.getFacturesNonPayees(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      '${snapshot.error} occurred',
                      style: const TextStyle(fontSize: 18),
                    ),
                  );
                  // if we got our data
                } else if (snapshot.hasData) {
                  if (facturesList.isNotEmpty) {
                    return SingleChildScrollView(
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 5.0,
                          ),
                          buildWrapSection(context),
                          const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            child: Divider(
                              color: Color(0xFFD4D4D4),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 15.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  f.numF.format(total),
                                  style: GoogleFonts.lato(
                                      fontSize: 25.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Color(0xFFD8F1E7),
                                        offset: Offset(1, 1),
                                        blurRadius: 1,
                                        spreadRadius: 1)
                                  ]),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 16.0, vertical: 5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                  const SizedBox(
                                    height: 1.0,
                                  ),
                                  ListView.separated(
                                      shrinkWrap: true,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      itemBuilder:
                                          (BuildContext context, index) {
                                        //Account account = StaticDate.userAccounts[index];
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        OneFacture(facturesList[
                                                            index]))).then(
                                                (value) => {
                                                      getFacturesImpay(),
                                                      btn1Pressed()
                                                    });
                                            ;
                                          },
                                          child: buildDismissible(
                                              facturesList, index, context)

                                          /* Dismissible(
                                            key: Key(
                                                'facture ${facturesList[index]}'),
                                            direction:
                                                DismissDirection.horizontal,
                                            background: const ColoredBox(
                                              color: Color.fromRGBO(
                                                  43, 80, 108, 1),
                                              child: Align(
                                                alignment:
                                                    Alignment.centerRight,
                                                child: Padding(
                                                  padding:
                                                      EdgeInsets.all(16.0),
                                                  child: Icon(Icons.archive,
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                            onDismissed: (DismissDirection
                                                direction) async {
                                              if (direction ==
                                                  DismissDirection
                                                      .endToStart) {
                                                Facture fac =
                                                    facturesList[index];
                                                fac.archived = 1;
                                                await helper
                                                    .updateFacture(fac);
                                                initState();
                                                //get(expenseList1.id);

                                                SnackBar snackBar = SnackBar(
                                                  content: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceBetween,
                                                    children: [
                                                      const Text(
                                                          " 1 facture archivée."),
                                                      GestureDetector(
                                                        onTap: () async {
                                                          //setState(() {});
                                                        },
                                                        child: const Text(
                                                          "Annuler.",
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  backgroundColor:
                                                      const Color.fromRGBO(
                                                          43, 80, 108, 1),
                                                );
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(snackBar);
                                              }
                                            },
                                            */ /* confirmDismiss: (DismissDirection
                                                direction) {},*/ /*
                                            child: FactureCard(
                                                facturesList[index]),
                                          )*/
                                          ,
                                        );
                                      },
                                      separatorBuilder:
                                          (BuildContext context, index) {
                                        return const Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 5.0),
                                          child: Divider(
                                            color: Color(0xFFD4D4D4),
                                          ),
                                        );
                                      },
                                      itemCount: facturesList.length),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          //Expanded(child: Accounts())
                        ],
                      ),
                    );
                  } else {
                    return buildEmptyList(context);
                  }
                }
              }
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              );
            }),
        floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FactureForm(facture, true)))
                  .then((value) => {
                        getFacturesImpay(),
                        btn1Pressed(),
                        //create a notification
                        // _notificationService.addNotification()
                      });
            },
            tooltip: 'Ajouter une facture',
            child: const Icon(Icons.add),
            backgroundColor: const Color(0xFF21ca79)));
  }

  Widget buildWrapSection(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.start,
      spacing: 12,
      runSpacing: 12,
      children: [
        GestureDetector(
          onTap: () {
            getFacturesImpay();
            btn1Pressed();
            title = "Impayées";
          },
          child: Chip(
            label: const Text(
              "Impayées",
            ),
            backgroundColor: btn1 ? Colors.black.withOpacity(0.3) : null,
            //backgroundColor: Colors.black.withOpacity(0.3),
          ),
        ),
        GestureDetector(
          onTap: () {
            getFacturesPay();
            btn2Pressed();
            title = "Payées";
          },
          child: Chip(
            label: const Text(
              "Payées",
            ),
            backgroundColor: btn2 ? Colors.black.withOpacity(0.3) : null,
            //backgroundColor: Colors.black.withOpacity(0.3),
          ),
        ),
        GestureDetector(
          onTap: () {
            getFactureArchivees();
            btn3Pressed();
            title = "Archivées";
          },
          child: Chip(
            label: Text(
              "Archivées",
            ),
            backgroundColor: btn3 ? Colors.black.withOpacity(0.3) : null,
            //backgroundColor: Colors.black.withOpacity(0.3),
          ),
        ),
      ],
    );
  }

  Widget buildEmptyList(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(
            height: 5.0,
          ),
          buildWrapSection(context),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AspectRatio(
                    aspectRatio: 1 / 1,
                    child: Image.asset('assets/img-icons/facture-img.png')),
                const SizedBox(height: 8.0),
                const Text(
                  'Aucune facture ',
                  style: TextStyle(fontSize: 21.0),
                ),
                const SizedBox(height: 16.0),
                const Text(
                  'Vous n\'avez aucune facture pour l\'instant.\n'
                  'Appuyez sur le bouton + en bas de l\'écran pour ajouter',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget buildDismissible(
      List<Facture> facturesList, index, BuildContext context) {
    if (facturesList[index].completed == 0) {
      //facture non payee, le glissage permettra de marquer comme payée
      return Dismissible(
        key: Key('facture ${index}'),
        direction: DismissDirection.horizontal,
        background: const ColoredBox(
          color: Colors.green,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child:
                  Icon(Icons.check_circle_outline_sharp, color: Colors.white),
            ),
          ),
        ),
        secondaryBackground: const ColoredBox(
          color: Colors.red,
          child: Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Icon(Icons.delete_forever, color: Colors.white),
            ),
          ),
        ),
        onDismissed: (DismissDirection direction) async {
          if (direction == DismissDirection.endToStart) {
            //glisser de la droite vers la gauche, on supprime
            Facture fac = facturesList[index];
            fac.deleted_at = DateTime.now().toString();
            await helper.updateFacture(fac);
            getFacturesImpay();
            btn1Pressed();
            title = "Impayées";

            SnackBar snackBar = SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(" 1 facture supprimée."),
                  GestureDetector(
                    onTap: () async {
                      await helper.softRestoreFacture(fac.id);
                      getFacturesImpay();
                      //setState(() {});
                    },
                    child: const Text(
                      "Annuler.",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              backgroundColor: const Color.fromRGBO(43, 80, 108, 1),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            //glisser de la gauche vers la droite, on marque comme payee
            Facture fac = facturesList[index];
            fac.completed = 1;
            await helper.updateFacture(fac);
            getFacturesImpay();
            btn1Pressed();
            title = "Impayées";
            //get(expenseList1.id);

            SnackBar snackBar = SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(" 1 facture marquée comme payée."),
                  GestureDetector(
                    onTap: () async {
                      fac.completed = 0;
                      await helper.updateFacture(fac);
                      getFacturesImpay();
                    },
                    child: const Text(
                      "Annuler.",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              backgroundColor: const Color.fromRGBO(43, 80, 108, 1),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
        /* confirmDismiss: (DismissDirection
                                                  direction) {},*/
        child: FactureCard(facturesList[index]),
      );
    } else if (facturesList[index].completed == 1 &&
        facturesList[index].archived == 0) {
      //la facture est payée et non archivée
      return Dismissible(
        key: Key('facture${facturesList[index]}'),
        direction: DismissDirection.horizontal,
        background: ColoredBox(
          color: Color.fromRGBO(43, 80, 108, 1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.archive, color: Colors.white),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.archive, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
        onDismissed: (DismissDirection direction) async {
          //glisser de la droite vers la gauche permettra d'archiver
          //if (direction == DismissDirection.endToStart) {
          Facture fac = facturesList[index];
          fac.archived = 1;
          await helper.updateFacture(fac);
          getFacturesImpay();
          btn1Pressed();
          title = "Impayées";
          //get(expenseList1.id);

          SnackBar snackBar = SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(" 1 facture archivée."),
                GestureDetector(
                  onTap: () async {
                    fac.archived = 0;
                    await helper.updateFacture(fac);
                    initState();
                    //setState(() {});
                  },
                  child: const Text(
                    "Annuler.",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            backgroundColor: const Color.fromRGBO(43, 80, 108, 1),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          //}
        },
        /* confirmDismiss: (DismissDirection
                                                  direction) {},*/
        child: FactureCard(facturesList[index]),
      );
    } else {
      //la facture est archivee , on pourra supprimer quelque soit la direction
      return Dismissible(
        key: Key('facture ${facturesList[index]}'),
        direction: DismissDirection.horizontal,
        background: ColoredBox(
          color: Color.fromRGBO(255, 80, 108, 1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Align(
                alignment: Alignment.centerRight,
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.delete_forever, color: Colors.white),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(Icons.delete_forever, color: Colors.white),
                ),
              ),
            ],
          ),
        ),
        onDismissed: (DismissDirection direction) async {
          //glisser de la droite vers la gauche permettra d'archiver
          //if (direction == DismissDirection.endToStart) {
          Facture fac = facturesList[index];
          await helper.deleteFacture(fac.id);
          getFacturesImpay();
          btn1Pressed();
          title = "Impayées";
          //get(expenseList1.id);

          SnackBar snackBar = SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(" 1 facture supprimée."),
                GestureDetector(
                  onTap: () async {
                    fac.archived = 0;
                    await helper.updateFacture(fac);
                    initState();
                    //setState(() {});
                  },
                  child: const Text(
                    "Annuler.",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            backgroundColor: const Color.fromRGBO(184, 185, 186, 1.0),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
          //}
        },
        /* confirmDismiss: (DismissDirection
                                                  direction) {},*/
        child: FactureCard(facturesList[index]),
      );
    }
  }
}

//Not used
class FacturesListUI1 extends StatelessWidget {
  const FacturesListUI1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    late List<Facture> factureList = [];
    DbHelper helper = DbHelper();

    Future getFactures() async {
      factureList = await helper
          .getFacturesLists(); //get the shopping list from the helper
    }

    return FutureBuilder(
        future: getFactures(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {}
          return Scaffold(
            appBar: AppBar(
              //leading: Icon(FlutterIcons.menu_fea),
              //centerTitle: true,
              title: const Text('Factures'),
              backgroundColor: const Color(0xFF21ca79),
              actions: [
                Container(
                  padding: const EdgeInsets.symmetric(
                      vertical: 10.0, horizontal: 15.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                        child: const Icon(Icons.add),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => FactureForm(
                                      Facture(
                                          0,
                                          '',
                                          0.0,
                                          DateTime.now().toString(),
                                          0,
                                          0,
                                          '',
                                          ''),
                                      true)));
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
            body: Container(
              color: const Color(0xFFECECEC), // Colors.green[100],
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    const SizedBox(
                      height: 5.0,
                    ),

                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFD8F1E7),
                                  offset: Offset(1, 1),
                                  blurRadius: 1,
                                  spreadRadius: 1)
                            ]),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Non payées",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                ),
                                Text(
                                  factureList.length.toString(),
                                  style: Theme.of(context).textTheme.labelLarge,
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            /* Text(
                        "3.550",
                        style: Theme.of(context).textTheme.headlineSmall,
                      ),*/
                            const SizedBox(
                              height: 10.0,
                            ),
                            ListView.separated(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (BuildContext context, index) {
                                  //Account account = StaticDate.userAccounts[index];
                                  return FactureCard(factureList[index]);
                                },
                                separatorBuilder:
                                    (BuildContext context, index) {
                                  return const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  );
                                },
                                itemCount: factureList.length),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Container(
                        decoration: const BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFD8F1E7),
                                  offset: Offset(1, 1),
                                  blurRadius: 1,
                                  spreadRadius: 1)
                            ]),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16.0, vertical: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Payées",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                                ),
                                Text(
                                  "",
                                  style: Theme.of(context).textTheme.labelLarge,
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            ListView.separated(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemBuilder: (BuildContext context, index) {
                                  //Account account = StaticDate.userAccounts[index];
                                  return FactureCard(factureList[index]);
                                },
                                separatorBuilder:
                                    (BuildContext context, index) {
                                  return const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 5.0),
                                    child: Divider(
                                      color: Color(0xFFD4D4D4),
                                    ),
                                  );
                                },
                                itemCount: factureList.length),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    //Expanded(child: Accounts())
                  ],
                ),
              ),
            ),
          );
        });
  }
}
