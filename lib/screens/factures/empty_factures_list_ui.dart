import 'package:flutter/material.dart';

class EmptyFacturesList extends StatefulWidget {
  const EmptyFacturesList({Key? key}) : super(key: key);

  @override
  State<EmptyFacturesList> createState() => _EmptyFacturesListState();
}

class _EmptyFacturesListState extends State<EmptyFacturesList> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AspectRatio(
                aspectRatio: 1 / 1,
                child: Image.asset('assets/fooderlich_assets/empty_list.png')),
            const SizedBox(height: 8.0),
            const Text(
              'Aucune facture ',
              style: TextStyle(fontSize: 21.0),
            ),
            const SizedBox(height: 16.0),
            const Text(
              'Vous n\'avez aucune facture pour l\'instant.\n'
              'Appuyez sur le bouton + en haut de l\'écran pour en ajouter!',
              textAlign: TextAlign.center,
            ),
            /*MaterialButton(
                  textColor: Colors.white,
                  child: Text('Ajouter'),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  color: Colors.green,
                  onPressed: () {})*/
          ],
        ),
      ),
    );
  }
}
