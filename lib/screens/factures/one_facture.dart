import 'package:finanxee/components/dialogs/delete_facture_dialog.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../models/facture.dart';

class OneFacture extends StatefulWidget {
  late Facture facture;
  //const OneFacture({Key? key}) : super(key: key);
  OneFacture(this.facture);

  @override
  State<OneFacture> createState() => _OneFactureState(facture);
}

class _OneFactureState extends State<OneFacture> {
  //final Facture facture = new Facture(0, '', 0.0, '', 0, '');
  DeleteFactureDialog dialog =
      DeleteFactureDialog(Facture(0, '', 0.0, '', 0, 0, '', ''));
  String dropdownValue = '';
  List<String> list = [];
  List<String> list0 = <String>[
    "Marquer comme payée" /*, "Editer"*/,
    "Supprimer"
  ];
  List<String> list1 = <String>["Archiver", /*"Editer",*/ "Supprimer"];
  List<String> list2 = <String>[/*"Editer"*/ "Supprimer"];
  _OneFactureState(Facture facture);
  var f = StaticDate();

  @override
  void initState() {
    // TODO: implement initState
    //dialog = DeleteFactureDialog(this.facture);
    if (widget.facture.completed == 0) {
      setState(() {
        list = list0;
      });
    } else if (widget.facture.completed == 1 &&
        widget.facture.archived == 0) //payee et non archivee
    {
      setState(() {
        list = list1;
      });
    } else {
      setState(() {
        list = list2;
      });
    }
    dropdownValue = list.first;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //leading: Icon(FlutterIcons.menu_fea),
        //centerTitle: true,
        title: Text('Facture'),
        backgroundColor: const Color(0xFF21ca79),
        actions: [
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                DropdownButton<String>(
                  value: dropdownValue,
                  icon: const Icon(
                    Icons.more_vert,
                    color: Colors.white,
                    size: 25.0,
                  ),
                  elevation: 16,
                  //style: const TextStyle(color: Color(0xFF21ca79)),
                  focusColor: const Color(0xFF21ca79),
                  underline: Container(
                    height: 0,
                    color: Colors.deepPurpleAccent,
                  ),
                  selectedItemBuilder: (BuildContext context) {
                    //<-- SEE HERE
                    return list.map((String value) {
                      return Text(
                        dropdownValue,
                        style: const TextStyle(color: Color(0xFF21ca79)),
                      );
                    }).toList();
                  },
                  onChanged: (String? value) {
                    // This is called when the user selects an item.
                    setState(() {
                      dropdownValue = value!;
                    });

                    if (dropdownValue == "Marquer comme payée") {
                      dropdownValue == "Marquer comme payée";
                      payerFacture(widget.facture);
                      Navigator.pop(context);
                    } else if (dropdownValue == "Editer") {
                      dropdownValue == "Marquer comme payée";
                    } else if (dropdownValue == "Archiver") {
                      dropdownValue == "Archiver";
                      archiverFacture(widget.facture);
                    } else {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) =>
                              DeleteFacture(facture: widget.facture)).then(
                          (value) =>
                              {if (value == 'x') Navigator.pop(context)});
                    }
                  },
                  items: list.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                      onTap: () {
                        if (value == "Supprimer") {
                          GestureDetector(
                            child: const Icon(Icons.delete_outline),
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) =>
                                      DeleteFacture(facture: widget.facture));
                            },
                          );
                        }
                      },
                    );
                  }).toList(),
                ),
                /* GestureDetector(
                  child: const Icon(Icons.delete_outline),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) =>
                            DeleteFactureDialog(widget.facture));
                  },
                ),*/
              ],
            ),
          ),
        ],
      ),
      body: Container(
        color: const Color(0xFFECECEC), // Colors.green[100],
        padding: const EdgeInsets.symmetric(
          //horizontal: 16.0,
          horizontal: 10.0,
        ),
        child: Column(
          children: [
            Container(
              color: Colors.white,
              height: 300,
              padding: const EdgeInsets.symmetric(
                horizontal: 20.0,
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 150,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const SizedBox(
                            height: 10,
                          ),
                          Text(
                            widget.facture.intitule,
                            style: Theme.of(context).textTheme.titleLarge,
                            textAlign: TextAlign.center,
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const Divider(
                    color: Color(0xFFD4D4D4),
                  ),
                  Row(
                    children: [
                      Container(
                        color: Colors.white,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (widget.facture.completed == 0)
                              Text(
                                'Montant dû :',
                                style: Theme.of(context).textTheme.headline3,
                              ),
                            if (widget.facture.completed > 0)
                              Text(
                                'Montant Payé:',
                                style: Theme.of(context).textTheme.headline3,
                              ),
                          ],
                        ),
                      ),
                      const Spacer(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            f.numF.format(widget.facture.montant),
                            //widget.facture.montant.toString(),
                            style: Theme.of(context).textTheme.headline5,
                          ),
                        ],
                      )
                    ],
                  ),
                  const Divider(
                    color: Color(0xFFD4D4D4),
                  ),
                  if (widget.facture.completed > 0)
                    const Icon(
                      Icons.check_circle,
                      color: Colors.green,
                      size: 50.0,
                    ),
                  if (widget.facture.completed == 0)
                    const Icon(
                      Icons.pending,
                      color: Colors.red,
                      size: 50.0,
                    ),
                ],
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                color: Colors.white,
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        'A payer au plus tard le : ',
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Text(
                        DateFormat('dd-MM-yyyy')
                            .format(DateTime.parse(widget.facture.echeance)),
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<void> payerFacture(Facture facture) async {
    DbHelper helper = DbHelper();
    facture.completed = 1;
    await helper.updateFacture(facture);
    SnackBar snackBar = const SnackBar(
      content: Text('Facture marquée comme payée!'),
      backgroundColor: Colors.green,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  Future<void> archiverFacture(Facture facture) async {
    DbHelper helper = DbHelper();
    facture.archived = 1;
    await helper.updateFacture(facture);
    SnackBar snackBar = const SnackBar(
      content: Text('Facture archivée!'),
      backgroundColor: Colors.green,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}

class DeleteFacture extends StatelessWidget {
  const DeleteFacture({Key? key, required this.facture}) : super(key: key);
  final Facture facture;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer cette facture?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            await helper.deleteFacture(facture.id);
            SnackBar snackBar = const SnackBar(
              content: Text('Facture supprimée avec succès!'),
              backgroundColor: Colors.green,
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ],
    );
  }

  /**
   * Methode utilisee pour payer une facture
   */
}
