import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../navigation/finanxee_pages.dart';
import '../providers/app_state_manager.dart';
import '../utils/static_data.dart';

class ConditionsGenerales extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: FinanxeePages.countryListPath,
      key: ValueKey(FinanxeePages.termsAndConditionsPath),
      child: const ConditionsGenerales(),
    );
  }

  const ConditionsGenerales({Key? key}) : super(key: key);

  @override
  State<ConditionsGenerales> createState() => _ConditionsGeneralesState();
}

class _ConditionsGeneralesState extends State<ConditionsGenerales> {
  bool toShow = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Colors.transparent,
        backgroundColor: const Color(0xfff3f3f3),
        foregroundColor: Colors.black,
        title: const Text('Termes et Conditions'),
      ),
      backgroundColor: const Color(0xfff3f3f3),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
        child: ListView(
          children: [
            Text(
              "Conditions Générales d'Utilisation",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              " L’objet des CGU est de définir et encadrer l’utilisation des Services de l’application Finanxee par les Utilisateurs.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 50,
            ),
            Text(
              "2. Définitions",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              " Les termes suivants, lorsqu’ils sont employés avec une initiale en majuscule, ont la signification suivante ",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Application Finanxee ou Application :  Désigne l’application mobile éditée par Goshen Media Communication sous la marque Finanxee",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Autres actifs: Désigne tous les actifs financiers, produits d’épargne ou bancaires, incluant les crédits, consultables en ligne par l’Utilisateur au moyen de Données d’Accès émises par un Gestionnaire.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "CGU: Désignent les présentes conditions générales d’utilisation des Services Finanxee, conclues entre la société et l’Utilisateur.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Code secret: Désigne le code à 6 chiffres déterminé par l’Utilisateur dans le cadre de l’utilisation de certaines fonctionnalités et/ou pour ouvrir l’Application suivant les paramétrages choisis par l’Utilisateur.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Compte: Désigne tout compte de paiement tenu par un Gestionnaire de comptes.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Donnée(s): Désigne l’ensemble des données de l’Utilisateur. Cela comprend notamment les données liées aux Comptes et Autres actifs de l’Utilisateur, les données accessibles depuis les interfaces mises à disposition par le Gestionnaire, ou encore les données définies ci-dessous (Données d’Accès, Données d’Identification, Données Personnelles).",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Espace Personnel: Désigne l’espace dédié de l’Utilisateur dans l’Application Finanxee ou sur le Site.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Services Finanxee ou Services: Désignent l’ensemble des services proposés par Finanxee.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Services Premiums: Désignent les offres Finanxee Pro ou Finanxee + auxquelles l’Utilisateur peut souscrire depuis son Espace Personnel.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Site: Désigne www.finanxee.com",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Utilisateur: Désigne la personne physique ayant conclu les présentes CGU et détentrice des Données d’Accès.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "3. Conditions tarifaires.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "La Fonctionnalité Consultation sont mis à la disposition de l’utilisateur gratuitement. Les conditions tarifaires des Services Premiums seront disponibles et énoncées sur le site web officiel seront consultables à tout moment par l’Utilisateur depuis l’Espace Personnel",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "4. Souscription aux Services Finanxee",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "4.1. Création de l’Espace Personnel",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Le prospect qui souhaite souscrire aux Services Finanxee télécharge l’Application ou se rend sur le Site internet ou sur Playstore, crée des Données d’Identification, puis est invité à prendre connaissance des CGU. "
              "L’acceptation sans réserve par l’Utilisateur de l’ensemble des CGU se fait selon un processus permettant d’assurer l’intégrité de son consentement : l’Utilisateur est tout d’abord invité à lire les CGU dans leur ensemble, "
              "ensuite l’Utilisateur matérialise sa lecture et son acceptation en cliquant sur la case « j’ai lu et j’accepte les CGU » ; l’ensemble des stipulations des CGU lui sont opposables dès cet instant. "
              "Les CGU constituent, avec, le cas échéant, les annexes et d’éventuelles conditions particulières, les documents contractuels qui s’imposent à l’Utilisateur et constitue le contrat entre l’Utilisateur d’une part et Finanxee d’autre part dès son acceptation par "
              "l’Utilisateur (ci-après le « Contrat »). Si une ou plusieurs clauses de ces documents devaient être déclarées nulles, invalides ou sans effet, et pour quelque cause que ce soit, les autres clauses garderont toutefois leur plein effet, sauf dans "
              "l'hypothèse de la nullité d’une ou plusieurs clauses essentielles des CGU et/ou de l’interdépendance des stipulations contractuelles qui serait de nature à entrainer l’annulation de tout ou partie des CGU."
              "Les Données d’Identification renseignées par l’Utilisateur doivent rester strictement personnelles et confidentielles. L’Utilisateur est le seul responsable de l'utilisation de ses Données d’Identification",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "4.2. Modalités de connexion et déconnexion à l’Espace Personnel",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "L’Utilisateur se connecte au Site ou à l’Application par ses Données d’Identification. L’Utilisateur peut toutefois créer un Code secret qui lui permettra de se connecter à son Espace Personnel."
              "Lorsque l’Utilisateur est connecté, en l’absence de déconnexion manuelle par l’Utilisateur de son Espace Personnel, celui-ci reste connecté. Si l’Utilisateur se déconnecte manuellement du site ou de "
              "l’Application, il devra à nouveau saisir ses Données d’Identification pour accéder à nouveau à son Espace Personnel."
              "À tout moment, l’Utilisateur a la possibilité de supprimer des Données d’Accès. Dans ce cas, Perspecteev arrêtera de se connecter au Gestionnaire concerné et supprimera de son système d’information "
              "les Données d’Accès ainsi que toutes les informations consolidées qui y sont associées."
              "",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "4.3. Déclarations de l’Utilisateur",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "L’Utilisateur déclare et garantit Finanxee du fait que les éléments saisis par ses soins, principalement dans le cadre de la création de son Espace Personnel, "
              "correspondent effectivement à sa situation personnelle. Il s’engage à ce que ces éléments soient et demeurent exacts et actuels. Il s’engage en ce sens à les mettre à jour "
              "en cas de changement de ceux-ci."
              "L’Utilisateur déclare et garantit Finanxee du fait qu’il est titulaire des Données d’Accès renseignées en vue de l’utilisation des Services Finanxee ou qu’il a tous les pouvoirs "
              "et autorisations nécessaires pour autoriser Finanxee à lui fournir les Services Finanxee pour le compte de tiers (voir ci-dessous)."
              "Il déclare et garantit Finanxee d’être et demeurer seul et unique responsable de l’accès à l’Espace Personnel et de l’utilisation des Services Finanxee et qu’il a renseigné une "
              "adresse e-mail valide, dont il est propriétaire."
              "",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "4.5. Habilitation",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Lorsque l’Utilisateur souscrit aux présentes CGU pour les besoins de tierces personnes (y inclus de personnes morales), il certifie sur l’honneur être habilité et disposer de tous les pouvoirs et autorisations nécessaires "
              "pour souscrire le Contrat et autoriser Finanxee à lui fournir les Services Finanxee pour leur compte."
              "Dès lors, l’Utilisateur garantit entièrement Finanxee de toutes conséquences des réclamations émanant des tierces personnes (y compris morales) concernées portant sur cette habilitation et/ou en cas de fraude de l’Utilisateur."
              "",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "5. Services Premiums",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Les services premium seront bientôt disponibles et les conditions attachées seront dûment communiquées aux utilisateurs. ",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "6. Maintenance, suspension et disponibilité des Services",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "L’Utilisateur est informé qu’en cas de panne, de maintenance, ou de mise à jour des systèmes, l’accès à son Espace Personnel pourra être suspendu "
              "temporairement. Finanxee s’efforce de prévenir l’Utilisateur et fait ses meilleurs efforts en vue de "
              "rétablir l’accès aux Services dès que possible.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "7. Sécurité des Services",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "La sécurité des Données des Utilisateurs est l’absolue priorité de Finanxee et offre pour ce faire un très haut niveau de sécurité des Services Finanxee. Finanxee utilise à cette fin des algorithmes de chiffrement selon des standards les plus élevés et éprouvés en matière de sécurité informatique. Les Services Finanxee seront par ailleurs régulièrement audités par des sociétés indépendantes expertes en matière de sécurité informatique, elles-mêmes régulièrement mandatées par les banques et institutions financières de premier plan."
              "Les standards de sécurité des Services Finanxee sont ainsi parmi les plus élevés du marché, permettant ainsi à Finanxee de fournir la technologie Finanxee auprès de leaders mondiaux de services financiers et informatiques, lesquels procèdent également à un audit de la sécurité des Services."
              "Finanxee s’oblige en conséquence à faire tout son possible en vue d’assurer la sécurité de l’Espace Personnel et des Données des Utilisateurs."
              "Il est par ailleurs indiqué que l’Espace Personnel constitue un système de traitement automatisé de Données et que tout accès frauduleux à ce dernier ou toute altération dudit système est interdit et fera l’objet de poursuites pénales."
              "L’ensemble des Données, incluant les données sensibles, est conservé sous la responsabilité de Finanxee qui en garantit l’intégrité. Cependant, dans le cas où l’Utilisateur exporterait et stockerait de telles Données sur tout support auquel Finanxee est étranger, il a conscience que les Données circulant sur tout système d’information ne sont pas nécessairement protégées, notamment contre les détournements éventuels, ce qu’il reconnaît."
              "L’Utilisateur déclare en conséquence connaître la nature et les caractéristiques techniques des systèmes d’information et en accepte les contraintes techniques, les temps de réponse pour consulter, interroger ou transférer les Données relatives aux Services et les risques qu’ils comprennent."
              "L’Utilisateur est de ce fait seul responsable de l’utilisation et de la mise en œuvre de moyens de sécurité, de protection et de sauvegarde de ses équipements, Données et logiciels. A ce titre, il s’engage à prendre toutes les mesures appropriées de façon à les protéger."
              "L’Utilisateur s’engage en outre à ne commettre aucun acte qui pourrait mettre en cause la sécurité des systèmes d’information de Finanxee."
              "De façon générale, il est vivement recommandé à l’Utilisateur (i) de s’assurer que tout mot de passe choisi comprend à la fois des chiffres, des lettres en majuscules et en minuscules ainsi que des signes de ponctuation, de façon à être suffisamment complexe, et de le renouveler tous les trois mois, (ii) de mettre à jour les logiciels dont il dispose et qui permettent, directement ou indirectement, d’utiliser les Services en toute sécurité."
              "Il est fortement déconseillé à l’Utilisateur d’utiliser les Services sur un téléphone mobile ou un ordinateur dont il n’est pas propriétaire ou dont l’usage est partagé avec une tierce personne.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "8. Durée, modification et résiliation des CGU",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "8.1. Durée",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Les présentes CGU sont conclues pour une durée indéterminée à compter de leur acceptation par l'Utilisateur.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "8.2. Modification des CGU",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Finanxee se réserve la possibilité de modifier les présentes CGU, en vue notamment de les adapter aux évolutions de l’offre Finanxee ou à l'évolution de la législation en vigueur."
              "Finanxee communiquera tout projet de modification des CGU à l’Utilisateur sur support durable par e-mail (comportant un lien pour télécharger le projet en format PDF), au plus tard un (1) mois avant la date d'application proposée pour son entrée en vigueur."
              "L’Utilisateur est réputé avoir accepté la modification s'il n’a pas notifié à Finanxee, avant la date d'entrée en vigueur proposée de cette modification, qu'il ne l'acceptait pas."
              "Si l’Utilisateur refuse la modification, il peut résilier les CGU, sans frais, avant la date d'entrée en vigueur proposée des CGU modifiées selon les formes décrites ci-dessous."
              "Il en résulte que Finanxee ne pourra en aucun cas être tenue responsable d’un quelconque dommage résultant de la modification des CGU dès lors que l’Utilisateur s’abstient de résilier le Contrat et continue à utiliser les Services Finanxee après la date d’entrée en vigueur de la modification.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "8.3. Résiliation à l’initiative de l’Utilisateur",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "L’Utilisateur peut résilier de plein droit les CGU, à tout moment, à partir des paramètres de son Espace Personnel en cliquant sur l’option « Supprimer mon compte Finanxee », lorsque cette option sera disponible."
              "La résiliation des CGU entraine résiliation sans préavis des Services Premiums optionnels éventuellement souscrits par l’Utilisateur et ne donne droit à aucun remboursement à quelque titre que ce soit."
              "L’Utilisateur ne supporte pas de frais pour la résiliation des CGU.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "8.4. Résiliation à l’initiative de Finanxee",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Finanxee peut résilier les CGU moyennant un préavis d'au moins deux (2) mois. Dans ce cas et pour les Utilisateurs ayant souscrit aux Services Premium uniquement, Finanxee dédommagera l’Utilisateur concerné pour la période non utilisée sur une base prorata temporis."
              "En toute hypothèse, Perspecteev informe immédiatement l’Utilisateur de la résiliation des CGU par voie de courrier électronique ou par tout autre moyen",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "8.5. Conséquences de la résiliation",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "En cas de résiliation des CGU, Finanxee procède à la suppression dans ses systèmes d’information des Données d’Accès, Données d’Identification, Données Personnelles et de toute autre Donnée afférente à l’Utilisateur et à son "
              "Espace Personnel, à l’exception des informations qui doivent éventuellement être conservées par Finanxee au titre de ses obligations légales, notamment en matière de lutte contre le blanchiment de capitaux et le financement du terrorisme.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "9. Responsabilité de Finanxee",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Finanxee est responsable de la fourniture des Services Bankin’."
              "Finanxee ne peut en aucun cas être tenue responsable des dommages causés à l’Utilisateur résultant d'une cause extérieure non liée à la fourniture des Services Finanxee et indépendante de Finanxee."
              "La responsabilisé de Finanxee n’est pas engagée lorsque l’impossibilité de fournir correctement les Services Finanxee est due à un cas de force majeure."
              "Finanxee n’offre par ailleurs aucune garantie quant à ce que les Gestionnaires auprès desquels l’Utilisateur détient des Données d’Accès fassent partie de son offre.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "10. Responsabilité de l’Utilisateur",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "10.1. Utilisation des Services Finanxee",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "L’Utilisateur s’engage à utiliser les Services dans le respect des prescriptions des CGU ainsi que de la législation en vigueur."
              "En cas de non-respect d’une ou plusieurs dispositions et/ou stipulations des CGU, la responsabilité de l’Utilisateur pourra être engagée et celui-ci pourra être tenu d'indemniser Finanxee de toute conséquence qui pourrait en résulter."
              "L’Utilisateur est informé et accepte expressément que l'utilisation des Services Finanxee, des informations et des outils mis à sa disposition s'effectue sous sa propre responsabilité."
              "L'utilisateur accepte expressément et reconnaît que la réception ou le téléchargement de tout contenu obtenu à l'aide des Services Finanxee est effectué sous sa seule responsabilité et reste entièrement responsable de tous dégâts ou dommages qui pourraient être causés à ses systèmes informatiques et/ou de téléphonie mobile ainsi que, le cas échéant, de toute perte de données qui pourrait en résulter.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "10.2. Confidentialité des Données d’Identification",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Text(
              "Lorsque l’Utilisateur a connaissance de la perte, du vol, du détournement ou de toute utilisation non autorisée de son Espace Personnel ou de ses Données d’Identification, il en informe sans tarder Finanxee aux fins du blocage de son Espace Personnel."
              "A cet effet, l’Utilisateur cliquera sur le lien « mot de passe oublié ? » lorsque cette fonctionnalité sera disponible. Dans cette hypothèse, un courrier électronique contenant un lien sécurisé sera envoyé à l’Utilisateur en vue de changer son mot de passe, et l’ancien mot de passe sera alors automatiquement désactivé."
              "L’Utilisateur étant responsable de la préservation et de la confidentialité de son mot de passe, il assume toute responsabilité quant aux conséquences d’une divulgation dudit mot de passe à quiconque.",
              style:
                  GoogleFonts.lato(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 50,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Checkbox(
                    value: toShow,
                    onChanged: (value) {
                      value = !value!;
                      setState(() {
                        toShow = !toShow;
                      });
                    }),
                Expanded(
                  child: Text(
                    "J'ai lu et j'approuve !",
                    style: GoogleFonts.lato(fontSize: 18.0),
                  ),
                ),
                _showNextButton(context)
              ],
            )
          ],
        ),
      ),
    );
  }

  /// Showing this button when the user has checked "J'ai lu et j'approuve"
  Widget _showNextButton(BuildContext context) {
    if (toShow) {
      return ElevatedButton(
        onPressed: () async {
          final prefs = await SharedPreferences.getInstance();
          String termsAndConditionsCompletedStatus =
              StaticDate.termsAndConditionsCompletedStatus;
          prefs.setBool(termsAndConditionsCompletedStatus, true);
          Provider.of<AppStateManager>(context, listen: false)
              .completeTermsAndConditions();
        },
        child: const Text('Continuer'),
      );
    } else {
      return Container();
    }
  }
}
