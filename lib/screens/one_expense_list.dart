import 'package:finanxee/components/dialogs/expense_task_dialog.dart';
import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/models/expense_task.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';

import '../components/dialogs/expense_list_dialog.dart';
import '../components/expense_task_card.dart';

class OneExpenseList extends StatefulWidget {
  //const OneExpenseList({Key? key}) : super(key: key);
  final ExpenseList expenseList;
  OneExpenseList(this.expenseList);

  @override
  State<OneExpenseList> createState() => _OneExpenseListState();
}

class _OneExpenseListState extends State<OneExpenseList> {
  List<ExpenseTask> expenseTasks = [];
  bool toShow = false;
  int selectedIndex = 0;
  late ExpenseList expenseList1 = ExpenseList(0, "name", 0, "created_at", '');
  double listSum = 0.0;

  _OneExpenseListState();

  DbHelper helper = DbHelper();
  ExpenseTaskDialog dialog = ExpenseTaskDialog(
      ExpenseTask(0, 0, '', 0.0, DateTime.now().toString(), 0,
          DateTime.now().toString()),
      true,
      ExpenseList(0, '', 0, '', ''));
  ExpenseListDialog dialog_1 = ExpenseListDialog();
  var f = StaticDate();

  @override
  void initState() {
    dialog = ExpenseTaskDialog(
        ExpenseTask(0, widget.expenseList.id, '', 0.0,
            DateTime.now().toString(), 0, ''),
        true,
        widget.expenseList);

    //expenseTasks = await helper.getDepensesFromList(idList);
    getExpenses(widget.expenseList.id);
    super.initState();
  }

  //function to show the list of expense tasks
  Future<List<ExpenseTask>> getExpenses(idList) async {
    //var expenseTasks1 = await helper.getDepensesFromList(idList);
    var expenseTasks1 = await helper.getDepensesFromListUndeleted(idList);
    var sum = 0.0;
    expenseTasks1.forEach((element) {
      sum += element.price;
    });
    setState(() {
      expenseTasks = expenseTasks1;
      listSum = sum;
    });

    return expenseTasks;
  }

  @override
  Widget build(BuildContext context) {
    //getExpenses(this.expenseList.id);
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.expenseList.name),
        backgroundColor: const Color(0xFF21ca79),
        actions: [
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                  icon: const Icon(Icons.edit),
                  onPressed: () async {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => dialog_1.buildDialog(
                            context, widget.expenseList, false)).then((value) =>
                        {toShow = false, getExpenses(widget.expenseList.id)});
                  },
                ),
                GestureDetector(
                  child: const Icon(Icons.delete_outline),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) =>
                            DeleteExpenseListDialog(
                              list: widget.expenseList,
                            )).then((value) => {
                          if (value == 'x')
                            {
                              Navigator.pop(context, 'x'),
                              print('Value is : ' + value)
                            }
                        });
                  },
                ),
                const SizedBox(
                  width: 20,
                ),
                GestureDetector(
                  child: widget.expenseList.archived == 0
                      ? const Icon(Icons.archive)
                      : const Icon(Icons.unarchive),
                  onTap: () {
                    //lancer le dialog
                    showDialog(
                        context: context,
                        builder: (BuildContext context) =>
                            ArchiveExpenseListDialog(
                              list: widget.expenseList,
                              action:
                                  widget.expenseList.archived == 0 ? 'u' : 'r',
                            )).then((value) => {
                          if (value == 'r')
                            {
                              Navigator.pop(context, 'r'),
                              //print('Value is : ' + value)
                            }
                          else
                            {
                              Navigator.pop(context, 'r'),
                              //Navigator.pop(context, 'r'),
                            }
                        });
                  },
                ),
              ],
            ),
          ),
        ],
      ),
      body: FutureBuilder(
          future: helper.getDepensesFromList(
              widget.expenseList.id), //helper.insertAnExpenseTask(), /,
          builder: (BuildContext context, snapshot) {
            // TODO: Add nested ListViews
            if (snapshot.connectionState == ConnectionState.done) {
              if (expenseTasks.isNotEmpty) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 10.0),
                      child: SizedBox(
                        height: 30,
                        child: Text(
                          f.numF.format(listSum).toString(),
                          style: Theme.of(context).textTheme.headline2,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 10.0),
                      child: SizedBox(
                        height: 20,
                        child: Text(
                          widget.expenseList.name,
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                    ),
                    const Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 1.0, vertical: 0),
                      child: Divider(
                        color: Color(0xFFD4D4D4),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 1.0),
                        child: SingleChildScrollView(
                          child: ListView.separated(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: expenseTasks.length,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onLongPress: () {
                                  setState(() {
                                    toShow = true;
                                    selectedIndex = index;
                                  });
                                },
                                child: Container(
                                  foregroundDecoration:
                                      _foreG(toShow, index, selectedIndex),
                                  child: Dismissible(
                                    key: Key('item ${expenseTasks[index]}'),
                                    child: ExpenseTaskCard(expenseTasks[index]),
                                    background: const ColoredBox(
                                      color: Colors.green,
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                          padding: EdgeInsets.all(16.0),
                                          child: Icon(Icons.check_circle,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    secondaryBackground: const ColoredBox(
                                      color: Colors.red,
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Padding(
                                          padding: EdgeInsets.all(16.0),
                                          child: Icon(Icons.delete_forever,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    onDismissed:
                                        (DismissDirection direction) async {
                                      if (direction ==
                                          DismissDirection.endToStart) {
                                        //droite vers gauche, on supprime
                                        ExpenseTask task = expenseTasks[index];
                                        task.deleted_at =
                                            DateTime.now().toString();
                                        await helper.updateExpenseTask(task);
                                        getExpenses(widget.expenseList.id);

                                        SnackBar snackBar = SnackBar(
                                          content: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              const Text(
                                                  " 1 dépense supprimée."),
                                              GestureDetector(
                                                onTap: () async {
                                                  await helper
                                                      .softRestoreExpenseTask(
                                                          task);
                                                  getExpenses(
                                                      widget.expenseList.id);
                                                },
                                                child: const Text(
                                                  "Annuler.",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                            ],
                                          ),
                                          backgroundColor: Colors.grey,
                                        );
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(snackBar);
                                      } else {
                                        //on marque comme effectuee
                                        ExpenseTask task = expenseTasks[index];
                                        task.completed = 1;
                                        await helper.updateExpenseTask(task);
                                        getExpenses(widget.expenseList.id);

                                        SnackBar snackBar = SnackBar(
                                          content: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              const Text(
                                                  " 1 dépense marquée comme effectuée."),
                                              GestureDetector(
                                                onTap: () async {
                                                  task.completed = 0;
                                                  await helper
                                                      .updateExpenseTask(task);
                                                  getExpenses(
                                                      widget.expenseList.id);
                                                  //setState(() {});
                                                },
                                                child: const Text(
                                                  "Annuler.",
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                              ),
                                            ],
                                          ),
                                          backgroundColor: Colors.green,
                                        );
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(snackBar);
                                        //print(widget.expenseList.id.toString());
                                      }
                                    },

                                    /* confirmDismiss:
                                      (DismissDirection direction) async {
                                    expenseTasks.removeAt(index);
                                    SnackBar snackBar = SnackBar(
                                      content: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(" 1 dépense supprimée."),
                                          GestureDetector(
                                            onTap: () {
                                              Navigator.of(context).pop(false);
                                            },
                                            child: const Text(
                                              "Annuler.",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ],
                                      ),
                                      backgroundColor:
                                          Color.fromRGBO(43, 80, 108, 1),
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                              },*/
                                  ),
                                ),
                              );
                            },
                            separatorBuilder: (BuildContext context, index) {
                              return const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 1.0, vertical: 0),
                                child: Divider(
                                  color: Color(0xFFD4D4D4),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(height: 8.0),
                      Text(
                        'Aucune dépense ',
                        style: TextStyle(fontSize: 21.0),
                      ),
                      SizedBox(height: 16.0),
                      Text(
                        'Vous n\'avez aucune dépense dans cette liste pour l\'instant.'
                        'Appuyez sur le bouton + en bas pour ajouter!',
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                );
              }
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Color(0xFF21ca79),
                ),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
          onPressed: () async {
            showDialog(
                    context: context, builder: (BuildContext context) => dialog)
                .then((value) => getExpenses(widget.expenseList.id));
          },
          child: const Icon(Icons.add),
          backgroundColor: const Color(0xFF21ca79)),
      bottomNavigationBar: _showBottomBar(toShow),
    );
  }

  _showBottomBar(bool toShow1) {
    if (toShow1) {
      return BottomAppBar(
        color: const Color.fromRGBO(241, 241, 241, 1.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.edit),
              onPressed: () async {
                dialog.expenseTask = expenseTasks[selectedIndex];
                dialog.expenseList = widget.expenseList;
                dialog.isNew = false;
                showDialog(
                        context: context,
                        builder: (BuildContext context) => dialog)
                    .then((value) =>
                        {toShow = false, getExpenses(widget.expenseList.id)});
              },
            ),
            IconButton(
              icon: const Icon(Icons.delete_outline),
              onPressed: () async {
                await helper
                    .updateDeleteExpenseTask(expenseTasks[selectedIndex]);
                SnackBar snackBar = const SnackBar(
                  content: Text('Supprimé avec succès!'),
                  backgroundColor: Colors.green,
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                setState(() {
                  toShow = false;
                });
                getExpenses(widget.expenseList.id);
              },
            ),
            IconButton(
              icon: const Icon(Icons.arrow_back_outlined),
              onPressed: () {
                setState(() {
                  toShow = false;
                });

                getExpenses(widget.expenseList.id);
              },
            ),
          ],
        ),
      );
    } else {
      return Container(
        height: 1,
      );
    }
  }

  _foreG(bool toShow, int index, int selectedIndex) {
    if (toShow && selectedIndex == index) {
      return const BoxDecoration(
        color: Color.fromRGBO(0, 0, 0, 0.1),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      );
    }
    return const BoxDecoration(
      //color: Color.fromRGBO(0, 0, 0, 0.1),
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    );
  }
}

class DeleteExpenseListDialog extends StatelessWidget {
  final ExpenseList list;
  const DeleteExpenseListDialog({Key? key, required this.list})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer cette liste?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            //Navigator.pop(context);
            list.deleted_at = DateTime.now().toString();
            await helper.updateExpenseList(list);
            SnackBar snackBar = const SnackBar(
              content: Text('Supprimé avec succès!'),
              backgroundColor: Colors.green,
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ],
    );
  }

  Widget _showBottomBar(bool toShow) {
    if (toShow) {
      return BottomAppBar(
        color: const Color.fromRGBO(0, 203, 0, 1.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.archive),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.delete_outline),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.arrow_back_outlined),
              onPressed: () {},
            ),
          ],
        ),
      );
    } else {
      return Container(
        height: 1,
      );
    }
  }
}

class ArchiveExpenseListDialog extends StatelessWidget {
  final ExpenseList list;
  final String action;
  const ArchiveExpenseListDialog(
      {Key? key, required this.list, required this.action})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: list.archived == 0
          ? const Text("Archiver")
          : const Text("Désarchiver"),
      content: list.archived == 0
          ? const Text("Voulez-vous vraiment archiver cette liste?")
          : const Text("Voulez-vous vraiment désarchiver cette liste?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, list.archived == 0 ? 'r' : 'u');
            //Navigator.pop(context);
            list.archived == 0 ? list.archived = 1 : list.archived = 0;
            await helper.updateExpenseList(list);
            SnackBar snackBar = SnackBar(
              content: list.archived == 1
                  ? const Text('Archivée avec succès!')
                  : const Text('Désarchivée avec succès!'),
              backgroundColor: Colors.green,
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ],
    );
  }

  Widget _showBottomBar(bool toShow) {
    if (toShow) {
      return BottomAppBar(
        color: const Color.fromRGBO(0, 203, 0, 1.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.archive),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.delete_outline),
              onPressed: () {},
            ),
            IconButton(
              icon: Icon(Icons.arrow_back_outlined),
              onPressed: () {},
            ),
          ],
        ),
      );
    } else {
      return Container(
        height: 1,
      );
    }
  }
}
