import 'package:finanxee/components/dialogs/delete_revenu_dialog.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/models/payloads/details_depense_payload.dart';
import 'package:finanxee/models/revenu.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../components/forms/revenu_form.dart';

class DetailsRevenuScreen extends StatefulWidget {
  Revenu revenu;
  DetailsRevenuScreen(this.revenu);

  @override
  State<DetailsRevenuScreen> createState() => _DetailsRevenuScreenState(revenu);
}

class _DetailsRevenuScreenState extends State<DetailsRevenuScreen> {
  Revenu revenu = Revenu(0, '', 0, 0, 0.0, '', '', DateTime.now().toString(),
      DateTime.now().toString());
  Compte compte1 = Compte(0, TypeCompte.BANK.index, '', 0.0, '', 'APP',
      'assets/nsia_logo.png', '', '');
  _DetailsRevenuScreenState(this.revenu);
  TypeRevenu typeR = TypeRevenu(0, "intitule", "code", "author", "icon",
      "description", "created_at", "updated_at");

  var dateF = StaticDate().formatterAbbrMonth;
  var f = StaticDate();
  DbHelper helper = DbHelper();

  @override
  void initState() {
    // TODO: implement initState
    //getDepensesSubObject();
    super.initState();
  }

  Future<DetailsRevenuPayload> getRevenuSubObject() async {
    DetailsRevenuPayload revenuPayload =
        DetailsRevenuPayload(compte1, typeR, revenu);

    final compte2 = await helper.getCompteById(widget.revenu.compte);
    if (compte2 != null) {
      revenuPayload.compte = compte2!;
    }

    typeR = (await helper.getTypeRevenuById(widget.revenu.type_revenu))!;
    revenuPayload.typeRevenu = typeR;

    return revenuPayload;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //leading: Icon(FlutterIcons.menu_fea),
        //centerTitle: true,
        title: const Text('Détails revenu'),
        backgroundColor: const Color(0xFF21ca79),
        actions: [
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                GestureDetector(
                  child: const Icon(Icons.edit),
                  onTap: () async {
                    final result = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    RevenuForm(widget.revenu, false)))
                        .then((value) => getRevenuSubObject());
                  },
                ),
                const SizedBox(
                  width: 20,
                ),
                GestureDetector(
                  child: const Icon(Icons.delete_outline),
                  onTap: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) =>
                            DeleteRevenuDialog(widget.revenu)).then((value) => {
                          if (value == 'x')
                            {
                              Navigator.pop(context),
                            }
                        });
                  },
                ),
              ],
            ),
          ),
        ],
      ),
      body: FutureBuilder(
          future: getRevenuSubObject(),
          builder: (context, AsyncSnapshot<DetailsRevenuPayload> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(
                    '${snapshot.error} occurred',
                    style: const TextStyle(fontSize: 18),
                  ),
                );

                // if we got our data
              } else {
                DetailsRevenuPayload revenuPayload = snapshot.data!;
                return Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 200,
                        child: Center(
                          child: Text(
                            f.numF.format(widget.revenu.montant),
//                    widget.revenu.intitule,
                            style: GoogleFonts.lato(fontSize: 40.0),
                            textAlign: TextAlign.center,
                            //style: Theme.of(context).textTheme.headline2,
                          ),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          widget.revenu.intitule,
                          //f.numF.format(widget.revenu.montant),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        subtitle: Text(
                          'Intitulé',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: ListTile(
                              title: Text(
                                revenuPayload.compte.intitule,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Compte',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                          revenuPayload.compte.deleted_at != null
                              ? GestureDetector(
                                  onTap: () {
                                    print(revenuPayload.compte.deleted_at);
                                    SnackBar snackBar = const SnackBar(
                                      content: Text(
                                          'Ce compte est inexistant ou a été supprimé !'),
                                      backgroundColor: Colors.black,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                  },
                                  child: const Icon(
                                    Icons.warning,
                                    color: Colors.red,
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          typeR.intitule,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        subtitle: Text(
                          'Catégorie',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          dateF.format(DateTime.parse(widget.revenu.date)),
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        subtitle: Text(
                          'Date',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      ListTile(
                        title: Text(
                          revenu.description,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        subtitle: Text(
                          revenu.description.isEmpty
                              ? "Ajoutez une description"
                              : "Editer la description",
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return UpdateDescriptionDepense(
                                revenu: widget.revenu,
                              );
                            },
                          ).then((value) => setState(() {
                                widget.revenu.description = value.toString();
                              }));
                        },
                      ),
                    ],
                  ),
                );
              }
            }

            return const Center(
              child: CircularProgressIndicator(
                color: Colors.green,
              ),
            );
          }),
    );
  }

  String fromNumToMonthString(String month) {
    switch (month) {
      case "1":
        {
          return 'Jan';
        }
      case "2":
        {
          return 'Fev';
        }
      case "3":
        {
          return 'Mar';
        }
      case "4":
        {
          return 'Avr';
        }

      default:
        {
          return 'ok';
        }
    }
  }
}

class UpdateDescriptionDepense extends StatelessWidget {
  const UpdateDescriptionDepense({Key? key, required this.revenu})
      : super(key: key);
  final Revenu revenu;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();
    final descriptionDep = TextEditingController();
    if (revenu.description.isNotEmpty) {
      descriptionDep.text = revenu.description;
    }

    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.5,
      child: AlertDialog(
        title: const Text("Ajouter une description"),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: descriptionDep,
              decoration:
                  const InputDecoration(hintText: 'Ajouter une description'),
            ),
          ],
        ),
        actions: [
          TextButton(
            child: const Text("Annuler"),
            onPressed: () {
              Navigator.pop(context, 'n');
            },
          ),
          TextButton(
            child: const Text("Continuer"),
            onPressed: () async {
              revenu.description = descriptionDep.text;
              Navigator.pop(context, descriptionDep.text);
              //Navigator.pop(context);
              await helper.updateRevenu(revenu);
            },
          ),
        ],
      ),
    );
  }
}
