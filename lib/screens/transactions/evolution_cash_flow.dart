import 'package:finanxee/services/compe_service.dart';
import 'package:finanxee/services/revenu_service.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../components/cahflow_month_card.dart';
import '../../models/payloads/evolution_avoir_net.dart';
import '../../models/payloads/courbe_cashflow.dart';
import '../../models/payloads/stats_avoir_net.dart';
import '../../services/depense_service.dart';
import '../../utils/dbhelper.dart';
import '../../utils/static_data.dart';

class EvolutionCashFlow extends StatefulWidget {
  const EvolutionCashFlow({Key? key}) : super(key: key);

  @override
  State<EvolutionCashFlow> createState() => _EvolutionCashFlowState();
}

class _EvolutionCashFlowState extends State<EvolutionCashFlow> {
  //var firstOfMonth = DateTime(today.year, today.month - 1, 1).toString();
  //var lastOfMonth = DateTime(today.year, today.month, 0).toString();

  //passif
  double cash_hypotheque = 0;
  double cash_dettes = 0;

  DbHelper helper = DbHelper();
  var f = StaticDate();

  Future<CourbeCashFlow> getEvolution() async {
    DateTime today = DateTime.now();
    var firstOfCurrentMonth = DateTime(today.year, today.month, 1).toString();
    var lastOfMonth_1 = DateTime(today.year, today.month, 0).toString();
    var firstOfMonth_1 = DateTime(today.year, today.month - 1, 1).toString();

    var lastOfMonth_2 = DateTime(today.year, today.month - 1, 0).toString();
    var firstOfMonth_2 = DateTime(today.year, today.month - 2, 1).toString();

    var lastOfMonth_3 = DateTime(today.year, today.month - 2, 0).toString();
    var firstOfMonth_3 = DateTime(today.year, today.month - 3, 1).toString();

    var lastOfMonth_4 = DateTime(today.year, today.month - 3, 0).toString();
    var firstOfMonth_4 = DateTime(today.year, today.month - 4, 1).toString();

    var lastOfMonth_5 = DateTime(today.year, today.month - 4, 0).toString();
    var firstOfMonth_5 = DateTime(today.year, today.month - 5, 1).toString();

    double depenseCurrentMonth =
        await DepenseService().total_depense_current_month();
    double revenuCurrentMonth =
        await RevenuService().total_revenu_current_month();
    double cashFlowCurrentMonth = revenuCurrentMonth - depenseCurrentMonth;

    //month -1
    double depenseCurrentMonth_1 = await DepenseService()
        .total_depense_between(firstOfMonth_1, lastOfMonth_1);
    double revenuCurrentMonth_1 = await RevenuService()
        .total_revenu_between(firstOfMonth_1, lastOfMonth_1);
    double cashFlowCurrentMonth_1 =
        revenuCurrentMonth_1 - depenseCurrentMonth_1;

    //month - 2
    double depenseCurrentMonth_2 = await DepenseService()
        .total_depense_between(firstOfMonth_2, lastOfMonth_2);
    double revenuCurrentMonth_2 = await RevenuService()
        .total_revenu_between(firstOfMonth_2, lastOfMonth_2);
    double cashFlowCurrentMonth_2 =
        revenuCurrentMonth_2 - depenseCurrentMonth_2;

    //month - 3
    double depenseCurrentMonth_3 = await DepenseService()
        .total_depense_between(firstOfMonth_3, lastOfMonth_3);
    double revenuCurrentMonth_3 = await RevenuService()
        .total_revenu_between(firstOfMonth_3, lastOfMonth_3);
    double cashFlowCurrentMonth_3 =
        revenuCurrentMonth_3 - depenseCurrentMonth_3;

    //month - 4
    double depenseCurrentMonth_4 = await DepenseService()
        .total_depense_between(firstOfMonth_4, lastOfMonth_4);
    double revenuCurrentMonth_4 = await RevenuService()
        .total_revenu_between(firstOfMonth_4, lastOfMonth_4);
    double cashFlowCurrentMonth_4 =
        revenuCurrentMonth_4 - depenseCurrentMonth_4;

    //month - 5
    double depenseCurrentMonth_5 = await DepenseService()
        .total_depense_between(firstOfMonth_5, lastOfMonth_5);
    double revenuCurrentMonth_5 = await RevenuService()
        .total_revenu_between(firstOfMonth_5, lastOfMonth_5);
    double cashFlowCurrentMonth_5 =
        revenuCurrentMonth_5 - depenseCurrentMonth_5;

    CourbeCashFlow ev = CourbeCashFlow(
      cashFlowCurrentMonth,
      depenseCurrentMonth,
      revenuCurrentMonth,
      cashFlowCurrentMonth_1,
      depenseCurrentMonth_1,
      revenuCurrentMonth_1,
      cashFlowCurrentMonth_2,
      depenseCurrentMonth_2,
      revenuCurrentMonth_2,
      cashFlowCurrentMonth_3,
      depenseCurrentMonth_3,
      revenuCurrentMonth_3,
      cashFlowCurrentMonth_4,
      depenseCurrentMonth_4,
      revenuCurrentMonth_4,
      cashFlowCurrentMonth_5,
      depenseCurrentMonth_5,
      revenuCurrentMonth_5,
    );

    return ev;
  }

  final List<SalesData> chartData = [
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 5, 1), 33),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 4, 1), 35),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 3, 1), 35),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 2, 1), 35),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month - 1, 1), 35),
    SalesData(DateTime(DateTime.now().year, DateTime.now().month, 1), 35),
  ];
  late TooltipBehavior _tooltipBehavior;

  @override
  void initState() {
    _tooltipBehavior = TooltipBehavior(enable: true);
    super.initState();
  }

  void tool(TooltipArgs args) {
    args.locationX = 30;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
        future: getEvolution(),
        builder:
            (BuildContext context, AsyncSnapshot<CourbeCashFlow> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 14),
                ),
              );

              // if we got our data
            } else if (snapshot.hasData) {
              CourbeCashFlow? data = snapshot.data;

              List<SalesData> depenseChartData = [
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 5, 1),
                    data!.depense_current_month_5),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 4, 1),
                    data!.depense_current_month_4),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 3, 1),
                    data!.depense_current_month_3),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 2, 1),
                    data!.depense_current_month_2),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 1, 1),
                    data!.depense_current_month_1),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month, 1),
                    data!.depense_current_month),
              ];
              List<SalesData> revenuChartData = [
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 5, 1),
                    data!.revenu_current_month_5),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 4, 1),
                    data!.revenu_current_month_4),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 3, 1),
                    data!.revenu_current_month_3),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 2, 1),
                    data!.revenu_current_month_2),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 1, 1),
                    data!.revenu_current_month_1),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month, 1),
                    data!.revenu_current_month),
              ];
              List<SalesData> chartData = [
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 5, 1),
                    data!.current_month_5),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 4, 1),
                    data!.current_month_4),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 3, 1),
                    data!.current_month_3),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 2, 1),
                    data!.current_month_2),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month - 1, 1),
                    data!.current_month_1),
                SalesData(
                    DateTime(DateTime.now().year, DateTime.now().month, 1),
                    data!.current_month),
              ];

              return SingleChildScrollView(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        Text(
                          "Evolution du Cashflow",
                          style: GoogleFonts.lato(
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        SfCartesianChart(
                            primaryXAxis: DateTimeAxis(),
                            tooltipBehavior: _tooltipBehavior,
                            onTooltipRender: (TooltipArgs args) => tool(args),
                            onMarkerRender: (MarkerRenderArgs markerargs) {
                              if (markerargs.pointIndex == 2) {
                                markerargs.markerHeight = 20.0;
                                markerargs.markerWidth = 20.0;
                                markerargs.shape = DataMarkerType.triangle;
                              }
                            },
                            annotations: [
                              CartesianChartAnnotation(
                                  widget: Container(
                                      child: const Text('Annotation')),
                                  coordinateUnit: CoordinateUnit.point,
                                  region: AnnotationRegion.chart,
                                  x: 3,
                                  y: 60),
                            ],
                            series: <ChartSeries>[
                              // Renders line chart
                              /* LineSeries<SalesData, DateTime>(
                                  name: "Evolution du cashFlow",
                                  dataSource: chartData,
                                  xValueMapper: (SalesData sales, _) =>
                                      sales.date,
                                  yValueMapper: (SalesData sales, _) =>
                                      sales.sales),*/
                              LineSeries<SalesData, DateTime>(
                                  name: "Evolution des revenus",
                                  dataSource: revenuChartData,
                                  xValueMapper: (SalesData sales, _) =>
                                      sales.date,
                                  yValueMapper: (SalesData sales, _) =>
                                      sales.sales),
                              LineSeries<SalesData, DateTime>(
                                  name: "Evolution des dépenses",
                                  dataSource: depenseChartData,
                                  xValueMapper: (SalesData sales, _) =>
                                      sales.date,
                                  yValueMapper: (SalesData sales, _) =>
                                      sales.sales),
                            ]),
                        const SizedBox(
                          height: 10,
                        ),
                        /* Text(
                          "Evolution du Cashflow",
                          style: GoogleFonts.lato(
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 10,
                        ),*/
                        CashFlowMonthCard(
                          data!.current_month,
                          DateTime(
                              DateTime.now().year, DateTime.now().month, 1),
                          data!.depense_current_month,
                          data!.revenu_current_month,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CashFlowMonthCard(
                          data!.current_month_1,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 1, 1),
                          data!.depense_current_month_1,
                          data!.revenu_current_month_1,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CashFlowMonthCard(
                          data!.current_month_2,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 2, 1),
                          data!.depense_current_month_2,
                          data!.revenu_current_month_2,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CashFlowMonthCard(
                          data!.current_month_3,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 3, 1),
                          data!.depense_current_month_3,
                          data!.revenu_current_month_3,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CashFlowMonthCard(
                          data!.current_month_4,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 4, 1),
                          data!.depense_current_month_4,
                          data!.revenu_current_month_4,
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        CashFlowMonthCard(
                          data!.current_month_5,
                          DateTime(
                              DateTime.now().year, DateTime.now().month - 5, 1),
                          data!.depense_current_month_5,
                          data!.revenu_current_month_5,
                        ),
                      ],
                    )),
              );
            }
          }
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        },
      ),
    );
  }
}

class SalesData {
  SalesData(this.date, this.sales);
  final DateTime date;
  final double sales;
}
