import 'package:finanxee/components/cash_flow.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/screens/transactions/repartition_depenses.dart';
import 'package:finanxee/screens/transactions/repartition_revenus.dart';
import 'package:finanxee/services/depense_service.dart';
import 'package:finanxee/services/revenu_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CashFlowScreen extends StatefulWidget {
  const CashFlowScreen({Key? key}) : super(key: key);

  @override
  State<CashFlowScreen> createState() => _CashFlowScreenState();
}

class _CashFlowScreenState extends State<CashFlowScreen> {
  DbHelper helper = DbHelper();
  List<CategorieDepense> categories = [];
  List<Depense> depenses = [];
  List<Revenu> revenus = [];
  var f = StaticDate();
  DepenseService depenseService = DepenseService();
  RevenuService revenuService = RevenuService();

  double data = 0;
  double dataR = 0;
  /* late TabController tabController =
      TabController(initialIndex: 0, length: 3, vsync: this);*/

  @override
  void initState() {
    // TODO: implement initState
    getCategories();
    getDepenses();
    getData();
    getRevenus();
    //print('On est dans Cashflow');
    //tabController = TabController(length: 3, vsync: this, initialIndex: 0);
    super.initState();
  }

  Future<List<CategorieDepense>> getCategories() async {
    final cat = await helper.getCategoriesDepense(); //get the list of factures
    setState(() {
      //facturesList = factures;
      categories = cat;
    });
    return categories;
  }

  Future<List<Depense>> getDepenses() async {
    final dep = await helper.getDepensesOfMonth();
    setState(() {
      //facturesList = factures;
      depenses = dep;
    });
    return depenses;
  }

  //
  Future<double> getData() async {
    var total_dep_mnth = await depenseService.total_depense_current_month();
    setState(() {
      data = total_dep_mnth;
    });
    return total_dep_mnth;
  }

  Future<double> getRevenus() async {
    final reven = await helper.getRevenusOfMonth();

    var total_rev_mnth = await revenuService.total_revenu_current_month();
    setState(() {
      revenus = reven;
      dataR = total_rev_mnth;
    });
    //print("revenues : ");
    //print(dataR);
    return total_rev_mnth;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            const CashFlow(),
            const SizedBox(
              height: 16,
            ),
            Container(
                child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(43, 80, 108, 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xFFD8F1E7),
                              offset: Offset(1, 1),
                              blurRadius: 1,
                              spreadRadius: 1)
                        ]),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 20),
                    child: FutureBuilder(
                        future: helper.getDepensesOfMonth(),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.hasError) {
                              return Center(
                                child: Text(
                                  '${snapshot.error} occurred',
                                  style: const TextStyle(fontSize: 18),
                                ),
                              );

                              // if we got our data
                            } else if (snapshot.hasData) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const Text(
                                        "Dépenses",
                                        style: /*Theme.of(context)
                                            .textTheme
                                            .bodyLarge,*/
                                            TextStyle(
                                                color: Colors.white,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.w300),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      RepartitionDepenses()));
                                          SnackBar snackBar = const SnackBar(
                                              content: Text(
                                                  "Répartition des dépenses"));
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(snackBar);
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 10.0,
                                            vertical: 6.0,
                                          ),
                                          decoration: BoxDecoration(
                                            color: const Color(0xFF21ca79),
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                          ),
                                          child: const Center(
                                            child: Text(
                                              "Détails",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        f.numF.format(data),
                                        style: GoogleFonts.lato(
                                            fontSize: 25.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                ],
                              );
                            }
                          }
                          return const Center(
                            child: CircularProgressIndicator(
                              color: Colors.green,
                            ),
                          );
                        }),
                  ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Container(
                    decoration: const BoxDecoration(
                        color: Color.fromRGBO(0, 151, 111, 1.0),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [
                          BoxShadow(
                              color: Color(0xFFD8F1E7),
                              offset: Offset(1, 1),
                              blurRadius: 1,
                              spreadRadius: 1)
                        ]),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 16.0, vertical: 20),
                    child: FutureBuilder(
                        future: helper.getDepensesOfMonth(),
                        builder: (context, snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            if (snapshot.hasError) {
                              return Center(
                                child: Text(
                                  '${snapshot.error} occurred',
                                  style: const TextStyle(fontSize: 18),
                                ),
                              );

                              // if we got our data
                            } else if (snapshot.hasData) {
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Revenus",
                                        style: /*Theme.of(context)
                                          .textTheme
                                          .bodyLarge,*/
                                            TextStyle(
                                                color: Colors.white,
                                                fontSize: 16.0,
                                                fontWeight: FontWeight.w300),
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      RepartitionRevenus()));
                                          SnackBar snackBar = const SnackBar(
                                              content: Text(
                                                  "Répartition des revenus"));
                                          ScaffoldMessenger.of(context)
                                              .showSnackBar(snackBar);
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 10.0,
                                            vertical: 6.0,
                                          ),
                                          decoration: BoxDecoration(
                                            color: const Color(0xFF21ca79),
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                          ),
                                          child: const Center(
                                            child: Text(
                                              "Détails",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 10,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 20.0,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        f.numF.format(dataR),
                                        style: GoogleFonts.lato(
                                            fontSize: 25.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10.0,
                                  ),
                                ],
                              );
                            }
                          }
                          return const Center(
                            child: CircularProgressIndicator(
                              color: Colors.green,
                            ),
                          );
                        }),
                  ),
                ),
              ],
            )
                //Text(categories.length.toString()
                ),
          ],
        ),
      ),
    );
  }
}
