import 'dart:math';

import 'package:collection/collection.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class RepartitionRevenus extends StatefulWidget {
  const RepartitionRevenus({Key? key}) : super(key: key);
  //RepartitionRevenus();
  //List<Revenu> revenus = [];

  @override
  State<RepartitionRevenus> createState() => _RepartitionRevenusState();
}

class _RepartitionRevenusState extends State<RepartitionRevenus> {
  //late List<Revenu> revenus = [];

  //_RepartitionRevenusState();

  late List<TypeRevenu> types =
      []; // pour contenir les types de revenus identifiés dans les revenus
  late List<double> sumRevenus =
      []; // pour contenir dans l'ordre, le cumul des revenus par type de revenu
  DbHelper helper = DbHelper();
  List<TypeRevenu> types_ = [];
  List<double> revenusSum = [];
  List<Revenu> revenus = [];

  Future<List<Revenu>> getRevenus() async {
    final reven = await helper.getRevenusOfMonth();
    setState(() {
      revenus = reven;
    });
    return reven;
  }

  @override
  void initState() {
    // TODO: implement initState
    getTypes();
    super.initState();
  }

  //renvoie les types de revenus identifiés dans la liste des revenus
  Future<List<TypeRevenu>> getTypes() async {
    final reven = await helper.getRevenusOfMonth();
    setState(() {
      revenus = reven;
    });

    var newMap = groupBy(this.revenus, (Revenu d) {
      return d.type_revenu;
    });

    //print(depenses.first.toMap());

    for (var element in newMap.keys) {
      var cat = await helper.getTypeRevenuById(element);
      types_.add(cat!);
    }
    //print(newMap.values.elementAt(0));
    for (int i = 0; i < newMap.length; i++) {
      double s = 0;
      var t = newMap.values.elementAt(i);
      for (int j = 0; j < t.length; j++) {
        s += t.elementAt(j).montant;
      }
      revenusSum.add(s);
    }

    setState(() {
      types = types_;
    });

    setState(() {
      sumRevenus = revenusSum;
    });
    //print(sumDepenses);
    return types;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Details'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
          future: helper.getRevenus(),
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              return ListView(
                //scrollDirection: Axis.vertical,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  if (types.isNotEmpty)
                    showGraphics(context, types, sumRevenus),
                  if (types.isEmpty) noDataOnChart(),
                  const SizedBox(
                    height: 16,
                  ),
                ],
              );
            }
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.green,
              ),
            );
          }),
    );
  }

  Widget showGraphics(
      BuildContext context, List<TypeRevenu> categories, List<double> sum) {
    Random random = new Random();
    late List<_ChartData> data = [];
    late List<_ChartData> data1;
    late TooltipBehavior _tooltip;

    print(categories);
    data1 = [];
    for (int i = 0; i < categories.length; i++) {
      data.add(_ChartData(categories.elementAt(i).intitule, sum.elementAt(i)));
    }
    data1 = [
      _ChartData('CHN', 12),
      _ChartData('GER', 15),
      _ChartData('RUS', 30),
      _ChartData('BRZ', 6.4),
      _ChartData('IND', 54)
    ];
    _tooltip = TooltipBehavior(enable: true);
    int col_1 = random.nextInt(256);
    int col_2 = random.nextInt(256);
    int col_3 = random.nextInt(256);
    print(col_1);
    print(col_2);
    print(col_3);

    return Container(
      height: 400,
      color: Colors.transparent,
      child: ListView(
        //shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 1.5,
            child: SfCartesianChart(
                primaryXAxis: CategoryAxis(),
                primaryYAxis: NumericAxis(
                    minimum: 0, maximum: sum.max, interval: sum.max / 10),
                tooltipBehavior: _tooltip,
                title: ChartTitle(
                  text: "Répartition des revenus par catégorie",
                ),
                series: <ChartSeries<_ChartData, String>>[
                  BarSeries<_ChartData, String>(
                      dataSource: data,
                      xValueMapper: (_ChartData data, _) => data.x,
                      yValueMapper: (_ChartData data, _) => data.y,
                      name: 'Catégorie',
                      color: Color.fromRGBO(43, 80, 108, 1.0),
                      width: 0.5,
                      animationDuration: 2)
                ]),
          ),
        ],
      ),
    );
  }
}

Widget noDataOnChart() {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const [
        Text(
          "Aucune donnée disponible pour l'instant ",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 21.0),
        ),
        /* SizedBox(height: 16.0),
          Text(
            'Vous n\'avez pas encore ajouté des revenus pour cette période.\n Cliquez sur le bouton \n + pour ajouter',
            textAlign: TextAlign.center,
          ),*/
        SizedBox(height: 16.0),
      ],
    ),
  );
}

class _ChartData {
  _ChartData(this.x, this.y);
  final String x;
  final double y;
}

//algo de repartition des revenus
/**
 * 1- avoir la liste des revenus
 * 2- identifer les types de revenus concernes dans la liste
 * 3- calculer la somme des revenus pour chaque type de revenus dans la liste
 * 4- ranger cette somme dans un tableau avec comme index, le type de revenus
 *
 */
