import 'dart:math';

import 'package:finanxee/models/models.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import "package:collection/collection.dart";

import '../../models/payloads/stats_depenses_mois.dart';
import '../../utils/static_data.dart';

class RepartitionDepenses extends StatefulWidget {
  RepartitionDepenses();
  //final List<Depense> depenses;

  @override
  State<RepartitionDepenses> createState() => _RepartitionDepensesState();
}

class _RepartitionDepensesState extends State<RepartitionDepenses> {
  //late List<Depense> depenses = [];
  late List<CategorieDepense> categories = [];
  late List<double> sumDepenses = [];
  var f = StaticDate();

  _RepartitionDepensesState();
  @override
  void initState() {
    // TODO: implement initState
    //getCategories(widget.depenses);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
          future: getDepensesAll(),
          builder: (BuildContext context,
              AsyncSnapshot<List<StatsDepensesMois>> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              List<StatsDepensesMois>? stats = snapshot.data;
              return ListView(
                //scrollDirection: Axis.vertical,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  if (stats![0].categories.isNotEmpty)
                    showGraphics(
                        context: context, stats: stats![0], stats_1: stats![1]),
                  if (stats![0].categories.isEmpty) noDataOnChart(),
                  const SizedBox(
                    height: 16,
                  ),
                ],
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              );
            }
          }),
    );
  }

  Widget showGraphics(
      {required BuildContext context,
      required StatsDepensesMois stats,
      required StatsDepensesMois stats_1}) {
    Random random = Random();
    late List<_ChartData> data = [];
    late List<_ChartData> data_1 = [];
    late TooltipBehavior _tooltip;

    print(categories);
    //data1 = [];
    for (int i = 0; i < stats.categories.length; i++) {
      data.add(_ChartData(stats.categories.elementAt(i).intitule,
          stats.sumDepenses.elementAt(i)));
    }

    for (int i = 0; i < stats_1.categories.length; i++) {
      data_1.add(_ChartData(stats_1.categories.elementAt(i).intitule,
          stats_1.sumDepenses.elementAt(i)));
    }
    /*data1 = [
      _ChartData('Alimentation et Restaurant', 3000),
      _ChartData('GER', 1200),
      _ChartData('IND', 5450)
    ];*/
    _tooltip = TooltipBehavior(enable: true);
    int col_1 = random.nextInt(256);
    int col_2 = random.nextInt(256);
    int col_3 = random.nextInt(256);

    return Container(
      height: 400,
      color: Colors.transparent,
      child: ListView(
        //shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width * 1.5,
            child: SfCartesianChart(
                primaryXAxis: CategoryAxis(),
                primaryYAxis: NumericAxis(
                    minimum: 0,
                    maximum: stats.sumDepenses.max,
                    interval: stats.sumDepenses.max / 10),
                tooltipBehavior: _tooltip,
                title: ChartTitle(
                  text: "Répartition des dépenses par catégorie",
                ),
                series: <ChartSeries<_ChartData, String>>[
                  BarSeries<_ChartData, String>(
                      dataSource: data,
                      xValueMapper: (_ChartData data, _) => data.x,
                      yValueMapper: (_ChartData data, _) => data.y,
                      name: stats.intituleMois,
                      color: const Color.fromRGBO(43, 80, 108, 1.0),
                      width: 0.5,
                      animationDuration: 2),
                  BarSeries<_ChartData, String>(
                      dataSource: data_1,
                      xValueMapper: (_ChartData data1, _) => data1.x,
                      yValueMapper: (_ChartData data1, _) => data1.y,
                      name: stats_1.intituleMois,
                      color: const Color.fromRGBO(00, 250, 108, 1.0),
                      width: 0.5,
                      xAxisName: "Mois de Mai",
                      animationDuration: 2)
                ]),
          ),
        ],
      ),
    );
  }

  Widget noDataOnChart() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Text(
            "Aucune donnée disponible pour l'instant ",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 21.0),
          ),
          /* SizedBox(height: 16.0),
          Text(
            'Vous n\'avez pas encore ajouté des revenus pour cette période.\n Cliquez sur le bouton \n + pour ajouter',
            textAlign: TextAlign.center,
          ),*/
          SizedBox(height: 16.0),
        ],
      ),
    );
  }

  DbHelper helper = DbHelper();

  Future<StatsDepensesMois> getCategories() async {
    List<Depense> depenses = await helper.getDepensesOfMonth();
    var newMap = groupBy(depenses, (Depense d) {
      return d.categorie;
    });
    List<CategorieDepense> categories_ = [];
    List<double> depensesSum = [];

    for (var element in newMap.keys) {
      var cat = await helper.getCatById(element);
      categories_.add(cat!);
    }
    //print(newMap.values.elementAt(0));
    for (int i = 0; i < newMap.length; i++) {
      double s = 0;
      var t = newMap.values.elementAt(i);
      for (int j = 0; j < t.length; j++) {
        s += t.elementAt(j).montant;
      }
      depensesSum.add(s);
    }

    DateTime intituleD = DateTime(DateTime.now().year, DateTime.now().month, 1);
    String intitule = f.monthYear(intituleD);
    StatsDepensesMois stats =
        StatsDepensesMois(categories_, depensesSum, intitule);

    //print(sumDepenses);
    return stats;
  }

  //recuperer les donnees du mois -1
  Future<StatsDepensesMois> getCategories_1() async {
    List<Depense> depenses = await helper.getDepensesOfMonth_1();
    var newMap = groupBy(depenses, (Depense d) {
      return d.categorie;
    });
    List<CategorieDepense> categories_ = [];
    List<double> depensesSum = [];

    for (var element in newMap.keys) {
      var cat = await helper.getCatById(element);
      categories_.add(cat!);
    }
    //print(newMap.values.elementAt(0));
    for (int i = 0; i < newMap.length; i++) {
      double s = 0;
      var t = newMap.values.elementAt(i);
      for (int j = 0; j < t.length; j++) {
        s += t.elementAt(j).montant;
      }
      depensesSum.add(s);
    }
    DateTime intituleD =
        DateTime(DateTime.now().year, DateTime.now().month - 1, 1);
    String intitule = f.monthYear(intituleD);
    StatsDepensesMois stats =
        StatsDepensesMois(categories_, depensesSum, intitule);
    return stats;
  }

  Future<List<StatsDepensesMois>> getDepensesAll() async {
    List<StatsDepensesMois> data = [];
    var stat = await getCategories();
    var stat_1 = await getCategories_1();
    data.add(stat);
    data.add(stat_1);
    return data;
  }
}

class _ChartData {
  _ChartData(this.x, this.y);
  final String x;
  final double y;
}

/*class _ChartData1 {
  _ChartData1(this.x, this.y, this.z);
  final String x;
  final double y;
  final double z;
}*/
