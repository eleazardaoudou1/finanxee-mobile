import 'package:finanxee/components/bilan_factures.dart';
import 'package:finanxee/components/last_expense_list.dart';
import 'package:flutter/material.dart';

class AutresOperationsScreen extends StatefulWidget {
  const AutresOperationsScreen({Key? key}) : super(key: key);

  @override
  State<AutresOperationsScreen> createState() => _AutresOperationsScreenState();
}

class _AutresOperationsScreenState extends State<AutresOperationsScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFFE3FAF0), // Colors.green[100],
      child: SingleChildScrollView(
        child: Column(
          children: const [
            SizedBox(
              height: 5.0,
            ),
            BilanFactures(),
            SizedBox(
              height: 15,
            ),
            LastExpenseList(),
            SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}
