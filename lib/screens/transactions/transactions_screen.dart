import 'package:finanxee/components/cash_flow.dart';
import 'package:finanxee/components/forms/depense_form.dart';
import 'package:finanxee/models/categorie_depense.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/screens/transactions/depenses_screen.dart';
import 'package:finanxee/screens/transactions/repartition_depenses.dart';
import 'package:finanxee/screens/transactions/revenus_screen.dart';
import 'package:finanxee/screens/transactions/autres_operations_screen.dart';
import 'package:finanxee/services/depense_service.dart';
import 'package:finanxee/services/revenu_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'cashflow_screen.dart';

class TransactionsScreen extends StatefulWidget {
  const TransactionsScreen({Key? key}) : super(key: key);

  @override
  State<TransactionsScreen> createState() => _TransactionsScreenState();
}

class _TransactionsScreenState extends State<TransactionsScreen>
    with RestorationMixin {
  DbHelper helper = DbHelper();
  List<CategorieDepense> categories = [];
  List<Depense> depenses = [];
  var f = StaticDate();
  DepenseService depenseService = DepenseService();
  RevenuService revenuService = RevenuService();

  double data = 0;
  double dataR = 0;
  /* late TabController tabController =
      TabController(initialIndex: 0, length: 3, vsync: this);*/

  @override
  void initState() {
    // TODO: implement initState
    getCategories();
    getDepenses();
    getData();
    getRevenus();
    print('On est dans TransactionScreen');
    //tabController = TabController(length: 3, vsync: this, initialIndex: 0);
    super.initState();
  }

  Future<List<CategorieDepense>> getCategories() async {
    final cat = await helper.getCategoriesDepense(); //get the list of factures
    setState(() {
      //facturesList = factures;
      categories = cat;
    });
    return categories;
  }

  Future<List<Depense>> getDepenses() async {
    final dep = await helper.getDepensesOfMonth(); //get the list of factures
    setState(() {
      //facturesList = factures;
      depenses = dep;
    });
    return depenses;
  }

  //

  Future<double> getData() async {
    var total_dep_mnth = await depenseService.total_depense_current_month();
    setState(() {
      data = total_dep_mnth;
    });
    return total_dep_mnth;
  }

  Future<double> getRevenus() async {
    var total_rev_mnth = await revenuService.total_revenu_current_month();
    setState(() {
      dataR = total_rev_mnth;
    });
    return total_rev_mnth;
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xFF21ca79),
          flexibleSpace: SafeArea(
            child: TabBar(
              labelColor: Colors.white,
              indicatorColor: Color(0xFFFFFFFF),
              indicatorWeight: 5.0,
              tabs: [
                Tab(child: Text('Cashflow')),
                Tab(
                  child: Text('Dépenses'),
                ),
                Tab(
                  child: Text('Revenus'),
                ),
                Tab(
                  child: Text('Autres opérations'),
                ),
              ],
            ),
          ),
          actions: [],
        ),
        body: TabBarView(
          children: [
            const CashFlowScreen(),
            const DepenseScreen(),
            const RevenusScreen(),
            const AutresOperationsScreen()
          ],
        ),
      ),
      initialIndex: 0,
    );
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'transactions_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
  }
}
