import 'package:finanxee/components/depense_card.dart';
import 'package:finanxee/components/dialogs/date_range_dialog.dart';
import 'package:finanxee/components/forms/depense_form.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/screens/transactions/details_depense_screen.dart';
import 'package:finanxee/screens/transactions/stats_depenses.dart';
import 'package:finanxee/services/depense_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../models/payloads/depense_by_day.dart';
import 'evolution_cash_flow.dart';

class DepenseScreen extends StatefulWidget {
  const DepenseScreen({Key? key}) : super(key: key);
  @override
  State<DepenseScreen> createState() => _DepenseScreenState();
}

class _DepenseScreenState extends State<DepenseScreen> {
  List<String> list = <String>[
    "Aujourd'hui",
    "Cette Semaine",
    "Ce Mois",
    "Choisir Dates"
  ];

  DbHelper helper = DbHelper();
  List<Depense> depenseList = [];
  List<Depense> listD = [];
  List<Depense> depenseList1 = [];
  DepenseService depenseService = DepenseService();
  double total = 0.0;
  var f = StaticDate();
  String dropdownValue = '';
  String dateValueToShow = '';
  late List<_ChartData> chartData;
  late TooltipBehavior _tooltip;
  late ZoomPanBehavior _zoomPanBehavior;
  DepenseByDay depenseByDayF = DepenseByDay([], 0, [], []);
  final intituleController = TextEditingController();

  //  f.numF.format(depense.montant)
  @override
  void initState() {
    // TODO: implement initState
    //getDepensesToday();
    dropdownValue = list[1];
    List<DateTime> dates = f.getThisWeekIntervalleDates();
    dateValueToShow = '${dates[0].day} '
        '${f.monthYear(dates[0])} - '
        '${dates[1].day} '
        '${f.monthYear(dates[1])}';
    _tooltip = TooltipBehavior(enable: true);
    _zoomPanBehavior = ZoomPanBehavior(
        // Enables pinch zooming
        zoomMode: ZoomMode.xy,
        enablePanning: true,
        enablePinching: true);
    super.initState();
  }

  Future<DepenseByDay> getData(
      String dropdownvalue, BuildContext context, List<Depense> list) async {
    if (/*listD.isNotEmpty && */ intituleController.text.isNotEmpty) {
      print("List D length: ");
      print(listD.length);
      return getDepensesSearch(listD);
    } else if (dropdownValue == "Aujourd'hui") {
      return getDepensesToday();
    } else if (dropdownValue == "Cette Semaine") {
      return getDepensesWeek();
    } else if (dropdownValue == "Ce Mois") {
      return getDepenses();
    } else if (dropdownValue == "Choisir Dates") {
      return getDepensesBetween(depenseByDayF);
    } else {
      return getAllDepenses();
    }
  }

  Future<DepenseByDay> getDepensesBetween(DepenseByDay depenseByDay) async {
    return depenseByDay;
  }

  Future<DepenseByDay> getDepensesToday() async {
    DateTime date = DateTime.now();
    DateTime today_0 = DateTime(date.year, date.month, date.day);
    DateTime tomorrow_ = DateTime(date.year, date.month, date.day + 1);

    final depenses =
        await helper.getDepensesOfToday(); //get the list of factures
    var total1 = await depenseService.total_depense_today();

    DepenseByDay depenseByDay = DepenseByDay([], 0, [], []);
    depenseByDay =
        await depenseService.getDepensesFilterByDay(today_0, tomorrow_);

    print(depenseByDay.depensesSum);
    depenseByDay.depenses = depenses;
    depenseByDay.total = total1;

    return depenseByDay;
  }

  Future<DepenseByDay> getDepensesWeek() async {
    DateTime date = DateTime.now();
    DateTime today_0 = DateTime(date.year, date.month, date.day);
    DateTime start_of_week =
        today_0.subtract(Duration(days: today_0.weekday - 1));
    DateTime end_of_week =
        today_0.add(Duration(days: DateTime.daysPerWeek - today_0.weekday));

    final depenses =
        await helper.getDepensesOfWeek(); //get the list of factures
    var total1 = await depenseService.total_depense_current_week();
    /*
    setState(() {
      depenseList = depenses;
      total = total1;
    });
    */

    DepenseByDay depenseByDay = DepenseByDay([], 0, [], []);
    depenseByDay =
        await depenseService.getDepensesFilterByDay(start_of_week, end_of_week);
    depenseByDay.depenses = depenses;
    depenseByDay.total = total1;

    return depenseByDay;
  }

  Future<DepenseByDay> getAllDepenses() async {
    DateTime date = DateTime.now();
    final depenses = await helper.getDepenses(); //get the list of factures
    var total1 = await depenseService.total_depenses();

    /*setState(() {
      depenseList = depenses;
      total = total1;
    });*/
    DepenseByDay depenseByDay = DepenseByDay([], 0, [], []);
    depenseByDay = await depenseService.getDepensesFilterByDay(date, date);
    depenseByDay.depenses = depenses;
    depenseByDay.total = total1;

    return depenseByDay;
  }

  Future<DepenseByDay> getDepenses() async {
    DateTime today = DateTime.now();
    var firstOfMonth = DateTime(today.year, today.month, 1).toString();
    var lastOfMonth = DateTime(today.year, today.month + 1, 0).toString();

    final depenses =
        await helper.getDepensesOfMonth(); //get the list of factures
    var total1 = await depenseService.total_depense_current_month();

    /* setState(() {
      depenseList = depenses;
      total = total1;
    });*/

    DepenseByDay depenseByDay = DepenseByDay([], 0, [], []);
    depenseByDay = await depenseService.getDepensesFilterByDay(
        DateTime(today.year, today.month, 1),
        DateTime(today.year, today.month + 1, 0));
    depenseByDay.depenses = depenses;
    depenseByDay.total = total1;

    return depenseByDay;
  }

  Future<DepenseByDay> getDepensesSearch(List<Depense> list) async {
    final depenses = list;
    //await helper.getDepensesOfMonth(); //get the list of factures
    var total1 = await depenseService.totalDepensesFromList(depenses);

    DepenseByDay depenseByDay = DepenseByDay([], 0, [], []);
    /* depenseByDay = await depenseService.getDepensesFilterByDay(
        DateTime(today.year, today.month, 1),
        DateTime(today.year, today.month + 1, 0));*/
    depenseByDay.depenses = depenses;
    depenseByDay.total = total1;

    return depenseByDay;
  }

  var btn1 = true;
  var btn2 = false;
  var btn3 = false;
  var btn4 = false;
  var title = "Aujourd'hui";

  void btn1Pressed() {
    setState(() {
      btn1 = true;
      btn2 = false;
      btn3 = false;
      btn4 = false;
      title = "Aujourd'hui";
    });
  }

  void btn2Pressed() {
    setState(() {
      btn1 = false;
      btn2 = true;
      btn3 = false;
      btn4 = false;
      title = "Cette semaine";
    });
  }

  void btn3Pressed() {
    setState(() {
      btn1 = false;
      btn2 = false;
      btn3 = true;
      btn4 = false;
      title = "Ce mois";
    });
  }

  void btn4Pressed() {
    setState(() {
      btn1 = false;
      btn2 = false;
      btn3 = false;
      btn4 = true;
      title = "Toutes";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
          future: getData(dropdownValue, context, listD),
          builder: (context, AsyncSnapshot<DepenseByDay> snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Center(
                  child: Text(
                    '${snapshot.error} occurred',
                    style: const TextStyle(fontSize: 18),
                  ),
                );

                // if we got our data
              } else if (snapshot.hasData) {
                DepenseByDay? data = snapshot!.data;
                depenseList = data!.depenses;
                if (intituleController.text.isEmpty) {
                  depenseList1 = data!.depenses;
                }
                total = data!.total;
                chartData = [];
                for (int i = 0; i < data!.depensesSum.length; i++) {
                  chartData.add(_ChartData(
                      data!.days[i].day.toString(), data!.depensesSum[i]));
                }
                var max = data!.depensesSum;
                max.sort();

                return SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 5.0, horizontal: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: Text(
                                dateValueToShow,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                              ),
                            ),
                            DropdownButton<String>(
                              value: dropdownValue,
                              icon: const Icon(
                                Icons.arrow_drop_down,
                                color: Colors.green,
                                size: 25.0,
                              ),
                              elevation: 16,
                              //style: const TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 0,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (String? value) async {
                                // This is called when the user selects an item.

                                /* setState(() {
                                  dropdownValue = value!;
                                });*/

                                if (value == "Aujourd'hui") {
                                  DateTime date = DateTime.now();
                                  dateValueToShow = '${date.day} '
                                      '${f.monthYear(date)}';
                                  setState(() {
                                    dropdownValue = value!;
                                  });
                                  getDepensesToday();
                                } else if (value == "Cette Semaine") {
                                  List<DateTime> dates =
                                      f.getThisWeekIntervalleDates();
                                  dateValueToShow = '${dates[0].day} '
                                      '${f.monthYear(dates[0])} - '
                                      '${dates[1].day} '
                                      '${f.monthYear(dates[1])}';

                                  setState(() {
                                    dropdownValue = value!;
                                  });
                                  getDepensesWeek();
                                } else if (value == "Ce Mois") {
                                  List<DateTime> dates =
                                      f.getThisMonthIntervalleDates();
                                  dateValueToShow = '${dates[0].day} '
                                      '${f.monthYear(dates[0])} - '
                                      '${dates[1].day} '
                                      '${f.monthYear(dates[1])}';

                                  setState(() {
                                    dropdownValue = value!;
                                  });
                                  getDepenses();
                                } /*else if (value == "Toutes") {
                                  setState(() {
                                    dropdownValue = value!;
                                  });
                                  getAllDepenses();
                                }*/
                                else if (value == "recherche") {
                                  setState(() {
                                    dropdownValue = value!;
                                  });
                                  //dropdownValue == "recherche"
                                } else if (value == "Choisir Dates") {
                                  DepenseByDay depenseByDay =
                                      DepenseByDay([], 0, [], []);

                                  List<Depense> depenses = [];
                                  double total1 = 0;

                                  await showDialog(
                                      context: context,
                                      builder: (BuildContext context) =>
                                          const DateRangeDialog(
                                              'D')).then((valueR) async => {
                                        dateValueToShow = '${valueR[1].day} '
                                            '${f.monthYear(valueR[1])} - '
                                            '${valueR[2].day} '
                                            '${f.monthYear(valueR[2])}',

                                        depenses = valueR[0],

                                        total1 = await depenseService
                                            .totalDepensesFromList(depenses),

                                        depenseByDay = await depenseService
                                            .getDepensesFilterByDay(
                                                valueR[1], valueR[2]),
                                        depenseByDay.depenses = depenses,
                                        depenseByDay.total = total1,
                                        //print(depenseByDay.depensesSum);
                                        depenseByDay.depenses = depenses,
                                        depenseByDay.total = total1,
                                        setState(() {
                                          dropdownValue = value!;
                                          depenseByDayF = depenseByDay;
                                        }),
                                        print("DepensesF depenses show = "),
                                        print(depenseByDayF.depenses.length),
                                      });
                                  //getDepensesBetween(depenseByDay);
                                }
                              },
                              items: list.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                      ),

                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      if (depenseList.isNotEmpty)
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                f.numF.format(total),
                                style: GoogleFonts.lato(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),

                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width * 2,
                          child: SfCartesianChart(
                              zoomPanBehavior: _zoomPanBehavior,
                              primaryXAxis: CategoryAxis(
                                  //maximumLabels: data!.depensesSum.length
                                  ),
                              primaryYAxis: NumericAxis(
                                  minimum: 0,
                                  maximum: max.isNotEmpty ? max.last : 10,
                                  interval: (max.isNotEmpty && max.last > 0)
                                      ? max.last / 10
                                      : 10),
                              tooltipBehavior: _tooltip,
                              series: <ChartSeries<_ChartData, String>>[
                                ColumnSeries<_ChartData, String>(
                                    dataSource: chartData,
                                    xValueMapper: (_ChartData data, _) =>
                                        data.x,
                                    yValueMapper: (_ChartData data, _) =>
                                        data.y,
                                    name: 'Dépenses',
                                    color: const Color.fromRGBO(8, 142, 255, 1))
                              ]),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            const Icon(
                              Icons.arrow_circle_right,
                              color: Colors.blueAccent,
                              size: 20.0,
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              StatsDepenses(depenseList)));
                                },
                                child: const Text("Statistiques")),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Container(
                          decoration: const BoxDecoration(
                              color: Color(0xFFEFEFEF),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20.0)),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFFD0CECE),
                                    offset: Offset(1, 1),
                                    blurRadius: 1,
                                    spreadRadius: 1)
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: TextField(
                                      onChanged: (text) async {
                                        //print('First text field: $text');
                                        List<Depense> revenus = await helper
                                            .searchStringInDepense(text);
                                        var total1 = await depenseService
                                            .totalDepensesFromList(revenus);
                                      },
                                      controller: intituleController,
                                      //cursorColor: _currentColor,
                                      decoration: const InputDecoration(
                                        hintText: "Taper un mot-clé ",
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      depenseList = depenseList1
                                          .where((x) => x.intitule.contains(
                                              intituleController.text))
                                          .toList();
                                      var total1 = await depenseService
                                          .totalDepensesFromList(depenseList);
                                      setState(() {
                                        depenseList = depenseList;
                                        total = total1;
                                        dropdownValue == "recherche";
                                        listD = depenseList;
                                      });
                                      print("DepenseList length : ");
                                      print(depenseList.length);
                                    },
                                    child: const Icon(
                                      Icons.search,
                                      color: Colors.grey,
                                      size: 25.0,
                                    ),
                                  ),
                                ]),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              boxShadow: [
                                BoxShadow(
                                    color: Color(0xFFD8F1E7),
                                    offset: Offset(1, 1),
                                    blurRadius: 1,
                                    spreadRadius: 1)
                              ]),
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 5),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(
                                height: 1.0,
                              ),
                              ListView.separated(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemBuilder: (BuildContext context, index) {
                                    //Account account = StaticDate.userAccounts[index];
                                    return GestureDetector(
                                      onTap: () async {
                                        Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DetailsDepenseScreen(
                                                            depenseList[
                                                                index])))
                                            .then((value) => {
                                                  setState(() {
                                                    dropdownValue = list[1];
                                                  })
                                                });
                                      },
                                      child: DepenseCard(depenseList[index]),
                                    );
                                  },
                                  separatorBuilder:
                                      (BuildContext context, index) {
                                    return const Padding(
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 5.0),
                                      child: Divider(
                                        color: Color(0xFFD4D4D4),
                                      ),
                                    );
                                  },
                                  itemCount: depenseList.length),
                              if (depenseList.isEmpty)
                                noDepenseScreen(
                                    context, dropdownValue, dateValueToShow),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),

                      //Expanded(child: Accounts())
                    ],
                  ),
                );
              }
            }

            return const Center(
              child: CircularProgressIndicator(
                color: Colors.green,
              ),
            );
          }),
      //floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
          onPressed: () async {
            final result = await Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DepenseForm(
                        Depense(0, '', '', 0, 0, 0, 0.0,
                            DateTime.now().toString(), '', ''),
                        true))).then((value) => {
                  setState(() {
                    dropdownValue = list[1];
                  }),
                  //getData(dropdownValue, context),
                });
          },
          child: const Icon(Icons.add),
          backgroundColor: const Color(0xFF21ca79)),
    );
  }
}

Widget noDepenseScreen(
    BuildContext context, String text, String dateValueToShow) {
  return Padding(
    padding: const EdgeInsets.all(30),
    child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AspectRatio(
              aspectRatio: 1 / 1,
              child: Image.asset('assets/img-icons/depense-2-img.png')),
          const SizedBox(height: 8.0),
          Text(
            'Aucune dépense pour ${dateValueToShow} ',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 21.0),
          ),
          const SizedBox(height: 16.0),
          Text(
            'Vous n\'avez pas encore ajouté des dépenses ${text}.\n Cliquez sur le bouton \n + pour ajouter',
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 16.0),
        ],
      ),
    ),
  );
}

class _ChartData {
  _ChartData(this.x, this.y);

  final String x;
  final double y;
}
/*
IndexedStack(
index: currentTab.index,
children: const [TodayTab(), MonthTab()],
),*/
