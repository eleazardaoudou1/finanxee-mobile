import 'package:finanxee/components/forms/revenu_form.dart';
import 'package:finanxee/models/revenu.dart';
import 'package:finanxee/screens/transactions/details_revenu_screen.dart';
import 'package:finanxee/services/revenu_service.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../components/dialogs/date_range_dialog.dart';
import '../../components/revenue_card.dart';

class RevenusScreen extends StatefulWidget {
  const RevenusScreen({Key? key}) : super(key: key);

  @override
  State<RevenusScreen> createState() => _RevenusScreenState();
}

class _RevenusScreenState extends State<RevenusScreen> {
  DbHelper helper = DbHelper();
  List<Revenu> revenusList = [];
  List<Revenu> revenusList1 = [];
  RevenuService revenuService = RevenuService();
  double total = 0.0;
  var f = StaticDate();
  final intituleController = TextEditingController();
  String dateValueToShow = '';

  @override
  void initState() {
    // TODO: implement initState
    getRevenusOfMonth();
    btn3Pressed();
    super.initState();
  }

  Future<List<Revenu>> getRevenus() async {
    final revenus = await helper.getRevenus(); //get the list of all revenues
    setState(() {
      revenusList = revenus;
      revenusList1 = revenus;
    });
    return revenusList;
  }

  Future<List<Revenu>> getRevenusWeek() async {
    final depenses = await helper.getRevenusOfWeek(); //get the list of factures
    var total1 = await revenuService.total_revenu_current_week();

    setState(() {
      revenusList = depenses;
      revenusList1 = depenses;
      total = total1;
    });
    return revenusList;
  }

  Future<List<Revenu>> getAllRevenus() async {
    final depenses = await helper.getRevenus(); //get the list of factures
    var total1 = await revenuService.total_revenus();

    setState(() {
      revenusList = depenses;
      revenusList1 = depenses;
      total = total1;
    });
    return revenusList;
  }

  Future<List<Revenu>> getRevenusOfMonth() async {
    final depenses =
        await helper.getRevenusOfMonth(); //get the list of factures
    var total1 = await revenuService.total_revenu_current_month();

    setState(() {
      revenusList = depenses;
      revenusList1 = depenses;
      total = total1;
    });
    return revenusList;
  }

  var btn1 = true;
  var btn2 = false;
  var btn3 = false;
  var btn4 = false;
  var title = "Aujourd'hui";

  void btn2Pressed() {
    setState(() {
      btn1 = false;
      btn2 = true;
      btn3 = false;
      btn4 = false;
      title = "Cette semaine";
    });
  }

  void btn3Pressed() {
    setState(() {
      btn1 = false;
      btn2 = false;
      btn3 = true;
      btn4 = false;
      title = "Ce mois";
      List<DateTime> dates = f.getThisMonthIntervalleDates();
      dateValueToShow = '${dates[0].day} '
          '${f.monthYear(dates[0])} - '
          '${dates[1].day} '
          '${f.monthYear(dates[1])}';
    });
  }

  void btn4Pressed() {
    setState(() {
      btn1 = false;
      btn2 = false;
      btn3 = false;
      btn4 = true;
      title = "Tout";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder(
            future: helper.getRevenus(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Center(
                    child: Text(
                      '${snapshot.error} occurred',
                      style: const TextStyle(fontSize: 18),
                    ),
                  );

                  // if we got our data
                } else if (snapshot.hasData) {
                  return SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 5.0,
                          ),
                          Wrap(
                            alignment: WrapAlignment.start,
                            spacing: 12,
                            runSpacing: 12,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  getRevenusWeek();
                                  btn2Pressed();
                                  title = "Cette semaine";
                                  List<DateTime> dates =
                                      f.getThisWeekIntervalleDates();
                                  dateValueToShow = '${dates[0].day} '
                                      '${f.monthYear(dates[0])} - '
                                      '${dates[1].day} '
                                      '${f.monthYear(dates[1])}';
                                },
                                child: Chip(
                                  label: const Text(
                                    "Cette semaine",
                                  ),
                                  backgroundColor: btn2
                                      ? Colors.black.withOpacity(0.3)
                                      : null,
                                  //backgroundColor: Colors.black.withOpacity(0.3),
                                ),
                              ),
                              GestureDetector(
                                onTap: () async {
                                  getRevenusOfMonth();
                                  btn3Pressed();
                                  title = "Ce mois";
                                  List<DateTime> dates =
                                      f.getThisMonthIntervalleDates();
                                  dateValueToShow = '${dates[0].day} '
                                      '${f.monthYear(dates[0])} - '
                                      '${dates[1].day} '
                                      '${f.monthYear(dates[1])}';
                                },
                                child: Chip(
                                  label: const Text(
                                    "Ce mois",
                                  ),
                                  backgroundColor: btn3
                                      ? Colors.black.withOpacity(0.3)
                                      : null,
                                  //backgroundColor: Colors.black.withOpacity(0.3),
                                ),
                              ),
                              GestureDetector(
                                onTap: () async {
                                  getAllRevenus();
                                  btn4Pressed();
                                  title = "Tout";

                                  await showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              const DateRangeDialog('R'))
                                      .then((valueR) async => {
                                            dateValueToShow = '${valueR[1].day} '
                                                '${f.monthYear(valueR[1])} - '
                                                '${valueR[2].day} '
                                                '${f.monthYear(valueR[2])}',
                                            revenusList = valueR[0],
                                            total = await revenuService
                                                .totalRevenuFromList(
                                                    revenusList),
                                            setState(() {
                                              revenusList = revenusList;
                                              //dropdownValue = value!;
                                              //depenseByDayF = depenseByDay;
                                            }),
                                          });
                                },
                                child: Chip(
                                  label: const Text(
                                    "Choisir",
                                  ),
                                  backgroundColor: btn4
                                      ? Colors.black.withOpacity(0.3)
                                      : null,
                                  //backgroundColor: Colors.black.withOpacity(0.3),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 5.0,
                          ),
                          Text(dateValueToShow),

                          const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            child: Divider(
                              color: Color(0xFFD4D4D4),
                            ),
                          ),
                          if (revenusList.isNotEmpty)
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 15.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    f.numF.format(total),
                                    style: GoogleFonts.lato(
                                        fontSize: 25.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                'Rechercher ',
                                style: GoogleFonts.lato(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Container(
                            decoration: const BoxDecoration(
                                color: Color(0xFFEFEFEF),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(20.0)),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFD0CECE),
                                      offset: Offset(1, 1),
                                      blurRadius: 1,
                                      spreadRadius: 1)
                                ]),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: TextField(
                                        onChanged: (text) async {
                                          //print('First text field: $text');
                                          List<Revenu> revenus = await helper
                                              .searchStringInRevenus(text);
                                          var total1 = await revenuService
                                              .totalRevenuFromList(revenus);
                                          /* setState(() {
                                            revenusList = revenus;
                                            total = total1;
                                          });*/

                                          /* print(
                                              'Revenus found = : ${revenus.length}');*/
                                        },
                                        controller: intituleController,
                                        //cursorColor: _currentColor,
                                        decoration: const InputDecoration(
                                          hintText: "Rechercher un revenu ",
                                          border: InputBorder.none,
                                        ),
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () async {
                                        revenusList = revenusList1
                                            .where((x) => x.intitule.contains(
                                                intituleController.text))
                                            .toList();
                                        var total1 = await revenuService
                                            .totalRevenuFromList(revenusList);
                                        setState(() {
                                          revenusList = revenusList;
                                          total = total1;
                                        });
                                        print("RevenuList length : ");
                                        print(revenusList.length);
                                      },
                                      child: const Icon(
                                        Icons.search,
                                        color: Colors.grey,
                                        size: 25.0,
                                      ),
                                    ),
                                  ]),
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Container(
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFD8F1E7),
                                      offset: Offset(1, 1),
                                      blurRadius: 1,
                                      spreadRadius: 1)
                                ]),
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16.0, vertical: 5),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(
                                  height: 10.0,
                                ),
                                const SizedBox(
                                  height: 1.0,
                                ),
                                ListView.separated(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemBuilder: (BuildContext context, index) {
                                      //Account account = StaticDate.userAccounts[index];
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailsRevenuScreen(
                                                          revenusList[
                                                              index]))).then(
                                              (value) => getRevenusOfMonth());
                                          ;
                                        },
                                        child: RevenueCard(revenusList[index]),
                                      );
                                    },
                                    separatorBuilder:
                                        (BuildContext context, index) {
                                      return const Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 5.0),
                                        child: Divider(
                                          color: Color(0xFFD4D4D4),
                                        ),
                                      );
                                    },
                                    itemCount: revenusList.length),
                                if (revenusList.isEmpty)
                                  noRevenuScreen(context),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                          ),

                          const SizedBox(
                            height: 15,
                          ),

                          //Expanded(child: Accounts())
                        ],
                      ),
                    ),
                  );
                }
              }

              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              );
            }),
        floatingActionButton: FloatingActionButton(
            onPressed: () async {
              Revenu revenu = Revenu(0, '', 0, 0, 0.0, '', '', '', '');
              final result = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => RevenuForm(revenu, true)))
                  .then((value) => getRevenusOfMonth());
            },
            child: const Icon(Icons.add),
            backgroundColor: const Color(0xFF21ca79)));
  }
}

Widget noRevenuScreen(BuildContext context) {
  return Padding(
    padding: const EdgeInsets.all(30),
    child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          AspectRatio(
              aspectRatio: 1 / 1,
              child: Image.asset('assets/img-icons/income.png')),
          const SizedBox(height: 8.0),
          const Text(
            'Aucun revenu pour le moment ',
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 21.0),
          ),
          const SizedBox(height: 16.0),
          const Text(
            'Vous n\'avez pas encore ajouté des revenus pour cette période.\n Cliquez sur le bouton \n + pour ajouter',
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 16.0),
        ],
      ),
    ),
  );
}
