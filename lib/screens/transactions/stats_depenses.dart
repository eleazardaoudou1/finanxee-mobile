import 'package:finanxee/models/payloads/stats_depenses_mois.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../components/depense_categorie_month_card.dart';
import '../../models/depense.dart';
import '../../services/depense_service.dart';

class StatsDepenses extends StatefulWidget {
  final List<Depense> depenses;
  const StatsDepenses(this.depenses);

  @override
  State<StatsDepenses> createState() => _StatsDepensesState();
}

class _StatsDepensesState extends State<StatsDepenses> {
  Future<StatsDepensesMois> getData() async {
    return DepenseService().getStatsDepenses(widget.depenses);
  }

  late TooltipBehavior _tooltipBehavior;

  @override
  void initState() {
    // TODO: implement initState
    _tooltipBehavior = TooltipBehavior(enable: true);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Détails'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
        future: getData(),
        builder:
            (BuildContext context, AsyncSnapshot<StatsDepensesMois> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 14),
                ),
              );

              // if we got our data
            } else if (snapshot.hasData) {
              StatsDepensesMois? data = snapshot.data;
              List<_ChartData> depenseChartData = [];
              for (int i = 0; i < data!.categories.length; i++) {
                depenseChartData.add(_ChartData(
                    data!.categories[i].intitule, data!.sumDepenses[i]));
              }

              return SingleChildScrollView(
                child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Répartition des depenses en catégories",
                          style: GoogleFonts.lato(
                              fontSize: 20.0, fontWeight: FontWeight.bold),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        SfCircularChart(
                            enableMultiSelection: true,
                            tooltipBehavior: _tooltipBehavior,
                            series: <CircularSeries<dynamic, dynamic>>[
                              DoughnutSeries<_ChartData, String>(
                                  legendIconType: LegendIconType.image,
                                  explodeAll: true,
                                  dataSource: depenseChartData,
                                  xValueMapper: (_ChartData data, _) => data.x,
                                  yValueMapper: (_ChartData data, _) => data.y,
                                  name: 'Dépenses')
                            ]),
                        const SizedBox(
                          height: 10,
                        ),
                        ListView.separated(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemBuilder: (BuildContext context, index) {
                              //Account account = StaticDate.userAccounts[index];
                              return GestureDetector(
                                onTap: () {
                                  /* Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailsCompteBancaireScreen(
                                              comptesMomoList[index])))
                                      .then((value) => getComptes());*/
                                },
                                child: DepenseCategorieMonthCard(
                                    data!.sumDepenses[index],
                                    data!.categories[index].intitule),
                              );
                            },
                            separatorBuilder: (BuildContext context, index) {
                              return const Padding(
                                padding: EdgeInsets.symmetric(horizontal: 5.0),
                                child: Divider(
                                  color: Color(0xFFD4D4D4),
                                ),
                              );
                            },
                            itemCount: data!.sumDepenses.length),
                        //const DepenseCategorieMonthCard();
                      ],
                    )),
              );
            }
          }
          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        },
      ),
    );
  }
}

class _ChartData {
  _ChartData(this.x, this.y);

  final String x;
  final double y;
}
