import 'package:finanxee/models/categorie_depense.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/models/depense.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

import '../../models/payloads/details_depense_payload.dart';

class DetailsDepenseScreen extends StatefulWidget {
  Depense depense;
  DetailsDepenseScreen(this.depense);

  @override
  State<DetailsDepenseScreen> createState() =>
      _DetailsDepenseScreenState(depense);
}

class _DetailsDepenseScreenState extends State<DetailsDepenseScreen> {
  Compte compte1 = Compte(0, TypeCompte.BANK.index, '', 0.0, '', 'APP',
      'assets/nsia_logo.png', '', '');
  CategorieDepense cat = CategorieDepense(0, '', '', '', '', '', []);

  DbHelper helper = DbHelper();
  _DetailsDepenseScreenState(this.depense);

  Depense depense =
      Depense(0, '', '', 0, 0, 0, 0.0, DateTime.now().toString(), '', '');
  var f = StaticDate();

  @override
  void initState() {
    // TODO: implement initState

    //getDepensesSubObject();

    super.initState();
  }

  Future<DetailsDepensePayload> getDepensesSubObject() async {
    DetailsDepensePayload depensePayload = DetailsDepensePayload(
        compte1, cat, depense, widget.depense.description);

    //print(widget.depense.compte);
    final compte2 = await helper.getCompteById(widget.depense.compte);
    if (compte2 != null) depensePayload.compte = compte2!;

    if (widget.depense.categorie != null) {
      final cat2 = await helper
          .getCatById(widget.depense.categorie); //get the list of factures
      depensePayload.categorie = cat2!;
    }

    return depensePayload;
  }

  @override
  Widget build(BuildContext context) {
    // show the dialog

    return FutureBuilder(
        future: getDepensesSubObject(),
        builder: (context, AsyncSnapshot<DetailsDepensePayload> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: const TextStyle(fontSize: 18),
                ),
              );

              // if we got our data
            } else {
              DetailsDepensePayload depensePayload = snapshot.data!;

              return Scaffold(
                appBar: AppBar(
                  //leading: Icon(FlutterIcons.menu_fea),
                  //centerTitle: true,
                  title: Text(widget.depense.intitule),
                  backgroundColor: const Color(0xFF21ca79),
                  actions: [
                    Container(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 15.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          /*GestureDetector(
                            child: const Icon(Icons.workspaces_outlined),
                            onTap: () {},
                          ),*/
                          GestureDetector(
                            child: const Icon(Icons.delete_outline),
                            onTap: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return DeleteDepense(
                                    depense: widget.depense,
                                  );
                                },
                              ).then((value) => {
                                    if (value == 'x')
                                      {
                                        Navigator.pop(context),
                                        //print('Value is : ' + value)
                                      }
                                  });
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                body: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: ListView(
                    children: [
                      SizedBox(
                        height: 200,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              f.numF.format(depense.montant),
                              style: GoogleFonts.lato(fontSize: 40.0),
                              //style: Theme.of(context).textTheme.headline2,
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.sticky_note_2_outlined),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                depense.intitule,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Intitulé',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.calendar_month),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                DateFormat('dd-MM-yyyy')
                                    .format(DateTime.parse(depense.date)),
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Date',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                            ),
                          ),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.category_outlined),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                depensePayload.categorie.intitule,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Catégorie',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                              onTap: () {},
                            ),
                          ),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(
                                Icons.account_balance_wallet_outlined),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                depensePayload.compte.intitule.isNotEmpty
                                    ? '${depensePayload.compte.intitule} '
                                    : 'compte inexistant ou supprimé !',
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                'Compte',
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                              onTap: () {},
                            ),
                          ),
                          depensePayload.compte.deleted_at != null
                              ? GestureDetector(
                                  onTap: () {
                                    print(depensePayload.compte.deleted_at);
                                    SnackBar snackBar = const SnackBar(
                                      content: Text(
                                          'Ce compte est inexistant ou a été supprimé !'),
                                      backgroundColor: Colors.black,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                  },
                                  child: const Icon(
                                    Icons.warning,
                                    color: Colors.red,
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      ),
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.content_paste),
                            onPressed: () {},
                          ),
                          Expanded(
                            child: ListTile(
                              title: Text(
                                depense.description,
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                              subtitle: Text(
                                depense.description.isEmpty
                                    ? "Ajoutez une description"
                                    : "Editer la description",
                                style: Theme.of(context).textTheme.bodyMedium,
                              ),
                              onTap: () {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return UpdateDescriptionDepense(
                                      depense: widget.depense,
                                    );
                                  },
                                ).then((value) => setState(() {
                                      widget.depense.description =
                                          value.toString();
                                    }));
                              },
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            }
          }

          return const Center(
            child: CircularProgressIndicator(
              color: Colors.green,
            ),
          );
        });
  }
}

class DeleteDepense extends StatelessWidget {
  const DeleteDepense({Key? key, required this.depense}) : super(key: key);
  final Depense depense;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer cette dépense?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            //Navigator.pop(context);
            await helper.deleteDepense(depense.id);
          },
        ),
      ],
    );
  }
}

class UpdateDescriptionDepense extends StatelessWidget {
  const UpdateDescriptionDepense({Key? key, required this.depense})
      : super(key: key);
  final Depense depense;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();
    final descriptionDep = TextEditingController();
    if (depense.description.isNotEmpty) {
      descriptionDep.text = depense.description;
    }

    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.5,
      child: AlertDialog(
        title: const Text("Ajouter une description"),
        content: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: descriptionDep,
              decoration:
                  const InputDecoration(hintText: 'Ajouter une description'),
            ),
          ],
        ),
        actions: [
          TextButton(
            child: const Text("Annuler"),
            onPressed: () {
              Navigator.pop(context, 'n');
            },
          ),
          TextButton(
            child: const Text("Continuer"),
            onPressed: () async {
              depense.description = descriptionDep.text;
              Navigator.pop(context, descriptionDep.text);
              //Navigator.pop(context);
              await helper.updateDepense(depense);
            },
          ),
        ],
      ),
    );
  }
}
