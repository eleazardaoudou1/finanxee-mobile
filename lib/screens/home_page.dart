import 'dart:async';
import 'package:finanxee/navigation/finanxee_pages.dart';
import 'package:finanxee/providers/providers.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../cache/cache_helper.dart';
import '../navigation/app_router.dart';
import '../providers/profile_state_manager.dart';
import '../utils/static_data.dart';
import 'screens.dart';
import 'package:finanxee/screens/transactions/transactions_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FinanxeeHomePage extends StatefulWidget {
  static MaterialPage page(int currentTab) {
    return MaterialPage(
      name: FinanxeePages.home,
      key: ValueKey(FinanxeePages.home),
      child: FinanxeeHomePage(
        currentTab: currentTab,
      ),
    );
  }

  final int currentTab;

  const FinanxeeHomePage({Key? key, required this.currentTab})
      : super(key: key);

  @override
  State<FinanxeeHomePage> createState() => _FinanxeeHomePageState();
}

class _FinanxeeHomePageState extends State<FinanxeeHomePage>
    with RestorationMixin {
  int _selectedIndex = 0;
  RestorableInt _selectedIndex1 = RestorableInt(0);

  static List<Widget> pages = [
    const Dashboard(),
    const AvoirScreen(),
    const TransactionsScreen(),
    const PlanifierScreen(),
    //const AnalysesScreen(),
    //NotificationsScreen()
  ];

  void onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  int setTheIndex(int index) {
    setState(() {
      _selectedIndex = index;
    });

    return _selectedIndex;
  }

  @override
  void initState() {
    // TODO: implement initState
    setState(() {
      _selectedIndex = widget.currentTab;
    });
    //print('On est dans Init State');
    //_checkifCountrySet();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AppStateManager>(
      builder: (context, appStateManager, child) {
        return Scaffold(
          /* appBar: AppBar(
            //leading: Icon(FlutterIcons.menu_fea),
            centerTitle: true,
            title: const Text('Finanxee'),
            backgroundColor: const Color(0xFF21ca79),
            elevation: 0,
            actions: [
              Container(
                padding: const EdgeInsets.symmetric(
                    vertical: 10.0, horizontal: 15.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(Icons.settings),
                    GestureDetector(
                        onTap: () {
                          Provider.of<ProfileStateManager>(context,
                                  listen: false)
                              .tapOnProfile(true);
                        },
                        child: Icon(Icons.account_circle_outlined)),
                  ],
                ),
              ),
            ],
          ),*/
          body: /*IndexedStack(
            index: _selectedIndex,
            children: pages,
          ),*/
              pages[widget.currentTab],
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            selectedItemColor:
                Theme.of(context).textSelectionTheme.selectionColor,
            currentIndex: widget.currentTab,
            onTap: (index) {
              //tabManager.goToTab(index);
              appStateManager.goToTab(index);
              setState(() {
                _selectedIndex = index;
              });
            },
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.home_outlined),
                label: 'Aperçu',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.work_rounded),
                label: 'Avoirs',
                //backgroundColor: Color(0xFFCCCCCC)
              ),
              //SizedBox(width: 40,),
              BottomNavigationBarItem(
                icon: Icon(Icons.currency_exchange),
                label: 'Transactions',
                //backgroundColor: Color(0xFFCCCCCC)
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.pending_actions_rounded),
                label: 'Panifier',
                //backgroundColor: Color(0xFFCCCCCC)
              ),
              /* BottomNavigationBarItem(
                  icon: Icon(Icons.explore), label: 'Analyses'),*/
            ],
          ),
          /*floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const ActionsUser()));
              },
              child: const Icon(Icons.add),
              backgroundColor: const Color(0xFF21ca79)),*/
        );
      },
    );
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => "home_page";

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
    registerForRestoration(_selectedIndex1, 'selectedIndex1');
  }
}

class LauchScreen extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: FinanxeePages.launchPath,
      key: ValueKey(FinanxeePages.launchPath),
      child: const LauchScreen(),
    );
  }

  const LauchScreen({Key? key}) : super(key: key);

  @override
  State<LauchScreen> createState() => _LauchScreenState();
}

class _LauchScreenState extends State<LauchScreen> with RestorationMixin {
  late RestorableBool _isCountrySet = RestorableBool(false);

  @override
  void didChangeDependencies() {
    // TODO: Initialize App
    Provider.of<AppStateManager>(context).initializeApp();
    super.didChangeDependencies();
  }

  @override
  void initState() {
    // TODO: implement initState
    _checkifCountrySet();
    _checkifOnboardingCompletd();
    _checkifPasscodeSet();
    _checkifQuestionsCompleted();
    _checkifConditionsGenerealesCompleted();
    super.initState();
  }

  Future<void> _checkifCountrySet() async {
    final prefs = await SharedPreferences.getInstance();
    String selectCountryCode = StaticDate.selectedCountryCode;
    if (prefs.containsKey(selectCountryCode)) {
      Provider.of<AppStateManager>(context, listen: false).setCountry();
      setState(() {
        _isCountrySet.value = true;
      });
    }
    //Future<String> countryCode = CacheHelper.getString(selectCountryCode);
    print("OnBoarding countryCode = ");
    print(prefs.getString(selectCountryCode));
  }

  Future<void> _checkifOnboardingCompletd() async {
    final prefs = await SharedPreferences.getInstance();
    String onBoardingComplete = StaticDate.onBoardingCompletedStatus;
    if (prefs.containsKey(onBoardingComplete)) {
      Provider.of<AppStateManager>(context, listen: false).completeOnboarding();
    }
    //Future<String> countryCode = CacheHelper.getString(selectCountryCode);
    print("OnBoarding completed Status = ");
    print(prefs.getBool(onBoardingComplete));
  }

  Future<void> _checkifConditionsGenerealesCompleted() async {
    final prefs = await SharedPreferences.getInstance();
    String termsAndConditionsCompletedStatus =
        StaticDate.termsAndConditionsCompletedStatus;
    if (prefs.containsKey(termsAndConditionsCompletedStatus)) {
      Provider.of<AppStateManager>(context, listen: false)
          .completeTermsAndConditions();
    }
    //Future<String> countryCode = CacheHelper.getString(selectCountryCode);
    print("Conditions Generales completed Status = ");
    print(prefs.getBool(termsAndConditionsCompletedStatus));
  }

  Future<void> _checkifQuestionsCompleted() async {
    final prefs = await SharedPreferences.getInstance();
    String questionsCompleted = StaticDate.questionsCompletedStatus;
    if (prefs.containsKey(questionsCompleted)) {
      Provider.of<AppStateManager>(context, listen: false).completeQuestions();
    }
    //Future<String> countryCode = CacheHelper.getString(selectCountryCode);
    print("Questions completed Status = ");
    print(prefs.getBool(questionsCompleted));
  }

  Future<void> _checkifPasscodeSet() async {
    final prefs = await SharedPreferences.getInstance();
    String passCodeKey = StaticDate.passCodeKey;
    if (prefs.containsKey(passCodeKey)) {
      Provider.of<AppStateManager>(context, listen: false)
          .completeSettingPasscode();
    }
    //Future<String> countryCode = CacheHelper.getString(selectCountryCode);
    print("Passcode completed Status = ");
    print(prefs.getBool(passCodeKey));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Image.asset('assets/core/logo_final.png')); // <-- SEE HERE
    //FlutterLogo(size: MediaQuery.of(context).size.height));
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'launch_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
    registerForRestoration(_isCountrySet, 'isCountrySet');
  }
}
