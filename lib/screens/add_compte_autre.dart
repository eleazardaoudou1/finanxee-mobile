import 'package:finanxee/components/dialogs/gsm_list_dialog.dart';
import 'package:finanxee/models/compte.dart';
import 'package:finanxee/providers/app_state_manager.dart';
import 'package:finanxee/providers/data_init.dart';
import 'package:finanxee/providers/tab_manager.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class AddCompteAutre extends StatefulWidget {
  final Compte compte;
  final bool isNew;

  const AddCompteAutre(this.compte, this.isNew);

  @override
  State<AddCompteAutre> createState() => _AddCompteAutreState(compte, isNew);
}

class _AddCompteAutreState extends State<AddCompteAutre> {
  Compte compte1 = Compte(0, TypeCompte.MOMO.index, '', 0.0, '', 'APP',
      'assets/nsia_logo.png', '', '');
  bool isNew = false;

  _AddCompteAutreState(this.compte1, this.isNew);

  double solde = 0.0;
  final soldeController = TextEditingController();
  final intituleController = TextEditingController();
  DbHelper helper = DbHelper();

  //soldeController.text = 4.0;

  @override
  void initState() {
    // TODO: implement initState
    //intituleController.text = widget.intitule;
    if (!widget.isNew) {
      soldeController.text = widget.compte.solde.toString();
      intituleController.text = widget.compte.intitule;
    }
    compte1 = widget.compte;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final solde = Provider.of<DataInit>(context);
    //soldeController.text = solde.compte_momo_solde.toString();
    return Scaffold(
        appBar: AppBar(
          title: const Text("Ajouter un compte particulier"),
          backgroundColor: const Color(0xFF21ca79),
        ),
        body: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            children: [
              Expanded(
                child: Center(
                  child: SingleChildScrollView(
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height / 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          buildSoldeField(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Column(
// 2
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
// 3
                  Text(
                    'Intitulé du compte',
                    style: GoogleFonts.lato(fontSize: 18.0),
                  ),
// 4
                  TextField(
// 5
                    controller: intituleController,
// 6
                    //cursorColor: _currentColor,
// 7
                    decoration: const InputDecoration(
// 8
                      hintText: "Ex: Mon Porte-monnaie ",
// 9
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.black),
                      ),
                    ),
                  ),
                ],
              ),
              /* ListTile(
                title: Text(
                  "Saisir l'intitulé du compte",
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                subtitle: Text(
                  solde.compte_selected.intitule,
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
                onTap: () {
                  solde.updateSoldeAutre(double.parse(soldeController.text));
                  //soldeController.text = solde.compte_momo_solde.toString();
                },
              ),*/
              const SizedBox(
                height: 15.0,
              ),
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(backgroundColor: const Color(0xFF21cA79)),
                child: const Text('Ajouter'),
                onPressed: () {
                  if (soldeController.text.isEmpty) {
                    SnackBar snackBar1 = const SnackBar(
                      content: Text("Le solde doit être défini"),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar1);
                    return;
                  } else if (intituleController.text.isEmpty) {
                    SnackBar snackBar2 = const SnackBar(
                      content: Text("Entrez l'intitulé du compte"),
                      backgroundColor: Colors.red,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar2);
                    return;
                  }

                  solde.updateSoldeAutre(double.parse(soldeController.text));
                  compte1 = solde.compte_selected_3;
                  compte1.solde = double.parse(soldeController.text);
                  compte1.intitule = intituleController.text;
                  compte1.deleted_at = '';

                  //list.name = txtName.text;
                  helper.insertCompte(compte1);
                  if (widget.isNew) {
                    SnackBar snackBar = const SnackBar(
                      content: Text('Compte ajouté avec succès!'),
                      backgroundColor: Colors.green,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    Navigator.pop(context);
                    Navigator.pop(context);
                    Provider.of<AppStateManager>(context, listen: false)
                        .gotToAvoirs();
                  } else {
                    SnackBar snackBar = const SnackBar(
                      content: Text('Compte édité avec succès !'),
                      backgroundColor: Colors.green,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);

                    Navigator.pop(context);
                    Navigator.pop(context);

                    Provider.of<TabManager>(context, listen: false)
                        .gotToAccount();
                  }

                  //Navigator.pop(context);
                },
              )
            ],
          ),
        ));
  }

  Widget buildSoldeField() {
// 1
    return Column(
// 2
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
// 3
        Text(
          'Solde',
          style: GoogleFonts.lato(fontSize: 18.0),
        ),
// 4
        TextField(
// 5
          controller: soldeController,
          textAlign: TextAlign.center,
          style: GoogleFonts.lato(fontSize: 60.0),
// 6
          //cursorColor: _currentColor,
// 7
          decoration: InputDecoration(
// 8
            hintText: '25 000 FCFA ',
            hintStyle: GoogleFonts.lato(fontSize: 60.0),
// 9
            enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFBBBBBB)),
            ),
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFBBBBBB)),
            ),
            border: const UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xFFBBBBBB)),
            ),
          ),
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            FilteringTextInputFormatter.digitsOnly
          ], // Only numbers can be entered
        ),
      ],
    );
  }
}
