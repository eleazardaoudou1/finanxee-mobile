import 'package:finanxee/components/dialogs/expense_list_dialog.dart';
import 'package:finanxee/components/expense_list_card.dart';
import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/screens/screens.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'archived_expense_list_screen.dart';

class ExpenseListUI extends StatefulWidget {
  const ExpenseListUI({Key? key}) : super(key: key);

  @override
  State<ExpenseListUI> createState() => _ExpenseListUIState();
}

class _ExpenseListUIState extends State<ExpenseListUI> {
  final intituleController = TextEditingController();

  List<ExpenseList> expenseList = [];
  List<ExpenseList> expenseList_1 = [];
  DbHelper helper = DbHelper(); // c'est ici que nous faisons l'injection
  ExpenseListDialog dialog = ExpenseListDialog();
  int archived = 0;

  @override
  initState() {
    dialog = ExpenseListDialog();
    getExpenseList();
    super.initState();
  }

  Future<void> getExpenseList() async {
    List<ExpenseList> expenseList1 = await helper.getExpenseLists();
    var archList = await helper.getExpenseListsArchivees();
    setState(() {
      expenseList = expenseList1;
      expenseList_1 = expenseList1;
      archived = archList.length;
    }); //get the shopping list from the helper
  }

  Future<void> getExpenseListArchivees() async {
    List<ExpenseList> expenseList1 = await helper.getExpenseListsArchivees();
    setState(() {
      expenseList = expenseList1;
    }); //get the shopping list from the helper
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Listes de dépense'),
        backgroundColor: const Color(0xFF21ca79),
        actions: [
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                // Icon(Icons.add),
                //Icon(Icons.account_circle_outlined),
              ],
            ),
          ),
        ],
      ),
      body: FutureBuilder(
          future: helper.getExpenseLists(),
          builder: (BuildContext context, snapshot) {
            // TODO: Add nested ListViews
            if (snapshot.connectionState == ConnectionState.done) {
              if (expenseList.isNotEmpty ||
                  (expenseList.isEmpty && intituleController.text.isNotEmpty)) {
                return Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 20.0),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Rechercher ',
                            style: GoogleFonts.lato(
                                fontSize: 20.0, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Container(
                        decoration: const BoxDecoration(
                            color: Color(0xFFEFEFEF),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color(0xFFD0CECE),
                                  offset: Offset(1, 1),
                                  blurRadius: 1,
                                  spreadRadius: 1)
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: TextField(
                                    controller: intituleController,
                                    //cursorColor: _currentColor,
                                    decoration: const InputDecoration(
                                      hintText: "Ex: Achats du fin du mois ",
                                      border: InputBorder.none,
                                    ),
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () async {
                                    expenseList = expenseList_1
                                        .where((x) => x.name
                                            .toLowerCase()
                                            .contains(intituleController.text
                                                .toLowerCase()))
                                        .toList();

                                    setState(() {
                                      expenseList = expenseList;
                                    });
                                    print("ExpenseList length : ");
                                    print(expenseList.length);
                                  },
                                  child: const Icon(
                                    Icons.search,
                                    color: Colors.grey,
                                    size: 25.0,
                                  ),
                                ),
                              ]),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Vos listes ',
                            style: GoogleFonts.lato(
                                fontSize: 15.0, fontWeight: FontWeight.bold),
                          ),
                          /*   Text(
                            'Archivées (${archived}) ',
                            style: GoogleFonts.lato(
                                fontSize: 15.0, fontWeight: FontWeight.normal),
                          ),*/
                          TextButton(
                            onPressed: () {
                              //ArchivedExpenseListScreen
                              Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              const ArchivedExpenseListScreen()))
                                  .then((value) => getExpenseList());
                            },
                            child: Text(
                              'Archivées (${archived}) ',
                              style: GoogleFonts.lato(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.normal),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Expanded(
                        child: ListView.separated(
                          shrinkWrap: true,
                          //physics: NeverScrollableScrollPhysics(),
                          itemCount: expenseList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  OneExpenseList(
                                                      expenseList[index])))
                                      .then((value) => getExpenseList());
                                },
                                child: buildDismissible(
                                    expenseList, index, context));
                          },
                          separatorBuilder: (BuildContext context, index) {
                            return const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 2.0),
                              child: Divider(
                                color: Color(0xFFFFFFFF),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                return buildEmptyList(context);
              }
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
          onPressed: () async {
            showDialog(
                context: context,
                builder: (BuildContext context) => dialog.buildDialog(
                    context,
                    ExpenseList(0, '', 0, DateTime.now().toString(), ''),
                    true)).then((value) => getExpenseList());
          },
          child: const Icon(Icons.add),
          backgroundColor: const Color(0xFF21ca79)),
    );
  }

  Widget buildDismissible(
      List<ExpenseList> expenseList, index, BuildContext context) {
    DbHelper helper = DbHelper();
    //liste non archivee, le glissage permettra d'archiver ou supprimer
    return Dismissible(
        key: Key('liste ${index}'),
        direction: DismissDirection.horizontal,
        background: const ColoredBox(
          color: Color.fromRGBO(43, 80, 108, 1),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Icon(Icons.archive_rounded, color: Colors.white),
            ),
          ),
        ),
        secondaryBackground: const ColoredBox(
          color: Colors.red,
          child: Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: EdgeInsets.all(16.0),
              child: Icon(Icons.delete_forever, color: Colors.white),
            ),
          ),
        ),
        onDismissed: (DismissDirection direction) async {
          if (direction == DismissDirection.endToStart) {
            //glisser de la droite vers la gauche, on supprime
            ExpenseList expenseL = expenseList[index];
            expenseL.deleted_at = DateTime.now().toString();
            await helper.updateExpenseList(expenseL);
            getExpenseList();
            SnackBar snackBar = SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(" 1 liste supprimée."),
                  GestureDetector(
                    onTap: () async {
                      await helper.softRestoreList(expenseL.id);
                      getExpenseList();
                    },
                    child: const Text(
                      "Annuler.",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              backgroundColor: const Color.fromRGBO(43, 80, 108, 1),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            //glisser de la gauche vers la droite, on archive
            ExpenseList expenseL = expenseList[index];
            expenseL.archived = 1;
            await helper.updateExpenseList(expenseL);
            getExpenseList();

            SnackBar snackBar = SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(" 1 liste archivée."),
                  GestureDetector(
                    onTap: () async {
                      expenseL.archived = 0;
                      await helper.updateExpenseList(expenseL);
                      getExpenseList();
                    },
                    child: const Text(
                      "Annuler.",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              backgroundColor: const Color.fromRGBO(43, 80, 108, 1),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
        /* confirmDismiss: (DismissDirection
                                                  direction) {},*/
        child: ExpenseListCard(
            expenseList[index]) // FactureCard(facturesList[index]),
        );
  }
}

Widget buildEmptyList(BuildContext context) {
  return Container(
    child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(
            height: 5.0,
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                AspectRatio(
                    aspectRatio: 1 / 1,
                    child: Image.asset('assets/core/sheet.png')),
                const SizedBox(height: 8.0),
                const Text(
                  'Aucune liste ',
                  style: TextStyle(fontSize: 21.0),
                ),
                const SizedBox(height: 16.0),
                const Text(
                  'Vous n\'avez aucune liste pour l\'instant.\n'
                  'Appuyez sur le bouton + en haut de l\'écran pour ajouter!',
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
