import 'package:finanxee/components/components.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/utils/utils.dart';
import 'package:flutter/material.dart';

import '../screens.dart';

class LastProjects extends StatefulWidget {
  const LastProjects({Key? key}) : super(key: key);

  @override
  State<LastProjects> createState() => _LastProjectsState();
}

class _LastProjectsState extends State<LastProjects> {
  var f = StaticDate();
  List<Project> projects = [];
  DbHelper helper = DbHelper();

  @override
  void initState() {
    getProjects();
    // TODO: implement initState
    super.initState();
  }

  Future<void> getProjects() async {
    var theProjects = await helper.getLastProjets();
    setState(() {
      projects = theProjects;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: helper.getProjets(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.hasError) {
            return Center(
              child: Text(
                '${snapshot.error} occurred',
                style: const TextStyle(fontSize: 18),
              ),
            );
          } else if (snapshot.hasData) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                          color: Color(0xFFD8F1E7),
                          offset: Offset(1, 1),
                          blurRadius: 1,
                          spreadRadius: 1)
                    ]),
                padding:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Projets",
                          style: Theme.of(context)
                              .textTheme
                              .bodyLarge, //TextStyle(color: Colors.grey, fontSize: 16.0),
                        ),
                        Text(
                          "",
                          style: Theme.of(context).textTheme.labelLarge,
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 10.0,
                    ),
                    ListView.separated(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, index) {
                          //Account account = StaticDate.userAccounts[index];
                          return ProjectCard(projects[index]);
                        },
                        separatorBuilder: (BuildContext context, index) {
                          return const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5.0),
                            child: Divider(
                              color: Color(0xFFD4D4D4),
                            ),
                          );
                        },
                        itemCount: projects.length),
                    const SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ProjectListScreen()))
                            .then((value) => getProjects());
                        SnackBar snackBar = const SnackBar(content: Text(' '));
                        // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      },
                      child: Wrap(children: [
                        Container(
                          padding: const EdgeInsets.symmetric(
                            //horizontal: 16.0,
                            vertical: 8.0,
                          ),
                          decoration: BoxDecoration(
                              //color: Color(0xFF21ca79),
                              borderRadius: BorderRadius.circular(20.0),
                              border: Border.all(
                                color: const Color(0xFF21ca79),
                                width: 1,
                              )),
                          child: Center(
                            child: Text(
                              "Tout voir",
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                        ),
                      ]),
                    )
                  ],
                ),
              ),
            );
          }
        }
        return Center(
          child: CircularProgressIndicator(
            color: Colors.green,
          ),
        );
      },
    );
  }
}
