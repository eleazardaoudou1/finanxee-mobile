import 'package:finanxee/components/components.dart';
import 'package:finanxee/components/forms/project_form.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/utils/utils.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

import '../screens.dart';
import 'details_projet_screen.dart';

class ProjectListArchiveesScreen extends StatefulWidget {
  const ProjectListArchiveesScreen({Key? key}) : super(key: key);
  @override
  State<ProjectListArchiveesScreen> createState() =>
      _ProjectListArchiveesScreenState();
}

class _ProjectListArchiveesScreenState
    extends State<ProjectListArchiveesScreen> {
  Project project = Project(0, "intitule", 0.0, 0.0, 0.0, "description",
      "delai", 0, DateTime.now().toString(), DateTime.now().toString(), 0);
  DbHelper helper = DbHelper();
  List<Project> projectList = [];
  // c'est ici que nous faisons l'injection
  Future getProjects() async {
    List<Project> list1 = await helper.getProjetsArchivees();
    setState(() {
      projectList = list1;
    }); //get the shopping list from the helper
  }

  @override
  void initState() {
    // TODO: implement initState
    getProjects();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Archivés'),
        backgroundColor: const Color(0xFF21ca79),
        actions: [
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                // Icon(Icons.add),
                //Icon(Icons.account_circle_outlined),
              ],
            ),
          ),
        ],
      ),
      body: FutureBuilder(
          future: helper.getExpenseLists(),
          builder: (BuildContext context, snapshot) {
            // TODO: Add nested ListViews
            if (snapshot.connectionState == ConnectionState.done) {
              if (projectList.isNotEmpty) {
                return Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 25,
                        ),
                        ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: projectList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailsProjetScreen(
                                                      projectList[index])))
                                      .then((value) => getProjects());
                                },
                                child: ProjectCard(projectList[index]));
                          },
                          separatorBuilder: (BuildContext context, index) {
                            return const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 2.0),
                              child: Divider(
                                color: Color(0xFFFFFFFF),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const [
                        SizedBox(height: 8.0),
                        Text(
                          'Aucun projet archivé',
                          style: TextStyle(fontSize: 21.0),
                        ),
                        SizedBox(height: 16.0),
                        Text(
                          'Vous n\'avez archivé aucun projet pour l\'instant.',
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ),
                );
              }

              /*if (snapshot.hasData) {
              final recipes = snapshot.data;
              //TODO : replace this with today recipe list view
              return TodayRecipeListView(recipes: recipes!.todayRecipes);
            } else if (snapshot.hasError) {
              return Center(
                child: Text('${snapshot.error} is the error'),
              );
            } else
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              );*/
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }

  Widget topCard(int t) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: const BoxDecoration(
            color: Colors.white, //Color(0xFF00BB),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFECECEC),
                  offset: Offset(1, 1),
                  blurRadius: 5,
                  spreadRadius: 5)
            ]),
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Projets',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            const SizedBox(
              height: 18.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  t.toString(),
                  //style: Theme.of(context).textTheme.headline2,
                  style: GoogleFonts.lato(
                      fontSize: 60.0, fontWeight: FontWeight.w900),
                ),
              ],
            ),
            const SizedBox(
              height: 7.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                    vertical: 8.0,
                  ),
                  decoration: BoxDecoration(
                    color: const Color(0xFF21ca79),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: const Center(
                    child: Text(
                      "Archivés (5)",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {},
                  child: Row(
                    children: const [
                      Text(
                        '+20%',
                        style: TextStyle(
                            color: Color(0xFF21ca79),
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold),
                      ),
                      Icon(
                        Icons.arrow_right_sharp,
                        //FlutterIcons.caret_up_faw,
                        color: Color(0xFF21ca79),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
