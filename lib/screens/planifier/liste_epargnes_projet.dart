import 'package:finanxee/models/expense_list.dart';
import 'package:finanxee/utils/dbhelper.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';
import '../../components/epargne_projet_card.dart';
import '../../models/epargne_projet.dart';
import '../../models/project.dart';

class ListeEpargnesProjet extends StatefulWidget {
  //const ListeEpargnesProjet({Key? key}) : super(key: key);
  final Project projet;
  ListeEpargnesProjet(this.projet);

  @override
  State<ListeEpargnesProjet> createState() => _ListeEpargnesProjetState(projet);
}

class _ListeEpargnesProjetState extends State<ListeEpargnesProjet> {
  List<EpargneProjet> epargnesProjets = [];
  bool toShow = false;
  int selectedIndex = 0;
  late Project projet_;
  double listSum = 0.0;

  _ListeEpargnesProjetState(this.projet_);

  DbHelper helper = DbHelper();

  var f = StaticDate();

  //function to get the epargnes for the projetc
  Future<ResultObjet> getEpargnes(idProjet) async {
    ResultObjet result = ResultObjet([], 0);
    var listEpargnes = await helper.getEpargneProjetFromProjet(idProjet);
    var sum = 0.0;
    listEpargnes.forEach((element) {
      sum += element.montant;
    });

    result.epProList = listEpargnes;
    result.sum = sum;

    /* setState(() {
      epargnesProjets = listEpargnes;
      listSum = sum;
    });*/
    return result;
  }

  @override
  void initState() {
    //expenseTasks = await helper.getDepensesFromList(idList);
    //getEpargnes(widget.projet.id);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    //getExpenses(this.expenseList.id);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Liste Epagnes"),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: FutureBuilder(
          future:
              getEpargnes(widget.projet.id), //helper.insertAnExpenseTask(), /,
          builder: (BuildContext context, AsyncSnapshot<ResultObjet> snapshot) {
            // TODO: Add nested ListViews
            if (snapshot.connectionState == ConnectionState.done) {
              ResultObjet? resultObjet = snapshot!.data;
              //print(list);

              if (resultObjet!.epProList.isNotEmpty) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 10.0),
                      child: SizedBox(
                        height: 30,
                        child: Text(
                          f.numF.format(resultObjet.sum).toString(),
                          style: Theme.of(context).textTheme.headline2,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 05.0),
                      child: SizedBox(
                        height: 20,
                        child: Text(
                          "Montant  épargné",
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ),
                    ),
                    const Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 1.0, vertical: 0),
                      child: Divider(
                        color: Color(0xFFD4D4D4),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 1.0),
                        child: SingleChildScrollView(
                          child: ListView.separated(
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: resultObjet.epProList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                foregroundDecoration:
                                    _foreG(toShow, index, selectedIndex),
                                child: Dismissible(
                                  key: Key(
                                      'item ${resultObjet.epProList[index]}'),
                                  child: EpargneProjetCard(
                                      resultObjet.epProList[index]),
                                  background: const ColoredBox(
                                    color: Colors.red,
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Padding(
                                        padding: EdgeInsets.all(16.0),
                                        child: Icon(Icons.delete_forever,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                  secondaryBackground: const ColoredBox(
                                    color: Colors.red,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Padding(
                                        padding: EdgeInsets.all(16.0),
                                        child: Icon(Icons.delete_forever,
                                            color: Colors.white),
                                      ),
                                    ),
                                  ),
                                  onDismissed:
                                      (DismissDirection direction) async {
                                    //droite vers gauche, on supprime
                                    EpargneProjet task =
                                        resultObjet.epProList[index];
                                    task.deleted_at = DateTime.now().toString();
                                    await helper.updateEpargneProjet(task);
                                    getEpargnes(widget.projet.id);
                                    setState(() {});

                                    //getExpenses(expenseList1.id);

                                    SnackBar snackBar = SnackBar(
                                      content: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          const Text(" 1 épargne supprimée."),
                                          GestureDetector(
                                            onTap: () async {
                                              await helper
                                                  .softRestoreEpargneProjet(
                                                      task);
                                              getEpargnes(widget.projet.id);
                                              setState(() {});
                                            },
                                            child: const Text(
                                              "Annuler.",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ],
                                      ),
                                      backgroundColor: Colors.grey,
                                    );
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(snackBar);
                                  },
                                ),
                              );
                            },
                            separatorBuilder: (BuildContext context, index) {
                              return const Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 1.0, vertical: 0),
                                child: Divider(
                                  color: Color(0xFFD4D4D4),
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(height: 8.0),
                      Text(
                        'Aucune épargne ',
                        style: TextStyle(fontSize: 21.0),
                      ),
                      SizedBox(height: 16.0),
                      Text(
                        'Vous n\'avez aucune épargne pour  ce projet.'
                        'Appuyez sur le bouton + en bas pour ajouter!',
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                );
              }
            } else {
              return const Center(
                child: CircularProgressIndicator(
                  color: Color(0xFF21ca79),
                ),
              );
            }
          }),
      bottomNavigationBar: _showBottomBar(toShow),
    );
  }

  _showBottomBar(bool toShow1) {
    if (toShow1) {
      return BottomAppBar(
        color: const Color.fromRGBO(241, 241, 241, 1.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            IconButton(
              icon: const Icon(Icons.archive),
              onPressed: () {},
            ),
            IconButton(
              icon: const Icon(Icons.delete_outline),
              onPressed: () async {
                /* await helper
                    .updateDeleteExpenseTask(expenseTasks[selectedIndex]);*/
                SnackBar snackBar = const SnackBar(
                  content: Text('Supprimé avec succès!'),
                  backgroundColor: Colors.green,
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                setState(() {
                  toShow = false;
                });
                // getExpenses(expenseList1.id);
              },
            ),
            IconButton(
              icon: const Icon(Icons.arrow_back_outlined),
              onPressed: () {
                setState(() {
                  toShow = false;
                });

                //getExpenses(expenseList1.id);
              },
            ),
          ],
        ),
      );
    } else {
      return Container(
        height: 1,
      );
    }
  }

  _foreG(bool toShow, int index, int selectedIndex) {
    if (toShow && selectedIndex == index) {
      return const BoxDecoration(
        color: Color.fromRGBO(0, 0, 0, 0.1),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      );
    }
    return const BoxDecoration(
      //color: Color.fromRGBO(0, 0, 0, 0.1),
      borderRadius: BorderRadius.all(Radius.circular(10.0)),
    );
  }
}

//
class DeleteExpenseListDialog extends StatelessWidget {
  final ExpenseList list;
  const DeleteExpenseListDialog({Key? key, required this.list})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer cette épargne?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            //Navigator.pop(context);
            list.deleted_at = DateTime.now().toString();
            await helper.updateExpenseList(list);
            SnackBar snackBar = const SnackBar(
              content: Text('Supprimée avec succès!'),
              backgroundColor: Colors.green,
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          },
        ),
      ],
    );
  }
}

class ResultObjet {
  List<EpargneProjet> epProList = [];
  double sum = 0;
  ResultObjet(this.epProList, this.sum);
}
