import 'package:finanxee/components/forms/project_form.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

import '../../components/circle_image.dart';
import '../../components/dialogs/projet_epargne_dialog.dart';
import '../../components/expense_list_card.dart';
import '../../components/forms/facture_form.dart';
import '../../models/epargne_projet.dart';
import 'liste_epargnes_projet.dart';

class DetailsProjetScreen extends StatefulWidget {
  final Project project;
  const DetailsProjetScreen(this.project);

  @override
  State<DetailsProjetScreen> createState() =>
      _DetailsProjetScreenState(project);
}

class _DetailsProjetScreenState extends State<DetailsProjetScreen> {
  var f = StaticDate();
  DbHelper helper = DbHelper();
  double total = 00;

  final Project _project = Project(0, "intitule", 0.0, 0.0, 0.0, "description",
      "delai", 0, "created_at", "", 0);
  EpargneProjet epargneProjet = EpargneProjet(0, 0, "", 0, "", "");
  _DetailsProjetScreenState(_project);

  late List<EpargneProjet> epargnes;
  late ProjetEpargneDialog
      dialog; /* ProjetEpargneDialog(
      EpargneProjet(0, 0, "", 0, "", ""),
      true,
      Project(0, "intitule", 0.0, 0.0, 0.0, "description", "delai", 0,
          "created_at", ""));*/

  Future<DataEpargne> getData() async {
    epargnes = await helper.getEpargneProjetFromProjet(widget.project.id);
    total = await getTotalEpargne(epargnes);
    DataEpargne result = DataEpargne([], 0);
    result.epargnes = epargnes;
    result.total = total;
    return result;
  }

  Future<double> getTotalEpargne(List<EpargneProjet> list) async {
    double solde = 0.0;
    for (int i = 0; i < list.length; i++) {
      solde += list[i].montant;
    }
    return solde;
  }

  @override
  void initState() {
    // TODO: implement initState
    dialog = ProjetEpargneDialog(epargneProjet, true, widget.project);

    //recuperer les epargnes du projet

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Projet'),
          backgroundColor: const Color(0xfff3f3f3),
          foregroundColor: Colors.black,
          actions: [
            Container(
              padding:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    child: const Icon(Icons.delete_outline),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) => DeleteProject(
                                project: widget.project,
                              )).then((value) =>
                          {if (value == 'x') Navigator.pop(context)});
                    },
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  GestureDetector(
                    child: const Icon(Icons.edit),
                    onTap: () {
                      SnackBar snackBar =
                          const SnackBar(content: Text("Editer"));
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      //lancer le dialog
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  ProjectForm(widget.project, false)));
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
        body: FutureBuilder(
            future: getData(), //helper.insertAnExpenseTask(), /,
            builder:
                (BuildContext context, AsyncSnapshot<DataEpargne> snapshot) {
              // TODO: Add nested ListViews
              if (snapshot.connectionState == ConnectionState.done) {
                var progressValue = 25;
                DataEpargne? dataF = snapshot!.data;
                return SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      children: [
                        Text(
                          widget.project.intitule,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Text(
                          f.monthYear(DateTime.parse(widget.project.delai)),
                          style: GoogleFonts.lato(fontSize: 20.0),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        SfRadialGauge(axes: <RadialAxis>[
                          RadialAxis(
                            annotations: <GaugeAnnotation>[
                              GaugeAnnotation(
                                  positionFactor: 0.1,
                                  angle: 90,
                                  widget:
                                      /* Text(
                                    (dataF!.total / widget.project.montant)
                                            .toStringAsFixed(0) +
                                        ' %',
                                    style: const TextStyle(fontSize: 55),
                                  )*/
                                      Text(
                                    (dataF!.total *
                                                100 /
                                                widget.project.montant)
                                            .toStringAsFixed(2) +
                                        ' %',
                                    style: const TextStyle(fontSize: 55),
                                  ))
                            ],
                            pointers: <GaugePointer>[
                              RangePointer(
                                color: Colors.greenAccent,
                                value:
                                    dataF!.total * 100 / widget.project.montant,
                                cornerStyle: CornerStyle.bothCurve,
                                width: 0.2,
                                sizeUnit: GaugeSizeUnit.factor,
                              )
                            ],
                            minimum: 0,
                            maximum: 100,
                            showLabels: false,
                            showTicks: false,
                            axisLineStyle: const AxisLineStyle(
                              thickness: 0.2,
                              cornerStyle: CornerStyle.bothCurve,
                              color: Color(0x21ca7f3d4),
                              thicknessUnit: GaugeSizeUnit.factor,
                            ),
                          )
                        ]),
                        Text(
                          '${f.numF.format(dataF?.total)} / ${f.numF.format(widget.project.montant)}',
                          style: GoogleFonts.lato(fontSize: 20.0),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 40.0,
                            vertical: 8.0,
                          ),
                          decoration: BoxDecoration(
                              color: const Color(0xFF21ca79),
                              borderRadius: BorderRadius.circular(20.0),
                              border: Border.all(
                                color: const Color(0xFF21ca79),
                                width: 1,
                              )),
                          child: GestureDetector(
                            onTap: () async {
                              showDialog(
                                      context: context,
                                      builder: (BuildContext context) => dialog)
                                  .then((value) => {setState(() {})});
                            },
                            child: Text(
                              "Ajouter",
                              style: GoogleFonts.lato(
                                  fontSize: 20.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 1.0, vertical: 0),
                          child: Divider(
                            color: Color(0xFFB3B3B3),
                          ),
                        ),
                        const SizedBox(
                          height: 15,
                        ),
                        const Text(
                          'Ajouter une épargne pour le compte de ce projet. Cliquez sur le bouton pour ajouter.',
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 50.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Total de vos épargnes : ',
                                  style: GoogleFonts.lato(fontSize: 15.0)),
                              Text(
                                f.numF.format(dataF?.total),
                                style: GoogleFonts.lato(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 35,
                        ),
                        Center(
                          child: Text(
                            'Vous avez prévu épargner ${f.numF.format(widget.project.montant_epargne_mensuel)}  par mois, pour atteindre votre objectif en '
                            '${f.monthYear(DateTime.parse(widget.project.delai))}.',
                            style: GoogleFonts.lato(
                                fontSize: 15.0, fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        const SizedBox(
                          height: 35,
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 50.0,
                            vertical: 8.0,
                          ),
                          decoration: BoxDecoration(
                              color: const Color(0xFF21ca79),
                              borderRadius: BorderRadius.circular(20.0),
                              border: Border.all(
                                color: const Color(0xFF21ca79),
                                width: 1,
                              )),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => ListeEpargnesProjet(
                                          widget.project))).then(
                                  (value) => {getData(), setState(() {})});
                              //ListeEpargnesProjet
                            },
                            child: Text(
                              "Historique des épargnes",
                              style: GoogleFonts.lato(
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 35,
                        ),
                        /*  Row(
                          children: [
                            IconButton(
                              icon: const Icon(Icons.sticky_note_2_outlined),
                              onPressed: () {},
                            ),
                            Expanded(
                              child: ListTile(
                                title: Text(
                                  widget.project.intitule,
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                subtitle: Text(
                                  'Intitulé',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const Padding(
                          padding: EdgeInsets.symmetric(horizontal: 5.0),
                          child: Divider(
                            color: Color(0xFFD4D4D4),
                          ),
                        ),
                        Row(
                          children: [
                            IconButton(
                              icon: const Icon(Icons.calendar_month),
                              onPressed: () {},
                            ),
                            Expanded(
                              child: ListTile(
                                title: Text(
                                  DateFormat('dd-MM-yyyy').format(
                                      DateTime.parse(
                                          widget.project.created_at)),
                                  style: Theme.of(context).textTheme.bodyText1,
                                ),
                                subtitle: Text(
                                  'Date de création',
                                  style: Theme.of(context).textTheme.bodyMedium,
                                ),
                              ),
                            ),
                          ],
                        ),*/
                      ],
                    ),
                  ),
                );
              } else {
                return const Center(
                  child: CircularProgressIndicator(
                    color: Color(0xFF21ca79),
                  ),
                );
              }
            }));
  }

  Widget infosProjet(Project project) {
    return Container(
      //height: 150.0,
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          boxShadow: [
            BoxShadow(
                color: Color(0xFFFAFAFA),
                spreadRadius: 2,
                blurRadius: 1,
                offset: Offset(0, 1))
          ]),
      padding: const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
      child: Row(
        children: [
          const SizedBox(
            width: 15.0,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.project.montant.toString(),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  "Montant estimé du projet",
                  style: Theme.of(context).textTheme.bodySmall,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                /* Text(
                    widget.expenseList.created_at,
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),*/
                Text(
                  '${DateTime.parse(widget.project.created_at).day}  '
                  '${fromNumToMonth(DateTime.parse(widget.project.created_at).month)} '
                  '${(DateTime.parse(widget.project.created_at).year)} ',
                  style: Theme.of(context).textTheme.bodyMedium,
                )
              ],
            ),
          ),
          /* Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '5 dépenses',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  '${DateTime.parse(widget.expenseList.created_at).day}  '
                  '${fromNumToMonth(DateTime.parse(widget.expenseList.created_at).month)}',
                  style: Theme.of(context).textTheme.bodyMedium,
                ),
              ],
            )*/
        ],
      ),
    );
  }
}

class DeleteProject extends StatelessWidget {
  const DeleteProject({Key? key, required this.project}) : super(key: key);
  final Project project;
  @override
  Widget build(BuildContext context) {
    DbHelper helper = DbHelper();

    return AlertDialog(
      title: const Text("Supprimer"),
      content: const Text("Voulez-vous vraiment supprimer ce projet?"),
      actions: [
        TextButton(
          child: const Text("Annuler"),
          onPressed: () {
            Navigator.pop(context, 'n');
          },
        ),
        TextButton(
          child: const Text("Continuer"),
          onPressed: () async {
            Navigator.pop(context, 'x');
            //Navigator.pop(context);
            await helper.deleteProjet(project.id);
          },
        ),
      ],
    );
  }
}

class DataEpargne {
  List<EpargneProjet> epargnes = [];
  double total = 0.0;
  DataEpargne(this.epargnes, this.total);
}
