import 'package:finanxee/models/models.dart';
import 'package:finanxee/screens/avoirs/add_actif_form.dart';
import 'package:finanxee/utils/static_data.dart';
import 'package:flutter/material.dart';

import '../../components/forms/project_form.dart';

class ProjectActionsScreen extends StatefulWidget {
  const ProjectActionsScreen({Key? key}) : super(key: key);

  @override
  State<ProjectActionsScreen> createState() => _ProjectActionsScreenState();
}

class _ProjectActionsScreenState extends State<ProjectActionsScreen> {
  TypeActif _typeActif = StaticDate.typesActifs[0];
  Actif actif = Actif(0, 'intitule', 1, 120000, "description",
      DateTime.now().toString(), DateTime.now().toString());
  Project project = Project(
      0,
      "intitule",
      0,
      0,
      0,
      "description",
      DateTime.now().toString(),
      0,
      DateTime.now().toString(),
      DateTime.now().toString(),
      0);

  List<TypeActif> typesActifs = StaticDate.typesActifs;
  List<String> typesProjet = StaticDate().typesProjet;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Ajout de projet"),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 15.0),
        decoration: const BoxDecoration(
          shape: BoxShape.rectangle,
          color: Color(0xffffffff),
          border: Border(
              top: BorderSide(
            color: Colors.black,
            width: 1.0,
          )),
          //borderRadius: new BorderRadius.all(new Radius.circular(3.0)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: ListView.separated(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    itemBuilder: (BuildContext context, index) {
                      String act = typesProjet[index];
                      return GestureDetector(
                        onTap: () {
                          //catData.updateSousCategoriesDepenseSelected(sCat);
                          project.intitule = act;
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ProjectForm(project, true)));
                        },
                        child: Container(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: ListTile(
                            title: Text(act),
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, index) {
                      return const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Divider(
                          color: Color(0xFFD4D4D4),
                        ),
                      );
                    },
                    itemCount: typesProjet.length),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
