import 'package:finanxee/components/components.dart';
import 'package:finanxee/components/forms/project_form.dart';
import 'package:finanxee/models/models.dart';
import 'package:finanxee/screens/planifier/project_list_archivees_screen.dart';
import 'package:finanxee/utils/utils.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

import '../screens.dart';
import 'details_projet_screen.dart';

class ProjectListScreen extends StatefulWidget {
  const ProjectListScreen({Key? key}) : super(key: key);
  @override
  State<ProjectListScreen> createState() => _ProjectListScreenState();
}

class _ProjectListScreenState extends State<ProjectListScreen> {
  Project project = Project(0, "intitule", 0.0, 0.0, 0.0, "description",
      "delai", 0, DateTime.now().toString(), DateTime.now().toString(), 0);
  DbHelper helper = DbHelper();
  List<Project> projectList = [];
  List<Project> projectListAll = [];
  int projectMonth = 0;
  int archived = 0;
  // c'est ici que nous faisons l'injection
  Future getProjects() async {
    List<Project> list1 = await helper.getProjets();
    List<Project> list2 = await helper.getProjetsArchivees();
    List<Project> list3 = await helper.getProjetsAll();
    List<Project> list4 = await helper.getProjetsOfMonth();
    archived = list2.length;
    projectMonth = list4.length;
    setState(() {
      projectList = list1;
      projectListAll = list3;
      archived = archived;
      projectMonth = projectMonth;
    }); //get the shopping list from the helper
  }

  @override
  void initState() {
    // TODO: implement initState
    getProjects();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Listes des projets'),
        backgroundColor: const Color(0xFF21ca79),
        actions: [
          Container(
            padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 15.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                // Icon(Icons.add),
                //Icon(Icons.account_circle_outlined),
              ],
            ),
          ),
        ],
      ),
      body: FutureBuilder(
          future: helper.getExpenseLists(),
          builder: (BuildContext context, snapshot) {
            // TODO: Add nested ListViews
            if (snapshot.connectionState == ConnectionState.done) {
              if (projectListAll.isNotEmpty) {
                return Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        topCard(projectListAll.length),
                        const SizedBox(
                          height: 25,
                        ),
                        ListView.separated(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: projectList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailsProjetScreen(
                                                      projectList[index])))
                                      .then((value) => getProjects());
                                },
                                child: Dismissible(
                                    key: Key('item ${projectList[index]}'),
                                    child: ProjectCard(projectList[index]),
                                    background: const ColoredBox(
                                      color: Color.fromRGBO(43, 80, 108, 1),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Padding(
                                          padding: EdgeInsets.all(16.0),
                                          child: Icon(Icons.archive_rounded,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    secondaryBackground: const ColoredBox(
                                      color: Color.fromRGBO(43, 80, 108, 1),
                                      child: Align(
                                        alignment: Alignment.centerRight,
                                        child: Padding(
                                          padding: EdgeInsets.all(16.0),
                                          child: Icon(Icons.archive_rounded,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    onDismissed:
                                        (DismissDirection direction) async {
                                      Project task = projectList[index];
                                      task.archived = 1;
                                      await helper.updateProjet(task);
                                      getProjects();
                                      setState(() {});
                                      SnackBar snackBar = SnackBar(
                                        content: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            const Text(" 1 projet archivé."),
                                            GestureDetector(
                                              onTap: () async {
                                                task.archived = 0;
                                                await helper.updateProjet(task);
                                                getProjects();
                                              },
                                              child: const Text(
                                                "Annuler.",
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            ),
                                          ],
                                        ),
                                        backgroundColor: const Color.fromRGBO(
                                            43, 80, 108, 1),
                                      );
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    }));
                          },
                          separatorBuilder: (BuildContext context, index) {
                            return const Padding(
                              padding: EdgeInsets.symmetric(horizontal: 2.0),
                              child: Divider(
                                color: Color(0xFFFFFFFF),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                );
              } else {
                return Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: const [
                      SizedBox(height: 8.0),
                      Text(
                        'Aucun projet ',
                        style: TextStyle(fontSize: 21.0),
                      ),
                      SizedBox(height: 16.0),
                      Text(
                        'Vous n\'avez ajouté aucun projet pour l\'instant.'
                        'Appuyez sur le bouton + en bas de l\'écran pour ajouter!',
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                );
              }

              /*if (snapshot.hasData) {
              final recipes = snapshot.data;
              //TODO : replace this with today recipe list view
              return TodayRecipeListView(recipes: recipes!.todayRecipes);
            } else if (snapshot.hasError) {
              return Center(
                child: Text('${snapshot.error} is the error'),
              );
            } else
              return const Center(
                child: CircularProgressIndicator(
                  color: Colors.green,
                ),
              );*/
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
          onPressed: () async {
            Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProjectActionsScreen()))
                .then((value) => getProjects());
          },
          child: const Icon(Icons.add),
          backgroundColor: const Color(0xFF21ca79)),
    );
  }

  Widget topCard(int t) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: const BoxDecoration(
            color: Colors.white, //Color(0xFF00BB),
            borderRadius: BorderRadius.all(Radius.circular(10)),
            boxShadow: [
              BoxShadow(
                  color: Color(0xFFECECEC),
                  offset: Offset(1, 1),
                  blurRadius: 5,
                  spreadRadius: 5)
            ]),
        padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Projets',
              style: Theme.of(context).textTheme.bodyLarge,
            ),
            const SizedBox(
              height: 18.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  t.toString(),
                  //style: Theme.of(context).textTheme.headline2,
                  style: GoogleFonts.lato(
                      fontSize: 60.0, fontWeight: FontWeight.w900),
                ),
              ],
            ),
            const SizedBox(
              height: 7.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 16.0,
                    vertical: 8.0,
                  ),
                  decoration: BoxDecoration(
                    color: const Color(0xFF21ca79),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Center(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        const ProjectListArchiveesScreen()))
                            .then((value) => getProjects());
                      },
                      child: Text(
                        "Archivés (${archived})",
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () {
                    SnackBar snackBar = SnackBar(
                      content:
                          Text('${projectMonth} projet(s) ajouté(s) ce mois.'),
                      backgroundColor: Colors.green,
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  },
                  child: Row(
                    children: [
                      Text(
                        '+${projectMonth}',
                        style: const TextStyle(
                            color: Color(0xFF21ca79),
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold),
                      ),
                      const Icon(
                        Icons.arrow_right_sharp,
                        //FlutterIcons.caret_up_faw,
                        color: Color(0xFF21ca79),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
