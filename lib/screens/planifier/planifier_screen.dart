import 'package:finanxee/screens/planifier/last_projects.dart';
import 'package:finanxee/screens/planifier/project_list_screen.dart';
import 'package:flutter/material.dart';

class PlanifierScreen extends StatefulWidget {
  const PlanifierScreen({Key? key}) : super(key: key);

  @override
  State<PlanifierScreen> createState() => _PlanifierScreenState();
}

class _PlanifierScreenState extends State<PlanifierScreen>
    with RestorationMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Planifier'),
        backgroundColor: const Color(0xFF21ca79),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5.0, vertical: 5),
        //color: const Color(0xFFE3FAF0), // Colors.green[100],
        child: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(
                height: 5.0,
              ),
              projetsCard(context),
//              LastProjects(),
              const SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  // TODO: implement restorationId
  String? get restorationId => 'planifier_screen';

  @override
  void restoreState(RestorationBucket? oldBucket, bool initialRestore) {
    // TODO: implement restoreState
  }
}

Widget projetsCard(BuildContext context) {
  return Container(
    decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
              color: Color(0xFFD8F1E7),
              offset: Offset(1, 1),
              blurRadius: 1,
              spreadRadius: 1)
        ]),
    child: Padding(
      padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 16.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Projets',
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
              ],
            ),
            const SizedBox(
              height: 18.0,
            ),
            AspectRatio(
                aspectRatio: 1 / 1,
                child: Image.asset('assets/core/projet-3.png')),
            const SizedBox(height: 8.0),
            const SizedBox(height: 16.0),
            const Text(
              "Planifez vos projets financiers puis définissez une stratégie d'épargne pour les réaliser.\n ",
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 16.0),
            GestureDetector(
              onTap: () {
                Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProjectListScreen()))
                    .then((value) => (value));
                SnackBar snackBar = const SnackBar(content: Text(' '));
                // ScaffoldMessenger.of(context).showSnackBar(snackBar);
              },
              child: Wrap(children: [
                Container(
                  padding: const EdgeInsets.symmetric(
                    //horizontal: 16.0,
                    vertical: 8.0,
                  ),
                  decoration: BoxDecoration(
                      //color: Color(0xFF21ca79),
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                        color: const Color(0xFF21ca79),
                        width: 1,
                      )),
                  child: Center(
                    child: Text(
                      "Gérer",
                      style: Theme.of(context).textTheme.bodyText1,
                    ),
                  ),
                ),
              ]),
            )
          ],
        ),
      ),
    ),
  );
}
